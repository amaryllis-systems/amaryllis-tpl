<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>


<section itemscope="" itemtype="http://schema.org/WebPage">

    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container fixed-1024 fixed-1200">
            <div class ="row full xxsmall-push-updown-30">
                <!-- general admin page title -->
                <{if $acmsContents->get('acmsTitle')}>
                    <div class="column xxsmall-12">
                        <div class="page-title">
                            <h1><{$acmsContents->get('acmsTitle')}></h1>
                            <p><{translate id="Hier können Sie neue Inhalte einreichen." textdomain="Core"}></p>
                        </div>
                    </div>
                <{/if}>
            </div>
        </div>
    </section>
    <section>
        <div class="container fixed-1024 fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <!-- Admin Form -->
                    <{if $acmsContents->get('acms_form')}>
                        <{include file=$acmsContents['acms_form']['template'] form=$acmsContents['acms_form']}>
                    <{elseif $acmsContents->get('acmsForm')}>
                        <{include file=$acmsContents['acmsForm']['template'] form=$acmsContents['acmsForm']}>
                    <{/if}>
                </div>
            </div>
        </div>
    </section>
</section>
