<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="amaryllis-sitemap" itemscope="" itemtype="http://schema.org/WebPage" >
    <{* Breadcrumb *}>
    <{if $acmsContents->get('acmsBreadCrumb')}>
        <div class="container full">
            <div class="row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <{* Header *}>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container full">
            <div class="row">
                <div class="large center aligned icon page-title v7">
                    <i aria-hidden="true" class="circular acms-icon acms-icon-sitemap text-muted"></i>
                    <div class="content">
                        <h1 class="title white-text">
                            <span itemprop="name">Sitemap</span>
                        </h1>
                        <div class="sub-title white-text">Unsere Webseite</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <{* Sitemap Content *}>
    <section role="main" class="sitemap fixed-1600 container" data-cssfile="media/css/view/sitemap">
        <div class="full row xxsmall-push-updown-25" itemprop="mainContentOfPage">
            <div class="column xxsmall-12 medium-6">
                <{if $acmsData->acmsIsUser}>
                    <a class="default button" href="<{$acmsData->user['itemUrl']}>" title="Login">
                        <div class="small midle aligned inline-avatar avatar">
                            <img src="<{$acmsData->user['avatar']}>" alt="<{$acmsData->user['username']}>" title="<{$acmsData->user['username']}>" />
                        </div>
                        <span class="middle aligned content push-leftright-5"><{$acmsData->user['username']}></span>
                    </a>
                <{else}>
                    <a class="default button" href="<{generateUrl name="core_login"}>" title="Login">
                        <div class="small midle aligned inline-avatar avatar">
                            <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
                        </div>
                        <span class="middle aligned content push-leftright-5">Login</span>
                    </a>
                <{/if}>
            </div>
            <div class="column xxsmall-12 medium-6">

                <div class="text-small clearfix">
                    <div class="xxsmall right floated horizontal divided list v1">
                        <{foreach $acmsContents->policies as $policie}>
                            <a class="item" href="<{$policie['url']}>" title="<{$policie['title']}>"><{$policie['title']}></a>
                        <{/foreach}>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="sitemap-section content-blocks v5">
                    <{function name=sitemapSubItems}>
                        <{foreach $items as $item}>
                        <li class="list-group-item item">
                            <a href="<{$item['url']}>" class="text-theme" itemprop="url" title="Erfahren Sie mehr: <{$item['title']}>">
                                <span itemprop="name">
                                    <{$item['title']}>
                                </span>
                            </a>
                            <{if isset($item['subs']) && $item['subs']}>
                                <ul class="list list-unstyled">
                                    <{call name=sitemapSubItems items=$item['subs']}>
                                </ul>
                            <{/if}>
                        </li>
                        <{/foreach}>
                    <{/function}>
                    <{foreach $acmsContents->module_sitemap as $module => $config}>
                        <{if $config['hasCustomTemplate']}>
                            <{include file=$config['customTemplate'] config=$config}>
                        <{else}>
                            <section class="sitemap-section content-block v5">
                                <div class="page-title v7">
                                    <a class="title" href="<{$config['modulelink']}>" title="<{$config['modulename']}>">
                                        <{$config['modulename']}>
                                    </a>
                                </div>
                                <{if $config['items']}>
                                    <div class="sitemap-list clearfix push-updown-30" role="menu">
                                        <{assign value=$config['items'] var="items"}>
                                        
                                        <{call name=sitemapSubItems items=$config['items']}>
                                    </div>
                                <{/if}>
                            </section>
                        <{/if}>
                    <{/foreach}>
                </div>
            </div>
        </div>
    </section>
</section>
