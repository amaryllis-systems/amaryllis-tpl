<!DOCTYPE html>
<html class="no-js" lang="<{$acmsData->acmsLangcode|lower}>">
    <head>
        <!-- Theme by QM-B (Steffen Flohrer) | www.amaryllis-systems.eu -->
        <!-- meta descriptions -->
        <meta charset="utf-8" />
        <base href="<{$acmsData->acmsUrl}>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <!-- site title and slogan -->
        <title><{if isset($acms_pagetitle) && $acms_pagetitle !== ''}><{$acms_pagetitle}> : <{/if}><{$acms_sitename}></title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="<{$acmsData->acmsTheme['url']}>media/icons/favicon.ico" />
        <link rel="icon" type="image/png" href="<{$acmsData->acmsTheme['url']}>media/icons/icon.png" />
        <link rel="apple-touch-icon" href="<{$acmsData->acmsTheme['url']}>media/icons/icon.png" />
        <!-- Ubuntu font -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin" />
        <!-- RMV: added module header and scripts -->
        <{$header}>
    </head>
    <body class="site-error" role="document" itemscope="" itemtype="http://schema.org/WebPage">
        <div class="container" role="main">
            <div class="row">
                <div class="column xxsmall-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3">
                    <h1><span itemprop="name"><{translate id="Themen-Renderung fehlgeschlagen"}></span></h1>
                </div>
            </div>
            <div class="row">
                <div class="column xxsmall-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3">
                    <{if $acms_system_notifications}>
                        <{foreach $acms_system_notifications as $type => $notifications}>
                            <div class="alert v1 <{if $type == 2}>danger<{elseif $type == 1}>warning<{else}>info<{/if}>" data-module="ui/components/alert">
                                <div class="alert-title">
                                    <h3>Fehler!</h3>
                                </div>
                                <div class="alert-content">
                                    <ul class="list list-striped">
                                        <{foreach $notifications as $notification}>
                                            <li><{$notification}></li>
                                            <{/foreach}>
                                    </ul>
                                </div>
                            </div>
                        <{/foreach}>
                    <{else}>
                        <div class="alert v1 danger" data-module="ui/components/alert">
                            <div class="alert-title">
                                <h3>Fehler</h3>
                            </div>
                            <div class="alert-content">
                                <p>
                                    Es kam leider zu schwerwiedgenden Fehlern beim Rendern der Seite!
                                </p>
                            </div>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
        <{$footer}>
    </body>
</html>
