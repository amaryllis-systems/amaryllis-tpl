<!DOCTYPE html>
<html class="no-js" lang="<{$acmsData->acmsLangcode|lower}>">
<head>
    <!-- Theme by QM-B (Steffen Flohrer) | www.amaryllis-systems.eu -->
<!-- meta descriptions -->
    <meta charset="utf-8" />
    <base href="<{$acmsData->acmsUrl}>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<!-- site title and slogan -->
    <title><{$acmsContents->acmsTitle}> @ <{$acmsData->acmsSitename}></title>
<!-- Favicon -->
    <link rel="shortcut icon" href="<{$acmsData->acmsUrl}>/media/icons/favicon.ico" />
    <link rel="icon" type="image/png" href="<{$acmsData->acmsUrl}>/media/icons/icon.png" />
    <link rel="apple-touch-icon" href="<{$acmsData->acmsUrl}>/media/icons/icon.png" />
<!-- RMV: added module header and scripts -->
    <{$header}>
</head>
<body class="siteRestricted has-sticky-footer" role="document" itemscope="" itemtype="http://schema.org/WebPage">
    <div class="page">
        <div class="inner pull-updown-30">
            <header id="core-header" class="header">
                <nav class="application-launcher dark" data-module="ui/navs/application-launcher">
                    <div class="left-bar">
                        <div class="branding" role="region" aria-label="<{translate id="Webseite von %s" args=$acmsData->acmsSitename}>" itemprop="publisher" itemscope="" itemtype="http://schema.org/Organization">
                            <div class="logo">
                                <img itemprop="logo" src="<{$acmsData->acmsUrl}>/media/icons/64x64.png" alt="Logo" title="<{translate id="Logo von %s" textdomain="Core" args=$acmsData->acmsSitename}>" />
                            </div>
                            <a itemprop="url" class="sitename" href="<{$acmsData->acmsUrl}>">
                                <span itemprop="name"><{$acmsData->acmsSitename}></span>
                            </a>
                            <div class="slogan" itemprop="">
                                <{$acmsData->acmsSlogan}>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
            <main class="container pull-updown-30" role="main">
                <div class="row xxsmall-push-updown-30">
                    <div class="column xxsmall-12">
                        <div class="center aligned xlarge icon page-title v7">
                            <i aria-hidden="true" class="circular acms-icon acms-icon-lock"></i>
                            <div class="content">
                                <h1 class="text-primary">
                                    <span itemprop="name"><{$acmsContents->acmsTitle}> <small class="sub-title"><{$acmsContents->acmsSitename}></small></span>
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row xxsmall-push-updown-30">
                    <div class="column xxsmall-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3 pull-updown-30">
                        <div class="alert v3 danger">
                            <h3 class="alert-title">
                                <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i> 
                                Info
                            </h3>
                            <p class="alert-content">
                                <{if $acmsContents->restrictedMessage}>
                                    <{$acmsContents->restrictedMessage}>
                                <{else}>
                                    Diese Seite ist nur für Mitglieder geöffnet. Bitte melden Sie sich an oder, wenn Sie eine Anmeldung bekommen haben, folgen Sie der Anleitung in der E-Mail.
                                <{/if}>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="column xxsmall-12 large-6 large-offset-3 xlarge-4 xlarge-offset-4">
                        <{loadBlock name="core_login"}>
                    </div>
                </div>
            </main>
            <footer id="acms-footer" class="main-footer page-footer" role="contentinfo" aria-label="Fußbereich - Ergänzende Informationen" itemscope="" itemtype="https://schema.org/WPFooter">
                <div class="container full">
                    <div class="row">
                        <div class="column xxsmall-12">
                            <{include file="acmsfile:Theme|CopyrightFooter.tpl" nocache}>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <{$footer}>
</body>
</html>
