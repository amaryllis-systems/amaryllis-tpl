<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div itemprop="breadcrumb" 
     itemscope =""
     itemtype="http://schema.org/BreadcrumbList"
     class="acms-breadcrumb"
     aria-label="<{translate id="Brotkrumennavigation" textdomain="Core"}>"
     role="navigation" aria-labelledby="bc-title">
    <ol class="breadcrumb v1">
        <li class="bcllabel<{if !$acmsContents['acmsBreadCrumb']['config']['show_here']}> hidden<{/if}>">
            <span id="bc-title" class="breadcrumb-here">
                <{translate id="Sie befinden sich hier" textdomain="Core"}>:
            </span>
        </li>
        <{foreach $acmsContents['acmsBreadCrumb']['items'] as $breadcrumbItem}>
            <li itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem"
                class="breadcrumb-item <{if $breadcrumbItem->current}>active<{/if}>">
                <span itemprop="item" itemscope="" itemtype="http://schema.org/CreativeWork">
                    <{if $breadcrumbItem->description}>
                        <meta itemprop="description" content="<{$breadcrumbItem->description}>" />
                    <{/if}>
                    <meta itemprop="name" content="<{$breadcrumbItem->title}>" />
                    <{if $breadcrumbItem->link}>
                        <a itemprop="url"
                           title="<{if $breadcrumbItem->description}><{$breadcrumbItem->description}><{else}><{translate id="Gehe zu: %s" textdomain="Core" args=$breadcrumbItem->title}><{/if}>"
                           href="<{$breadcrumbItem->link}>"
                           aria-labelledby="bc-level-<{$breadcrumbItem->position}>"
                        >
                            <{if $breadcrumbItem->class}>
                                <span class="<{$breadcrumbItem->class}>"></span>
                            <{/if}>
                            <span id="bc-level-<{$breadcrumbItem->position}>">
                                <{$breadcrumbItem->title}>
                            </span>
                        </a>
                    <{else}>
                        <{if $breadcrumbItem->class}>
                            <span class="<{$breadcrumbItem->class}>"></span>
                        <{/if}>
                        <span aria-disabled="true">
                            <{$breadcrumbItem->title}>
                        </span>
                    <{/if}>
                </span>
                <meta itemprop="position" content="<{$breadcrumbItem->position}>" />
            </li>
        <{/foreach}>
    </ol>
</div>
