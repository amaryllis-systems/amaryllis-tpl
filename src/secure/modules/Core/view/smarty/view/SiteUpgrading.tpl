<!DOCTYPE html>
<html class="no-js" lang="<{$acmsData->acmsLangcode|lower}>">
<head>
    <!-- Theme by QM-B (Steffen Flohrer) | www.amaryllis-systems.eu -->
<!-- meta descriptions -->
    <meta charset="utf-8" />
    <base href="<{$acmsData->acmsUrl}>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<!-- site title and slogan -->
    <title>
        <{if $acmsContents->get('acmsTitle') !== ''}>
            <{$acmsContents->acmsTitle}> : 
        <{/if}>
        <{$acmsData->acmsSitename}>
    </title>
<!-- Favicon -->
    <link rel="shortcut icon" href="<{$acmsData->acmsTheme['url']}>media/icons/favicon.ico" />
    <link rel="icon" type="image/png" href="<{$acmsData->acmsTheme['url']}>media/icons/icon.png" />
    <link rel="apple-touch-icon" href="<{$acmsData->acmsTheme['url']}>media/icons/icon.png" />
<!-- RMV: added module header and scripts -->
    <{$header}>
</head>
<body class="siteUpgrade" role="document" itemscope="" itemtype="http://schema.org/WebPage">
    <div class="container" role="main">
        <div class="row">
            <div class="column xxsmall-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3">
                <h1>
                    <span itemprop="name"><{translate id="Amaryllis Systems wird aktualisiert"}>&hellip;</span></h1>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12">
                <div class="alert v3 danger">
                    <i aria-hidden="true" class="acms-icon acms-icon-3x acms-icon-exclamation-triangle"></i>
                    <div class="alert-title">
                        <h3>Upgrade in Arbeit</h3>
                    </div>
                    <p class="alert-content">
                        <{if $acmsContents->restrictedMessage}>
                            <{$acmsContents->restrictedMessage}>
                        <{else}>
                            Bitte haben Sie einen Moment Geduld. Amaryllis CMS 
                            wird gerade auf eine neue Version aktualisiert.
                        <{/if}>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3">
                <{loadBlock name="core_login"}>
            </div>
        </div>
    </div>
    <footer id="acms-footer" class="main-footer page-footer" role="contentinfo" aria-label="Fußbereich - Ergänzende Informationen" itemscope="" itemtype="https://schema.org/WPFooter">
        <div class="container full">
            <div class="row">
                <div class="column xxsmall-12">
                    <{include file="acmsfile:Theme|CopyrightFooter.tpl" nocache}>
                </div>
            </div>
        </div>
    </footer>
    <{$footer}>
</body>
</html>
