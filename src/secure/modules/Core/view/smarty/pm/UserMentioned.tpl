<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="page-title v7 pull-down-35 push-down-35">
    <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
    <div class="content">
        <h2 class="title"><{translate id="Hallo, %s" textdomain="Core" args=$receiver['username']}></h2>
        <div class="sub-title">Sie wurden in einem Beitrag erwähnt</div>
    </div>
</div>
<hr><br>
<div class="text-center compact button-toolbar">
    <a class="primary button" href="<{$submission['itemUrl']}>" title="<{translate id='Mehr erfahren'}>">
        <span>jetzt lesen!</span>
    </a>
</div>
<br/><br/>
<div>
    <div class="inline-avatar avatar">
        <a class="inline-avatar avatar" href="<{$from['itemUrl']}>" title="Profil besuchen">
            <img src="<{$from['avatar']}>" alt="" title="<{$from['username']}>" />
        </a>
    </div>
    <a href="<{$from['itemUrl']}>" class="text-muted" title="">
        <{$from['username']}>
    </a><br/>
</div>
