<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper">
    <form role="form" <{$form['attributes']}>>
        <{if $form.title}>
            <legend><{$form['title']}></legend>
        <{/if}>
        <{foreach $form['fields'] as $field}>
            <{include file=$field['template'] field=$field nocache}>
        <{/foreach}>
        <{if $field['buttons']['template']}>
            <{include file=$field['buttons']['template'] field=$field['buttons'] nocache}>
        <{/if}>
    </form>
</div>
