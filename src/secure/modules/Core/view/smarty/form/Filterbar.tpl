<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<button type="button" 
        class="dark icon button"
        data-module="apps/ui/helpers/collapse"
        data-target="#filter-bar"
        aria-expanded="true"
        aria-controls="filter-bar"
        data-duration="0.5"
        data-duration-hide="0.5"
        aria-label="Filter-Bar umschalten">
    <i aria-hidden="true" class="acms-icon acms-icon-filter"></i>
</button>

<div class="collapse filter-bar" aria-expanded="false" id="filter-bar">
    <div class="filter-wrapper content-block v5">
        <form role="form" <{$form.attributes}>>
            <{if $form.title}>
                <legend><{$form.title}></legend>
            <{/if}>
            <div class="fields fields-<{if $form.fields|count <= 10}><{$form.fields|count}><{else}>10<{/if}>">
                <{foreach $form.fields as $field}>
                    <{include file=$field['template'] field=$field nocache}>
                <{/foreach}>
            </div>
            <{if $form['withButton'] && $form['buttons']['template']}>
                <div class="form-group">
                    <{include file=$form['buttons']['template'] field=$form['buttons']}>
                </div>
            <{/if}>
        </form>
    </div>
</div>
