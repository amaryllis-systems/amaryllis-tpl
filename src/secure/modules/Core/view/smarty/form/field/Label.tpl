<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<label <{$field.attributes}> >
    <{$field.caption}>
    <{if $field.description}>
        <a class="acms-helptip" title="<{$field.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
            <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
        </a>
    <{/if}>
</label>
