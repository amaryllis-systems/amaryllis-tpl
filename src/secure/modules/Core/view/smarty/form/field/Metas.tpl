<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $field['forceHiddens']}>
    <input type="hidden" name="meta_description" value="<{$field['descriptionValue']}>" />
    <input type="hidden" name="meta_keywords" value="<{$field['keywordsValue']}>" />
    <input type="hidden" name="meta_eyperts" value="<{$field['expertsValue']}>" />
<{else}>
    <div class="row full" data-name="<{$field->name}>">
        <{foreach $field['metas'] as $meta => $element}>
            <div class="column xxsmall-12">
                <{if $meta == "meta_experts"}>
                    <div class="text-right">
                        <a href="#meta-experts-dropdown"
                           data-module="apps/ui/helpers/collapse" 
                           data-target="#meta-experts-dropdown"
                           class="small">
                            <{translate id="Experten-Einstellungen" 
                                        textdomain="Core"}>&nbsp;
                            <i aria-hidden="true" class="acms-icon acms-icon-arrow-alt-circle-down regular"></i>
                        </a>
                    </div>
                    <div id="meta-experts-dropdown" class="collapse">
                <{/if}>
                <{if $element['template']}>
                    <{include file=$element['template'] field=$element nocache}>
                <{else}>
                    Element <{$name}> ohne Template!
                <{/if}>
                <{if $meta == "meta_experts"}>
                    </div>
                <{/if}>
            </div>
        <{/foreach}>
    </div>
<{/if}>
