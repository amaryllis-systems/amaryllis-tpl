<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
    <{if $field['with_label']}>
        <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}>
        <{if $field.required}>
            <span class="text-danger required">*</span>
         <{/if}>
         <{if $field.label.description && $field.show_tooltip}>
             <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                 <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
             </a>
         <{/if}>
        </label>
    <{/if}>
    <div class="button-group" data-toggle="buttons">
        <{foreach $field.options as $value => $name}>
            <{if $field.delimeter == 'linebreak'}>
                <div class="checkbox">
            <{/if}>
                   <label class="acms-option-label <{if $field.delimeter == 'inline'}>checkbox-inline<{/if}>">
                       <input type="checkbox" <{$field.data}> name="<{$field.name}>" <{if $value|in_array:$field.values || isset($field['values'][$value])}>checked="checked"<{/if}> value="<{$value}>" />
                       <{$name}>
                   </label>
            <{if $field.delimeter == 'linebreak'}>
                </div>
            <{/if}>
        <{/foreach}>
        <{if $field['totalOptions'] > 1}>
            <button class="acms-toggle-all button info tiny"><i aria-hidden="true" class="acms-icon acms-icon-check-square regular"></i></button>
        <{/if}>
    </div>
</div>
