<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
<div class="full container">
    <{if $field.with_label}>
        <div class="row">
            <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
                <span class="text-danger required">*</span><{/if}>
                <{if $field.label.description && $field.show_tooltip}>
                    <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="right" data-html="true">
                        <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
                    </a>
                <{/if}>
            </label>
        </div>
    <{/if}>
    <div class="full row" data-module="ui/components/duallist">
        <div class="dual-list v1 list-left column xxsmall-12 medium-5 large-5 xlarge-5 xxlarge-5">
            <div class="wide content-block v6">
                <{if $field.leftTitle}>
                    <div class="full row">
                        <div class="center aligned page-title v7">
                            <h3 id="<{$field.baseName}>leftTitle" class="title">
                                <span itemprop="name"><{$field.leftTitle}></span>
                            </h3>
                        </div>
                    </div>
                <{/if}>
                <div class="center-xxsmall full row">
                    <div class="column xxsmall-9">
                        <div class="field-group">
                            <label for="search-duallist-left" class="src-only" aria-label="Suchen Sie in der Liste">Suchen Sie in der Liste</label>
                            <input type="search"
                                   role="search"
                                   aria-controls="<{$field.baseName}>-list-left"
                                   data-role="duallist-search-left"
                                   name="search-duallist-left"
                                   class="field"
                                   placeholder="<{translate id='Suchen' textdomain='Core'}>" />
                            <span class="field-icon cursor-pointer"><i aria-hidden="true" class="acms-icon acms-icon-search"></i></span>
                        </div>
                    </div>
                    <div class="column xxsmall-2">
                        <button type="button"
                                role="button"
                                class="large info icon button selector"
                                data-role="check-em-all"
                                data-title="<{translate id='Wähle alle' textdomain='Core'}>"
                                data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                                data-placement="left"
                                data-list="left"
                        >
                            <i aria-hidden="true" class="acms-icon acms-icon-square regular"></i>
                        </button>
                    </div>
                </div>
                <ul id="<{$field.baseName}>-list-left"
                    class="collection v1 duallist-left duallist-list"
                    role="combobox"
                    aria-labelledby="<{$field.baseName}>leftTitle" data-role="duallist-left">
                    <{foreach $field.options as $value => $name}>
                        <{if $value|in_array:$field.values}>
                            <{continue}>
                        <{/if}>
                        <li class="collection-item duallist-item" 
                            role="option" 
                            tabindex="0" 
                            aria-selected="false"
                            data-role="duallist-item" data-value="<{$name}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-square regular left"></i>
                            <span data-role="name"><{$name}></span>
                            <input type="checkbox"
                                   name="<{$field.name}>"
                                   class="hidden"
                                   />
                        </li>
                    <{/foreach}>
                </ul>
            </div>
        </div>

        <div class="list-arrows column xxsmall-12 medium-1 large-1 xlarge-1 xxlarge-1 text-center" data-role="move-buttons">
            <button role="button" type="button" class="button default tiny move-left">
                <span class="acms-icon acms-icon-chevron-left"></span>
            </button>

            <button role="button" type="button" class="button default tiny move-right">
                <span class="acms-icon acms-icon-chevron-right"></span>
            </button>
        </div>

        <div class="dual-list v1 list-left column xxsmall-12 medium-5 large-5 xlarge-5 xxlarge-5">
            <div class="content-block v6 wide">
                <{if $field.rightTitle}>
                    <div class="row text-center">
                        <div class="center aligned page-title v7">
                            <h3 id="<{$field.baseName}>rightTitle" class="title">
                                <span itemprop="name"><{$field.rightTitle}></span>
                            </h3>
                        </div>
                    </div>
                <{/if}>
                <div class="row">
                    <div class="column xxsmall-10">
                        <div class="field-group">
                            <input type="search"
                                   role="search"
                                   data-role="duallist-search-right"
                                   name="search-duallist-rignt"
                                   class="field"
                                   placeholder="<{translate id='Suchen' textdomain='Core'}>" />
                            <span class="field-icon"><i aria-hidden="true" class="acms-icon acms-icon-search"></i></span>
                        </div>
                    </div>
                    <div class="column xxsmall-2">
                            <button type="button"
                                    role="button"
                                    class="large info icon button selector"
                                    data-role="check-em-all"
                                    data-title="<{translate id='Wähle alle' textdomain='Core'}>"
                                    data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                                    data-placement="left"
                                    data-list="right"
                            >
                                <i aria-hidden="true" class="acms-icon acms-icon-square regular"></i>
                            </button>
                    </div>
                </div>
                <ul class="collection v1 duallist-left duallist-list" role="list" data-role="duallist-right">
                    <{foreach $field.values as $value}>
                        <{if isset($field['options'][$value])}>
                    <li class="collection-item duallist-item"
                        role="listitem"
                        data-role="duallist-item"
                        data-value="<{$field['options'][$value]}>"
                    >
                        <i class="left floated acms-icon acms-icon-check-square regular"
                        <span data-role="name"><{$field['options'][$value]}></span>
                        <input type="checkbox"
                               name="<{$field.name}>"
                               value="<{$value}>"
                               class="hidden"
                               />
                    </li>
                        <{/if}>
                    <{/foreach}>
                </ul>
            </div>
        </div>

    </div>
</div>
</div>
