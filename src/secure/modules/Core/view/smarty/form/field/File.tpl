<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
    <{if $field.with_label}>
    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
        <span class="text-danger required">*</span><{/if}>
        <{if $field.label.description && $field.show_tooltip}>
            <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
            </a>
        <{/if}>
    </label>
    <{/if}>
    <{if $field['useUploadDisclaimer']}>
    <div class="row" data-module="ui/form/upload-disclaimer-required">
        <div class="column xxsmall-12 medium-6 large-3">
            <span role="button" class="accept-disclaimer acms-icon acms-icon-square regular"></span> <{$field["disclaimerText"]}>
        </div>
        <div class="column xxsmall-12 medium-6 large-9">
    <{/if}>
    <{if $field.additionalHelptext}>
        <div class="field-group">
    <{/if}>

    <div class="file-input v5" data-module="apps/ui/elements/file-upload-button" data-has-list="true" data-multi="<{if $field['isMultiUpload']}>true<{else}>false<{/if}>">
        <input type="file" <{$field.attributes}> data-file-value="#<{$field.id}>_label">
        <label for="<{$field.id}>">
            <span id="<{$field.id}>_label" data-role="file-label"><{if $field.value}><{$field.value}><{/if}></span>
            <strong><i aria-hidden="true" class="acms-icon acms-icon-upload"></i> hochladen</strong>
        </label>
    </div>

    <{if $field.additionalHelptext}>
        <span class="field-icon cursor-pointer" data-module="apps/ui/informations/modal" data-toggle="modal" data-target="#<{$field["id"]}>-modal-restrictions">
            <i aria-hidden="true" class="acms-icon acms-icon-question"></i>
        </span>
    <{/if}>
    <{if $field.additionalHelptext}>
        </div>
    <{/if}>
    <{if $field['useUploadDisclaimer']}>
        </div>
    </div>
    <{/if}>
    
        <input type="hidden" name="<{$field["name"]}>_accept_disclaimer" class="accepted-disclaimer" value="<{if $field['useUploadDisclaimer']}>false<{else}>true<{/if}>"  />
    <{if $field.with_label && $field.label.description && !$field.show_tooltip}>
        <span class="field-hint"><{$field.label.description}></span>
    <{/if}>
    
        <div class="file-list v1" data-role="file-list">
            <{if $field.value}>
                <li class="file"><label><{$field.value}></label></li>
            <{/if}>
        </div>
    
    <{if $field.additionalHelptext}>
        <div id="<{$field["id"]}>-modal-restrictions" 
             class="modal"
             role="dialog"
             aria-hidden="true" 
             aria-labelledby="<{$field["secName"]}>-modal-restrictions-title"
             tabindex="-1">
            <div class="modal-dialog">
                <div class="content">
                    <div class="heading">
                        <button class="close" 
                                type="button"
                                data-dismiss="modal" 
                                aria-label="<{translate id="Schließen"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        </button>
                        <h4 class="title" 
                            id="<{$field["secName"]}>-modal-restrictions-title">
                            <{translate id="Upload-Restriktionen" textdomain="Core"}>
                        </h4>
                    </div>
                    <div class="main">
                        <p class="help-block">
                            <{translate id="Für den Datei-Upload gelten die folgenden Restriktionen:" textdomain="Core"}>
                        </p>
                        <ul class="collection v1 has-image">
                            <{foreach $field.additionalHelptext as $restriction}>
                                <li class="collection-item">
                                    <i aria-hidden="true" class="acms-icon acms-icon-check item-image"></i>
                                    <div class="title"><{$restriction}></div>
                                    <{if $restriction@last}>
                                        <ul class="list bullet-list">
                                        <{foreach $field.acceptedMimetypes as $ext => $mime}>
                                            <li><{$ext}> (<{$mime}>)</li>
                                        <{/foreach}>
                                        </ul>
                                    <{/if}>
                                </li>
                            <{/foreach}>
                        </ul>
                    </div>
                    <div class="footer">
                        <button class="button small primary" data-dismiss="modal" type="button">
                            <span><{translate id="Verstanden" textdomain="Core"}></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <{/if}>
</div>
