<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}> <{/if}>
        <span class="text-danger required">*</span>
        <{if $field.label.description && $field.show_tooltip}>
            <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
            </a>
        <{/if}>
    </label>
    <button class="button tiny info"
        data-module="apps/ui/helpers/collapse"
        data-target="#field-theme-info-<{$field['version']->getDirname()}>"
        >
        <i aria-hidden="true" class="acms-icon acms-icon-iquestion-circle regular"></i>
        <span class="src-only">
            <{translate id="Klicken Sie um die Versions-Informationen von %s anzuschauen" textdomain="Core" args=$field['version']->getName()}>
        </span>
    </button>
    <div class="collapse" id="field-theme-info-<{$field['version']->getDirname()}>">
        <h2><{$field['version']->getName()}> <small><{$field['version']->getVersion()}> (<{$field['version']->getBuildversion()}>)</small></h2>
        <p>
            <small>
                <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i> <{$field['version']->getReleaseDate()}>&nbsp;
                <i aria-hidden="true" class="acms-icon acms-icon-folder"></i> <{$field['version']->getDirname()}>&nbsp;
                <i aria-hidden="true" class="acms-icon acms-icon-tags"></i>&nbsp;
                <{foreach $field['version']->getTags() as $tag}>
                    <span class="tag">
                        <{$tag}>
                    </span>
                <{/foreach}>
            </small>
        </p>
        <p>
            <{$field['version']->getDescription()}>
        </p>
        <{assign var=status value=$field['version']->getVersionStatus()}>
        <{assign var=warning value=$field['version']->getVersionWarning()}>
        <{if $status == "Alpha" || $status == "Beta"}>
            <div class="alert v1 danger bordered" data-cssfile="media/css/components/alert.min.css">
                <div class="alert-title">
                    <h3><{$status}></h3>
                </div>
                <div class="alert-content">
                    <p><{$warning}></p>
                </div>
            </div>
        <{elseif $status == "Release Candidate"}>
            <div class="alert v1 warning bordered" data-cssfile="media/css/components/alert.min.css">
                <div class="alert-title">
                    <h3><{$status}></h3>
                </div>
                <div class="alert-content">
                    <p><{$warning}></p>
                </div>
            </div>
        <{else}>
            <div class="alert v1 success bordered" data-cssfile="media/css/components/alert.min.css">
                <div class="alert-title">
                    <h3><{$status}></h3>
                </div>
                <div class="alert-content">
                    <p><{$warning}></p>
                </div>
            </div>
        <{/if}>
        <{if $field['version']->getAuthor()}>
            <{assign var=author value=$field['version']->getAuthor()}>
            <div class="card v1 profile-card" data-module="ui/components/cards">
                <{if $author['email']}>
                    <div class="profile-avatar">
                        <img src="<{gravatar email=$author['email'] returntype="url" type="identicon"}>" alt="" title="<{$author['name']}>" />
                    </div>
                <{/if}>
                <div class="profile-name">
                    <a href="<{if $author['url']}><{$author['url']}><{elseif $author['company']}><{$author['company']['url']}><{/if}>"
                       target="_blank"
                       title="<{translate id="Webseite des Designers" textdomain="Core"}>">
                        <{if $author['url']}>
                            <i aria-hidden="true" class="acms-icon acms-icon-link"></i>
                        <{elseif $author['company']}>
                            <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>
                        <{/if}>
                        <{$author['name']}>
                    </a>
                    <{if $author['url'] && $author['company']}>
                        <a href="<{$author['company']['url']}>"
                           target="_blank"
                           title="<{translate id="Webseite der Firma" textdomain="Core"}>"
                        >
                             <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>
                             <{$author['company']['name']}>
                        </a>
                    <{/if}>
                </div>
            </div>
        <{/if}>
        <{assign var=demo value=$field['version']->getDemo()}>
        <{assign var=donation value=$field['version']->getDonation()}>
        <{assign var=license value=$field['version']->getLicense()}>
        <{if $demo || $donation || $license}>
            <div class="button-toolbar">
                <{if $donation}>
                    <div class="button-group">
                        <a class="button primary small"
                           href="<{$donation}>"
                           target="_blank"
                           title="<{translate id="Spenden Sie für dieses Thema" textdomain="Core"}>"
                        >
                            <i aria-hidden="true" class="acms-icon acms-icon-paypal"></i>
                            <{translate id="Spenden" textdomain="Core"}>
                        </a>
                    </div>
                <{/if}>
                <{if $license}>
                    <div class="button-group">
                        <a class="button warning small"
                           href="<{$license['url']}>"
                           target="_blank"
                           title="<{translate id="Lesen Sie die Endbenutzer-Lizenz des Themas (EULA)" textdomain="Core"}>"
                        >
                            <i aria-hidden="true" class="acms-icon acms-icon-gavel"></i>
                            <{if $license['name']}><{$license['name']}><{else}>EULA<{/if}>
                        </a>
                    </div>
                <{/if}>
                <{if $demo}>
                    <div class="button-group">
                        <a class="button info small"
                           href="<{$donation}>"
                           target="_blank"
                           title="<{translate id="Besuchen Sie die Demo des Themas" textdomain="Core"}>"
                        >
                            <i aria-hidden="true" class="acms-icon acms-icon-eye"></i>
                            <{translate id="Demo" textdomain="Core"}>
                        </a>
                    </div>
                <{/if}>
            </div>
        <{/if}>
        <div class="theme-defaults">
            <h3><{translate id="Aktuelle Standards" textdomain="Core"}></h3>
            <ul class="list-unstyled">
            <{foreach $field['defaults'] as $key => $isDefault}>
                <li>
                    <i aria-hidden="true" class="acms-icon acms-icon-<{if $isDefault}>ok green-text<{else}>ban red-text<{/if}>"></i>&nbsp;
                    <span><{$key}></span>
                </li>
            <{/foreach}>
            </ul>
        </div>
        <div class="theme-supports">
            <h3><{translate id="Unterstützte Module" textdomain="Core"}></h3>
            <ul class="list-unstyled">
            <{foreach $field['version']->getSupportedModules() as $dirname => $name}>
                <li>
                    <i aria-hidden="true" class="acms-icon acms-icon-ok green-text"></i>&nbsp;
                    <span><{$name}></span>
                </li>
            <{foreachelse}>
                <li>
                    <i aria-hidden="true" class="acms-icon acms-icon-ban red-text"></i>&nbsp;
                    <span><{translate id="Dieses Thema unterstützt derzeit keine Module" textdomain="Core"}></span>
                </li>
            <{/foreach}>
            </ul>
        </div>
        <div class="theme-profile">
            <h3><{translate id="Unterstützte Profil-Layouts" textdomain="Core"}></h3>
            <ul class="list-unstyled">
            <{foreach $field['version']->getProfileThemes() as $dirname => $name}>
                <li>
                    <i aria-hidden="true" class="acms-icon acms-icon-ok green-text"></i>&nbsp;
                    <span><{$name}></span>
                </li>
            <{foreachelse}>
                <li>
                    <i aria-hidden="true" class="acms-icon acms-icon-ban red-text"></i>&nbsp;
                    <span><{translate id="Dieses Thema unterstützt derzeit keine Profil-Themen" textdomain="Core"}></span>
                </li>
            <{/foreach}>
            </ul>
        </div>
    </div>
    
    <{if $field.label.description && !$field.show_tooltip}>
        <span class="field-hint"><{$field.label.description}></span>
    <{/if}>
</div>
