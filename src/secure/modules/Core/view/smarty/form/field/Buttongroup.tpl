<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $field.isGroup}>
    <div class="button-toolbar compact clearfix" role="toolbar">
        <{foreach $field.groups as $group}>
            <{if $group['template']}>
                <{include file=$group['template'] field=$group}>
            <{else}>
                <{$group['template']}> | Missing Grp Template<br>
            <{/if}>
        <{/foreach}>
    </div>
<{elseif $field.buttons}>
    <div class="button-group">
        <{foreach $field.buttons as $btn}>
            <{if $btn['template']}>
                <{include file=$btn['template'] field=$btn}>
            <{else}>
                <{$btn['template']}> | Missing Btn Template<br>
            <{/if}>
        <{/foreach}>
    </div>
<{/if}>
