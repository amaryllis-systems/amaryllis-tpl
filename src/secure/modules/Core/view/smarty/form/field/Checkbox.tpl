<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group"<{if $field.useButton}> 
     data-module="ui/components/button"<{/if}>
     data-required="<{if $field.required}>true<{else}>false<{/if}>"
     data-name="<{$field->name}>">
    <{if $field['with_label']}>
        <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}>
            <{if $field.required}>
                <span class="text-danger required">*</span>
            <{/if}>
            <{if $field.label.description && $field.show_tooltip}>
                <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                    <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
                </a>
            <{/if}>
        </label>
    <{/if}>
    <{if !$field['disableList']}>
        <div class="checkbox-list v1 checkbox-labels clearfix">
            <{foreach $field.optionsStack as $option}>
                <div data-module="ui/components/checkbox-switch">
                    <input id="<{if $field['id']}><{$field['id']}>_<{$option@iteration}><{else}><{$field['name']|substr:0:-2}>_<{$option@iteration}><{/if}>" 
                           name="<{$field['name']}>" 
                           value="<{$option->getValue()}>" 
                           <{$field.data}> 
                           <{$option->renderAttributes()}>
                           type="checkbox">
                    <label for="<{if $field['id']}><{$field['id']}>_<{$option@iteration}><{else}><{$field['name']|substr:0:-2}>_<{$option@iteration}><{/if}>">
                        <span><{$option->getName()}></span>
                    </label>
                </div>
            <{/foreach}>
        </div>
    <{else}>
        <div class="<{if $field.useButton}>button-group <{/if}>checkbox-options" <{if $field.useButton}>data-toggle="buttons"<{/if}>>
            <{foreach $field.optionsStack as $option}>
                <{if $field.delimeter == 'linebreak'}>
                    <div class="checkbox">
                    <{/if}>

                    <label class="acms-option-label<{if $field.useButton}> button<{/if}><{if $field.delimeter == 'inline'}> checkbox-inline<{/if}><{if $option->isChecked()}> checked<{/if}>">
                        <input type="checkbox" 
                               name="<{$field.name}>"
                               <{$field.data}>
                               <{$option->renderAttributes()}> 
                               <{if $field.disabled}>disabled="disabled"<{/if}>
                               value="<{$option->getValue()}>" />
                        <{$option->getName()}>
                    </label>
                    <{if $field.delimeter == 'linebreak'}>
                    </div>
                <{/if}>
            <{/foreach}>
            <{if $field['totalOptions'] > 1}>
                <button class="acms-toggle-all button info tiny block">
                    <i aria-hidden="true" class="acms-icon acms-icon-check-square regular acms-icon-2x"></i>
                </button>
            <{/if}>
        </div>
    <{/if}>
</div>
