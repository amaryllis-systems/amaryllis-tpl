<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
    <{if $field.with_label}>
    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
        <span class="text-danger required">*</span><{/if}>
        <{if $field.label.description && $field.show_tooltip}>
            <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
            </a>
        <{/if}>
    </label>
    <{/if}>
    <div class="row full">
    <{foreach $field.optionsStack as $option}>
        <div class="column xxsmall-6 medium-4 large-4 xlarge-4 xxlarge-4">
            <div class="switch v2 success compact toggle-variant round<{if $field.delimeter == 'linebreak'}> display-block<{/if}>" data-module="ui/components/checkbox-switch">
                <span class="option-text text-bold">
                    <{$option->getName()}>
                </span>
                <input id="<{$field['name']|replace:['[', ']']:''}>-<{$option->getValue()}>"
                       type="checkbox" 
                        <{$field.data}>
                       <{if $field['disabled']}>disabled="disabled"<{/if}> 
                       name="<{$field['name']}>" 
                       <{$option->renderAttributes()}> 
                       value="<{$option->getValue()}>" 
                       >
                <label for="<{$field['name']|replace:['[', ']']:''}>-<{$option->getValue()}>"></label>
            </div>
        </div>
    <{/foreach}>
    </div>
    <{if $field.with_label && $field.label.description && !$field.show_tooltip}>
        <span class="field-hint"><{$field.label.description}></span>
    <{/if}>
</div>
