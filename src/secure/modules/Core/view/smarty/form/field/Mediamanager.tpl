<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" data-name="<{$field->name}>">
    <{if $field.with_label}>
    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
        <span class="text-danger required">*</span><{/if}>
        <{if $field.label.description && $field.show_tooltip}>
            <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
            </a>
        <{/if}>
    </label>
    <{/if}>
    <{toJson data=$field['extensions'] assign="extensions"}>
    <div class="left corner labeled field-group" data-module="apps/profile/media-manager/media-manager-field" data-accept="<{$extensions|escape}>" data-remote="<{$field['mediaManagerUrl']}>" data-token="<{$field['mediaManagerToken']}>">
        <input class="field" type="text" data-role="media" readonly="readonly" value="<{if $field['media']}><{$field['media']['title']}><{/if}>" />
        <span class="field-icon cursor-pointer" data-toggle="manager">
            <i aria-hidden="true" class="acms-icon acms-icon-folder-open"></i>
        </span>
        <span class="cursor-pointer left corner-label label v2" data-module="apps/ui/informations/modal" data-target="#modal-restrictions-<{$field['id']}>" data-toggle="modal">
            <i aria-hidden="true" class="acms-icon acms-icon-question"></i>
        </span>
        <input data-role="manager-field" type="hidden" name="<{$field->name}>" value="<{if $field['media']}><{$field['media']['id']}><{/if}>" />
    </div>

    <{if $field.with_label && $field.label.description && !$field.show_tooltip}>
        <span class="field-hint"><{$field.label.description}></span>
    <{/if}>
    
    <{if $field.additionalHelptext}>
        <div id="modal-restrictions-<{$field['id']}>" 
             class="modal"
             role="dialog"
             aria-hidden="true" 
             aria-labelledby="<{$field["id"]}>-modal-restrictions-title"
             tabindex="-1">
            <div class="modal-dialog">
                <div class="content">
                    <div class="heading">
                        <button class="close" 
                                type="button"
                                data-dismiss="modal" 
                                aria-label="<{translate id="Schließen"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        </button>
                        <h4 class="title" 
                            id="<{$field["id"]}>-modal-restrictions-title">
                            <{translate id="Upload-Restriktionen" textdomain="Core"}>
                        </h4>
                    </div>
                    <div class="main">
                        <p class="help-block">
                            <{translate id="Für die Datei gelten die folgenden Restriktionen:" textdomain="Core"}>
                        </p>
                        <ul class="collection v1 has-image">
                            <{foreach $field.additionalHelptext as $restriction}>
                                <li class="collection-item">
                                    <i aria-hidden="true" class="acms-icon acms-icon-check item-image"></i>
                                    <div class="title"><{$restriction}></div>
                                    <{if $restriction@last}>
                                        <ul class="list bullet-list">
                                        <{foreach $field.acceptedMimetypes as $ext => $mime}>
                                            <li><{$ext}> (<{$mime}>)</li>
                                        <{/foreach}>
                                        </ul>
                                    <{/if}>
                                </li>
                            <{/foreach}>
                        </ul>
                    </div>
                    <div class="footer">
                        <button class="button small primary" data-dismiss="modal" type="button">
                            <span><{translate id="Verstanden" textdomain="Core"}></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <{/if}>
</div>
