<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if !$field.hasRepeat}>
    <div class="form-group" data-name="<{$field->name}>" data-module="ui/components/password-show">
        <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}> 
            <span class="text-danger required">*</span> <{/if}>
            <{if $field.label.description && $field.show_tooltip}>
                <a class="acms-helptip" 
                   title="<{$field.label.description}>"
                   data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                   data-trigger="hover click"
                   data-placement="right"
                   data-html="true">
                    <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
                </a>
            <{/if}>
        </label>
        <div class="field-group">
            <input type="password" <{$field.attributes}> />
            <input type="text" class="hidden field">
        </div>
        <{if $field.label.description && !$field.show_tooltip}>
            <span class="field-hint"><{$field.label.description}></span>
        <{/if}>
    </div>
<{else}>
    <div class="container full">
        <div class="row">
            <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                <div class="push-15">
                <div class="form-group" data-module="ui/components/password-show" data-toggle="password">
                    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}> 
                        <span class="text-danger required">*</span> <{/if}>
                        <{if $field.label.description && $field.show_tooltip}>
                            <a class="right floated acms-helptip clearfix"
                               title="<{$field.label.description}>"
                               data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                               data-trigger="hover click"
                               data-placement="top" data-html="true">
                                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
                            </a>
                        <{/if}>
                    </label>
                    <div class="field-group">
                        <input type="password" <{$field.attributes}> data-module="ui/components/password-strength" data-toggle="passwordstrength" />
                    </div>
                    <{if $field.label.description && !$field.show_tooltip}>
                        <span class="field-hint"><{$field.label.description}></span>
                    <{/if}>
                    <div class="progress" data-module="ui/widgets/progressbar" data-toggle="progressbar">
                        <div class="progress-meter" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                            <span class="progress-status"></span>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                <div class="push-15">
                <div class="form-group">
                    <label for="<{$field.name}>2"><{translate id="Passwort wiederholen" textdomain="Core"}>
                        <span class="text-danger required">*</span>
                    </label>
                    <input type="password" class="field password-repeat" name="<{$field.name}>2" data-toggle="passwordrepeat" />
                    <{if $field.label.description && !$field.show_tooltip}>
                        <span class="field-hint"><{$field.label.description}></span>
                    <{/if}>
                </div>
                </div>
            </div>
        </div>
    </div>
<{/if}>
