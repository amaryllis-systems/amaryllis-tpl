<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="alert <{if $field['alertIcon']}>v2<{else}>v3<{/if}> bordered <{$field['alertType']}>" data-name="<{$field->name}>">
    <{if $field['alertIcon']}>
        <i aria-hidden="true" class="acms-icon <{$field['alertIcon']}>"></i>
    <{/if}>
    <{if $field['alertTitle']}>
        <div class="alert-title">
            <h3><{$field['alertTitle']}></h3>
        </div>
    <{/if}>
    <div class="alert-content">
        <p><{$field['alertMessage']}></p>
    </div>
</div>
