<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper">
    <form role="form" <{$form.attributes}> data-module="apps/form/model-form">
        <{if $form.title}>
            <legend><{$form.title}></legend>
        <{/if}>
        <div class="tab-container" data-module="ui/navs/tabs" data-history="false" data-effect="fadeIn">
            <div class="row full">
                <div class="column xxsmall-12 medium-2 large-4 xlarge-4 xxlarge-4" role="tabpanel">
                    <ul class="tabs tabs-vertical tabs-stacked" role="tablist">
                        <{foreach $form['fieldsets'] as $name => $tab}>
                            <li role="presentation" class="tab<{if $tab['active']}> active<{/if}>">
                                <a href="#<{$name}>" role="tab" data-target="#<{$name}>" data-toggle="pill">
                                    <{$tab['title']}>
                                </a>
                            </li>
                        <{/foreach}>
                    </ul>
                </div>

                <div class="column xxsmall-12 medium-10 large-8 xlarge-8 xxlarge-8 tab-content">
                    <{foreach $form['fields'] as $name => $field}>
                        <{if $field['template']}>
                            <{include file=$field['template'] field=$field nocache}>
                        <{else}>
                            <{$name}> ohne Template! <br>
                        <{/if}>
                    <{/foreach}>
                </div>
            </div>
        </div>
    </form>
</div>
