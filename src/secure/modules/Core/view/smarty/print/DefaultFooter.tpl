<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<footer id="print-footer">

    <div class="container">
        <table class="clearfix">
            <tbody>
                <tr>
                    <td>
                        <div class="text-small">
                            <span><strong><{$acmsContents->acmsCompany['company_name']}></strong></span><br /><br>
                            <span><{$acmsContents->acmsCompany['company_street']}></span> | <span><{$acmsContents->acmsCompany['company_postal']}> <{$acmsContents->acmsCompany['company_city']}></span><br /><br>
                            <span><{$acmsData->acmsUrl}></span><br>
                            <span>UStId: <{$acmsContents->acmsCompany['company_vatID']}></span>
                        </div>
                    </td>
                    <td>
                        <div class="text-small">
                            <span><b>Kontakt</b></span><br /><br>
                            <span><i class="acms-icon acms-icon-phone"></i> <span><{$acmsContents->acmsCompany['company_phone']}></span></span><br>
                            <span><i class="acms-icon acms-icon-fax"></i> <span><{$acmsContents->acmsCompany['company_fax']}></span></span><br /><br>
                            <span><i class="acms-icon acms-icon-at"></i> <a href="mailto:<{$acmsContents->acmsCompany['company_email']}>"><{$acmsContents->acmsCompany['company_email']}></a></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="copyright text-small text-center">
                            &copy; <{$acmsData->acmsYear}> <{$acmsData->acmsSitename}> 
        | Bereitgestellt über Amaryllis CMS von <a href="https://www.amaryllis-systems.de/" title="Besuchen Sie Amaryllis Systems GmbH">Amaryllis Systems GmbH</a> 
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</footer>
