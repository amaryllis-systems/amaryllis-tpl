<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<header class="clearfix">
    <div class="container">
        <div>
            <div class="h2">
                <div class="sitename"><{$acmsContents->acmsCompany['company_name']}></div> <small class="slogan display-block push-updown-5"><{$acmsData['acmsSlogan']}></small>
            </div>
        </div>
        <div class="company-info text-small">
            <span><{$acmsContents->acmsCompany['company_name']}>, <{$acmsContents->acmsCompany['company_postal']}> <{$acmsContents->acmsCompany['company_city']}></span><br>
            <a class="phone" href="tel:<{$acmsContents->acmsCompany['company_phone']}>"><{$acmsContents->acmsCompany['company_phone']}></a><br>
            <a class="email" href="mailto:<{$acmsContents->acmsCompany['company_email']}>"><{$acmsContents->acmsCompany['company_email']}></a>
        </div>
    </div>
</header>
