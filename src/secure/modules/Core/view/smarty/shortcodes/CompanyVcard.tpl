<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="row full">
    <div class="column xsmall-12 small-12 medium-6 medium-offset-3 h-card v1vcard" itemscope="" itemtype="https://schema.org/LocalBusiness">
        <div class="push-15">
            <{if $config['title']}>
                <div class="block-title">
                    <h3 class=""><{$config['title']}></h3>
                </div>
            <{/if}>
            <div class="vcard-content">
                <div class="text-bold p-org org" itemprop="name"><{$company['company_legal_name']}></div>
                <span class="<{if !$config['contact']}>hidden<{/if}> p-name fn"><{$company['company_contact_person']}></span>
                <{if $config['address']}>
                <div class="p-addr h-addr work push-updown-5" itemprop="address" itemscope="" itemtype="https://schema.org/PostalAddress">
                    <span class="p-street-address street-address" itemprop="streetAddress"><{$company['company_street']}></span><br>
                    <span class="p-postal-code postal-code" itemprop="postalCode"><{$company['company_postal']}></span>
                    <span class="p-locality locality" itemprop="addressLocality"><{$company['company_city']}></span>
                </div>
                <{/if}>
                <meta itemprop="image" content="<{$acmsData['acmsUrl']}><{$company['company_logo']}>">
                <meta itemprop="logo" content="<{$acmsData['acmsUrl']}><{$company['company_logo']}>">
                <meta itemprop="url" content="<{$acmsData['acmsUrl']}>">
                <meta itemprop="telephone" content="<{$company['company_phone']}>">
                <meta itemprop="faxNumber" content="<{$company['company_fax']}>">
                <div class="hidden" itemprop="description">

                </div>
            </div>
            <div class="column xxsmall-12 ">
                <{if $config['phone']}>
                <div class="row full">
                    <div class="column xxsmall-2">
                        <i aria-hidden="true" class="acms-icon acms-icon-phone"></i>
                    </div>
                    <div class="column xxsmall-10">
                        <span class="tel p-tel work voice pref" itemprop="telephone">
                            <{$company['company_phone']}>
                        </span>
                    </div>
                </div>
                <{/if}>
                <{if $config['fax']}>
                <div class="row full">
                    <div class="column xxsmall-2">
                        <i aria-hidden="true" class="acms-icon acms-icon-fax"></i>
                    </div>
                    <div class="column xxsmall-10">
                        <span class="tel p-tel work fax" itemprop="faxNumber">
                            <{$company['company_fax']}>
                        </span>
                    </div>
                </div>
                <{/if}>
                <div class="row full">
                    <div class="column xxsmall-2">
                        <i aria-hidden="true" class="acms-icon acms-icon-envelope "></i>
                    </div>
                    <div class="column xxsmall-10">
                        <a class="" href="mailto:<{$company['company_email']}>" title="Kontaktieren Sie uns schnell und unverbindlich per E-Mail">
                            <span class="u-email email" itemprop="email">
                                <{$company['company_email']}>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="row full">
                    <div class="column xxsmall-2 "><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i></div>
                    <div class="column xxsmall-10"><p class="text-small " itemprop="openingHours">Mo-Fr 9:00Uhr bis 17:00 Uhr</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
