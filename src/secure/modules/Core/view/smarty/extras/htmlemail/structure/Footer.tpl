<!-- Footer-Content -->
<table bgcolor="<{$palette['grey']}>" cellpadding="15" cellspacing="0" role="presentation" width="100%" style="width: 100%;border-collapse: collapse; border-spacing: 0; padding: 0;">
	<tr>
		<td>
			<!--
					Footer-Container

					Copyright Informationen, Impressum Angaben, Datenschutz Hinweise/Verweise und optional Social Media Verlinkungen.
			-->
			<table class="wrapper" bgcolor="transparent" cellpadding="15" cellspacing="0" role="presentation" width="100%">
				<tr>
					<td>
						<table class="container" align="center" width="640px" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; border-spacing: 0; padding: 0;width:640px; max-width:640px;">
							<tr>
								<td>
									<center>
										<table cellspacing="0" cellpadding="0" width="600" class="w320">
											<tr>
												<td style="padding: 25px 0 25px">
													<strong><{$acmsCompany.company_legal_name}></strong>
													<br /> <{$acmsCompany.company_street}>
													<br /> <{$acmsCompany.company_postal}> <{$acmsCompany.company_city}>
													<br />
													<br />
												</td>
												<td style="padding: 25px 0 25px">
													<br />
													<i class="fa fa-phone"></i> <{$acmsCompany.company_phone}>
													<br />
													<i class="fa fa-fax"></i> <{$acmsCompany.company_fax}>
													<br />
													<br />
												</td>
												<td style="padding: 25px 0 25px">

													<!-- SOCIAL NETWORKS -->
													<!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 -->
												</td>
											</tr>
											<tr>
												<td style="font-size: 80%">
													UStId: <{$acmsCompany.company_vat}>
												</td>
												<td style="font-size: 80%">
													Handlesregister: <{$acmsCompany.company_hrn}>
												</td>
											</tr>
											<tr>
												<td style="font-size: 80%" colspan="2">Amtsgericht Pinneberg; Gesellschafter: Annette Flohrer; Geschäftsführer:
													Annette Flohrer, Steffen Flohrer;</td>
											</tr>
										</table>
									</center>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
						padding-top: 25px;" class="social-icons">
						<table width="256" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0;">
							<tr>

								<!-- ICON 1 -->
								<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;">
									<a target="_blank" href="https://facebook.com/amaryllis.systems" style="text-decoration: none;">
										<img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
												color: <{$palette['black']}>;" alt="F" title="Facebook" width="44" height="44" src="https://www.amaryllis-cms.de/themes/cms/media/icons/fa/brands/facebook.svg">
									</a>
								</td>

								<!-- ICON 2 -->
								<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;">
									<a target="_blank" href="https://twitter.com/software-systeme" style="text-decoration: none;">
										<img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
												color: <{$palette['black']}>;" alt="T" title="Twitter" width="44" height="44" src="https://www.amaryllis-cms.de/themes/cms/media/icons/fa/brands/twitter.svg">
									</a>
								</td>

								<!-- ICON 3 -->
								<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;">
									<a target="_blank" href="https://plus.google.com/communities/100411263420060961652" style="text-decoration: none;">
										<img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
												color: <{$palette['black']}>;" alt="G" title="Google Plus" width="44" height="44" src="https://www.amaryllis-cms.de/themes/cms/media/icons/fa/brands/google-plus.svg">
									</a>
								</td>

								<!-- ICON 4 -->
								<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;">
									<a target="_blank" href="https://www.amaryllis-systems.de/" style="text-decoration: none;">
										<img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
												color: <{$palette['black']}>;" alt="I" title="Amaryllis Systems GmbH" width="44" height="44" src="https://www.amaryllis-cms.de/themes/cms/media/icons/fa/solid/industry.svg">
									</a>
								</td>

								<!-- ICON 5 -->
								<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;">
									<a target="_blank" href="https://www.amaryllis-cms.de/" style="text-decoration: none;">
										<img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
												color: <{$palette['black']}>;" alt="I" title="Amaryllis CMS Webseite" width="44" height="44" src="https://www.amaryllis-cms.de/themes/cms/media/icons/fa/solid/globe.svg">
									</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
