<!-- 
	Farb HG Sektion Kopfbereich
-->
<table bgcolor="<{$palette['primary']}>" cellpadding="15" cellspacing="0" role="presentation" width="100%" style="width: 100%;border-collapse: collapse; border-spacing: 0; padding: 0;">
	<tr>
		<td>
			<table class="wrapper" bgcolor="transparent" cellpadding="15" cellspacing="0" role="presentation" width="100%">
				<tr>
					<td>
						<!--
							Header Container
						-->
						<table class="container" align="center" width="640px" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; border-spacing: 0; padding: 0;width:640px; max-width:640px;">
							<tr>
								<td>
									<table class="container" align="center" bgcolor="<{$palette['white']}>" cellpadding="0" cellspacing="0" role="presentation">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;">
													<tr>
														<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 20px; padding-right: 20px; width: 87.5%;
															padding-top: 20px;padding-bottom: 20px;">

															<!-- PREHEADER -->
															<!-- LOGO -->
															<a target="_blank" style="text-decoration: none;" href="<{$acmsData->acmsUrl}>">
																<img border="0" vspace="0" hspace="0" src="<{$acmsData->acmsUrl}>/logo.png" height="50"
																		alt="Logo" title="Logo" style="color: <{$palette['black']}>;font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;"
																		/>
															</a>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<!-- HEADER -->
							<tr>
								<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
									padding-top: 20px;
									color: <{$palette['white']}>;
									font-family: sans-serif;" class="header">
									Amaryllis CMS Projekt
								</td>
							</tr>
							<!-- SUBHEADER -->
							<tr>
								<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
									padding-top: 5px;
									color: <{$palette['white']}>;
									font-family: sans-serif;" class="subheader">
									Amaryllis-CMS ist ein Community Management System mit herausstechenden Features, einem Fokus auf Sicherheit und flexibler
									Erweiterbarkeit.
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
