<!-- 
	E-Mail Styles 
	Die Styles kommen in den Style-Container, so wie sie in dem CSS-File 
	von SASS erstellt wurden.
-->
<style type="text/css">
	@media only screen {
		font-family: "Droid serif", Georgia, serif !important; }

	.sans-serif {
		font-family: "Droid sans", "Open Sans", Arial, sans-serif !important; } }
	#outlook a {
		padding: 0; }

	table {
		border-collapse: collapse; }

	.column,
	td,
	th {
		font-size: 16px;
		line-height: 26px;
		vertical-align: top; }

	.column,
	td,
	th,
	div,
	p,
	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
		font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", "Roboto", "Helvetica Neue", Arial, sans-serif; }

	img {
		border: 0;
		display: block;
		line-height: 100%; }

	.container {
		margin: 0 auto; }

	.nav-item {
		display: inline-block; }

	.spacer,
	.divider {
		mso-line-height-rule: exactly;
		overflow: hidden;
		vertical-align: middle; }

	a {
		text-decoration: none; }

	.social-button {
		padding: 15px 10px;
		border: 1px solid #494949;
		vertical-align: middle;
		line-height: 100%; }
	.social-button i {
		vertical-align: middle; }

	@media only screen and (max-width: 699px) {
		u ~ div .wrapper {
			min-width: 100vw; }

		.container {
			-webkit-text-size-adjust: 100%;
			width: 100% !important; }

		.column {
			box-sizing: border-box;
			display: inline-block !important;
			line-height: 23px;
			width: 100% !important; }

		.column-small-1 {
			max-width: 8.33333%; }

		.column-small-2 {
			max-width: 16.66667%; }

		.column-small-3 {
			max-width: 25%; }

		.column-small-4 {
			max-width: 33.33333%; }

		.column-small-5 {
			max-width: 41.66667%; }

		.column-small-6 {
			max-width: 50%; }

		.column-small-7 {
			max-width: 58.33333%; }

		.column-small-8 {
			max-width: 66.66667%; }

		.column-small-9 {
			max-width: 75%; }

		.column-small-10 {
			max-width: 83.33333%; }

		.column-small-11 {
			max-width: 91.66667%; }

		.column-small-push-1 {
			margin-left: 8.33333%; }

		.column-small-push-2 {
			margin-left: 16.66667%; }

		.column-small-push-3 {
			margin-left: 25%; }

		.column-small-push-4 {
			margin-left: 33.33333%; }

		.column-small-push-5 {
			margin-left: 41.66667%; }

		.column-small-push-6 {
			margin-left: 50%; }

		.column-small-push-7 {
			margin-left: 58.33333%; }

		.column-small-push-8 {
			margin-left: 66.66667%; }

		.column-small-push-9 {
			margin-left: 75%; }

		.column-small-push-10 {
			margin-left: 83.33333%; }

		.column-small-push-11 {
			margin-left: 91.66667%; }

		img {
			width: 100% !important;
			height: auto !important; }

		.toggle-content {
			max-height: 0;
			overflow: auto;
			transition: max-height .4s linear;
			-webkit-transition: max-height .4s linear; }

		.toggle-trigger:hover + .toggle-content,
		.toggle-content:hover {
			max-height: 999px !important; }

		.show-sm {
			display: inherit !important;
			font-size: inherit !important;
			line-height: inherit !important;
			max-height: none !important; }

		.hide-sm {
			display: none !important; }

		.align-sm-center {
			display: table !important;
			float: none;
			margin-left: auto !important;
			margin-right: auto !important; }

		.align-sm-left {
			float: left; }

		.align-sm-right {
			float: right; }

		.text-sm-center {
			text-align: center !important; }

		.text-sm-left {
			text-align: left !important; }

		.text-sm-right {
			text-align: right !important; }

		.full-width-sm {
			display: table !important;
			width: 100% !important; }

		.stack-sm-top {
			display: table-caption !important;
			max-width: 100%;
			padding-left: 0 !important; }

		.stack-sm-first {
			display: table-header-group !important; }

		.stack-sm-last {
			display: table-footer-group !important; }

		.borderless-sm {
			border: none !important; }

		.spacer,
		.divider {
			height: 30px;
			line-height: 100% !important; }

		.nav-item {
			padding: 0 10px 0 !important; }

		.nav-sm-vertical .nav-item {
			display: block; }

		.nav-sm-vertical .nav-item a {
			display: inline-block;
			padding: 5px 0 !important; }

		.p-sm-0 {
			padding: 0 !important; }

		.p-sm-1 {
			padding: 10px !important; }

		.p-sm-2 {
			padding: 30px !important; }

		.pt-sm-0 {
			padding-top: 0 !important; }

		.pt-sm-1 {
			padding-top: 10px !important; }

		.pt-sm-2 {
			padding-top: 30px !important; }

		.pr-sm-0 {
			padding-right: 0 !important; }

		.pr-sm-1 {
			padding-right: 10px !important; }

		.pr-sm-2 {
			padding-right: 30px !important; }

		.pb-sm-0 {
			padding-bottom: 0 !important; }

		.pb-sm-1 {
			padding-bottom: 10px !important; }

		.pb-sm-2 {
			padding-bottom: 30px !important; }

		.pl-sm-0 {
			padding-left: 0 !important; }

		.pl-sm-1 {
			padding-left: 10px !important; }

		.pl-sm-2 {
			padding-left: 30px !important; }

		.px-sm-0 {
			padding-right: 0 !important;
			padding-left: 0 !important; }

		.px-sm-1 {
			padding-right: 10px !important;
			padding-left: 10px !important; }

		.px-sm-2 {
			padding-right: 30px !important;
			padding-left: 30px !important; }

		.py-sm-0 {
			padding-top: 0 !important;
			padding-bottom: 0 !important; }

		.py-sm-1 {
			padding-top: 10px !important;
			padding-bottom: 10px !important; }

		.py-sm-2 {
			padding-top: 30px !important;
			padding-bottom: 30px !important; } }

</style>
