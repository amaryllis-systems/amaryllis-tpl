<{assign var="palette" value=[
	"primary"       => "#0050ef",
	"secondary"     => "#4b626d",
	"info"          => "#0095ff",
	"success"       => "#5da423",
	"warning"       => "#f0ad4e",
	"danger"        => "#c60f13",
	"royal"         => "#8A2BE2",
	"default"		=> "#e8e8e8",
	"highlight"     => "#ffe484",
	"light"         => "#2F2F2",
	"dark"          => "#292929",
	"grey"          => "#6F6F6F",
	"greyLight"     => "#E4E4E4",
	"greyDark"      => "#1e1e1e",
	"white"         => "#FFFFFF",
	"black"         => "#000000",
	"muted" 		=> "#6F6F6F",

	"office" => [
		"explorer" => "#0097A7",
		"tasks" => "#5C6BC0",
		"mail" => "#7F5CC0",
		"notepad" => "#EC407A",
		"notes" => "#C0B15C",
		"calendar" => "#5FD38E",
	]
] scope=global}>
