<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="xxsmall-push-updown-30">
    <div class="container fixed-1024 fixed-1200 fixed-1600">
        <div class="row full xxsmall-pushup-15">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-folder text-muted text-xxxlarge"></i>
                    <div class="content">
                        <h1 class="text-primary title">
                            <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                        </h1>
                        <p class="sub-title">
                            <{translate id="Hier finden Sie Ihre erworbenen Module"}>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row full xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div id="loading" class="text-center">
                    <span>
                        <i aria-hidden="true" class="acms-icon acms-icon-spinner acms-icon-pulse acms-icon-3x acms-icon-fw"></i>
                        <span class="src-only"><{translate id="Lädt"}>&hellip;</span>
                    </span>
                </div>
                <div class="push-15" data-remote="<{generateUrl name="core_marketplace_action" parameters=['action' => 'action']}>" data-module="apps/admin/module/module-loader">
                    <div id="module-response" class="small cards">
                        
                    </div>
                </div>
                <div class="alert v2 danger center-block bordered">
                    <i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle acms-icon-3x"></i> 
                    <h3 class="alert-title">
                        <span>ACHTUNG</span>
                    </h3>
                    <div class="alert-content">
                        <p>
                            Der System-Download sollte nur genutzt werden, um das System 
                            erneut herunterzuladen, wenn es aus irgendwelchen Gründen 
                            erforderlich sein sollte! Im Rahmen dieses Downloads wird 
                            <strong class="text-uppercase">
                                kein System-Update durchgeführt
                            </strong>!
                        </p>
                        <div class="button-toolbar compact clearfix push-15">
                            <button id="force-system-download" class="left labeled danger icon-button button" type="button" data-dirname="Core">
                                <i aria-hidden="true" class="acms-icon acms-icon-download"></i>
                                <span class="content">System herunterladen</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row full xxsmall-push-updown-30">
            <div class="column xxsmall-12 xxsmall-push-updown-30">
                <div class="content-teaser v1 horizontal-teaser text-center">
                    <div class="teaser-item">
                        <div class="inner">
                            <h2 class="heading">Nicht das richtige gefunden?</h2>
                            <div class="deco-line"></div>
                            <p class="lead text-left">
                                Wenden Sie sich an unser 
                                <a href="https://www.amaryllis-systems.de/kontakt/" 
                                target="_blank" 
                                title="Amaryllis Systems Support kontaktieren">
                                    Support-Team
                                </a> und stellen Sie eine Anfrage oder melden Sie 
                                sich mit Ihrem Kunden-Account an, wenn Sie einen besitzen, 
                                um eine Modul-Anfrage zu stellen oder mehr Module zu 
                                erwerben.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
