<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="core-logger-button-wrapper" data-role="logger-button">
    <button class="xxsmall left labeled <{if $tabs['errors']['count']}>danger<{else}>default<{/if}> icon button <{if $tabs['errors']['count']}>danger<{else}>inverse<{/if}>" 
            data-module="apps/ui/informations/modal"
            data-target="#core-logger-modal">
        <i aria-hidden="true" class="acms-icon acms-icon-terminal"></i> 
        <span class="content">
            System Logger
            <{if $tabs['errors']['count']}>
                <span class="badge v1 danger"><{$tabs['errors']['count']}></span>
            <{else}>
                <span class="badge v1 success">0</span>
            <{/if}>
        </span>
    </button>
</div>
<div class="inverted full-screen modal" 
     id="core-logger-modal"
     data-modal-index="1" 
     tabindex="-1"
     role="dialog"
     aria-labelledby="core-logger-modal-title"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <div class="header">
                <button class="close" data-dismiss="modal" aria-label="<{translate id="Schließen" textdomain="Core"}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                </button>
                <h4 class="title text-center" id="core-logger-modal-title">Core Logger</h4>
            </div>
            <div class="main">
                <!-- Logger Output -->
                <div class="container full logger-container">
                    <div class="row">
                        <div class="column xxsmall-12 medium-10 large-10 xlarge-10 medium-offset-1">
                            <div id='acms-logger-output' class="tabbable tab-container core-logger" data-module="core/logger">
                                <ul class="tabs tabs-center tabs-justified" data-history="false" role="tablist" data-module="ui/navs/tabs" data-effect="zoomIn">
                                    <{foreach $tabs as $name => $tab}>
                                        <{if $tab@first}><{continue}><{/if}>
                                        <li class="tab debugger-tab<{if $tab@index == 1}> active<{/if}>" aria-expanded="<{if $tab['active']}>true<{else}>false<{/if}>">
                                            <a href="#acms-logger-<{$name}>" data-tab="<{$name}>" data-target="#acms-logger-<{$name}>" role="tab" data-toggle="tab">
                                                <{$tab['title']}> <span class="badge v1"><{if $tab['count']}><{$tab['count']}><{else}>0<{/if}></span>
                                            </a>
                                        </li>
                                    <{/foreach}>
                                </ul>
                                <div class="tab-content">
                                    <{foreach $tabs as $name => $tab}>
                                        <{if $tab@first}><{continue}><{/if}>
                                        <div class="tab-panel<{if $tab@index == 1}> active<{/if}>" id="acms-logger-<{$name}>" aria-expanded="<{if $tab['active']}>true<{else}>false<{/if}>" data-pane="<{$name}>">
                                            <{if $tab@index == 1}>
                                                <div class="alert v1 info">
                                                    <div class="alert-title">
                                                        Informations-Angaben für Fragen im Forum
                                                    </div>
                                                    <div class="alert-content">
                                                        <pre id="core-logger-forum-angaben" class="bg-inverted">
                                                            * **Amaryllis-CMS Version**: <{$acmsData->acmsVersion}>
                                                            * **Modul**: <{$acmsData->acmsModule['dirname']}>
                                                            * **Modul Version**: <{$acmsData->acmsModule['version']}>.<{$acmsData->acmsModule['dbversion']}>
                                                            * **Browser**: <{$acmsData->browser}> (<{$acmsData->engine}>
                                                            * **OS**: <{$acmsData->os}>
                                                        </pre>
                                                    </div>
                                                </div>
                                            <{/if}>
                                            <{if isset($data[$name])}>
                                                <{foreach $data[$name] as $index => $log}>
                                                    <div class="logger-entry <{if $log['type'] == 'error'}>danger<{elseif $log['type'] == 'success'}>success<{elseif $log['type'] == 'info'}>info<{elseif $log['type'] == 'warning'}>warning<{elseif $log['type'] == 'notice'}>muted<{else}>secondary<{/if}>">
                                                        <{if is_array($log['message'])}>
                                                            
                                                            <{foreach $log['message'] as $message}>
                                                                <{if $message@first}>
                                                                    <a data-toggle="collapse" 
                                                                        aria-controls="1" 
                                                                        data-target="#log-entry-<{$name}>-<{$index}>" 
                                                                        data-icon-collapsed="acms-icon-caret-square-right"
                                                                        data-icon-expanded="acms-icon-caret-square-down"
                                                                        data-module="apps/ui/helpers/collapse" 
                                                                        href="#log-entry-<{$name}>-<{$index}>" 
                                                                        aria-expanded="false" class="push-right-20 white-text primary-text-hover">
                                                                        <i aria-hidden="true" class="acms-icon regular acms-icon-caret-square-right"></i>
                                                                    </a>
                                                                    <strong><{$message}></strong>&nbsp;
                                                                    <div id="log-entry-<{$name}>-<{$index}>" class="collapse hiding" role="tabpanel" aria-expanded="false">
                                                                        <ul class="secondary boxed list pull-leftright-35 pull-updown-30">
                                                                <{else}>
                                                                    <li class="text-italic"><{$message}></li>
                                                                <{/if}>
                                                                <{if $message@last}>
                                                                        </ul>
                                                                    </div>
                                                                <{/if}>
                                                            <{/foreach}>
                                                        <{else}>
                                                            <span><{$log['message']}></span>
                                                        <{/if}>
                                                    </div>
                                                <{/foreach}>
                                            <{elseif $name != "none" && $name != "javascript"}>
                                                <{translate id="Keine Log-Einträge vorhanden" textdomain="Core"}>
                                            <{/if}>
                                        </div>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer text-center">
                <button class="primary left labeled icon button" data-dismiss="modal">
                    <i aria-hidden="true" class="acms-icon acms-icon-check"></i>
                    <span class="content">schließen</span>
                </button>
            </div>
        </div>
        
    </div>
</div>
