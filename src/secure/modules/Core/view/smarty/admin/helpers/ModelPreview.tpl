<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="apf-model-preview-modal-<{$model->id}>" class="modal" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="siteinfo-modal-title">
    <div class="modal-dialog">
        <div class="content">
            <div class="main">
                <{foreach $model->columns as $data}>
                <div class="row full">
                    <div class="column xxsmall-3">
                        <i aria-hidden="true" class="<{$data->icon}>"></i> 
                    </div>
                    <div class="column xxsmall-9 text-center">
                        <p><{$data->value}></p>
                    </div>
                </div>
                <{/foreach}>
            </div>
            <div class="footer">
                <button type="button" data-dismiss="modal" class="button small primary">
                    <span>Ok</span>
                </button>
            </div>
        </div>
    </div>
</div>
