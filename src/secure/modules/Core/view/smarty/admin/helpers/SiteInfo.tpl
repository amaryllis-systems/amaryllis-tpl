<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<a class="site-info-link" 
   data-module="view/admin/version/core-update-modal"
   data-target="#siteinfo-modal" 
   data-updatecheck="<{generateUrl name="core_update_action" parameters=['action' => 'action']}>" 
   data-updateprocess="<{generateUrl name="core_update" parameters=['action' => 'update']}>"
   href="#">
    <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
</a>
<div id="siteinfo-modal" class="modal" role="dialog" tabindex="-1" aria-hidden="true" aria-labelledby="siteinfo-modal-title">
    <div class="modal-dialog">
        <div class="content">
            <div class="main">
                <{getReleaseInfo assign="releaseInfos"}>
                <div class="row full">
                    <div class="column xxsmall-3">
                        <img class="responsive" src="<{$acmsData->acmsUrl}>/media/icons/favicon-96x96.png" alt="" title="Amaryllis CMS" />
                    </div>
                    <div class="column xxsmall-9 text-center">
                        <div class="version-title">
                            <h2 id="siteinfo-modal-title" class="text-center">Amaryllis-CMS</h2>
                            <small>
                                <a title="<{$releaseInfos['provider']['company']}>" 
                                   class="provider-link" 
                                   href="<{$releaseInfos['provider']['uri']}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>
                                    <span>
                                        <{$releaseInfos['provider']['company']}>
                                    </span>
                                </a> | 
                                <span>
                                    <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>&nbsp;
                                    <span><{$releaseInfos['releaseDate']}></span>
                                </span>
                            </small>
                        </div>
                        <div class="version h3">
                            <{$releaseInfos['synonym']}> <{$releaseInfos['version']}>.<{$releaseInfos['buildVersion']}>
                        </div>
                        <div class="copyright">
                            <p><{translate id="Alle Rechte vorbehalten" textdomain="Core"}><br>
                            &copy; <{$acmsData->acmsYear}> <{$releaseInfos['provider']['company']}></p>
                        </div>
                        <div class="update-check">
                            <div class="percentage-40">
                                <span id="update-check-icon" style="font-size: 3rem;">
                                    <span class="acms-icon-stack acms-icon-lg">
                                        
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-circle regular acms-icon-stack-2x text-success"></i>
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-check acms-icon-stack-1x text-success"></i>
                                    </span>
                                </span>
                            </div>
                            <div class="percentage-50">
                                <div class="h3" data-role="notice">Die aktuelleste Version ist bereits installiert</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <button type="button" data-dismiss="modal" class="button small primary">
                    <span>Ok</span>
                </button>
            </div>
        </div>
    </div>
</div>
