<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container full">
    <div class="row">
        <div class="column xxsmall-12">
            <div class="panel v1 info text-center collapsible" data-module="apps/ui/components/panel">
                <div class="heading">
                    <h2 class="title">
                        <i aria-hidden="true" class="acms-icon acms-icon-info-circle left"></i>
                        <{if $acmsContents['acmsInfoTitle']}>
                            <span><{$acmsContents['acmsInfoTitle']}></span>
                        <{/if}>
                    </h2>
                </div>
                <div class="content text-left">
                    <p><{$acmsContents['acmsInfo']}></p>
                </div>
            </div>
        </div>
    </div>
</div>
