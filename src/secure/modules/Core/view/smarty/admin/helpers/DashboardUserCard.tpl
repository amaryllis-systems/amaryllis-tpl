<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="primary cover v1">
    <h2 class="welcome-heading text-capitalize white-text">
        <{getDate assign="currentDate"}>
        <{assign var="hour" value=$currentDate->format('G')}>
        <{if $hour > 4 &&  $hour < 10}>
            <{translate id="Guten Morgen, %s!" args=[$acmsData->user['username']] assign="message"}>
        <{elseif $hour >= 10 &&  $hour < 15}>
            <{translate id="Guten Tag, %s!" args=[$acmsData->user['username']] assign="message"}>
        <{elseif $hour >= 15 &&  $hour < 16}>
            <{translate id="Hallo, %s!" args=[$acmsData->user['username']] assign="message"}>
        <{elseif $hour >= 16 &&  $hour < 23}>
            <{translate id="Schönen Abend, %s!" args=[$acmsData->user['username']] assign="message"}>
        <{else}>
            <{translate id="Schöne Träume, %s!" args=[$acmsData->user['username']] assign="message"}>
        <{/if}>
        <{$message}>
    </h2>
    <hr />
    <div class="welcome-info">
        <p class="left floated">
            <a class="xxlarge round avatar push-right-35" href="<{generateurl name="core_profile_show" parameters=['username' => $acmsContents.currentUser.username|lower]}>">
                <img class="" src="<{$acmsContents.currentUser.avatar}>" title="<{$acmsContents['currentUser']['firstName']}> <{$acmsContents['currentUser']['lastName']}>" alt="Avatar">
            </a>
        </p>
        <div class="welcome-info-body white-text">
            <div class="page-title v7">
                <h4 class="white-text title">
                    <{$acmsContents['currentUser']['firstName']}> <{$acmsContents['currentUser']['lastName']}>
                </h4>
                <div class="sub-title"> <{$acmsContents['currentUser']['username']}></div>
            </div>

            <{if $acmsContents['currentUser']['lastLogin']}>
                <div class="text-small last-login">
                    <span class="display-block push-updown-5">
                        <span class="xxsmall default right arrow label v2"><{translate id="Letzter Login"}></span> <{$acmsContents['currentUser']['lastLogin']}><br>
                    </span>
                    <span class="display-block push-updown-5">
                        <span class="xxsmall default right arrow label v2"><{translate id="Von IP-Adresse"}></span> <{$acmsContents['currentUser']['lastIP']}><br>
                    </span>
                    <span class="display-block push-updown-5">
                        <span class="xxsmall default right arrow label v2"><{translate id="In Agent"}></span> <{$acmsContents['currentUser']['lastAgent']}><br>
                    </span>
                </div>
            <{/if}>
        </div>
    </div>
</div>
