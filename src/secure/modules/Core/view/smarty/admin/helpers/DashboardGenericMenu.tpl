<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="button-toolbar">
    <{foreach $block.content as $link}>
        <div class="button-group">
            <a class="shortcut-button auto-width" href="<{$link.link}>" title="<{$link.title}>">
                <{if $link['fonticon']}>
                    <i class="<{$link['fonticon']}>"></i>
                <{/if}>
                <span class="title text-small"><{$link.title}></span>
            </a>
        </div>
    <{/foreach}>
</div>
