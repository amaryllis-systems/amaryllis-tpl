<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper">
    <form role="form" <{$form.attributes}>>
        <{if $form.title}>
            <legend><{$form.title}></legend>
        <{/if}>
        <div class="tab-container" data-module="ui/navs/tabs" data-history="0">
            <div class="row full">
                <div class="column xxsmall-12 medium-12 large-3 xlarge-3 xxlarge-3">
                    <ul class="tabs tabs-stacked tabs-vertical" role="tabs">
                        <{foreach $form['tabs'] as $name => $tab}>
                            <li role="presentation" class="tab<{if $tab['active']}> active<{/if}>" <{if $tab['active']}>aria-selected="true"<{else}>aria-selected="false"<{/if}>>
                                <a href="#<{$name}>" role="tab" data-target="#<{$name}>" data-toggle="tab">
                                    <{$tab['title']}>
                                </a>
                            </li>
                        <{/foreach}>
                    </ul>
                </div>
                <div class="column xxsmall-12 medium-12 large-9 xlarge-9 xxlarge-9 tab-content">
                    <{foreach $form.fields as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
            </div>
        </div>
    </form>
</div>

