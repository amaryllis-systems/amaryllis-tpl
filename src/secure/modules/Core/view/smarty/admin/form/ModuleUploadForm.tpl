<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper">
    <form role="form" <{$form.attributes}> >
        <{foreach $form.fields as $field}>
            <{include file=$field['template'] field=$field}>
        <{/foreach}>
    </form>
    <div>
        <div data-role="fileName"></div>
        <div data-role="fileSize"></div>
        <div data-role="fileType"></div>
        <div class="progress">
            <div class="progress-bar progress-bar-success"
                 role="progressbar" aria-valuenow="0" aria-valuemin="0"
                 aria-valuemax="100"
                 >
                <span>0%</span>
            </div>
        </div>
    </div>
</div>
