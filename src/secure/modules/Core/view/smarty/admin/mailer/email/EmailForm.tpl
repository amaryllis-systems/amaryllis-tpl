<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal bottom sheet"
     id="email-modal-form-<{$email['id']}>"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="email-modal-form-<{$email['id']}>-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" <{$email['form']['attributes']}>>
                <div class="header">
                    <span role="button" class="close right" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="src-only">Schließen</span>
                    </span>
                    <h4 class="title" id="email-modal-form-<{$email['id']}>-title">
                        <{$email['form']['title']}>
                    </h4>
                </div>
                <div class="main">
                    <{foreach $email['form']['fields'] as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
                <div class="footer">
                    <{include file=$email['form']['buttons']['template'] field=$email['form']['buttons']}>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>
