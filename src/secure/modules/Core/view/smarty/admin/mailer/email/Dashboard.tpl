<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-at"></i>
                        <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Verwalten Sie Ihre E-Mail-Adressen für den Versand von E-Mails. Jede E-Mail Adresse kann in den E-Mail-Templates ausgewählt werden." textdomain="Core"}>
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-user-plus text-muted"></i>
                        <span><{translate id="E-Mail-Adresse hinzufügen" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Legen Sie neue E-Mail-Adressen zum senden Ihrer System- und Modul-E-Mails an. Wir empfehlen Ihnen, jede E-Mail Adresse via SMTP-Authentifizierung einzurichten." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar clearfix compact">
                        <a class="right button primary small" 
                           data-module="apps/ui/informations/modal"
                           data-target="#email-modal-form-add"
                           title="<{translate id="Benutzer anlegen" textdomain="Core"}>"
                           href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'mailer', 'item' => 'email', 'action' => 'neu']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                            <span>E-Mail-Adresse hinzufügen</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach wie Sie E-Mail Adressen einrichten können. Erfahren Sie, wie Sie die passenden E-Mail-Einstellungen setzen und was der Unterschied zwischen den Sende-Methoden ist." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="https://www.amaryllis-support.de/core/administration/mailer/email-adressen/?_utm=email-dashboard"
                           target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="emails" class="container full">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="push-10">
                <div class="feed-collection v1 one-small-only five-large-up-only" data-module="view/admin/mailer/emails">
                    <{foreach $acmsContents->get('emails') as $email}>
                        <{include file="acmsfile:Core|admin/mailer/email/Email.tpl" email=$email nocache}>
                    <{/foreach}>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-action-button">
        <a role="button" 
           class="button floating large red"
           href="#"
           data-module="apps/ui/informations/modal"
           data-target="#email-modal-form-add"
           title="<{translate id="Neue E-Mail Adresse erstellen" textdomain="Core"}>">
            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
        </a>
    </div>
    <{include file="acmsfile:Core|admin/mailer/email/EmailAddForm.tpl" form=$acmsContents->get('addForm') nocache}>
</section>
