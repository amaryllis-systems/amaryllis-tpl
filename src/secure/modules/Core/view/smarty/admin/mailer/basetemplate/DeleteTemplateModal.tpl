<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal"
     id="emailtemplate-modal-form-delete"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="email-modal-form-delete-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" method="post" action="#" data-remote="<{$acmsContents->remote}>" data-reload="true" data-module="view/form/model-form">
                <div class="header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Schließen</span>
                    </button>
                    <h4 class="title" id="email-modal-form-delete-title">
                        Basis-Template löschen
                    </h4>
                </div>
                <div class="main">
                    <div class="alert v2 danger bordered">
                        <p class="alert-content">
                            Sind Sie sicher dass Sie das Template löschen möchten?
                        </p>
                    </div>
                    <input type="hidden" name="op" value="delete">
                    <input type="hidden" name="token" value="<{$acmsContents->token}>">
                    <input type="hidden" name="id" value="0">
                </div>
                <div class="footer">
                    <button class="button small border-button danger" data-trigger="abort" type="reset" data-dismiss="modal">
                        <span>abbrechen</span>
                    </button>
                    <button class="button small danger" data-trigger="save-close" type="submit">
                        <span>enfernen</span>
                    </button>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>


<div class="modal"
     id="emailtemplate-modal-form-toggle"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="email-modal-form-toggle-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" method="post" action="#" data-remote="<{$acmsContents->remote}>" data-reload="true" data-module="view/form/model-form">
                <div class="header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Schließen</span>
                    </button>
                    <h4 class="title" id="email-modal-form-toggle-title">
                        Standard-Template ändern
                    </h4>
                </div>
                <div class="main">
                    <div class="form-group">
                        <div class="checkbox v4">
                            <input type="checkbox" name="toggle_all" id="basetemplate_0_toggle_all" value="1">
                            <label for="basetemplate_0_toggle_all">Alle Templates des alten Standard-Template dem neuen zuweisen?</label>
                        </div>
                    </div>
                    <input type="hidden" name="op" value="toggle">
                    <input type="hidden" name="token" value="<{$acmsContents->token}>">
                    <input type="hidden" name="id" value="0">
                </div>
                <div class="footer">
                    <button class="button small border-button primary" data-trigger="abort" type="reset" data-dismiss="modal">
                        <span>abbrechen</span>
                    </button>
                    <button class="button small primary" data-trigger="save-close" type="submit">
                        <span>anwenden</span>
                    </button>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>
