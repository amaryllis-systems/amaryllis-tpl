<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-files-o text-muted"></i>
                        <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Verwalten Sie Ihre Basis-Templates für den versand von E-Mails. Jedes Basis-Template kann ein oder mehrere E-Mail-Templates zugewiesen bekommen und stellen das Grundgerüst der E-Mail dar." textdomain="Core"}>
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            </div>
        </div>
    </div>
    <div class="container full">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="pull-updown-30">
                    <div class="panel v2 primary bordered">
                        <div class="heading">
                            <h3 class="title">Email-Queue <span class="right badge default"><{$acmsContents->total}></span></h3>
                        </div>
                        <div class="content">
                            <table class="table responsive striped hover compact"
                                    <{*data-module="view/admin/mailer/basetemplate"*}>>
                                <thead>
                                    <tr>
                                        <th class="size-50">
                                            ID
                                        </th>
                                        <th>
                                            Itemname
                                        </th>
                                        <th>
                                            Modul
                                        </th>
                                        <th>
                                            Template
                                        </th>
                                        <th class="size-150">
                                            Aktionen
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach $acmsContents->queue as $item}>
                                        <tr data-id="<{$item['id']}>"<{if $item['isdefault']}> class="primary"<{/if}>>
                                            <td data-th="ID">
                                                <{$item['id']}>
                                            </td>
                                            <td data-th="Bezeichnung">
                                                <{$item['itemname']}>
                                            </td>
                                            <td data-th="Name">
                                                <{$item['module']}>
                                            </td>
                                            <td data-th="Template">
                                                <{$item['template']}>
                                            </td>
                                            <td data-th="Aktionen">
                                                
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
