<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-files-o text-muted"></i>
                        <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Verwalten Sie Ihre Basis-Templates für den versand von E-Mails. Jedes Basis-Template kann ein oder mehrere E-Mail-Templates zugewiesen bekommen und stellen das Grundgerüst der E-Mail dar." textdomain="Core"}>
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-file-code-o text-muted"></i>
                        <span><{translate id="Basis-Template hinzufügen" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Legen Sie neue Basis-Templates zum senden Ihrer System- und Modul-E-Mails an. Wir empfehlen Ihnen, jedes Basis-Template von einem geeigneten Designer anfertigen zu lassen, der die Unterschiede zwischen verschiedenen E-Mail Clients kennt.." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar clearfix compact">
                        <a class="right button primary small" 
                           title="<{translate id="Benutzer anlegen" textdomain="Core"}>"
                           href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'mailer', 'item' => 'basetemplate', 'action' => 'neu']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                            <span>Basis-Template hinzufügen</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach wie Sie am Besten mit Basis-Templates verfahren und wie Sie diese Einrichten können." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="https://www.amaryllis-support.de/core/administration/mailer/basis-templates/"
                           target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container full">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="pull-updown-30">
                    <div class="panel v2 default bordered">
                        <div class="heading">
                            <h3 class="title">Basis-Templates</h3>
                        </div>
                        <div class="content">
                            <table class="table responsive striped hover compact"
                                   data-module="view/admin/mailer/basetemplate">
                                <thead>
                                    <tr>
                                        <th class="size-50">
                                            ID
                                        </th>
                                        <th>
                                            Bezeichnung
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th class="size-200">
                                            Aktionen
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach $acmsContents->basetemplates as $template}>
                                        <tr data-id="<{$template['id']}>"<{if $template['isdefault']}> class="primary"<{/if}>>
                                            <td data-th="ID">
                                                <{$template['id']}>
                                            </td>
                                            <td data-th="Bezeichnung">
                                                <{$template['title']}>
                                            </td>
                                            <td data-th="Name">
                                                <{$template['name']}>
                                            </td>
                                            <td data-th="Aktionen">
                                                <div class="button-toolbar compact">
                                                    <a href="<{$template['editUrl']}>"
                                                       class="button icon-button <{if !$template['isdefault']}>primary<{else}>secondary<{/if}>"
                                                       title="<{translate id="Bearbeite %s" textdomain="Core" args=$template['title']}>"
                                                       data-action="edit"
                                                       data-module="apps/ui/informations/tooltip">
                                                        <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                                                    </a>
                                                    <{if !$template['isdefault']}>
                                                        <button
                                                            aria-label="<{translate id="Lösche %s" textdomain="Core" args=$template['title']}>"
                                                            data-action="delete"
                                                            data-module="apps/ui/informations/modal"
                                                            data-target="#emailtemplate-modal-form-delete"
                                                            class="button icon-button danger">
                                                            <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                                        </button>
                                                        <button
                                                            aria-label="<{translate id="Setze %s als Standard" textdomain="Core" args=$template['title']}>"
                                                            data-action="makeDefault"
                                                            data-module="apps/ui/informations/modal"
                                                            data-target="#emailtemplate-modal-form-toggle"
                                                            class="button icon-button inverse">
                                                            <i aria-hidden="true" class="acms-icon acms-icon-toggle-on"></i>
                                                        </button>
                                                    <{else}>
                                                        <span class="acms-helptip cursor-default" data-module="apps/ui/informations/tooltip" data-title="Standard-Template">
                                                            <i class="acms-icon acms-icon-check-circle text-success acms-icon-3x"></i>
                                                        </span>
                                                    <{/if}>
                                                </div>
                                            </td>
                                        </tr>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/mailer/basetemplate/DeleteTemplateModal.tpl"}>
</section>
