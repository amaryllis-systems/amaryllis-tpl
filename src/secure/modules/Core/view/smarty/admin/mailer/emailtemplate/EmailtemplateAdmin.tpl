<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-envelope text-muted"></i>
                        <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Verwalten Sie Ihre E-Mail-Templates für den versand von E-Mails. Jedes E-Mail-Template repräsentiert eine E-Mail-Benachrichtigung für einen Fall. E-Mail-Templates stellen den Inhalt der E-Mail dar." textdomain="Core"}>
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-file text-muted"></i>
                        <span><{translate id="E-Mail-Templates" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{if $acmsContents->module == 'Core'}>
                            <{assign var="case" value="System"}>
                        <{else}>
                            <{assign var="caseMod" value=$acmsContents->module}>
                            <{assign var="case" value="$caseMod Modul"}>
                        <{/if}>
                        <{translate id="Die Text-Inhalte Ihrer E-Mail Templates für das %s. Diese E-Mail-Templates können bearbeitet werden und so angeglichen, wie Sie es brauchen. Neue Templates kann man derzeit nicht hinzufügen, da Templates ausschließlich vom System verwaltet werden." textdomain="Core" args=$case}>
                    </p>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach was Benutzer sind und wie Sie diese Einrichten können. Erfahren Sie, wie Sie die passenden E-Mail-Einstellungen setzen und was der Unterschied zwischen den Sende-Methoden ist." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="https://www.amaryllis-support.de/core/administration/mailer/email-templates/"
                           target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container full">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="pull-updown-30">
                    <div class="panel v2 default bordered">
                        <div class="heading">
                            <h3 class="title">E-Mail Templates</h3>
                        </div>
                        <div class="content">
                            <table class="table responsive striped compact hover">
                                <thead>
                                    <tr>
                                        <th class="size-50">
                                            ID
                                        </th>
                                        <th>
                                            Bezeichnung
                                        </th>
                                        <th>
                                            Betreff
                                        </th>
                                        <th class="size-200">
                                            Name
                                        </th>
                                        <th class="size-50">
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <{foreach $acmsContents->basetemplates as $template}>
                                        <{include file="acmsfile:Core|admin/mailer/emailtemplate/EmailtemplateItem.tpl" template=$template}>
                                    <{/foreach}>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="pull-updown-30">
                    
                </div>
            </div>
        </div>
    </div>
</section>
