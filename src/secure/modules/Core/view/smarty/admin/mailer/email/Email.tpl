<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="card v1hoverable email-card" data-module="ui/components/cards">
    <div class="card-image text-center">
        <img class="circle image-avatar" src="<{gravatar email=$email['email_raw'] returntype="url" type="identicon"}>">
    </div>
    <div class="card-content">
        <div class="card-title">
            <span class="small"><{$email['title']}></span>
        </div>
        <div class="subtitle text-uppercases">
            <{$email['email_raw']}> <span class="label v1 default">ID: <{$email['id']}></span>
        </div>

    </div>
    <div class="card-reveal">
        <h3 class="card-title">
            <span><{$email['title']}></span>
            <i class="right small acms-icon acms-icon-times-circle regular grey-text text-lighten-2" aria-label="<{translate id="Schließen" textdomain="Core"}>" aria-hidden="true" data-toggle="close"></i>
        </h3>
        <ul class="list list-striped">
            <li>
                <{translate id="Sende-Methode"}><br>
                <{$email['method']}>
                <{if $email['method'] == "smtpauth"}>
                    <span class="right floated">
                        <i aria-hidden="true" class="acms-icon acms-icon-arrow-alt-circle-down regular"
                           data-module="apps/ui/helpers/collapse"
                           data-target="#smtp-auth-info-<{$email['id']}>"></i>
                    </span>
                    <div id="smtp-auth-info-<{$email['id']}>" class="collapse">
                        <div class="smtp-auth-info grey darken-2 white-text">
                            <b>Auth-Name:</b><br>
                            <{$email['smtpauth_uname']}><br />
                            <b>Auth-Methode:</b><br>
                            <{$email['smtpauth_method']}><br />
                            <b>Auth-Host:</b><br>
                            <{$email['smtpauth_host']}><br />
                            <b>Auth-Port:</b><br>
                            <{$email['smtpauth_port']}><br />
                        </div>
                    </div>
                    <span class="clearfix"></span>
                <{/if}>
            </li>
            <li>
                <{translate id="Signatur"}><br>
                <{$email['method']}>
                <span class="right floated">
                    <i aria-hidden="true" class="acms-icon acms-icon-arrow-alt-circle-down regular"
                       data-module="apps/ui/helpers/collapse"
                       data-target="#signature-info-<{$email['id']}>"></i>
                </span>
                <div id="signature-info-<{$email['id']}>" class="collapse">
                    <div class="smtp-auth-info grey darken-2 white-text">
                        <b><{translate id="E-Mail Signatur" textdomain="Core"}>:</b>
                        <div class="email-signature">
                            <{if $email['signature'] != ''}>
                                <{$email['signature']}>
                            <{else}>
                                <{translate id="Sie haben derzeit keine Signatur für diese E-Mail erstellt." textdomain="Core"}>
                            <{/if}>
                        </div>
                    </div>
                </div>
                <span class="clearfix"></span>
            </li>
        </ul>
    </div>
    <div class="card-action">
        <div class="button-toolbar clearfix compact">
            <button class="button tiny primary white-text" 
                    data-module="apps/ui/informations/modal"
                    data-target="#email-modal-form-<{$email['id']}>">
                <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                <span><{translate id="Bearbeiten" textdomain="Core"}></span>
            </button>
            <a href="<{$email['deleteUrl']}>"
                class="button tiny danger left floated white-text"
                title="<{translate id="Entferne %s" textdomain="Core" args=$email['title']}>"
                data-action="edit">
                 <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                 <span><{translate id="Entfernen" textdomain="Core"}></span>
            </a>

            <button class="button tiny right" data-toggle="activator" aria-label="Informationen einblenden">
                <i aria-hidden="true" class="acms-icon acms-icon-arrow-alt-circle-right regular white-text"></i>
            </button>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/mailer/email/EmailForm.tpl" email=$email}>
</div>
