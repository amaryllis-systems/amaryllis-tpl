<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<tr>
    <td data-th="ID">
        <span class="label v1 default"><{$template['id']}></span>
    </td>
    <td data-th="Bezeichnung">
        <{$template['title']}>
    </td>
    <td data-th="Betreff">
        <{$template['subject']}>
    </td>
    <td data-th="Name">
        <{$template['name']}>
    </td>
    <td>
        <a href="<{$template['editUrl']}>"
            class="button tiny primary"
            title="<{translate id="Bearbeite %s" textdomain="Core" args=$template['title']}>"
            data-action="edit">
             <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
             <span class="src-only"><{translate id="Bearbeiten" textdomain="Core"}></span>
         </a>
    </td>
</tr>
