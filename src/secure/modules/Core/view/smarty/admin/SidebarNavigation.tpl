<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="widget v1">
    <div class="widget-heading">
        <div class="widget-title">
            <h3 class="text-center">
                <i aria-hidden="true" class="acms-icon acms-icon-tachometer-alt  widget-icon"></i>
                <{translate id="%s Control Panel" textdomain="Core" args=$moduleName}>
            </h3>
        </div>
    </div>
    <nav class="widget-content full">
        <ul class="compact sidebar v1"
            itemscope=""
            itemtype="http://schema.org/SiteNavigationElement">
            <{foreach $acms_admin_menu.menu as $key => $menuItem}>
                <{if $menuItem['url'] == '#' || !$menuItem['url']}>
                    <li class="nav-separator"></li>
                    <{if isset($menuItem['title']) && $menuItem['title']}>
                        <li class="heading"><{$menuItem['title']}></li>
                    <{/if}>
                <{else}>
                    <li class="acp-menu-item<{if $key == $acms_admin_menu.current}> active<{/if}>">
                        <a title="<{$menuItem.description}>" href="<{$menuItem['url']}>" class="acp-menu-link" itemprop="url">
                            <i aria-hidden="true" class="<{$menuItem.fonticon}>"></i>
                            <span>
                                <{$menuItem.title}>
                            </span>
                        </a>
                    </li>
                <{/if}>
            <{/foreach}>
        </ul>
    </nav>
</div>
