<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container">
    <div class ="row">
        <div class="column xxsmall-12 medium-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <{if isset($acmsContents['acms_module_about']) && $acmsContents['acms_module_about']}>
                <{$acmsContents['acms_module_about']}>
            <{elseif isset($acmsContents['acms_module_help']) && $acmsContents['acms_module_help']}>
                <{$acmsContents['acms_module_help']}>
            <{else}>
                <!-- general admin page title -->
                <{if $acmsContents['acmsTitle']}>
                    <div class="page-title">
                        <h1><{$acmsContents['acmsTitle']}></h1>
                    </div>
                <{/if}>
                <!-- Admin Table -->
                <{if isset($acmsContents['acms_table']) && $acmsContents['acms_table']}>
                    <div><{$acmsContents['acms_table']}></div>
                <{/if}>
                <!-- Admin Form -->
                <{if $acmsContents['acms_form']}>
                    <{include file=$acmsContents['acms_form']['template'] form=$acmsContents['acms_form']}>
                <{/if}>
                <{if isset($acmsContents['acmsInfo']) && $acmsContents['acmsInfo']}>
                    <!-- Admin Info Panel @FIXME - das muss noch ein toggle werden -->
                    <div class="row">
                        <div class="panel v1 panel-info text-center" data-cssfile="media/css/components/panels.css">
                            <{if $acmsContents['acmsInfoTitle']}>
                            <div class="heading">
                                <h2 class="title">
                                    <a data-module="apps/ui/helpers/collapse" data-target="#acms-info" href="<{$acmsUrl}>#acms-info">
                                        <{$acmsContents['acmsInfoTitle']}>
                                        <b class="caret right">&nbsp;</b>
                                    </a>
                                </h2>
                            </div>
                            <{/if}>
                            <div id="acms-info" class="panel-collapse collapse">
                                <div class="content">
                                    <p><{$acmsContents['acmsInfo']}></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <{/if}>
            <{/if}>
        </div> <!-- /.col-md-12 -->
    </div> <!-- /.row -->
</div> <!-- /.container -->

<!-- Admin Footer -->
<{if $acmsContents['acms_admin_footer']}>
    <div class="container full">
        <div id="acms-admin-footer" class="row bg-primary medium-pushdown-30">
            <div class="column xxsmall-12 medium-10 medium-offset-1 medium-pushdown-30">
                <div class="text-center center-block">
	            <p><{$acmsContents['acms_admin_footer']}></p>
	        </div>
	    </div>
	</div>
    </div>
<{/if}>
