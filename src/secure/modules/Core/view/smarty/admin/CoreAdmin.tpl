<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div id="dashboard" data-module="view/admin/dashboard" data-cssfile="media/css/admin/dashboard.min.css">
        <div class="container fixed-1200 fixed-1024">
            <div class="row full">
                <div class="large center aligned icon page-title v7">
                    <i aria-hidden="true" class="circular acms-icon acms-icon-tachometer-alt"></i>
                    <div class="content">
                        <h1 class="text-primary title"><span><{$acmsContents['acmsTitle']}></span></h1>
                        <small class="sub-title text-capitalize">
                            <{translate id="Übersicht" textdomain="Core"}>
                        </small>
                    </div>
                </div>
            </div>
            <div class="row dashboard-usercard">
                <div class="column xxsmall-12 xxsmall-offset-0 medium-10 medium-offset-1 large-6 large-offset-3 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
                    <{include file="acmsfile:Core|admin/helpers/DashboardUserCard.tpl" nocache}>
                </div>
            </div>
            <div class="row push-updown-35 pull-updown-35 dashboard-next-action-wrapper">
                <div class="column xxsmall-12">
                    <div class="primary center aligned page-title v7 push-updown-35">
                        <h2 class="title"><{translate id="Was möchten Sie als nächstes bearbeiten?" textdomain="Core"}></h2>
                    </div>
                </div>
                <{if $acmsContents->bookmarks->get('core')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    System
                                </li>
                                <{foreach $acmsContents->bookmarks->get('core') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('mailer')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    E-Mail Management
                                </li>
                                <{foreach $acmsContents->bookmarks->get('mailer') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('security')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    Sicherheit
                                </li>
                                <{foreach $acmsContents->bookmarks->get('security') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('data')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    System-Bereiche
                                </li>
                                <{foreach $acmsContents->bookmarks->get('data') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('components')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    Komponenten
                                </li>
                                <{foreach $acmsContents->bookmarks->get('components') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('module')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    Apps
                                </li>
                                <{foreach $acmsContents->bookmarks->get('module') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('member')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    Benutzer &amp; Gruppen
                                </li>
                                <{foreach $acmsContents->bookmarks->get('member') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
                <{if $acmsContents->bookmarks->get('profile')}>
                    <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                        <div class="push-5 large-push-10">
                            <ul class="sidebar v1 compact">
                                <li class="heading">
                                    Benutzer-Profile
                                </li>
                                <{foreach $acmsContents->bookmarks->get('profile') as $component}>
                                    <li>
                                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                                            <i aria-hidden="true" class="<{$component['fonticon']}>"></i>
                                            <span><{$component['title']}></span>
                                        </a>
                                    </li>
                                <{/foreach}>
                            </ul>
                        </div>
                    </div>
                <{/if}>
            </div>
        </div>
    </div>
</section>
