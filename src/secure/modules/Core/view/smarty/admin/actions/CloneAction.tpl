<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div class="container">
        <div class ="row">
            <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                <!-- general admin page title -->
                <{if $acmsContents['acmsTitle']}>
                    <div class="page-title">
                        <h1><{$acmsContents['acmsTitle']}></h1>
                    </div>
                <{/if}>
            </div>
            <{if $acmsContents->acmsInfo}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <div class="panel v2 info text-center" data-module="apps/ui/components/panel" data-cssfile="media/css/ui/panels">
                        <{if $acmsContents['acmsInfoTitle']}>
                            <div class="heading">
                                <h2 class="title">
                                    <a href="<{$acmsUrl}>#acms-info" title="Infos">
                                        <{$acmsContents['acmsInfoTitle']}>
                                        <b class="caret right">&nbsp;</b>
                                    </a>
                                </h2>
                            </div>
                        <{/if}>
                        <div id="acms-info" class="panel-collapse collapse">
                            <div class="content">
                                <p><{$acmsContents['acmsInfo']}></p>
                            </div>
                        </div>
                    </div>
                </div>
            <{/if}>
            <!-- Admin Form -->
            <{if $acmsContents['acmsForm']}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <{include file=$acmsContents['acmsForm']['template'] form=$acmsContents['acmsForm']}>
                </div>
            <{elseif $acmsContents['message']}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <{$acmsContents->message}>
                </div>
            <{/if}>
        </div>
    </div>
</section>
