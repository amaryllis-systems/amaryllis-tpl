<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<a class="toggleMenu" href="#">
    <i aria-hidden="true" class="acms-icon acms-icon-bars"></i>
    <span class="src-only"><{translate id="Navigations-Menu" textdomain="Core"}></span>
</a>
<ul class="horizontal-menu compact" data-module="ui/navs/navbar">
    <li class="nav-header">
        <a href="" target="_blank" title="">
            <img class="logo" src="<{$acmsData->acmsUrl}>/logo.png" />
            <span>Amaryllis-CMS</span>
        </a>
    </li>
        <{foreach $nav as $index => $item}>
        <li>
            <a class="acp-menu-item dropdown-toggle"
               href="<{$item['link']}>"
               title="<{$item['description']}>"
               <{*data-module="apps/ui/helpers/collapse"*}>
               <{if $item['link'] == '#' || !$item['link']}>
               data-target="#collapsible-<{$index}>"
               <{/if}>
               >
                <span><{$item['title']}></span>
                <{if $item['link'] == '#' || !$item['link']}>
                <i aria-hidden="true" class="acms-icon acms-icon-chevron-down"></i>
                <{/if}>
            </a>
            <{if $item['items']}>
            <ul class="dropdown-menu" id="collapsible-<{$index}>">
                <{foreach $item['items'] as $c => $child}>
                    <{if !$child['link']}>
                        <li class="separator"></li>
                    <{else}>
                        <li>
                            <a href="<{$child['link']}>" 
                               title="<{if isset($child['description'])}><{$child['description']}><{else}><{$child['title']}><{/if}>"
                               class="acp-menu-item<{if isset($child['subs']) && $child['subs']}> dropdown-toggle<{/if}>"
                               <{if isset($child['subs']) && $child['subs']}>
                                   data-target="#collapsible-child-<{$index}>-<{$c}>"
                               <{/if}>
                               >
                                <{if !isset($child['fonticon']) OR !$child['fonticon']}>
                                    <i aria-hidden="true" class="acms-icon acms-icon-exclamation-circle"></i>
                                <{else}>
                                    <i class="<{$child['fonticon']}>"></i>
                                <{/if}>
                                <span><{$child['title']}></span>
                                <{if isset($child['subs']) && $child['subs']}>
                                    &nbsp;<i aria-hidden="true" class="acms-icon acms-icon-chevron-right"></i>
                                <{/if}>
                            </a>
                            <{if isset($child['subs']) && $child['subs']}>
                                <ul class="submenu sidebar v1" id="collapsible-child-<{$index}>-<{$c}>">
                                    <{foreach $child['subs'] as $s => $sub}>
                                        <li>
                                            <a href="<{$sub['link']}>"
                                               title="<{if isset($sub['description'])}><{$sub['description']}><{else}><{$sub['title']}><{/if}>"
                                               class="acp-menu-item">
                                                <{if !isset($sub['fonticon']) OR !$sub['fonticon']}>
                                                    <i aria-hidden="true" class="acms-icon acms-icon-exclamation-circle"></i>
                                                <{else}>
                                                    <i class="<{$sub['fonticon']}>"></i>
                                                <{/if}>
                                                <span><{$sub['title']}></span>
                                            </a>
                                        </li>
                                    <{/foreach}>
                                </ul>
                            <{/if}>
                        </li>
                    <{/if}>
                <{/foreach}>
            </ul>
            <{/if}>
        </li>
    <{/foreach}>
</ul>
