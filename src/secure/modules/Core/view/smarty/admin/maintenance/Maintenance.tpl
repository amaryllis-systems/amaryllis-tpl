<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="page-section v1 primary">
    <div class="container fixed-1200">
        <div class="row full xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="large center aligned icon page-title v7">
                    <i aria-hidden="true" class="circular white-text acms-icon acms-icon-wrench text-xxlarge"></i>
                    <div class="content">
                        <h1 class="white-text title" itemprop="name"><{$acmsContents->acmsTitle}></h1>
                        <p class="sub-title" itemprop="description">
                            <{$acmsContents->description}>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="white">
    <div class="container fixed-1200" data-module="view/admin/maintenance" data-remote="<{$acmsContents->remoteAction}>">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-8 large-offset-2 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2 tab-container" data-module="ui/navs/tabs">
                <ul class="tabs large tabs-horizontal tabs-stacked tabs-justified" role="tablist">
                    <li role="presentation" class="tab active">
                        <a href="#global-cache" data-target="#global-cache" aria-controls="global-cache" role="tab" data-toggle="tab">
                            <{translate id="Globaler Cache" textdomain="Core"}>
                        </a>
                    </li>
                    <li role="presentation" class="tab">
                        <a href="#selective-cache"
                           data-target="#selective-cache"
                           aria-controls="selective-cache"
                           role="tab"
                           data-toggle="tab">
                            <{translate id="Selektiver Cache" textdomain="Core"}>
                        </a>
                    </li>
                </ul>
                <div id="maintenance" data-listen="cache-trigger" class="tab-content">
                    <div role="tabpanel" class="tab-panel v1 active" id="global-cache">
                        <div class="content-block v5 no-border no-shadow">
                            <{foreach $acmsContents->caches as $name => $options}>
                                <div class="single-cache display-inline pull-10">
                                    <button 
                                        id="<{$name}>"
                                        name="<{$name}>"
                                        type="button"
                                        class="shortcut-button auto-width primary cache-trigger"
                                        data-name="<{$name}>"
                                        data-type="global"
                                        data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                                        data-trigger="hover"
                                        data-placement="top"
                                        data-title="<{$options['description']}>" >
                                        <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                        <div class="title"><{$options['caption']}></div>
                                    </button>
                                </div>
                            <{/foreach}>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-panel" id="selective-cache">
                        <div class="content-block v5 no-border no-shadow">
                            <{foreach $acmsContents->selectives as $name => $options}>
                                <div class="single-cache display-inline pull-10">
                                    <button
                                        id="<{$name}>"
                                        name="<{$name}>"
                                        type="button"
                                        class="shortcut-button auto-width primary cache-trigger"
                                        data-name="<{$name}>"
                                        data-type="selective"
                                        data-module="apps/ui/informations/tooltip" data-toggle="tooltip" 
                                        data-trigger="hover"
                                        data-placement="top"
                                        data-title="<{$options['description']}>"
                                        >
                                        <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                        <div class="title"><{$options['caption']}></div>
                                    </button>
                                </div>
                            <{/foreach}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-8 large-offset-2 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2 tab-container">
                <{include file="acmsfile:Core|admin/helpers/InfoBox.tpl" nocache}>
            </div>
        </div>
        
    </div>
</section>
