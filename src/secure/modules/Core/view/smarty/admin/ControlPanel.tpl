<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div id="dashboard" data-view="view/admin/dashboard">
        <div class="container fixed-1024 fixed-1200 fixed-1600 fixed-1900">
            <div class="row full">
                <div class="page-title">
                    <h1><span><{$acmsContents['acmsTitle']}></span> <small class="text-capitalize">Dashboard</small></h1>
                </div>
            </div>
            <div class="row full">
                <div class="column xxsmall-12 medium-10 medium-offset-1 large-6 large-offset-3 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
                    <{include file="acmsfile:Core|admin/helpers/DashboardUserCard.tpl" nocache}>
                </div>
            </div>
            <{if $acmsContents['postInstall']}>
                <div class="row full">
                    <{foreach $acmsContents['postInstall'] as $message}>
                        <div class="column xxsmall-12">
                            <div class="alert v3 warning" role="alert">
                                <i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle"></i>
                                <div class="alert-title">
                                    <h3><{$message['title']}></h3>
                                </div>
                                <div class="alert-content">
                                    <p><{$message['body']}></p>
                                    <form action="<{$message['url']}>" method="POST" class="clearfix">
                                        <input type="hidden" name="confirm" value="1" />
                                        <input type="hidden" name="id" value="<{$message['id']}>" />
                                        <p class="left floated"><span class="acms-icon acms-icon-calendar"></span> <{$message['pdate']}></p>
                                        <button class="button small success right close-alert" type="submit" onclick="this.form.submit()">
                                            <i aria-hidden="true" class="acms-icon acms-icon-check">&nbsp;</i> <{translate id="Nicht erneut zeigen" textdomain="Core"}>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <{/foreach}>
                    <{if $acmsContents['systemWarnings']}>
                        <div class="panel v2 danger bordered">
                            <i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle"></i>
                            <div class="heading">
                                <h3 class="title"><{$acmsContents['systemWarnings']['heading']}></h3>
                            </div>
                            <div class="content">
                                <ul class="collection v1 v1">
                                    <{foreach $acmsContents['systemWarnings']['warnings'] as $warning}>
                                        <li class="collection-item"><{$warning}></li>
                                    <{/foreach}>
                                </ul>
                            </div>
                        </div>
                    <{/if}>
                </div>
            <{/if}>
            <div class="row full">
                <div class="column xxsmall-12">
                    <{getAdminWidget name="core_info" assign="coreInfo"}>
                    <{if $coreInfo}>
                        <{$coreInfo}>
                    <{/if}>
                </div>
            </div>
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{getAdminWidget name="core_userinfo" assign="coreUserInfo"}>
                    <{if $coreUserInfo}>
                        <{$coreUserInfo}>
                    <{/if}>
                </div>
            </div>
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{getAdminWidget name="core_usercountryinfo" assign="coreUserCountryInfo"}>
                    <{if $coreUserCountryInfo}>
                        <{$coreUserCountryInfo}>
                    <{/if}>
                </div>
            </div>
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <div class="panel v2 primary collapsible bordered" data-module="apps/ui/components/panel">
                        <div class="heading">
                            <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt "></i>
                            <h3 class="title text-center">
                                <{translate id="Inhalte Bereitstellen" textdomain="Core"}>
                            </h3>
                            <i aria-hidden="true" class="acms-icon collapsible-icon"></i>
                        </div>
                        <div id="widgets-content-submit" class="widget-panel">
                            <div class="content">
                                <{loadBlock name="core_generic_submit_menu" title="" template="acmsfile:Core|admin/helpers/DashboardGenericMenu.tpl"}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container fixed-1024 fixed-1200 fixed-1600 fixed-1900 pull-updown-30">
            <div class="row full xxsmall-push-updown-30 bordered">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h2 class="text-center">Logins &amp; Registrierungen</h2>
                    </div>
                </div>
                <div class="column xxsmall-12 stat-widgets">
                    <div class="horizontal info dashboard-stat-list v2 pull-10">
                        <!-- Logins -->
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countLogins assign="loginsToday" options=['type' => 'today']}>
                                <{$loginsToday}>
                            </div>
                            <div class="title">Logins heute</div>
                        </div>
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countLogins assign="loginsThisMonth" options=['type' => 'this-month']}>
                                <{$loginsThisMonth}>
                            </div>
                            <div class="title">Logins diesen Monat</div>
                        </div>
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countLogins assign="loginsThisYear" options=['type' => 'this-year']}>
                                <{$loginsThisYear}>
                            </div>
                            <div class="title">Logins dieses Jahr</div>
                        </div>
                        <!-- registrations -->
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countRegistrations assign="registrationsToday" options=['type' => 'today']}>
                                <{$registrationsToday}>
                            </div>
                            <div class="title">Registrierungen heute</div>
                        </div>
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countRegistrations assign="registrationsThisMonth" options=['type' => 'this-month']}>
                                <{$registrationsThisMonth}>
                            </div>
                            <div class="title">Registrierungen diesen Monat</div>
                        </div>
                        <div class="dashboard-statistic">
                            <div class="counter">
                                <{countRegistrations assign="registrationsThisYear" options=['type' => 'this-year']}>
                                <{$registrationsThisYear}>
                            </div>
                            <div class="title">Registrierungen dieses Jahr</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container fixed-1024 fixed-1200 fixed-1600 fixed-1900">
            <div class="row full">
                <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6 bookmark-widgets">
                    <div class="panel v2 primary collapsible bordered large-up-push-right-15" data-module="apps/ui/components/panel">
                        <div class="heading text-center">
                            <i aria-hidden="true" class="acms-icon acms-icon-bookmark"></i>
                            <h2 class="title">
                                <{translate id="Schnellzugänge" textdomain="Core"}>
                            </h2>
                            <i aria-hidden="true" class="acms-icon collapsible-icon"></i>
                        </div>
                        <div id="widgets-bookmarks" class="content">
                            <div class="button-toolbar">
                                <{foreach $acmsContents['mainBookmarks'] as $bookmark}>
                                    <a href="<{$bookmark['url']}>"
                                       class="royal auto-width shortcut-button <{$bookmark['class']}>" role="button" title="<{$bookmark['title']}>">
                                        <i class="<{$bookmark['fonticon']}>"></i>
                                        <span><{$bookmark['title']}></span>
                                    </a>
                                <{/foreach}>
                            </div>
                            <div class="row full">
                                <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                                    <div class="button-toolbar">
                                        <{foreach $acmsContents['systemBookmarks'] as $bookmark}>
                                                <a href="<{$bookmark['url']}>"
                                                   class="warning auto-width shortcut-button <{$bookmark['class']}>" role="button" title="<{$bookmark['title']}>">
                                                    <i class="<{$bookmark['fonticon']}>"></i>
                                                    <span><{$bookmark['title']}></span>
                                                </a>
                                        <{/foreach}>
                                    </div>
                                </div>
                                <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                                    <div class="button-toolbar">
                                        <{foreach $acmsContents['componentBookmarks'] as $bookmark}>
                                            
                                                <a href="<{$bookmark['url']}>"
                                                   class="primary auto-width shortcut-button <{$bookmark['class']}>"
                                                   role="button" title="<{$bookmark['title']}>">
                                                    <i class="<{$bookmark['fonticon']}>"></i>
                                                    <span><{$bookmark['title']}></span>
                                                </a>
                                            
                                        <{/foreach}>
                                    </div>
                                </div>
                            </div>
                            <a href="<{$acmsData->acmsUrl}>" class="bottom plugged stacked xxlarge success vertical animation icon button" role="button">
                                <i aria-hidden="true" class="acms-icon acms-icon-globe"></i>
                                <span class="first transparent content">Website</span>
                                <span class="second transparent content">Die Startseite aufrufen</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6 app-widgets">
                    <{getAdminWidget name="core_apps" assign="coreApps" options=['admin' => true]}>
                    <{if $coreApps}>
                        <{$coreApps}>
                    <{/if}>
                </div>
            </div>
            <{getServerStat assign="ServerStats"}>
            <{if $ServerStats['cpuUsage'] || $ServerStats['freeDisk']}>
                <div class="row full xxsmall-push-updown-30">
                    <div class="column xxsmall-12 medium-6 large-3 xlarge-3 xxlarge-3">
                        <div class="medium-up-push-10">
                            <div class="panel v2 primary collapsible bordered" data-module="apps/ui/components/panel">
                                <div class="heading">
                                    <i aria-hidden="true" class="acms-icon acms-icon-hdd"></i> 
                                    <h2 class="title">
                                        <{translate id="Speicher-Auslastung"}>
                                    </h2>
                                    <i aria-hidden="true" class="acms-icon collapsible-icon"></i> 
                                </div>
                                <div class="content pull-updown-30" aria-hidden="false" data-cssfile="media/css/widgets/charts/pie-chart.min.css">
                                    <div class="percentage-90 display-block center-block">
                                        <div class="clearfix">
                                            <div class="pie-chart v1 c100 p<{$ServerStats['diskUsage']}>">
                                                <span><{$ServerStats['diskUsage']}>%</span>
                                                <div class="slice">
                                                    <div class="bar"></div>
                                                    <div class="fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <small>Gesamt: <{$ServerStats['totalDisk']}></small> <small>Frei: <{$ServerStats['freeDisk']}></small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column xxsmall-12 medium-6 large-3 xlarge-3 xxlarge-3">
                        <div class="medium-up-push-10">
                            <div class="panel v2 primary collapsible bordered" data-module="apps/ui/components/panel">
                                <div class="heading">
                                    <i aria-hidden="true" class="acms-icon acms-icon-server"></i> 
                                    <h2 class="title">
                                        <{translate id="Server-Auslastung"}>
                                        
                                    </h2>
                                    <i aria-hidden="true" class="acms-icon collapsible-icon"></i> 
                                </div>
                                <div class="content" aria-hidden="false" data-cssfile="media/css/widgets/progressbar.min.css">
                                    <div class="percentage-90 display-block center-block">
                                        <div class="clearfix">
                                            <h3>CPU-Auslastung <small><{$ServerStats['cpuNumbers']}> CPUs</small></h3>
                                            <div class="progress xlarge <{if $ServerStats['cpuUsage'] >= 70}>danger<{elseif $ServerStats['cpuUsage'] < 70 && $ServerStats['cpuUsage'] > 50}>warning<{else}>success<{/if}>">
                                                <div class="progress-meter" style="width: <{$ServerStats['cpuUsage']}>%">
                                                    <span class="progress-status"><{$ServerStats['cpuUsage']}>%</span>
                                                </div>
                                            </div>
                                            <h3>RAM-Auslastung</h3>
                                            <span class="text-muted"><{$ServerStats['ramUsage'][1]}>/<{$ServerStats['ramUsage'][0]}> (<{$ServerStats['ramUsage'][2]}>%)</span>
                                            <div class="progress xlarge <{if $ServerStats['ramUsage'][2] >= 70}>danger<{elseif $ServerStats['ramUsage'][2] < 70 && $ServerStats['ramUsage'][2] > 50}>warning<{else}>success<{/if}>">
                                                <div class="progress-meter" style="width: <{$ServerStats['ramUsage'][2]}>%">
                                                    <span class="progress-status hidden"><{$ServerStats['ramUsage'][2]}>%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <{/if}>
        </div>
        <{if $acmsContents['customWidgets']}>
            <div class="container fixed-1024 fixed-1200 fixed-1600 fixed-1900">
                <div class="row full">
                    <{foreach $acmsContents['customWidgets'] as $widget}>
                        <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                            <div class="panel v1 collapsible" data-module="apps/ui/components/panel">
                                <div class="heading text-center">
                                    <h2 class="title">
                                        <{if $widget['fonticon']}>
                                            <span class="<{$widget['fonticon']}>}"></span>&nbsp;
                                        <{/if}>
                                        <{$widget.title}>
                                    </h2>
                                </div>
                                <div id="widgets-<{$widget['name']}>" class="panel-collapse collapse in">
                                    <div class="content">
                                        <{$widget['content']}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <{/foreach}>
                </div>
            </div>
        <{/if}>
    </div>
</section>
