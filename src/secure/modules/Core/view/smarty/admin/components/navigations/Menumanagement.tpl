<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="primary arrow-bottom arrow-box page-section v1">
    <div class="container fixed-1200">
        <div class="full row">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <div class="center aligned content">
                        <h1 class="white-text title" itemprop="name"><{$acmsContents->acmsTitle}></h1>
                        <p class="white-text sub-title"><{$acmsContents->description}></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container fixed-1600">
    <div class ="row full">
        <div class="column xxsmall-12">
            <div id="menu-select-form" class="column xxsmall-12">
                <{include file=$acmsContents['select_form']['template'] form=$acmsContents['select_form'] nocache }>
            </div>
        </div>
    </div>
    
    <div class="row full" 
         data-module="apps/admin/components/navigations/menu-admin" 
         data-id="<{$acmsContents['currentMenu']['id']}>"
         data-remote="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'components', 'item' => 'navigations', 'action' => 'action']}>" 
         data-name="<{$acmsContents['currentMenu']['name']}>"
         data-id="<{$acmsContents['currentMenu']['id']}>"
         data-title="<{$acmsContents['currentMenu']['title']}>"
         data-locale="<{$acmsContents['currentMenu']['locale']}>"
         data-groups="<{$acmsContents['jgroups']}>">
        <div class="column xxsmall-12">
            <div id="menumanagement" 
                class="boxed sortable-collection collection v1 has-image"
                data-role="navigation-menu-admin"
                data-id="<{$acmsContents['currentMenu']['id']}>"
                data-name="<{$acmsContents['currentMenu']['name']}>"
                data-locale="<{$acmsContents['currentMenu']['locale']}>">
                <{if $acmsContents->menu['items']}>
                    <{function name=navigationsManagementList}>
                        <{if $items}>
                            <{foreach $items as $item}>
                                <div class="collection-item menu-item"
                                    data-role="menu-item"
                                    data-id="<{$item['id']}>" 
                                    data-weight="<{$item['weight']}>"
                                    data-access="<{$item['jgroups']}>"
                                    data-root="<{$item['root']}>" 
                                    data-parentid="<{$item['parentid']}>"
                                    data-description="<{$item['description']}>"
                                    data-title="<{$item['title']}>"
                                    data-url="<{$item['url']}>"
                                    data-target="<{$item['target']}>">
                                    <i aria-hidden="true" class="bg-transparent text-dark item-image acms-icon acms-icon-ellipsis-v"></i>
                                    <div class="title">
                                        <{$item['title']}>
                                    </div>
                                    <div class="text">
                                        <span class="xxsmall label v1"><{$item['url']}></span>
                                    </div>
                                    <div class="secondary-content">
                                        <button class="small info icon button" data-trigger="edit-item"><i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i></button>
                                        <button class="small secondary icon button" data-trigger="unlink-item"><i aria-hidden="true" class="acms-icon acms-icon-unlink"></i></button>
                                        <button class="small danger icon button" data-trigger="delete-item"><i aria-hidden="true" class="acms-icon acms-icon-trash"></i></button>
                                    </div>
                                    <div class="sub-list has-image boxed collection v1 navigation-menu">
                                        <{if $item['subs']}>
                                            <{call name=navigationsManagementList items=$item['subs']}>
                                        <{/if}>
                                    </div>
                                </div>
                            <{/foreach}>
                        <{/if}>
                    <{/function}>
                    <{call name=navigationsManagementList items=$acmsContents->menu['items']}>
                <{/if}>
            </div>
        </div>
        <div class="column xxsmall-12">
            <div class="page-title v7">
                <div class="content">
                    <h3 class="title">
                        <{translate id="Verfügbare Menu-Items" textdomain="Core"}>
                    </h3>
                    <div class="sub-title">
                        <{translate id="Die Menu-Items existieren und sind nicht zugewiesen" textdomain="Core"}>
                    </div>
                </div>
            </div>
            <div class="boxed horizontal has-image collection v1" data-role="available-items">
                <{if $acmsContents->availableItems}>
                    <{foreach $acmsContents->availableItems as $item}>
                        <div class="collection-item menu-item"
                             data-role="menu-item"
                             data-id="<{$item['id']}>" 
                             data-weight="<{$item['weight']}>" 
                             data-access="<{$item['jgroups']}>"
                             data-root="<{$item['root']}>" 
                             data-parentid="<{$item['parentid']}>"
                             data-description="<{$item['description']}>"
                             data-title="<{$item['title']}>"
                             data-url="<{$item['url']}>"
                             data-target="<{$item['target']}>">

                            <i aria-hidden="true" class="bg-transparent text-dark item-image acms-icon acms-icon-ellipsis-v"></i>
                            <div class="title">
                                <{$item['title']}>
                            </div>
                            <div class="text">
                                <span class="xxsmall label v1"><{$item['url']}></span>
                            </div>
                            <div class="secondary-content" data-role="item-actions">
                                <button class="small info icon button" data-trigger="edit-item"><i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i></button>
                                <button class="small secondary icon button" data-trigger="unlink-item"><i aria-hidden="true" class="acms-icon acms-icon-unlink"></i></button>
                                <button class="small danger icon button" data-trigger="delete-item"><i aria-hidden="true" class="acms-icon acms-icon-trash"></i></button>
                            </div>
                            <div class="sub-list has-image boxed collection v1 navigation-menu">
                            </div>
                        </div>
                    <{/foreach}>
                <{/if}>
            </div>
        </div>
        <div class="column xxsmall-12 medium-4">
            <div class="button-toolbar clearfix">
                <button class="primary icon button"
                        role="button"
                        data-trigger="add-item"
                        aria-label="<{translate id="Neuer Unterpunkt" textdomain="Core"}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                </button>
            </div>
        </div>
        <div class="column xxsmall-12 medium-8">
            <div class="button-toolbar compact clearfix text-right">
                <button type="button" 
                        data-trigger="add-menu"
                        class="primary icon button"
                        aria-label="<{translate id='Neues Navigations-Menu' textdomain='Core'}>">
                    <span class="acms-icon acms-icon-plus"></span>
                </button>
                <button type="button"
                        data-trigger="edit-menu"
                        class="info icon button"
                        aria-label="<{translate id='Menu bearbeiten' textdomain='Core'}>">
                    <span class="acms-icon acms-icon-pencil-alt"></span>
                </button>
                <button type="button"
                        class="danger icon button"
                        aria-label="<{translate id='Menu löschen' textdomain='Core'}>"
                        data-trigger="delete-menu">
                    <span class="acms-icon acms-icon-trash"></span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="container fixed-1600">
    <div class="row full">
        <div class="column xxsmall-12 medium-6 large-4">&nbsp;</div>
    </div>
    <div class="row full xxsmall-push-updown-30">
        <div class="column xxsmall-12 large-8 large-offset-2 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
            <div class="alert v1 info hoverable" id="menu-code-example" data-module="ui/components/alert">
                <div class="alert-title">
                    <h3><i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i> <{translate id="Navigation im Thema einbinden" textdomain="Core"}></h3>
                </div>
                <p>
                    <{translate
                        id="Um das Menu aus dem Theme heraus zu nutzen, kopieren Sie den folgenden Code an die Stelle, an der das Menu erscheinen soll:"
                        textdomain="Core"
                    }>
                </p>
                <p class="push-updown-20">
                    <code class="pull-15">
                        &lt;{menu name="<{$acmsContents['currentMenu']['name']}>" template="acmsfile:Core|pfad/zu/Template.tpl"}&gt;
                    </code>
                </p>
            </div>
        </div>
    </div>
</div>
