<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div class="container fixed-1200">
        <div class="row xxsmall-pushup-15">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-lock text-muted text-xxlarge"></i>
                    <div class="content">
                        <h1 class="title text-primary">
                            <span itemprop="name">
                                <{$acmsContents->acmsTitle}>
                            </span>
                        </h1>
                        <p class="sub-title" itemprop="description">
                            <{translate id="Hier finden Sie nicht genehmigte Tags. Wählen Sie die Tags zur Genehmigung aus."}>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xxsmall-pushupdown-25">
            <div class="column xxsmall-12">
                <div class="cards push-20">
                    <{if $acmsContents->tags}>
                        <{foreach $acmsContents->tags as $tag}>
                            <div class="card v2" data-module="apps/apf/approval/approval" data-remote="<{$acmsContents->remoteUrl}>" data-id="<{$tag['id']}>">
                                <div class="card-content">
                                    <div class="right floated small avatar">
                                        <i class="acms-icon acms-icon-exclamation-circle text-danger"></i>
                                    </div>
                                    <a class="card-title text-danger" href="<{$tag['itemUrl']}>" title="Beitrag aufrufen">
                                        <{$tag['title']}>
                                    </a>
                                    <div class="card-meta">
                                        <{translate id="Genehmigung ausstehend" textdomain="Core"}>
                                    </div>
                                    <div class="description">
                                        <{translate id="%s hat diesen Tag eingereicht und wartet auf deine Genehmigung zur Veröffentlichung." textdomain="Core" args=[$tag['tagsSubmitter']['username']]}>
                                    </div>
                                </div>
                                <div class="extra card-content">
                                    <a class="stacked primary icon border-button button" href="<{$tag['itemUrl']}>" title="Beitrag in neuem Fenster aufrufen" target="_blank">
                                        <span>aufrufen</span>
                                        <i class="icon-right acms-icon acms-icon-external-link-alt"></i>
                                    </a>
                                </div>
                                <div class="extra card-content">
                                    <div class="two buttons">
                                        <button class="button small success icon-button" data-trigger="approve">
                                            <i class="acms-icon acms-icon-check"></i>
                                            <span class="content"><{translate id="genehmigen" textdomain="Core"}></span>
                                        </button>
                                        <button class="button small danger icon-button border-button" data-trigger="decline" data-title="Tag Ablehnen">
                                            <i class="acms-icon acms-icon-ban"></i>
                                            <span class="content"><{translate id="ablehnen" textdomain="Core"}></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <{/foreach}>
                    <{else}>
                        <div class="text-center" data-cssfile="media/css/ui/update-bars">
                            <div class="update-bar v1 success">
                                <div class="split">
                                    <i aria-hidden="true" class="acms-icon acms-icon-check-circle"></i>
                                </div>
                                <div class="text">
                                    <{translate id="Alles erledigt" textdomain="Core"}>
                                </div>
                            </div>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
    </div>
</section>
