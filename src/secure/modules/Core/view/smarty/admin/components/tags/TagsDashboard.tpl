<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container full">
    <div class="row xxsmall-pushup-15">
        <div class="column xxsmall-12">
            <div class="page-title">
                <h1><{$acmsContents->acmsTitle}> <small>Dashboard</small></h1>
            </div>
        </div>
    </div>
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12">
            <div class="display-block percentage-80 center-block push-20 text-primary text-center">
                <i aria-hidden="true" class="acms-icon acms-icon-chart-pie acms-icon-4x"></i><br>
                <h2 class="text-primary">
                    <small>
                        <{translate id="%s Tags insgesamt" 
                                    textdomain="Core" 
                                    args=[$acmsContents->statistics['totalTags']]
                        }>
                    </small>
                </h2>
            </div>
        </div>
        <div class="column xxsmall-12">
            <div class="display-block percentage-80 center-block push-20 text-center">
                <h3 class="text-primary"><{translate id="Online-Status" textdomain="Core"}></h3>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-4 large-3 xlarge-3 xxlarge-3">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-success">
                        <i aria-hidden="true" class="acms-icon acms-icon-check-circle regular acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind online" textdomain="Core" args=$acmsContents->statistics['publishedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-3 large-3 xlarge-3 xxlarge-3">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center <{if $acmsContents->statistics['unpublishedTags']}>text-warning<{else}>text-muted<{/if}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-eye-slash acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind offline" textdomain="Core" args=$acmsContents->statistics['unpublishedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-3 large-3 xlarge-3 xxlarge-3">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-info">
                        <i aria-hidden="true" class="acms-icon acms-icon-archive acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind archiviert" textdomain="Core" args=$acmsContents->statistics['archievedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-3 large-3 xlarge-3 xxlarge-3">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-danger">
                        <i aria-hidden="true" class="acms-icon <{if $acmsContents->statistics['unpublishedTags']}>acms-icon-trash<{else}>acms-icon-trash-alt regular<{/if}> acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind gelöscht" textdomain="Core" args=$acmsContents->statistics['trashedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12">
            <div class="display-block percentage-80 center-block push-20 text-center">
                <h3 class="text-primary"><{translate id="Genehmigungs-Status" textdomain="Core"}></h3>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-success">
                        <i aria-hidden="true" class="acms-icon acms-icon-thumbs-up acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind genehmigt" textdomain="Core" args=$acmsContents->statistics['approvedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-info">
                        <i aria-hidden="true" class="acms-icon acms-icon-thumbs-up regular acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags warten auf Genehmigung" textdomain="Core" args=$acmsContents->statistics['pendingTags']}></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
            <div class="display-block percentage-80 center-block push-20">
                <div class="card v1 card-panel v1 hoverable">
                    <div class="display-block text-center text-danger">
                        <i aria-hidden="true" class="acms-icon <{if $acmsContents->statistics['declinedTags']}>acms-icon-thumbs-down<{else}>acms-icon-thumbs-down regular<{/if}> acms-icon-4x"></i><br>
                        <span><{translate id="%s Tags sind nicht genehmigt" textdomain="Core" args=$acmsContents->statistics['declinedTags']}></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <{*
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12">
            <h2>
                <{translate id="Was möchten Sie machen?" textdomain="Core"}>
            </h2>
        </div>
        <{foreach $acmsContents->dashboardActions as $component}>
            <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                <div class="display-block percentage-80 center-block push-20">
                    <div class="card v1 card-panel v1 <{$component['class']}> hoverable">
                        <a href="<{$component['url']}>" title="<{$component['title']}>">
                            <div class="display-block text-center">
                                <i class="<{$component['fonticon']}> acms-icon-4x"></i><br>
                                <small><{$component['title']}></small>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        <{/foreach}>
    </div>
    *}>
</div>
