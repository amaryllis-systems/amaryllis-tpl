<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope='' itemtype="http://schema.org/WebPage">
    <div id="dashboard" class="dashboard container full" data-module="view/admin/dashboard">
        <div class="row xxsmall-pushup-15">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <h1 class="title"><{$acmsContents->acmsTitle}></h1>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12<{if $acmsContents->jsonModuleStatistics}> medium-6 large-6 xlarge-6 xxlarge-6<{/if}>">
                <div class="display-block percentage-90 center-block push-20 text-primary text-center">
                    <i aria-hidden="true" class="acms-icon acms-icon-chart-pie acms-icon-4x"></i><br>
                    <h2 class="text-primary">
                        <small>
                            <{translate id="%s Bewertungen insgesamt" 
                                    textdomain="Core" 
                                    args=$acmsContents->statistics['totalRatings']
                            }>
                        </small>
                    </h2>
                </div>
            </div>
            <{*
            <{if $acmsContents->jsonModuleStatistics}>
                <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                    <div class="display-block percentage-90 svg-pie-chart" 
                         data-module="ui/widgets/charts/pie-chart" 
                         data-definition="<{$acmsContents->jsonModuleStatistics}>"
                         data-is-pie="true"
                         data-background ="#FaFaFa"
                         data-size="320"
                         >
                    </div>
                </div>
            <{/if}>
            *}>
        </div>
        <{if $acmsContents->autoren}>
            <div class="row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h2><{translate id="Top-Bewerter" textdomain="Core"}> <small><{translate id="Gesamt" textdomain="Core"}>: <{$acmsContents->gesamtAutoren}></small></h2>
                    </div>
                    <div class="feed-collection v1 two-small-only four-large-up-only">
                        <{foreach $acmsContents->autoren as $autor}>
                            <div class="card v1 hoverable">
                                <div class="card-content">
                                    <div class="card-title">
                                        <a class=""
                                           title="<{$autor['autor']['username']}>" 
                                           href="<{$autor['autor']['itemUrl']}>">
                                            <span><{$autor['autor']['first_name']}> <{$autor['autor']['last_name']}></span>
                                        </a>
                                        <span class="badge v1 primary right">
                                            <{$autor['contributions']}>
                                        </span>
                                    </div>
                                    <div class="image-container text-center display-block">
                                        <a class=""
                                           title="<{$autor['autor']['username']}>" 
                                           href="<{$autor['autor']['itemUrl']}>">
                                            <img class="circle" 
                                                 src="<{$autor['autor']['avatar']}>" 
                                                 title="<{$autor['autor']['username']}>" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        <{/if}>
    </div>
</section>
