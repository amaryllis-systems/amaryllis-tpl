<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>


<div class="container full">
    <div class="row xxsmall-pushup-15">
        <div class="column xxsmall-12">
            <div class="page-title">
                <h1>
                    <i aria-hidden="true" class="acms-icon acms-icon-folder text-muted"></i>
                    <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                </h1>
                <p class="lead">
                    <{translate id="Hier finden Sie Ihren vollständigen Kategorien-Baum." textdomain="Core"}>
                </p>
            </div>
        </div>
    </div>
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12 xxsmall-offset-0 medium-10 medium-offset-1 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <div class="display-block center-block percentage-80 category-categories nestable">
                <div class="sortable-collection has-image boxed collection v1" data-parent="0"
                    itemscope itemtype="http://schema.org/ItemList"
                    role="list"
                    data-module="apps/admin/components/category/category-list"
                    data-groups="<{$acmsContents->jgroups}>"
                    data-remote="<{$acmsContents->remoteAction}>">
                    <{if $acmsContents->categories}>
                    <{foreach $acmsContents->categories as $page}>
                        <{include file="acmsfile:Core|admin/components/category/CategoryItem.tpl" category=$page}>
                    <{/foreach}>
                    <{/if}>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fixed-action-button" data-nav="floating-button">
    <a role="button" class="large primary floating button" href="<{$acmsContents->addAction}>" title="<{translate id="Neu" textdomain="Core"}>" data-toggle="floating-menu">
        <i aria-hidden="true" class="acms-icon acms-icon-plus white-text"></i>
    </a>
</div>
