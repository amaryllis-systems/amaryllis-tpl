<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container full">
    <div class="row xxsmall-pushup-15">
        <div class="column xxsmall-12">
            <div class="large page-title v7">
                <i aria-hidden="true" class="acms-icon acms-icon-tags text-muted"></i>
                <div class="content">
                    <h1 class="title">
                        <span itemprop="name">
                            <{$acmsContents->acmsTitle}>
                        </span>
                    </h1>
                    <div class="sub-title" itemprop="description">
                        <{translate id="Tags Admin Übersicht."}>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12">
            <div class="push-20">
                <div class="large page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-tags text-muted"></i>
                    <div class="content">
                        <h2 class="title">
                            Nicht verwendete Tags
                        </h2>
                        <div class="sub-title" itemprop="description">
                            <{translate id="Diese Tags sind derzeit mit keinem Content verlinkt."}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12">
            <div class="push-20 horizontal boxed has-image collection v1">
                <{if $acmsContents->totalUnlinkedTags}>
                    <{foreach $acmsContents->unlinkedTags as $tag}>
                        <div class="collection-item">
                            <i aria-hidden="true" class="bg-transparent item-image acms-icon acms-icon-tags text-dark"></i>
                            <div class="title">
                                <{$tag['title']}>
                            </div>
                        </div>
                    <{/foreach}>
                <{else}>
                    <div class="collection-item">
                        <i aria-hidden="true" class="bg-success text-white item-image acms-icon acms-icon-check-circle regular"></i>
                        <div class="text-success title">
                            Perfekt!
                        </div>
                        <div class="text-muted text">
                            Alle Tags sind auch verlinkt!
                        </div>
                    </div>
                <{/if}>
            </div>
        </div>
    </div>
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12">
            <div class="push-20">
                <div class="large page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-tags text-muted"></i>
                    <div class="content">
                        <h2 class="title">
                            Vorhandene Tags
                        </h2>
                        <div class="sub-title" itemprop="description">
                            <{translate id="Diese Tags sind derzeit vorhanden."}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12">
            <div class="push-20 horizontal boxed has-image collection v1">
                <{if $acmsContents->totalUnlinkedTags}>
                    <{foreach $acmsContents->unlinkedTags as $tag}>
                        <div class="collection-item">
                            <i aria-hidden="true" class="bg-transparent item-image acms-icon acms-icon-tags text-dark"></i>
                            <div class="title">
                                <{$tag['title']}>
                            </div>
                        </div>
                    <{/foreach}>
                <{else}>
                    <div class="collection-item">
                        <i aria-hidden="true" class="bg-success text-white item-image acms-icon acms-icon-check-circle regular"></i>
                        <div class="text-success title">
                            Perfekt!
                        </div>
                        <div class="text-muted text">
                            Alle Tags sind auch verlinkt!
                        </div>
                    </div>
                <{/if}>
            </div>
        </div>
    </div>
</div>
