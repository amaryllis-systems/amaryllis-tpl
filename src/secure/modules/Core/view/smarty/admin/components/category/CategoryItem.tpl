<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="collection-item" 
     data-weight="<{$category.weight}>" 
     data-id="<{$category.id}>"
     data-root="<{$category['root']}>" 
     data-parentid="<{$category['parentid']}>" 
     draggable="true">
    <i aria-hidden="true" class="bg-transparent text-dark item-image acms-icon acms-icon-ellipsis-v"></i>
    <div class="title">
        <{$category['title']}>
    </div>
    <div class="text">
        <{if $category['status'] == 1}>
            <span class="xxsmall label v2 danger"><{translate id="Gelöscht" textdomain="Core"}></span>
        <{elseif $category['status'] == 2}>
            <span class="xxsmall label v2 warning"><{translate id="Offline" textdomain="Core"}></span>
        <{elseif $category['status'] == 3}>
            <span class="xxsmall label v2 success"><{translate id="Online" textdomain="Core"}></span>
        <{elseif $category['status'] == 4}>
            <span class="xxsmall label v2 secondary"><{translate id="Archiviert" textdomain="Core"}></span>
        <{/if}>
        
        <{if $category['approved'] == 1}>
            <span class="xxsmall label v2 danger"><{translate id="Abgelehnt" textdomain="Core"}></span>
        <{elseif $category['approved'] == 2}>
            <span class="xxsmall label v2 warning"><{translate id="Abwartend" textdomain="Core"}></span>
        <{elseif $category['status'] == 3}>
            <span class="xxsmall label v2 success"><{translate id="Genehmigt" textdomain="Core"}></span>
        <{/if}>
    </div>
    <div class="secondary-content">
        <a class="small primary icon button" href="<{$category['editUrl']}>" title="<{translate id="Bearbeiten" textdomain="Core"}>">
            <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
        </a>
        <a class="small secondary icon button" href="<{$category['cloneUrl']}>" title="<{translate id="Klonen" textdomain="Core"}>">
            <i aria-hidden="true" class="acms-icon acms-icon-clone"></i>
        </a>
        <{if $category['userCanDelete']}>
        <a class="small danger icon button" href="<{$category['deleteUrl']}>" title="<{translate id="Löschen" textdomain="Core"}>">
            <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
        </a>
        <{/if}>
    </div>
    <div class="sub-list has-image boxed collection v1" data-parent="<{$category.id}>">
        <{if $category.subs}>
            <{foreach $category.subs as $p}>
                <{include file="acmsfile:Core|admin/components/category/CategoryItem.tpl" category=$p}>
            <{/foreach}>
        <{/if}>
    </div>
</div>
