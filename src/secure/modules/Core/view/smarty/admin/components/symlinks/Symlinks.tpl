<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="dashboard" data-view="view/admin/dashboard">
    <div class="container fixed-1200 fixed-1024">
        <div class="row">
            <div class="page-title">
                <h1><span><{$acmsContents['acmsTitle']}></span> <small class="text-capitalize"><{translate id="Dashboard" textdomain="Core"}></small></h1>
            </div>
        </div>
        
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="push-10">
                    <table class="table striped hover compact">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Bezeichnung
                                </th>
                                <th>
                                    URL
                                </th>
                                <th>
                                    Modul
                                </th>
                            </tr>
                        </thead>
                        <tfoot class="bg-default">
                            <tr>
                                <td colspan="4">Insgesamt: <{$acmsContents->total}></td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <{foreach $acmsContents->symlinks as $symlink}>
                                <tr>
                                    <td><{$symlink['id']}></td>
                                    <td><{$symlink['title']}></td>
                                    <td><{$symlink['url']}></td>
                                    <td><{$symlink['module']}></td>
                                </tr>
                            <{/foreach}>
                        </tbody>
                    </table>
                    <{if $acmsContents->acmsPagination}>
                        <div class="pagination-container v1">
                            <{$acmsContents->acmsPagination['pagination']}>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
    </div>
</div>
