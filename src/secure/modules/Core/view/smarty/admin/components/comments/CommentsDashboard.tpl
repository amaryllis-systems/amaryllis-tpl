<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope='' itemtype="http://schema.org/WebPage">
    <div id="dashboard" class="dashboard container full" data-module="view/admin/dashboard">
        <div class="row xxsmall-pushup-15">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1><{$acmsContents->acmsTitle}></h1>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12<{if $acmsContents->jsonModuleStatistics}> medium-6 large-6 xlarge-6 xxlarge-6<{/if}>">
                <div class="display-block percentage-90 center-block push-20 text-primary text-center">
                    <i aria-hidden="true" class="acms-icon acms-icon-chart-pie acms-icon-4x"></i><br>
                    <h2 class="text-primary">
                        <small>
                            <{translate id="%s Kommentare insgesamt" 
                                    textdomain="Core" 
                                    args=[$acmsContents->statistics['totalComments']]
                            }>
                        </small>
                    </h2>
                </div>
            </div>
            
            <{if $acmsContents->jsonModuleStatistics}>
                <div class="hidden column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                    <div class="display-block percentage-90 svg-pie-chart" 
                         data-module="ui/widgets/charts/pie-chart" 
                         data-definition="<{$acmsContents->jsonModuleStatistics}>"
                         data-is-pie="true"
                         data-background="#FaFaFa"
                         data-size="320"
                         >
                    </div>
                </div>
            <{/if}>
            <div class="column xxsmall-12">
                <div class="display-block percentage-80 center-block push-20 text-center">
                    <h3 class="text-primary"><{translate id="Genehmigungs-Status" textdomain="Core"}></h3>
                </div>
            </div>
            <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
                <div class="display-block percentage-80 center-block push-20">
                    <div class="card v1 card-panel v1 hoverable">
                        <div class="display-block text-center text-success">
                            <i aria-hidden="true" class="acms-icon acms-icon-thumbs-up acms-icon-4x"></i><br>
                            <span><{translate id="%s Kommentare sind genehmigt" textdomain="Core" args=$acmsContents->statistics['approvedComments']}></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
                <div class="display-block percentage-80 center-block push-20">
                    <div class="card v1 card-panel v1 hoverable">
                        <div class="display-block text-center text-info">
                            <i aria-hidden="true" class="acms-icon acms-icon-thumbs-up regular acms-icon-4x"></i><br>
                            <span><{translate id="%s Kommentare warten auf Genehmigung" textdomain="Core" args=$acmsContents->statistics['pendingComments']}></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-4 xxlarge-4">
                <div class="display-block percentage-80 center-block push-20">
                    <div class="card v1 card-panel v1 hoverable">
                        <div class="display-block text-center text-danger">
                            <i aria-hidden="true" class="acms-icon <{if $acmsContents->statistics['declinedComments']}>acms-icon-thumbs-down<{else}>acms-icon-thumbs-down regular<{/if}> acms-icon-4x"></i><br>
                            <span><{translate id="%s Kommentare sind nicht genehmigt" textdomain="Core" args=$acmsContents->statistics['declinedComments']}></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <h2>
                    <{translate id="Was möchten Sie machen?" textdomain="Core"}>
                </h2>
            </div>
            <{foreach $acmsContents->dashboardActions as $component}>
                <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                    <div class="display-block percentage-80 center-block push-20">
                        <div class="card v1 card-panel v1 <{$component['class']}> hoverable">
                            <a href="<{$component['url']}>" title="<{$component['title']}>">
                                <div class="display-block text-center">
                                    <i class="<{$component['fonticon']}> acms-icon-4x"></i><br>
                                    <small><{$component['title']}></small>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <{/foreach}>
        </div>
        <{if $acmsContents->autoren}>
            <div class="row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h2><{translate id="Top-Kommentatoren" textdomain="Core"}> <small><{translate id="Gesamt" textdomain="Core"}>: <{$acmsContents->gesamtAutoren}></small></h2>
                    </div>
                    <div class="cards">
                        <{foreach $acmsContents->autoren as $autor}>
                            <div class="card v2">
                                <div class="card-content">
                                    <a class="right floated medium avatar no-shadow"
                                        title="<{$autor['autor']['username']}>" 
                                        href="<{$autor['autor']['itemUrl']}>">
                                        <img class="ircle" 
                                                src="<{$autor['autor']['avatar']}>" 
                                                title="<{$autor['autor']['username']}>" alt="" />
                                    </a>
                                    <div class="card-title middle aligned">
                                        <span><{$autor['autor']['first_name']}> <{$autor['autor']['last_name']}></span>
                                        <span class="primary badge v1">
                                            <{$autor['contributions']}>
                                        </span>
                                    </div>
                                    <div class="card-meta">
                                        <{$autor['autor']['username']}>
                                    </div>
                                </div>
                                <div class="extra card-content">
                                    <div class="two buttons">
                                        <a class="xsmall success icon-button button" href="<{$autor['autor']['itemUrl']}>" title="<{translate id='Profil aufrufen' textdomain='Core'}>">
                                            <i class="acms-icon acms-icon-user"></i>
                                            <span class="content"><{translate id='Profil' textdomain='Core'}></span>
                                        </a>
                                        <a class="xsmall default icon-button button" href="<{generateUrl name="core_comments_index"}>?submitter=<{$autor['autor']['id']}>">
                                            <i class="acms-icon acms-icon-comments"></i>
                                            <span class="content"><{translate id='Kommentare' textdomain='Core'}></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        <{/if}>
    </div>
</section>
