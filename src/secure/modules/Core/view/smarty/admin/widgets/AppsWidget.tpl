<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="panel v2 primary collapsible bordered large-up-push-left-15" data-module="apps/ui/components/panel">
    <div class="heading text-center">
        <i aria-hidden="true" class="acms-icon acms-icon-cubes"></i>
        <h2 class="title">
            <{translate id="Apps" textdomain="Core"}>
        </h2>
        <i aria-hidden="true" class="acms-icon collapsible-icon"></i>
    </div>
    <div id="widgets-apps" class="content clearfix">
        <div class="four eight-large-up-only feed-collection v1 push-updown-20">
            <{foreach $widgetContent as $module}>
                <a href="<{$module['url']}>" title="<{$module['name']}>" class="large square module avatar">
                    <img class="hoverable module-logo push-15 <{$module['dirname']|lower}>" src="<{$module['image']}>" title="<{$module['name']}>" alt="<{$module['name']}>" />
                </a>
            <{foreachelse}>
                <a href="/admin/core/marktplatz/" title="Module installieren" class="button stacked highlight">
                    <{translate id="Jetzt Module installieren" textdomain="Core"}>
                </a>
            <{/foreach}>
        </div>
        <{if $isAdmin}>
            <div class="two bottom plugged no-space buttons">
                <a href="/admin/core/module/" title="Module installieren" class="default button">
                    <span><{translate id="Module verwalten" textdomain="Core"}></span>
                </a>
                <a href="/admin/core/marktplatz/" title="Module installieren" class="primary button">
                    <span><{translate id="Neue Module installieren" textdomain="Core"}></span>
                </a>
            </div>
        <{/if}>
    </div>
</div>
