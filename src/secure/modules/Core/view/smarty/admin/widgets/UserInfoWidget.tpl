<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="panel v2 primary collapsible bordered" data-module="apps/ui/components/panel">
    <div class="heading text-center">
        <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
        <h2 class="title">
            <{translate id="Benutzer-Statistik" textdomain="Core"}>
        </h2>
        <i aria-hidden="true" class="acms-icon collapsible-icon"></i>
    </div>
    <div id="widgets-userstat" class="widget-panel">
        <div class="content" role="region" aria-label="<{translate id='Benutzer Informationen' textdomain='Core'}>">
            <div class="feed-collection v1 two-small-only three-medium-only five-large-only six-xlarge-up-only pull-10">
                <{foreach $widgetContent as $widget}>
                    <div class="acp-info-widget">
                        <i aria-hidden="true" class="widget-icon <{$widget['fonticon']}>"></i>
                        <div class="widget-content text-center">
                            <p class="widget-heading"><{$widget['heading']}></p>
                            <p class="widget-text"><{$widget['text']}></p>
                        </div>
                    </div>
                <{/foreach}>
            </div>
        </div>
    </div>
</div>
