<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container fixed-1200 fixed-1024">
    <div class="full row xxsmall-push-updown-30">
        <div class="column xxsmall-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 compact push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                    <span><{translate id="Upload-Berechtigungen"}></span>
                </h2>
                <p>
                    <{translate id="Die Mime-Types Verwaltung von Amaryllis-CMS sollte prinzipiell immer in den folgenden Schritten ablaufen:"}>
                </p>
                <ol>
                    <li>Auwählen des gewünschten Feldes. Bei Modulen mit nur einem Upload Feld kann man auch nur dieses auswählen.</li>
                    <li>Auswählen der Gewünschten Benutzer-Gruppe</li>
                    <li>Gewünschte Mime-Typen per Drag'n'Drop zufügen</li>
                    <li>Wenn das Feld Multi-Upload unterstützt, die maximal erlaubte Datei-Anzahl setzen</li>
                    <li>Gewünschte Maximal-Größe der Datei(en) setzen (in Abhängigkeit von den PHP Grundeinstellungen aus der php.ini) und, wenn Bild-Dateien erlaubt sind, die Maximal-Höhe und -Breite angleichen.</li>
                    <li>Schritte 2.-5. für alle Benutzer-Gruppen wiederholen</li>
                </ol>
                <p>Wir empfehlen <mark>dringend</mark> die Reihenfolge der Schritte einzuhalten um Fehler zu vermeiden! Hat eine Gruppe keine erlaubten Datei-Typen ist es <mark>nicht</mark> notwendig/möglich, die restlichen Felder zu ändern! es <b>muss</b> mindestens ein Datei-Typ erlaubt sein!</p>
            </div>
        </div>
        <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                    <span><{translate id="Sie brauchen Hilfe?"}></span>
                </h2>
                <p>
                    Lesen Sie unsere Dokumentation und erfahren Sie, wie Sie 
                    die Upload-Berechtigungen einrichten, was die Änderungen bedeuten 
                    und wie sich die Änderungen im System bemerkbar machen
                </p>
                <div class="button-toolbar clearfix compact">
                    <a class="button primary small right" href="https://www.amaryllis-dokumentation.de" title="<{translate id="Lesen Sie die Dokumentation"}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                        <span><{translate id="Dokumentation"}></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
