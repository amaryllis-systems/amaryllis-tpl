<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main">
    <div class="container fixed-1200 fixed-1024">
        <div class="row">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <i aria.hidden="true" class="acms-icon acms-icon-smile regular text-muted text-xlarge"></i>
                    <div class="content">
                        <h1 class="title">
                            <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                        </h1>
                        <p class="sub-title" itemprop="description"><{translate id="Eigene Emojis ergänzen" textdomain="Core"}></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="full row xxsmall-push-updown-30" data-module="apps/admin/data/emoji/emoji-admin" data-remote="<{$acmsContents->actionUrl}>" data-token="<{$acmsContents->token}>">
            <div class="column xxsmall-12">
                <div class="content-block v6 clearfix">
                    <div class="full row">
                        <div class="column xxsmall-12 medium-12 large-8 xlarge-8 xxlarge-8">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-filter text-muted"></i>
                                <span><{translate id="Reihenfolge angleichen" textdomain="Core"}></span>
                            </h2>
                            <div class="button-group">
                                <button id="sort-order"
                                        data-role="sort-changer"
                                        class="button small active"
                                        data-attribute="code"
                                        role="button"
                                        type="button"
                                        value="code">
                                    <i aria-hidden="true" class="acms-icon acms-icon-code"></i> 
                                    <span><{translate id="Code" textdomain="Core"}></span>
                                </button>
                                <button id="sort-order"
                                        data-role="sort-changer"
                                        class="button small"
                                        data-attribute="class"
                                        role="button"
                                        type="button"
                                        value="class">
                                    <i aria-hidden="true" class="acms-icon acms-icon-css3 brand"></i> 
                                    <span><{translate id="CSS-Klasse" textdomain="Core"}></span>
                                </button>
                            </div>
                            <div class="button-group">
                                <button id="sort-asc" 
                                        data-role="sort-asc"
                                        class="button small active"
                                        role="button"
                                        type="button"
                                        value="asc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-up"></i>
                                    <span><{translate id="aufsteigend" textdomain="Core"}></span>
                                </button>
                                <button id="sort-desc" 
                                        data-role="sort-desc"
                                        class="button small "
                                        role="button"
                                        type="button"
                                        value="desc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-down"></i>
                                    <span><{translate id="absteigend" textdomain="Core"}></span>
                                </button>
                            </div>
                        </div>
                        <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-plus-circle text-muted"></i>
                                <span><{translate id="Neuen Smiley hinzufügen" textdomain="Core"}></span>
                            </h2>
                            <p><{translate id="Fügen Sie einfach neue Smiley-Codes hinzu, um diese im System erkennen zu lassen. Die Smileys selber sind CSS-basiert und können einfach in Ihrem Stylesheet ergänzt werden." textdomain="Core"}></p>
                            <div class="button-toolbar">
                                <button type="button" data-trigger="add" class="button small right primary">
                                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                                    <span><{translate id="Neuer Smiley" textdomain="Core"}></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="table-wrapper">
                    <table class="responsive compact striped hover table">
                        <thead>
                            <tr>
                                <th><{translate id="Emoji-Gruppe" textdomain="Core"}></th>
                                <th><{translate id="Code" textdomain="Core"}></th>
                                <th><{translate id="Alternativ-Codes" textdomain="Core"}></th>
                                <th>Keywords</th>
                                <th><{translate id="CSS-Klasse" textdomain="Core"}></th>
                                <th><{translate id="Unicode" textdomain="Core"}></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <{foreach $acmsContents->allSmiles as $smile}>
                                <tr data-code="<{$smile['code']}>" 
                                    data-id="<{$smile['id']}>" 
                                    data-category="<{$smile['group_id']}>"
                                    data-keywords="<{$smile['keywords']}>"
                                    data-alt="<{$smile['alt']}>"
                                    data-class="<{$smile['class']}>"
                                    data-unicode="<{$smile['unicode']}>">

                                    <td data-th="<{translate id="Emoji-Gruppe" textdomain="Core"}>" data-col="category">
                                        <{$smile['group_id']}>
                                    </td>
                                    <td data-th="<{translate id="Code" textdomain="Core"}>" data-col="code">
                                        <{$smile['code']}>
                                    </td>
                                    <td data-th="<{translate id="Alternativ-Codes" textdomain="Core"}>" data-col="alt">
                                        <{$smile['alt']}>
                                    </td>
                                    <td data-th="Keywords" data-col="keywords">
                                        <{$smile['keywords']}>
                                    </td>
                                    <td data-th="<{translate id="CSS-Klasse" textdomain="Core"}>" data-col="class">
                                        <{$smile['class']}>
                                    </td>
                                    <td data-th="<{translate id="Aktionen" textdomain="Core"}>">
                                        <div class="button-toolbar compact">
                                            <button type="button" data-trigger="edit" class="primary icon button" aria-label="Bearbeiten">
                                                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                                            </button>
                                            <button type="button" data-trigger="remove" class="danger icon button" aria-label="Entfernen">
                                                <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <{/foreach}>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i> 
                        <span><{translate id="Smileys und deren Nutzung" textdomain="Core"}></span>
                    </h2>
                    <p>
                        Smiles werden meist in Text- oder HTML-Areas gesucht 
                        und durch den entsprechenden CSS-Code ersetzt. Dabei 
                        haben Sie die möglichkeit zu dem primären Smiley-Code 
                        auch Alternativ-Codes anzugeben (z.B. ähnliche 
                        Schreibweisen mit dem gleichen Ergebnis). Smileys des 
                        Systems benutzen einen Icon-Font um die Smileys 
                        darzustellen. Prinzipiell lässt sich aber alles verwenden,
                        was man per CSS steuern/einbinden kann (Bilder, Fonts, Text..).
                        Ändern Sie Smileys ab, indem Sie Ihre eigene CSS-Klasse 
                        einsetzen oder fügen Sie neue Smileys hinzu.
                    </p>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i> 
                        <span><{translate id="Sie brauchen Hilfe?" textdomain="Core"}></span>
                    </h2>
                    <p>
                        Sie brauchen Hilfe zur Verwendung von Smileys oder wie 
                        Sie diese angleichen können? Mehr Informationen über 
                        Smileys und das Layout der Smileys finden Sie in unserer 
                        Dokumentation.
                    </p>
                    <div class="button-toolbar compact">
                        <a class="button small primary right" target="_blank" href="https://www.amaryllis-dokumentation.de/">
                            <span><{translate id="Dokumentation" textdomain="Core"}></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
