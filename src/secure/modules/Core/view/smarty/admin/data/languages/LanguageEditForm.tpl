<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal large"
     id="modal-edit-<{$language['foldername']}>"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="modal-update-<{$language['foldername']}>-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" <{$language['form']['attributes']}> data-foldername="<{$language['foldername']}>">
                <div class="header">
                    <span role="button" class="close right" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="src-only">Schließen</span>
                    </span>
                    <h4 class="title" id="modal-form-<{$language['foldername']}>-title">
                        <{$language['form']['title']}>
                    </h4>
                </div>
                <div class="main">
                    <{foreach $language['form']['fields'] as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
                <div class="footer">
                    <{include file=$language['form']['buttons']['template'] field=$language['form']['buttons']}>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>
