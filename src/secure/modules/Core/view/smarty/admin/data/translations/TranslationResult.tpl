<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<tr data-language="<{$translation['locale']}>" 
    data-id="<{$translation['id']}>" 
    data-constant="<{$translation['constant']}>" 
    data-textdomain="<{$translation['textdomain']}>"
    data-translation="<{$translation['translation']}>"
    data-remote="<{$translation['remoteUrl']}>">
    <td><{$translation['constant']}></td>
    <td><{$translation['translation']}></td>
    <td><{$translation['locale']}></td>
    <td><{$translation['textdomain']}></td>
    <td>
        <{if $translation['mkjs']}>
            <span class="label v1 success"><{translate id="JS-Übersetzung" textdomain="Core"}></span>
        <{else}>
            <span class="label v1 default"><{translate id="JS-Übersetzung" textdomain="Core"}></span>
        <{/if}>
    </td>
    <td>
        <div class="button-toolbar compact">
            <button type="button" data-trigger="edit" data-target="#translations-edit-modal" class="button tiny primary">
                <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
            </button>
            <button type="button" data-trigger="delete" data-target="#translations-delete-modal" class="button tiny danger">
                <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
            </button>
        </div>
    </td>
</tr>
