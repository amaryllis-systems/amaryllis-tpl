<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal route-edit-modal" id="route-edit-<{$route['id']}>-modal" data-modal-index="1" tabindex="-1" role="dialog" aria-labelledby="route-edit-<{$route['id']}>-modal-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form">
                <div class="header">
                    <button class="close right" type="button" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                    </button>
                    <h4 class="title" id="route-edit-<{$route['id']}>-modal-title">
                        <{translate id="Route bearbeiten"}>
                    </h4>
                </div>
                <div class="main">
                    <{foreach $route['form']['fields'] as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
                <div class="footer">
                    <{include file=$route['form']['buttons']['template'] field=$route['form']['buttons']}>
                </div>
            </form>
        </div><!-- /.content -->
        </form>
    </div><!-- /.modal-dialog -->
</div>
