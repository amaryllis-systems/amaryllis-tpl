<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-search text-muted"></i>
                        <span itemprop="name"><{$acmsContents->acmsTitle}></span> <small><{translate id="Übersicht" textdomain="Core"}></small>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Hier können Sie Ihre Such-Anfragen über die System-Suche einsehen."}>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container fixed-1200">
        <main class="row full" itemprop="mainContentOfPage">
            <div class="column xxsmall-12 medium-12">
                <h2>
                    <{translate id="Sie hatten %s Such-Anfragen in den letzten 12 Monaten" textdomain="Core" args=$acmsContents->total}>
                </h2>
            </div>
            <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                <h2><{translate id="Häufig gesuchte Tags" textdomain="Core"}></h2>
                <{foreach $acmsContents->baseStat['tags'] as $tag}>
                    <span class="tag v1 round">
                        <span class="tag-image"><{$tag['frequency']}></span>
                        <span><{$tag['tag']}></span>
                    </span>
                <{/foreach}>
            </div>
            <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                <h2><{translate id="Häufig durchsuchte Module" textdomain="Core"}></h2>
                <{foreach $acmsContents->baseStat['modules'] as $module}>
                    <span class="tag v1 round">
                        <span class="tag-image"><{$module['frequency']}></span>
                        <span><{$module['module']}></span>
                    </span>
                <{/foreach}>
            </div>
            <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                <h2><{translate id="Häufig verwendete Such-Worte" textdomain="Core"}></h2>
                <{foreach $acmsContents->baseStat['keywords'] as $keyword}>
                    <span class="tag v1 round">
                        <span class="tag-image"><{$keyword['frequency']}></span>
                        <span><{$keyword['keyword']}></span>
                    </span>
                <{/foreach}>
            </div>
            <div class="column xxsmall-12 medium-12">
                <h2>
                    <{translate id="Sie hatten %s Such-Anfragen ohne Ergebnis" textdomain="Core" args=[$acmsContents->baseStat['withoutResults']|@count]}>
                </h2>
                <p class="lead">
                    <{translate id="Bitte beachten Sie: Die Suchen können auch im gleichen Such-Vorgang aufgetreten sein. Hier wird primär Wert auf Das Schlüsselwort und das in der Suche verwendete Modul gelgt!" textdomain="Core"}>
                </p>
                <{foreach $acmsContents->baseStat['withoutResults'] as $result}>
                    <div class="push-10 display-inline-block">
                        <div class="tile v1">
                            <div class="tile-content">
                                <h4><{$result['keyword']}> <small><{$result['module']}></small></h4>
                                <i aria-hidden="true" class="acms-icon acms-icon-key"></i>
                                <p class="h1 text-big">
                                    <{$result['frequency']}>
                                </p>
                            </div>
                        </div>
                    </div>
                <{/foreach}>
            </div>
        </main>
    </div>
</section>
