<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main">
    <div class="container fixed-1600 fixed-1200 fixed-1024">
        <div class="full row xxsmall-push-updown-30">
            <div class="large page-title v7">
                <i aria-hidden="true" class="acms-icon acms-icon-flag text-muted text-xlarge"></i>
                <div class="content">
                    <h1 class="title">
                        <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                    </h1>
                    <p class="sub-title" itemprop="description">
                        <{translate id="Ihre installierten und verfügbaren Sprachen im System."}>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container fixed-1600 fixed-1200 fixed-1024">
        <div class="full row xxsmall-push-updown-30">
            <div class="column xxsmall-12" data-module="apps/admin/data/language/language-admin" data-remote="<{$acmsContents->remote}>" data-token="<{$acmsContents->token}>">
                
            </div>
        </div>
    </div>
</section>
