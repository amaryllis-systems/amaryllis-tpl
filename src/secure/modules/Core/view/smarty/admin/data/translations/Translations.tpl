<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main">
    <div class="container fixed-1600 fixed-1200 fixed-1024">
        <div class="row">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <i aria.hidden="true" class="text-xxlarge acms-icon acms-icon-language text-muted"></i>
                    <div class="content">
                        <h1 class="title">
                            <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                        </h1>
                        <p class="sub-title" itemprop="description"><{translate id="Verwalten Sie eigene Übersetzungs-Kataloge"}></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <main class="container fixed-1600 fixed-1200 fixed-1024" 
         data-module="apps/admin/data/translations/translations-admin" 
         data-remote="<{$acmsContents->remote}>"
         data-token="<{$acmsContents->token}>">
        <div class="full row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="content-block v6 row full">
                    <div class="column xxsmall-12 medium-12 large-8 xlarge-8 xxlarge-8">
                        <h2>
                            <i aria-hidden="true" class="acms-icon acms-icon-filter text-muted"></i>
                            <span>Reihenfolge angleichen</span>
                        </h2>
                        <div class="button-group">
                            <button id="sort-constant" 
                                    data-role="sort-changer"
                                    data-attribute="constant"
                                    class="button small active"
                                    role="button"
                                    type="button"
                                    value="constant">
                                <i aria-hidden="true" class="acms-icon acms-icon-font"></i> 
                                <span><{translate id="Konstante" textdomain="Core"}></span>
                            </button>
                            <button id="sort-order"
                                    data-role="sort-changer"
                                    class="button small"
                                    data-attribute="language"
                                    role="button"
                                    type="button"
                                    value="language">
                                <i aria-hidden="true" class="acms-icon acms-icon-flag"></i> 
                                <span><{translate id="Sprache" textdomain="Core"}></span>
                            </button>
                            <button id="sort-order"
                                    data-role="sort-changer"
                                    class="button small"
                                    data-attribute="textdomain"
                                    role="button"
                                    type="button"
                                    value="textdomain">
                                <i aria-hidden="true" class="acms-icon acms-icon-cubes"></i> 
                                <span><{translate id="Textdomäne" textdomain="Core"}></span>
                            </button>
                        </div>
                        <div class="button-group">
                            <button id="sort-asc" 
                                    data-role="sort-asc"
                                    class="button small active"
                                    role="button"
                                    type="button"
                                    value="asc">
                                <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-up"></i>
                                <span><{translate id="aufsteigend" textdomain="Core"}></span>
                            </button>
                            <button id="sort-desc" 
                                    data-role="sort-desc"
                                    class="button small "
                                    role="button"
                                    type="button"
                                    value="desc">
                                <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-down"></i>
                                <span><{translate id="absteigend" textdomain="Core"}></span>
                            </button>
                        </div>
                    </div>
                    <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                        <h2>
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle text-muted"></i>
                            <span>Neue Übersetzung hinzufügen</span>
                        </h2>
                        <p>Fügen Sie einfach neue Übersetzungen hinzu, um diese im System erkennen zu lassen. Die Übersetzungen können im Layout genutzt werden, um Ihre eigenen Texte übersetzbar zu machen.</p>
                        <div class="button-toolbar">
                            <button type="button" class="small right floated primary icon button" data-trigger="add">
                                <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                                <span>Neue Übersetzung</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="table-wrapper">
                    <table class="responsive compact striped middle aligned table">
                        <thead>
                            <tr>
                                <th><{translate id="Konstante" textdomain="Core"}></th>
                                <th><{translate id="Übersetzung" textdomain="Core"}></th>
                                <th><{translate id="Sprache" textdomain="Core"}></th>
                                <th><{translate id="Textdomäne" textdomain="Core"}></th>
                                <th><{translate id="JS-Übersetzung" textdomain="Core"}></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <div class="container">
        <div class="row">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i> 
                        <span>Übersetzungen und deren Nutzung</span>
                    </h2>
                    <p>
                        Übersetzungen sollen Ihnen bei mehrsprachigen Seiten 
                        helfen, die Parts in Ihrem Layout übersetzbar zu machen, 
                        die entweder nicht im System sind, oder die Sie gerne 
                        in anderem Text in Ihrem Layout einsetzen möchten 
                        (insbesondere in System- und Modul-Templates 
                        enthaltene Texte).
                    </p>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i> 
                        <span>Sie brauchen Hilfe?</span>
                    </h2>
                    <p>
                        Sie brauchen Hilfe zur Verwendung von Übersetzungen 
                        oder wie Sie diese angleichen können? Mehr 
                        Informationen über Übersetzungen und im Layout finden 
                        Sie in unserer Dokumentation.
                    </p>
                    <div class="button-toolbar compact">
                        <a class="button small primary right" target="_blank" href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/sprachen-verwalten/eigene-uebersetzungen/">
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
