<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main">
    <div class="container">
        <div class="row">
            <div class="column xxsmall-12">
                <div class="page-emotion">
                    <h1>
                        <i aria.hidden="true" class="acms-icon acms-icon-road text-muted"></i>
                        <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                    </h1>
                    <p class="lead" itemprop="description"><{translate id="Verwalten Sie die Ihre eigenen Routen für spezielle Seiten und flexible Unterseiten."}></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container" 
         data-module="view/admin/data/routes" 
         data-remote="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'data', 'item' => 'route', 'action' => 'action']}>">
        <div class="row full xxsmall-push-updown-30"
        
             data-ordername="title"
             data-order="asc"
             data-tagname="tbody"
             data-listitem="tr"
             data-module="ui/components/listsorter"
             data-desc-sorter="[data-role=sort-desc]"
             data-asc-sorter="[data-role=sort-asc]"
             data-order-changer="[data-role=sort-changer]">
            <div class="column xxsmall-12">
                <div class="content-block v6">
                    <div class="row full">
                        <div class="column xxsmall-12 medium-12 large-8 xlarge-8 xxlarge-8">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-filter text-muted"></i>
                                <span>Reihenfolge angleichen</span>
                            </h2>
                            <div class="button-group">
                                <button id="sort-emotion" 
                                        data-role="sort-changer"
                                        data-ordername="title"
                                        class="button small active"
                                        role="button"
                                        type="button"
                                        value="title">
                                    <i aria-hidden="true" class="acms-icon acms-icon-font"></i> 
                                    <span><{translate id="Bezeichnung" textdomain="Core"}></span>
                                </button>
                                <button id="sort-order"
                                        data-role="sort-changer"
                                        class="button small"
                                        data-ordername="app"
                                        role="button"
                                        type="button"
                                        value="app">
                                    <i aria-hidden="true" class="acms-icon acms-icon-cube"></i> 
                                    <span><{translate id="Ziel-Modul" textdomain="Core"}></span>
                                </button>
                                <button id="sort-order"
                                        data-role="sort-changer"
                                        class="button small"
                                        data-ordername="online"
                                        role="button"
                                        type="button"
                                        value="online">
                                    <i aria-hidden="true" class="acms-icon acms-icon-eye"></i> 
                                    <span><{translate id="Online-Status" textdomain="Core"}></span>
                                </button>
                            </div>
                            <div class="button-group">
                                <button id="sort-asc" 
                                        data-role="sort-asc"
                                        class="button small active"
                                        role="button"
                                        type="button"
                                        value="asc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-up"></i>
                                    <span><{translate id="aufsteigend" textdomain="Core"}></span>
                                </button>
                                <button id="sort-desc" 
                                        data-role="sort-desc"
                                        class="button small "
                                        role="button"
                                        type="button"
                                        value="desc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-down"></i>
                                    <span><{translate id="absteigend" textdomain="Core"}></span>
                                </button>
                            </div>
                        </div>
                        <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-plus-circle text-muted"></i>
                                <span>Neue Route hinzufügen</span>
                            </h2>
                            <p>Fügen Sie einfach neue Routen hinzu, um diese im System erkennen zu lassen. Die Routen können in System- und Modul-Bereichen genutzt werden, um Ihre eigenen Seiten innerhalb der vorgegebenen zu erstellen.</p>
                            <div class="button-toolbar">
                                <a role="button" href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'data', 'item' => 'route', 'action' => 'neu']}>" 
                                   class="button small right primary">
                                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                                    <span>Neue Route</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="table-wrapper">
                    <table class="table compact striped hover">
                        <thead>
                            <tr>
                                <th><{translate id="Bezeichnung" textdomain="Core"}></th>
                                <th><{translate id="Name" textdomain="Core"}></th>
                                <th><{translate id="Url" textdomain="Core"}></th>
                                <th><{translate id="Modul" textdomain="Core"}></th>
                                <th><{translate id="Online-Status" textdomain="Core"}></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <{foreach $acmsContents->allRoutes as $route}>
                                <tr data-title="<{$route['title']}>" 
                                    data-id="<{$route['id']}>" 
                                    data-name="<{$route['name']}>" 
                                    data-url="<{$route['route']}>"
                                    data-app="<{$route['module']}>"
                                    data-online="<{$route['active']}>"
                                    data-remote="<{$route['remoteUrl']}>">
                                    <td><{$route['title']}></td>
                                    <td><{$route['name']}></td>
                                    <td><{$route['route']}></td>
                                    <td><{$route['module']}></td>
                                    <td>
                                        <{if $route['active']}>
                                            <span class="label v1 success">
                                                <i aria-hidden="true" class="acms-icon acms-icon-check-circle"></i>
                                                <{translate id="Online" textdomain="Core"}>
                                            </span>
                                        <{else}>
                                            <span class="label v1 default">
                                                <i aria-hidden="true" class="acms-icon acms-icon-ban"></i>
                                                <{translate id="Offline" textdomain="Core"}>
                                            </span>
                                        <{/if}>
                                    </td>
                                    <td>
                                        <div class="button-toolbar compact">
                                            <a role="button" href="<{$route['editUrl']}>" class="button tiny primary">
                                                <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                                                <span class="src-only">
                                                    Bearbeiten
                                                </span>
                                            </a>
                                            <button type="button" data-trigger="delete" data-target="#route-delete-modal" class="button tiny danger">
                                                <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <{/foreach}>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i> 
                        <span>Übersetzungen und deren Nutzung</span>
                    </h2>
                    <p>
                        Eigene Routen sollen Ihnen helfen, zusätzliche Bereich 
                        im System zu erschaffen.
                        Wir empfehlen Ihnen dringend, diese Funktion nicht für 
                        einfache Content-Seiten zu verwenden.
                        Diese Funktionalität ist nicht durchsuchbar und bietet 
                        nahezu keine SEO-Optimierung.
                        Nutzen Sie für solche Seiten das Content-Modul. 
                        Die eigenen Routen sind für Spezielle Sub-Seiten 
                        gedacht, die nicht vorhandene Seiten im System ergänzen 
                        (z.B. Unterseiten in Modulen oder Profil).
                    </p>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria.hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i> 
                        <span>Sie brauchen Hilfe?</span>
                    </h2>
                    <p>
                        Sie brauchen Hilfe zur Verwendung von eigenen Routen 
                        oder wie Sie diese angleichen können? Mehr 
                        Informationen über eigene Routen finden 
                        Sie in unserer Dokumentation.
                    </p>
                    <div class="button-toolbar compact">
                        <a class="button small primary right" target="_blank" href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/system-bereiche/eigene-routen/">
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/data/route/RouteModal.tpl"}>
    <{include file="acmsfile:Core|admin/data/route/RouteDeleteModal.tpl"}>
</section>
