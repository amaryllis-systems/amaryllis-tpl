<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal" id="route-delete-modal" data-modal-index="1" tabindex="-1" role="dialog" aria-labelledby="route-delete-modal-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" method="post">
                <div class="header">
                    <button class="close right" type="button" data-dismiss="modal"><span aria-hidden="true">&times;</span>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                    </button>
                    <h4 class="title" id="route-delete-modal-title">
                        <{translate id="Route entfernen"}>
                    </h4>
                </div>
                <div class="main">
                    <input type="hidden" name="agree" value="1" />
                    <input type="hidden" name="id" value="0" />
                    <input type="hidden" name="op" value="entferne" />
                    <p><{translate id="Sind Sie sicher, dass Sie die Route löschen möchten?" textdomain="Core"}></p>
                </div>
                <div class="footer">
                    <button type="submit" class="button small primary">
                        <{translate id="Ja" textdomain="Core"}>
                    </button>
                    <button type="submit" class="button small border-button">
                        <{translate id="Abbrechen" textdomain="Core"}>
                    </button>
                </div>
            </form>
        </div><!-- /.content -->
        </form>
    </div><!-- /.modal-dialog -->
</div>
