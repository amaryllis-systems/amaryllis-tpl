<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper">
    <form>
        <div class="fields fields-2">
            <div class="form-group">
                <label for="mimetypes-upload-field" class="field-label">Upload-Feld</label>
                <select id="mimetypes-upload-field" class="field" name="field" value="<{$acmsContents->field}>">
                    <{foreach $acmsContents->fields as $field => $label}>
                        <option value="<{$field}>"><{$label}></option>
                    <{/foreach}>
                </select>
            </div>
            <div class="form-group">
                <label for="mimetypes-group-id" class="field-label">Benutzer-Gruppe</label>
                <select id="mimetypes-group-id" class="field" name="group_id" value="<{$acmsContents->group_id}>">
                    <{foreach $acmsContents->userGroups as $gid => $label}>
                        <option value="<{$gid}>"><{$label}></option>
                    <{/foreach}>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="dual-list v1">
                <div class="list-wrapper">
                    <div class="list-left content-block v5" data-role="dual-list-left">
                        <div class="small page-title v7">
                            <i aria-hidden="true" class="acms-icon acms-icon-database"></i>
                            <div class="content">
                                <h3 id="mimetypesLeftTitle" class="title">
                                    <span itemprop="name">Verfügbare Mimetypen</span>
                                </h3>
                                <p class="sub-title">Im System registrierte Mime-Typen</p>
                            </div>
                        </div>
                        <div class="list-actions">
                            <div class="twelve wide icon form-group">
                                <label for="dual-list-search-left" class="src-only">In verfügbaren Mime-Typen suchen</label>
                                <input type="search"
                                    role="search"
                                    data-role="dual-list-search"
                                    data-side="left"
                                    id="dual-list-search-left"
                                    name="search-dual-list-left"
                                    class="field"
                                    placeholder="<{translate id='Suchen' textdomain='Core'}>" />
                                <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                            </div>
                            <div class="auto wide form-group">
                                <button type="button"
                                        role="button"
                                        class="large info icon button selector"
                                        data-role="check-all"
                                        data-title="<{translate id='Wähle alle' textdomain='Core'}>"
                                        data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                                        data-placement="right"
                                        data-list="left">
                                    <i aria-hidden="true" class="acms-icon acms-icon-square regular"></i>
                                </button>
                            </div>
                        </div>
                        <div class="dual-list-list">
                            <ul class="collection v1">
                            </ul>
                        </div>
                    </div>
                    <div class="list-arrows">
                        <button role="button" type="button" class="button default tiny move-left" data-role="move-button" data-direction="left" aria-label="<{translate id="Markierte nach links verschieben" textdomain="Core"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-chevron-left"></i>
                        </button>

                        <button role="button" type="button" class="default icon button move-right" data-role="move-button"  data-direction="right" aria-label="<{translate id="Markierte nach rechts verschieben" textdomain="Core"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-chevron-right"></i>
                        </button>
                    </div>
                    <div class="list-right content-block v5" data-role="dual-list-right">
                        <div class="small page-title v7">
                            <i aria-hidden="true" class="acms-icon acms-icon-check-circle text-success"></i>
                            <div class="content">
                                <h3 id="mimetypesLeftTitle" class="title">
                                    <span itemprop="name">Erlaubte Mimetypen</span>
                                </h3>
                                <p class="sub-title">Für diese Gruppe erlaubte Mime-Typen</p>
                            </div>
                        </div>
                        <div class="list-actions">
                            <div class="twelve wide icon form-group">
                                <label for="dual-list-search-right" class="src-only">In verfügbaren Mime-Typen suchen</label>
                                <input type="search"
                                    role="search"
                                    data-role="dual-list-search"
                                    data-side="right"
                                    id="dual-list-search-right"
                                    name="search-dual-list-right"
                                    class="field"
                                    placeholder="<{translate id='Suchen' textdomain='Core'}>" />
                                <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                            </div>
                            <div class="auto wide form-group">
                                <button type="button"
                                        role="button"
                                        class="large info icon button selector"
                                        data-role="check-all"
                                        data-title="<{translate id='Wähle alle' textdomain='Core'}>"
                                        data-module="apps/ui/informations/tooltip" data-toggle="tooltip"
                                        data-placement="left"
                                        data-list="right">
                                    <i aria-hidden="true" class="acms-icon acms-icon-square regular"></i>
                                </button>
                            </div>
                        </div>
                        <div class="dual-list-list">
                            <ul class="collection v1">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fields-2 fields <{if !$acmsContents['isMultiUpload']}>hidden<{/if}>" data-multi-upload="true">
            <div class="form-group">
                <label class="field-label" for='maxfiles'>
                    <{translate id="Maximal erlaubte Dateien" textdomain='Core'}>
                </label>
                <input data-value="<{$acmsContents['maxFiles']}>"
                    type="text"
                    class="field"
                    value="<{$acmsContents['maxFiles']}>"
                    name="maxfiles" id="maxfiles" />
            </div>
            <div class="form-group">
                <label class="field-label" for='minfiles'>
                    <{translate id="Mindestens erforderliche Dateien" textdomain='Core'}>
                </label>
                <input data-value="<{$acmsContents['minFiles']}>" type="text" class="field" value="<{$acmsContents['minFiles']}>"  name="minfiles" id="minfiles" />
            </div>
        </div>
    
        <div class="form-group wide wide-8">
            <label class="field-label" for='minsize'>
                <{translate id='Mindestens erforderliche Dateigröße' textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['minSize']}>" type="text" class="field" value="<{$acmsContents['minSize']}>" name="minsize" id="minsize" />
        </div>
        <div class="form-group wide wide-8">
            <label class="field-label" for='maxsize'>
                <{translate id='Maximal erlaubte Dateigröße' textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['maxSize']}>" type="text" class="field" value="<{$acmsContents['maxSize']}>" name="maxsize" id="maxsize" />
        </div>
        <div class="form-group wide wide-8">
            <label class="field-label" for='minheight'>
                <{translate id="Mindestens erforderliche Bild-Höhe" textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['minHeight']}>" type="text" class="field" value="<{$acmsContents['minHeight']}>" name="minheight" id="minheight" />
        </div>
        <div class="form-group wide wide-8">
            <label class="field-label" for='maxheight'>
                <{translate id="Maximal erlaubte Bild-Höhe" textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['maxHeight']}>" type="text" class="field" value="<{$acmsContents['maxHeight']}>" name="maxheight" id="maxheight" />
        </div>

        <div class="form-group wide wide-8">
            <label class="field-label" for='minwidth'>
                <{translate id="Mindestens erforderliche Bildbreite" textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['minWidth']}>" type="text" class="field" value="<{$acmsContents['minWidth']}>"  name="minwidth" id="minwidth" />
        </div>
        <div class="form-group wide wide-8">
            <label class="field-label"
                for='maxwidth'>
                <{translate id="Maximal erforderliche Bild-Breite" textdomain='Core'}>
            </label>
            <input data-value="<{$acmsContents['maxWidth']}>" type="text" class="field" value="<{$acmsContents['maxWidth']}>"  name="maxwidth" id="maxwidth" />
        </div>
        <div class="form-group">
            <label class="field-label"
                for='allow_overwrite'>
                <{translate id="Erlaube die Dateien zu überschreiben?" textdomain='Core'}>
            </label>
            <div class="small toggle-variant switch v2">
                <input type="checkbox" name="allow_overwrite" value="1" id="allow_overwrite" <{if $acmsContents['allow_overwrite'] }>checked="checked"<{/if}> />
                <label for="allow_overwrite" aria-label="Überschreiben aktivieren"></label>
            </div>
            <div class="field-static field-hint">
                <{translate id="Erlaube die Dateien zu überschreiben, wenn die Datei mit dem gleichen Dateinamen bereits exisiert. Ist die Option deaktiviert, wird die Datei mit einem generischen Namen hochgeladen, wenn es diese im Zielordner bereits gibt." textdomain='Core'}>
            </div>
        </div>
    </form>
</div>
