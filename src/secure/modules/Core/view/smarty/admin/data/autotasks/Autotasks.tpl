<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-tasks text-muted"></i>
                        <span itemprop="name"><{$acmsContents->acmsTitle}></span> <small><{translate id="Übersicht" textdomain="Core"}></small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container fixed-1200">
        <main class="row full" itemprop="mainContentOfPage">
            <div class="column xxsmall-12">
                <ul class="collection v1 v1">
                    <{foreach $acmsContents->allTasks as $id => $task}>
                        <li class="collection-item bordered push-updown-5 border-solid" data-id="<{$id}>" data-title="<{$task['title']}>" data-type="<{$task['type']}>">
                            <h3 class="title"><{$task['title']}></h3>
                            <p>
                                <span class="label v1 primary"><{$task['type']}></span>&nbsp;
                                <{if $task['active']}>
                                    <span class="label v1 success"><{translate id="aktiv" texdomain="Core"}></span>
                                <{else}>
                                    <span class="label v1 danger"><{translate id="inaktiv"}></span>
                                <{/if}>
                            </p>
                            <table class="table compact">
                                <thead>
                                    <tr>
                                        <td>
                                            <{translate id="Wiederholt alle" texdomain="Core"}>
                                        </td>
                                        <td>
                                            <{translate id="Zuletzt am" texdomain="Core"}>
                                        </td>
                                        <td>
                                            <{translate id="Läuft bis" texdomain="Core"}>
                                        </td>
                                    </tr>
                                </thead>
                                <tbdoy>
                                    <tr>
                                        <td>
                                            <span class="label v1 primary"><{$task['intervals']}></span>
                                        </td>
                                        <td>
                                            <span class="label v1 primary"><{$task['lastRun']}></span>
                                        </td>
                                        <td>
                                            <span class="label v1 primary"><{$task['repeats']}></span>
                                        </td>
                                    </tr>
                                </tbdoy>
                            </table>
                            <a class="secondary-content" data-module="apps/ui/informations/modal" data-target="#autotask-edit-<{$task['id']}>-modal" href="#" title="<{translate id="Bearbeiten"}>">
                                <i aria-hidden='true' class="acms-icon acms-icon-pencil-alt  text-theme"></i>
                                <span class="src-only"><{translate id="Bearbeiten" texdomain="Core"}></span>
                            </a>
                            <{if $task['form']}>
                                <{include file="acmsfile:Core|admin/data/autotasks/AutotasksEditModal.tpl" autotask=$task nocache}>
                            <{/if}>
                        </li>
                    <{/foreach}>
                </ul>
            </div>
        </main>
    </div>
    <div class="container fixed-1200">
        <div class="row full xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="content-block v6">
                    <h2><i aria-hidden='true' class="acms-icon acms-icon-lightbulb regular text-muted"></i> <{translate id="Auto-Tasks" texdomain="Core"}></h2>
                    <p>
                        <{translate id="Auto-Tasks sollen Ihnen helfen, Ihre Webseite am Laufen zu halten. Bitte beachten Sie, dass Auto-Tasks auch Schaden anrichten können, wenn Sie eigene Auto-Tasks hinzufügen. System-Tasks sind, ebenso wie die unserer Module, im allgemeiner so eingestellt, wie es optimal ist. Natürlich können Sie diese dennoch an Ihre Aktivität angleichen." textdomain="Core"}>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
