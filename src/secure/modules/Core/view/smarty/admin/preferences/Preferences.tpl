<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main" itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents->configForm}>
        <div class="container fixed-1200">
            <div class="row full">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-cogs text-muted"></i>
                        <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                    </h1>
                </div>
            </div>
            <div class="row full">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['configForm']['template'] form=$acmsContents['configForm']}>
                </div>
            </div>
            <{if $acmsContents->has('configInfo')}>
                <div class="content-block v6">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i> 
                        <span><{translate id="Informationen" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{$acmsContents->get('configInfo')}>
                    </p>
                </div>
            <{/if}>
        </div>
    <{/if}>
</section>
