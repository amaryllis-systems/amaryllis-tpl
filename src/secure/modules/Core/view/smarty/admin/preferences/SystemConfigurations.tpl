<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <div class="container fixed-1200">
        <div class="row full">
            <div class="page-title">
                <h1>
                    <i aria-hidden="true" class="acms-icon acms-icon-cogs text-muted"></i>
                    <span itemprop="name"><{translate id="Einstellungen" textdomain="Core"}></span>
                </h1>
                <p class="lead" itemprop="description">Konfigurieren Sie Das System nach Ihren Bedürfnissen</p>
            </div>
        </div>
        <div class="row full">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10" role="region" aria-label="<{translate id="System Konfiguration" textdomain="Core"}>">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-cog text-muted"></i>
                        <span><{translate id="System Konfiguration" textdomain="Core"}></span>
                    </h2>
                    <{translate id="Hier finden Sie alle unterstützten Konfiguations-Kategorien des Sytems. Achten Sie beim Einstellen darauf, dass Ihnen bewusst ist, welche Auswirkung eine geänderte Einstellung hat! Fehlerhaft gesetzte Konfigurations-Optionen können das System schwer beschädigen oder sogar unbrauchbar machen und Sicherheits-Lücken verursachen! Wir empfehlen Ihnen, die Dokumentation zu lesen und an einem Training teilzunehmen." textdomain="Core"}>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10" role="region" aria-label="<{translate id="System Dokumentation" textdomain="Core"}>">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Sie brauchen Hilfe?" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Mehr zum Thema Einstellungen und System-Konfiguration finden Sie in unserer Dokumentation. Lernen Sie das System zu konfigurieren und in voller Leistung zu nutzen." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button primary small right" href="" target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row full xxsmall-push-updown-30"
             data-ordername="title"
             data-order="asc"
             data-module="ui/components/listsorter"
             data-desc-sorter="[data-role=sort-desc]"
             data-asc-sorter="[data-role=sort-asc]"
             data-order-changer="[data-role=sort-changer]">
            <div class="column xxsmall-12">
                <div class="content-block v6">
                    <div class="row full">
                        <div class="column xxsmall-12 medium-12 large-8 xlarge-8 xxlarge-8">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-filter text-muted"></i>
                                <span>Reihenfolge angleichen</span>
                            </h2>
                            <div class="button-group">
                                <button id="sort-title" 
                                        data-role="sort-changer"
                                        data-ordername="title"
                                        class="button small primary active border-button"
                                        role="button"
                                        type="button"
                                        value="title">
                                    <i aria-hidden="true" class="acms-icon acms-icon-font"></i> 
                                    <span><{translate id="Titel" textdomain="Core"}></span>
                                </button>
                                <button id="sort-order"
                                        data-role="sort-changer"
                                        class="button small primary border-button"
                                        data-ordername="order"
                                        role="button"
                                        type="button"
                                        value="order">
                                    <i aria-hidden="true" class="acms-icon acms-icon-balance-scale"></i> 
                                    <span><{translate id="Gewichtung" textdomain="Core"}></span>
                                </button>
                            </div>
                            <div class="button-group">
                                <button id="sort-asc" 
                                        data-role="sort-asc"
                                        class="button small primary active border-button"
                                        role="button"
                                        type="button"
                                        value="asc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-up"></i>
                                    <span><{translate id="aufsteigend" textdomain="Core"}></span>
                                </button>
                                <button id="sort-desc" 
                                        data-role="sort-desc"
                                        class="button small primary border-button"
                                        role="button"
                                        type="button"
                                        value="desc">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sort-alpha-down"></i>
                                    <span><{translate id="absteigend" textdomain="Core"}></span>
                                </button>
                            </div>
                            <div class="button-group" data-module="view/admin/preferences/preffilter">

                                <button id="filter-system" 
                                        data-role="filter-system"
                                        class="button small primary active filter-button border-button"
                                        role="button"
                                        type="button"
                                        value="system">
                                    <i aria-hidden="true" class="acms-icon acms-icon-microchip"></i>
                                    <span><{translate id="Zeige System" textdomain="Core"}></span>
                                </button>
                                <button id="filter-module" 
                                        data-role="filter-module"
                                        class="button small primary active filter-button border-button"
                                        role="button"
                                        type="button"
                                        value="module">
                                    <i aria-hidden="true" class="acms-icon acms-icon-cubes"></i>
                                    <span><{translate id="Zeige Module" textdomain="Core"}></span>
                                </button>
                            </div>
                        </div>
                        <div class="column xxsmall-12 medium-12 large-4 xlarge-4 xxlarge-4">
                            <h2>
                                <i aria-hidden="true" class="acms-icon acms-icon-search text-muted"></i>
                                <span>Schnellsuche</span>
                            </h2>

                            <div class="form-group">
                                <div class="field-group">
                                    <span class="field-icon">
                                        <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                                    </span>
                                    <input type="search" 
                                           name="term" 
                                           id="quicksearch" 
                                           class="field" 
                                           data-module="view/admin/preferences/quicksearch"
                                           data-remote="<{generateUrl name='core_preferences_action' parameters=['module' => 'core', 'action' => 'search']}>"
                                           placeholder="Tippen um in den Einstellungen zu suchen..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div id="core-configs" class="confcat-list push-updown-30 clearfix">
                    <ul class="list-unstyled list list-striped" id="system-categories">
                        <{foreach $acmsContents->get('configCategories') as $category}>
                            <li class="list-item" data-title="<{$category['title']}>" data-order="<{$category['order']}>" data-typ="<{$category['type']}>">
                                <a href="<{$category['url']}>" title="<{translate id='Gehe zu: %s' textdomain="Core" args=$category['title']}>">
                                    <{$category['title']}>
                                </a>
                                &nbsp; <span class="text-muted"><{$category['body']}></span>
                            </li>
                        <{/foreach}>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
