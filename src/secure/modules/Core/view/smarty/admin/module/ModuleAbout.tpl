<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<style>
    .changelog {
        max-height: 400px;
        overflow: auto;
    }
</style>
<section id="module-about" itemprop="application" itemscope="" itemtype="http://schema.org/WebApplication">
    <section class="white">
        <div class="container">
            <div class="row xxsmall-push-updown-30">
                <div class="column xxsmall-12 medium-12 large-12 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                    <div class="bordered card v1">
                        <h2 class="card-title text-center">
                            <span itemprop="name"><{$moduleAbout->moduleName}></span>&nbsp;
                            <small itemprop="softwareVersion"><{$moduleAbout->moduleVersion}>.<{$moduleAbout->moduleBuildVersion}></small>
                        </h2>
                        <p class="text-center">
                            <span aria-label="Hersteller" itemprop="producer" itemscope="" itemtype="http://schema.org/Organization">
                                <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>&nbsp;
                                <a target="_blank" rel="nofollow" itemprop="url" href="https://www.amaryllis-systems.de/" title="Entwickelt und bereitgestellt von Amaryllis Systems GmbH">
                                    <span itemprop="name">Amaryllis Systems GmbH</span>
                                </a>
                            </span> | 
                            <span aria-label="Support">
                                <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>&nbsp;
                                <a target="_blank" rel="nofollow" href="mailto:support@amaryllis-systems.de?subject=Content%20Modul%20Support" title="Support-Anfrage stellen">
                                    Support kontaktieren
                                </a>
                            </span> | 
                            <span class="daysago" aria-label="Release-Datum">
                                <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>&nbsp;
                                <{$moduleAbout->moduleReleaseDate}>
                            </small>
                        </p>
                        <div class="tag-bar text-center">
                            <i aria-hidden="true" class="acms-icon acms-icon-tags"></i>
                            <{foreach $moduleAbout->moduleTags as $tag}>
                                <span class="tag v1 bg-info black-text" itemprop="keywords"><{$tag}></span>
                            <{/foreach}>
                        </div>
                        <div class="tab-panel-500">
                            <div class="update-check">
                                <div class="text-center" style="font-size: 3.5rem;">
                                    <span class="acms-icon-stack acms-icon-lg">
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-check acms-icon-stack-1x text-success"></i>
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-circle regular acms-icon-stack-2x text-success"></i>
                                    </span>
                                </div>
                                <div class="text-center">
                                    <span class="text-success" data-role="notice">Das Modul ist in der aktuellsten Version installiert</span>
                                </div>
                            </div>
                        </div>
                        <div class="text-center card-action button-toolbar compact clearfix">
                            <button class="button royal small" type="button" data-module="apps/ui/informations/modal" data-target="#modal-sponsors">
                                <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                                <span>Sponsoren</span>
                            </button>
                            <button class="button default small" type="button" data-module="apps/ui/informations/modal" data-target="#modal-changelog">
                                <i aria-hidden="true" class="acms-icon acms-icon-history"></i>
                                <span>Changelog</span>
                            </button>
                            <{foreach $moduleAbout['license'] as $k => $license}>
                                <button class="button default small" type="button" data-module="apps/ui/informations/modal" data-target="#modal-license-<{$k}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-gavel"></i>
                                    <span><{$license['name']}></span>
                                </button>
                            <{/foreach}>
                            <button class="button default small" type="button" data-module="apps/ui/informations/modal" data-target="#modal-people">
                                <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                                <span>Entwickler</span>
                            </button>
                            <button class="button default small" type="button" data-module="apps/ui/informations/modal" data-target="#modal-info">
                                <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
                                <span>Informationen</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal" id="modal-people" role="dialog" aria-hidden="true" aria-labeldby="modal-people-title" tabindex="-1">
        <div class="modal-dialog">
            <div class="content">
                <div class="heading">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Ok</span>
                    </button>
                    <h4 id="modal-people-title" class="text-theme">Entwickler und andere beteiligte Personen</h4>
                </div>
                <div class="main">
                    <div class="list v1 striped divided">
                        <{foreach $moduleAbout->author['developers'] as $author}>
                            <div class="item">
                                <i class="middle aligned large acms-icon acms-icon-user"></i>
                                <div class="content">
                                    <span class="heading">
                                        <span>
                                            <{$author['name']}>
                                        </span>&nbsp;
                                        <small>
                                            
                                        </small>
                                    </span>
                                    <div class="description">
                                        <a href="<{$moduleAbout->author['company']['url']}>"
                                                target="_blank"
                                                itemprop="url"
                                                title="<{translate id="Webseite des Entwicklers" textdomain="Core"}>">
                                            <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>
                                            <span><{$moduleAbout->author['company']['name']}></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            
                        <{/foreach}>
                    </div>
                </div>
                <div class="footer">
                    <button type="button" class="button primary small" data-dismiss="modal">
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modal-sponsors" role="dialog" aria-hidden="true" aria-labeldby="modal-sponsors-title" tabindex="-1">
        <div class="modal-dialog">
            <div class="content">
                <div class="heading">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Ok</span>
                    </button>
                    <h4 id="modal-sponsors-title" class="text-theme">Sponsoren des Moduls</h4>
                </div>
                <div class="main">
                    <{if $moduleAbout->people['sponsor']}>
                        <div class="list v1 striped divided">
                            <{foreach $moduleAbout->author['sponsor'] as $author}>
                                <div class="item" itemtype="http://schema.org/Organization">
                                    <div class="content">
                                        <span class="heading">
                                            <span>
                                                <{$author['name']}>
                                            </span>&nbsp;
                                            <small>
                                                
                                            </small>
                                        </span>
                                        <div class="description">
                                            <a href="<{$author['url']}>"
                                                    target="_blank"
                                                    itemprop="url"
                                                    title="<{translate id="Webseite des Sponsors" textdomain="Core"}>">
                                                <span><{$author['name']}></span>
                                            </a>
                                        </div>
                                    </div>
                                    
                                </div>
                            <{/foreach}>
                        </div>
                    <{else}>
                        
                    <{/if}>
                </div>
                <div class="footer">
                    <button type="button" class="button primary small" data-dismiss="modal">
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal large" id="modal-changelog" role="dialog" aria-hidden="true" aria-labeldby="modal-changelog-title" tabindex="-1">
        <div class="modal-dialog">
            <div class="content">
                <div class="heading">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Ok</span>
                    </button>
                    <h4 id="modal-changelog-title" class="text-theme">Changelog</h4>
                </div>
                <div class="main" itemprop="releaseNotes">
                    <{$moduleAbout['changelog']}>
                </div>
                <div class="footer">
                    <button type="button" class="button primary small" data-dismiss="modal">
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <{foreach $moduleAbout['license'] as $k => $license}>
    <div class="modal large" id="modal-license-<{$k}>" role="dialog" aria-hidden="true" aria-labeldby="modal-license-title-<{$k}>" tabindex="-1">
        <div class="modal-dialog">
            <div class="content">
                <div class="heading">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Ok</span>
                    </button>
                    <h4 id="modal-license-title-<{$k}>" class="text-theme"><{$license['name']}></h4>
                </div>
                <div class="main">
                    <{$license['content']}>
                </div>
                <div class="footer">
                    <button type="button" class="button primary small" data-dismiss="modal">
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <{/foreach}>

    <div class="modal large" id="modal-info" role="dialog" aria-hidden="true" aria-labeldby="modal-info-title" tabindex="-1">
        <div class="modal-dialog">
            <div class="content">
                <div class="heading">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">Ok</span>
                    </button>
                    <h4 id="modal-info-title" class="text-theme">Release Informationen</h4>
                </div>
                <div class="main">
                    <{$moduleAbout['releaseNotes']}>
                </div>
                <div class="footer">
                    <button type="button" class="button primary small" data-dismiss="modal">
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
