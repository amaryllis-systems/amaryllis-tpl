<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="card v1 hoverable"
     data-module="ui/components/cards"
     itemprop="application"
     itemscope=""
     itemtype="http://schema.org/SoftwareApplication"
     >
    <div class="card-image">
        <{* ACHTUNG: Module-Logos sind auch als Logos zu verstehen und damit Copyright-Protected. Die Darstellung der Logos darf nicht manipuliert werden! *}>
        <img src="<{$module.icon}>"
             title="<{$module.name}>"
             alt="<{$module.name}>"
             class="activator module-logo full <{$module['lowerDirname']}>"
             data-toggle="activator" />
    </div>
    <div class="card-content">
        <div class="card-title">
            <span itemprop="name"><{$module.name}></span>
        </div>
        <div class="subtitle text-uppercase" itemprop="producer" itemscope="" itemtype="http://schema.org/Organization">
            <a class="author-company"
                href="<{$module['author']['company']['url']}>"
                title="<{translate id="Besuchen Sie die Webseite des Entwicklers"}>"
                target="_blank"
                itemprop="url"
                >
                <div class="xxsmall inline image">
                    <img src="<{$module['author']['company']['logo']}>" alt="Logo" title="Logo">
                </div>
                <span itemprop="name">
                    <{$module['author']['company']['name']}>
                </span>
            </a>
        </div>
    </div>
    <div class="card-reveal">
        <h3 class="card-title">
            <{$module.name}>&nbsp;
            <small>
                <{$module.version}> (<{$module.dbversion}>)
            </small>
            <i class="right small acms-icon acms-icon-times-circle regular grey-text text-lighten-2" aria-label="<{translate id="Schließen" textdomain="Core"}>" aria-hidden="true" data-toggle="close"></i>
        </h3>
        <meta itemprop="applicationSuite" content="Amaryllis-CMS" />
        <span class="label v1 warning">
            <{translate id="Nicht installiert" textdomain="Core"}>
        </span>&nbsp;
        <{if $module['versionInfo']['status'] == 1 || !$module['versionInfo']['status']}>
            <span class="label v1 danger">Alpha</span>
        <{elseif $module['versionInfo']['status'] == 2}>
            <span class="label v1 danger">Beta</span>
        <{elseif $module['versionInfo']['status'] == 3}>
            <span class="label v1 warning">Release Candidate</span>
        <{elseif $module['versionInfo']['status'] == 10}>
            <span class="label v1 success">Final</span>
        <{/if}>
        <div class="module-tags">
            <p>
                <i aria-hidden="true" class="acms-icon acms-icon-tags">&nbsp;</i>
                <{foreach $module.tags as $tag}>
                    <span class="label v1 default"><{$tag}></span>
                <{/foreach}>
            </p>
        </div>
        <div class="module-author">
            <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>&nbsp;
            <span class="label v1 default"><{$module.releaseDate}></span>
        </div>
    </div>
    <div class="card-action">
        <a class="button tiny primary"
           href="<{$module.installLink}>"
           role="button"
           title="<{translate id='Installieren Sie dieses Modul' textdomain='Core'}>">
            <i aria-hidden="true" class="acms-icon acms-icon-install"></i>
            <span><{translate id='Installieren' textdomain='Core'}></span>
        </a>
    </div>
</div>
