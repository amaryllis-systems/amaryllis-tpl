<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
<{if $acmsContents->installedModules}>
    <div class="container fixed-1024 fixed-1200 fixed-1600" 
         data-module="apps/admin/module/modules"
         data-remote="<{generateUrl name="core_marketplace_action" parameters=['action' => 'action']}>"
         data-mremote="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'module', 'action' => 'action']}>" >
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="large page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-cubes text-muted text-xxxlarge"></i> 
                    <div class="content">
                        <h1 class="text-primary title">
                            <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                        </h1>
                        <p class="sub-title" itemprop="description"><{translate id="Verwalten Sie Ihre heruntergeladenen Module"}></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12 tab-container" data-module="ui/navs/tabs" data-effect="fadeInLeft" data-outeffect="fadeOutRight">

                <ul class="tabs tabs-justified large tabs-horizontal" role="tablist">
                    <li class="tab active">
                        <a
                            href="#updated-modules"
                            data-target="#updated-modules"
                            role="tab"
                            onclick="return false;"
                            title="<{translate id='Verfügbare Aktualisierungen' textdomain='Core'}>"
                        >
                            <span>
                                <{translate id='Aktualisierungen' textdomain='Core'}>
                            </span>
                        </a>
                    </li>
                    <li class="tab">
                        <a
                            href="#installed-modules"
                            role="tab"
                            data-target="#installed-modules"
                            onclick="return false;"
                            title="<{translate id='Installierte Module' textdomain='Core'}>"
                        >
                            <span>
                                <{translate id='Installierte Module' textdomain='Core'}>
                            </span> 
                            <span class="badge v1 primary">
                                <{$acmsContents['installedModules']|@count}>
                            </span>
                        </a>
                    </li>
                    <li class="tab">
                        <a
                            href="#uninstalled-modules"
                            data-target="#uninstalled-modules"
                            role="tab"
                            onclick="return false;"
                            title="<{translate id='Nicht installierte Module' textdomain='Core'}>"
                        >
                            <span>
                                <{translate id='Nicht installierte Module' textdomain='Core'}>
                            </span> 
                            <span class="badge v1 primary">
                                <{$acmsContents['uninstalledModules']|@count}>
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-panel v1 active" id="updated-modules" aria-label="Verfügbare Updates">
                        <h3>
                            <{translate id='Verfügbare Updates' textdomain='Core'}>
                            <span class="danger badge v1" data-role="updates-available"></span>
                        </h3>
                        <div id="updates-available">
                        </div>
                        <h3>
                            <{translate id='Zuletzt aktualisiert' textdomain='Core'}> 
                            <span class="badge v1 primary">
                                <{$acmsContents['updatedModules']|@count}>
                            </span>
                        </h3>
                        <{foreach $acmsContents['updatedModules'] as $module}>
                            <{include file="acmsfile:Core|admin/module/ModuleInstalled.tpl" module=$module}>
                        <{/foreach}>
                    </div> <!-- END updated Modules -->
                    
                    <div class="tab-panel" id="installed-modules" aria-labeledby="installed-modules-title">
                        <h3 id="installed-modules-title">
                            <{translate id='Installierte Module' textdomain='Core'}>
                        </h3>
                        <div data-remoteurl="<{$acmsContents['actionUrl']}>"
                            id="acms-sortable"
                            itemscope="" itemtype="http://schema.org/ControlAction"
                            class="installed-module-list"
                        >
                        <{foreach $acmsContents['installedModules'] as $module}>
                            <{include file="acmsfile:Core|admin/module/ModuleInstalled.tpl" module=$module}>
                        <{/foreach}>
                        </div>
                    </div>
                    <div class="tab-panel" id="uninstalled-modules">

                        <h3><{translate id='Nicht installierte Module' textdomain='Core'}></h3>
                        <div class="feed-collection v1 two-small-only five-large-up-only">
                            <{foreach $acmsContents['uninstalledModules'] as $module}>
                                <{include file="acmsfile:Core|admin/module/ModuleUninstalled.tpl" module=$module}>
                            <{/foreach}>
                        </div>
                    </div> <!-- END uninstalled Modules -->
                </div> <!-- END Tab List -->
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 medium-up-push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-shopping-cart text-muted"></i>
                        <span><{translate id="Module hinzufügen" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie können neue Module direkt über unseren Marktplatz beziehen und in Ihr System herunterladen oder die Module über den Reseller Ihrer Wahl beziehen. Dieser schaltet Ihnen dann die Module frei. Hier finden Sie alle Module, die auf Ihren Webserver heruntergeladen wurden." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar clearfix compact">
                        <a class="right button primary small" 
                           title="<{translate id="Benutzer anlegen" textdomain="Core"}>"
                           href="<{generateUrl name="core_marketplace"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-cart-plus"></i>
                            <span>Modul hinzufügen</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 medium-up-push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder wie Sie die Module konfigurieren können? Lesen Sie in unserer Dokumentation alles wichtige über Module, wie Sie Module beziehen und das Modul über die Modul-Verwaltung angleichen können." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/modul-verwaltung/"
                           target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<{elseif $acmsContents['confirmInstallation']}>
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12 medium-6 medium-offset-3 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                <h1><{$acmsContents->get('acmsTitle')}></h1>
                <{$acmsContents->get('confirmInstallation')}>
            </div>
        </div>
    </div>
<{elseif $acmsContents['installResult']}>
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12 medium-6 medium-offset-3 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                <h1><{$acmsContents->get('acmsTitle')}></h1>
                <div class="result">
                    <ul class="list-unstyled">
                        <{foreach $acmsContents['installResult'] as $message}>
                            <li class="text-<{$message['type']}>"><{$message['message']}></li>
                        <{/foreach}>
                    </ul>
                </div>
                <div class="button-toolbar">
                    <{if $acmsContents['newModuleUrl']}>
                    <div class="button-group">
                        <a class="button primary" title="<{translate id='Zurück' textdomain='Core'}>" href="<{$acmsContents['newModuleUrl']}>">
                            <{translate id='Gehe zu %s' textdomain='Core' args=$acmsContents['dirname']}>
                        </a>
                    </div>
                    <{/if}>
                    <div class="button-group">
                        <a class="button button-default" 
                           title="<{translate id='Zurück' textdomain='Core'}>"
                           href="<{generateUrl name="core_admin_pages" parameters=['module' => 'core', "type" => "module"]}>">
                            <{translate id='Zurück' textdomain='Core'}>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<{/if}>
</section>
