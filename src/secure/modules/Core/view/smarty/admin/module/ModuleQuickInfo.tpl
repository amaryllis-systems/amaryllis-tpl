<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal" id="module-info-<{$module.dirname}>" tabindex="-1" role="dialog" aria-labelledby="module-info-<{$module.dirname}>-title" aria-hidden="true">
    <div class="modal-dialog<{if !$acmsData->deviceIsMobile}> large<{/if}>">
        <div class="content">
            <div class="header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                </button>
                <h3 class="title" id="module-info-<{$module.dirname}>-title">
                    <{$module.name}>
                    <small> - (<{$module.dirname}>)</small>
                </h3>
            </div>
            <div class="main text-left">
                <div class="left floated module-quickinfo-icon">
                    <img class="image responsive rounded module-logo large" src="<{$module.icon}>" />
                </div>
                <div class="module-description">
                    <p class="lead"><{$module.description}></p>
                </div>
                <{if $module.requiredModules}>
                    <div class="required-modules">
                    
                        <h4 class="text-danger">
                            <{translate id="Erforderliche Module" textdomain="Core"}>
                        </h4>
                        <ul class="list-unstyled">
                            <{foreach $module.requiredModules as $requirement}>
                                <{if is_array($requirement)}>
                                    <li>
                                        <a class="text-danger" href="<{$requirement['url']}>" title="<{translate id="mehr erfahren" textdomain="Core"}>" target="_blank">
                                            <i class="acms-icon acms-icon-exclamation"></i>&nbsp;
                                            <span><{$requirement['name']}></span>
                                        </a>
                                    </li>
                                <{else}>
                                    <li>
                                        <i class="acms-icon acms-icon-exclamation"></i>&nbsp;
                                        <span><{$requirement}></span>
                                    </li>
                                <{/if}>
                            <{/foreach}>
                        </ul>
                    </div>
                <{/if}>
                <{if $module.recommendedModules}>
                    <div class="required-modules">
                        <h4>
                            <{translate id="Empfohlene Module" textdomain="Core"}>
                        </h4>
                        <ul class="list-unstyled">
                            <{foreach $module.recommendedModules as $recommand}>
                                <li>
                                    <span class="acms-icon <{if isset($modules[$recommand])}> acms-icon-check<{else}> acms-icon-info<{/if}>"></span>&nbsp;
                                    <{$recommand}>
                                </li>
                            <{/foreach}>
                        </ul>
                    </div>
                <{/if}>
            </div>
            <div class="footer">
                <button type="button" class="button button-default" data-dismiss="modal">
                    <{translate id="Schließen" textdomain="Core"}>
                </button>
            </div>
        </div>
    </div>
</div>
