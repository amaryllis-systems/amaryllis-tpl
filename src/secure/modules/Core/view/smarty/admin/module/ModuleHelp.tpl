<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container container full">
    <div class="row" data-module="ui/navs/toc">
        <div class="column xxsmall-12 large-8" data-toggle="toc">
            <{$acms_manual}>
        </div>
        <div class="column xxsmall-12 large-4">
            <div class="toc-wrapper">
                <div id="acms-toc"></div>
            </div>
        </div>
    </div>
</div>
