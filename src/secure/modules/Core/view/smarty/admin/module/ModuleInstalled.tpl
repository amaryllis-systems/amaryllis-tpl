<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div itemprop="target" itemscope="" itemtype="http://schema.org/EntryPoint"
     data-name="<{$module['name']}>"
     data-dirname="<{$module['dirname']}>"
     data-version="<{$module['version']}>"
     data-build="<{$module['dbversion']}>">

    <div class="module-card v1 hoverable z-depth-1 installed-module module-list <{if !$module['active']}>warning<{/if}>"
         itemprop="application"
         itemscope=""
         itemtype="http://schema.org/SoftwareApplication">
        <div class="card-inner">
            <figure>
                <img src="<{$module['icon']}>"
                     title="<{$module['name']}>"
                     alt="<{$module['name']}>"
                     itemprop="image"
                     class="activator module-logo full <{$module['lowerDirname']}>">
                
                <figcaption>
                    <h3 class="text-center"><{$module['dirname']}></h3>
                    <p class="text-center">Version <span itemprop="softwareVersion"><{$module['version']}>.<{$module['dbversion']}></span></p>
                </figcaption>
            </figure>
            <div class="card-content">
                <div class="card-title">
                    <h3><span itemprop="name"><{$module['name']}></span> <small><{$module['dirname']}></small></h3>
                </div>
                <div class="provider">
                    <a class="author-company"
                       href="<{$module['author']['company']['url']}>"
                       title="<{translate id="Besuchen Sie die Webseite des Entwicklers"}>"
                       target="_blank"
                       itemprop="url"
                       >
                       <div class="xxsmall inline image">
                        <img src="<{$module['author']['company']['logo']}>" alt="Logo" title="Logo">
                       </div>
                        <span itemprop="name">
                            <{$module['author']['company']['name']}>
                        </span>
                    </a>
                </div>
                <p>
                    <span class="tag v1 large">
                        <span class="tag-image">
                            <i aria-hidden="true" class="acms-icon acms-icon-folder"></i> 
                        </span>
                        <span>
                            <{$module['dirSize']}>
                        </span>
                    </span>
                    <span class="tag v1 large">
                        <span class="tag-image">
                            <i aria-hidden="true" class="acms-icon acms-icon-cloud-upload-alt"></i> 
                        </span>
                        <span>
                            <{$module['uploadSize']}>
                        </span>
                    </span>
                    <span class="tag v1 large" data-module="apps/ui/informations/tooltip" data-title="Anzahl Datenbank-Tabellen und Gesamtgröße">
                        <span class="tag-image">
                            <i class="acms-icon acms-icon-database"></i>
                        </span>
                        <span>
                            <{$module['tablesTotal']}> (<{$module['tablesSize']}>)
                        </span>
                    </span>
                    <span class="tag v1 large">
                        <span class="tag-image">
                            <i class="acms-icon acms-icon-calendar-alt regular"></i>
                        </span>
                        <span>
                            <{$module['releaseDate']}>
                        </span>
                    </span>
                    <meta itemprop="applicationSuite" content="Amaryllis-CMS" />
                    <{if !$module['active']}>
                        <span class="label v1 warning"><{translate id="Inaktiv" textdomain="Core"}></span>
                    <{else}>
                        <span class="label v1 success"><{translate id="Aktiv" textdomain="Core"}></span>
                    <{/if}>
                    <{if $module['versionInfo']['status'] == 1 || !$module['versionInfo']['status']}>
                        <span class="label v1 danger">Alpha</span>
                    <{elseif $module['versionInfo']['status'] == 2}>
                        <span class="label v1 danger">Beta</span>
                    <{elseif $module['versionInfo']['status'] == 3}>
                        <span class="label v1 warning">Release Candidate</span>
                    <{elseif $module['versionInfo']['status'] == 10}>
                        <span class="label v1 success">Final</span>
                    <{/if}>
                </p>
                <div class="tag-bar">
                    <i aria-hidden="true" class="acms-icon acms-icon-tags"></i>
                    <{foreach $module['tags'] as $tag}>
                        <span class="tag v1 default">
                            <span class="tag-image">
                                <i aria-hidden="true" class="acms-icon acms-icon-tag"></i>
                            </span>
                            <span><{$tag}></span>
                        </span>
                    <{/foreach}>
                </div>
                <div class="card-action">
                    <{if $module['dirname'] != 'Core' && $module['versionInfo']['hasMain']}>
                        <a class="button tiny highlight" href="<{$module['itemUrl']}>" title="Das Modul aufrufen">
                            <i aria-hidden="true" class="acms-icon acms-icon-link"></i>
                            <span class="src-only">Zum Modul</span>
                        </a>
                    <{/if}>
                    <a class="button tiny info module-info"
                       href="#module-info-<{$module['dirname']}>"
                       data-target="#module-info-<{$module['dirname']}>"
                       data-module="apps/ui/informations/modal"
                       title="<{translate id='Kurz-Info' textdomain='Core'}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
                        <span class="src-only"><{translate id='Modul-Beschreibung' textdomain='Core'}></span>
                    </a>
                    <a class="button tiny primary module-edit"
                       href="#module-edit-<{$module['id']}>"
                       data-target="#module-edit-<{$module['id']}>"
                       data-module="apps/ui/informations/modal"
                       title="<{translate id='Modul bearbeiten' textdomain='Core'}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                        <span class="src-only"><{translate id='Bearbeiten' textdomain='Core'}></span>
                    </a>
                    <{if $module['dirname'] != 'Core' && $module['versionInfo']['hasAdmin']}>
                        <a class="button tiny royal module-dashboard"
                           href="<{generateUrl name="core_admin_pages" parameters=['module' => $module['alias']]}>"
                           title="<{translate id='Modul Administration' textdomain='Core'}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-tachometer-alt "></i>
                            <span class="src-only"><{translate id='Administratives Dashboard' textdomain='Core'}></span>
                        </a>
                    <{/if}>
                </div>
                <div class="card-shortcuts">
                    <ul class="list-inline list-unstyled">
                        <{if $module['upgradeLink']}>
                            <li>
                                <a class="module-upgrade button tiny royal"
                                   role="button"
                                   href="<{$module['upgradeLink']}>"
                                   title="<{translate id='Modul Upgrade' textdomain='Core'}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-certificate"></i>
                                    <span class="src-only"><{translate id="Upgrade" textdomain="Core"}></span>

                                </a>
                            </li>
                        <{/if}>
                        <{if $module['updateLink']}>
                            <li>
                                <a class="module-update primary button tiny"
                                   href="<{$module['updateLink']}>"
                                   title="<{translate id='Modul Update' textdomain='Core'}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-sync-alt"></i>
                                    <span class="src-only"><{translate id="Update" textdomain="Core"}></span>
                                </a>
                            </li>
                        <{/if}>
                        <{if $module['aboutLink']}>
                            <li>
                                <a class="info module-about icon button"
                                   href="<{$module['aboutLink']}>"
                                   title="<{translate id='Alles über das Modul' textdomain='Core'}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular"></i>
                                </a>
                            </li>
                        <{/if}>
                        <{if $module['uninstallLink']}>
                            <li>
                                <a class="danger module-uninstall icon button"  href="<{$module['uninstallLink']}>" title="<{translate id='Deinstallieren' textdomain='Core'}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                </a>
                            </li>
                        <{/if}>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/module/ModuleQuickInfo.tpl" module=$module}>
    <{include file=$module['edit']['template'] form=$module['edit']}>
</div>
