<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="admin-section uninstall-module pull-updown-30">
    <{if $acmsContents['confirmMessage']}>
        <div class="container fixed-1200">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="large page-title v7">
                        <i aria-hidden="true" class="acms-icon acms-icon-unlink text-xlarge"></i>
                        <div class="content">
                            <h1 class="title text-primary"><{$acmsContents->get('acmsTitle')}></h1>
                            <p class="sub-title"><{translate id="Hier können Sie das Modul %s deinstallieren." args=[$acmsContents->dirname]}></p>
                        </div>
                    </div>
                    <{if $acmsContents->isPost}>
                        <div class="push-10 push-updown-20">
                            <div class="alert v3 danger size-500 center-block">
                                <h2 class="alert-title">
                                    Achtung
                                </h2>
                                <div class="alert-content">
                                    <p class="lead">
                                        Sie müssen der Deinstallation zustimmen, um das Modul entfernen zu können!
                                    </p>
                                </div>
                            </div>
                        </div>
                    <{/if}>
                    <{$acmsContents->get('confirmMessage')}>
                </div>
            </div>
        </div>
    <{elseif $acmsContents['result']}>
        <div class="container fixed-1200">
            <div class="row full">
                <div class="column xxsmall-12 medium-6 medium-offset-3 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                    <div class="page-title">
                        <h1><{$acmsContents->get('acmsTitle')}></h1>
                        <p><{translate id="Hier können Sie das Ergebnis der Modul-Deinstallation %s einsehen." args=[$acmsContents->dirname]}></p>
                    </div>
                    <div class="result">
                        <ul class="list-unstyled">
                            <{foreach $acmsContents['result'] as $message}>
                                <li class="text-<{$message['type']}>"><{$message['message']}></li>
                                <{/foreach}>
                        </ul>
                    </div>
                    <div class="button-toolbar compact">
                        <div class="button-group">
                            <a class="button default small" 
                               title="<{translate id='Zurück' textdomain='Core'}>"
                               href="<{generateUrl name="core_admin_pages" parameters=['module' => 'core', "type" => "module"]}>">
                                <{translate id='Zurück' textdomain='Core'}>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <{/if}>
</section>
