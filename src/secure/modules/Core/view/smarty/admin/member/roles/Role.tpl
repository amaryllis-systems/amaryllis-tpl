<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="column xxsmall-12 small-12 medium-10 medium-offset-1 large-10 large-offset-1 xlarge-10 xlarge-offset-1">
    <div class="card v1" data-module="ui/components/cards">
        <div class="card-title text-center">
            <h3>
                <span><{$role.title}></span>
            </h3>
        </div>
        <div class="card-content">
            <p><{$role.description}></p>

            <div class="row full">
                <div class="column medium-6 large-6 xlarge-6 xxlarge-6">
                    <div class="display-block percentage-80 center-block">
                        <div data-actionurl="<{$acmsContents['actionUrl']}>"
                             class="collection v1 call-to-action v2"
                             data-role="<{$role.id}>"
                             data-type="selectable"
                             data-toggle="actionlistgroup">
                            <div class="collection-header">
                                <span class="text-bold"><{translate id="Auswählbar von" textdomain="Core"}></span>
                            </div>
                            <{foreach $acmsContents['groups'] as $gid => $group}>
                                <div data-groupid="<{$gid}>" class="collection-item <{if in_array($gid, $role.selectable_by)}> active<{/if}>">
                                    <{$group}>
                                </div>
                            <{/foreach}>
                        </div>
                    </div>
                </div>
                <div class="column medium-6 large-6 xlarge-6 xxlarge-6">
                    <div class="display-block percentage-80 center-block">
                        <div data-actionurl="<{$acmsContents['actionUrl']}>"
                             class="collection v1 call-to-action v2"
                             data-role="<{$role.id}>"
                             data-type="visible"
                             data-toggle="actionlistgroup"
                             >
                            <div class="collection-header">
                                <span class="text-bold"><{translate id="Sichtbar für" textdomain="Core"}></span>
                            </div>
                            <{foreach $acmsContents['groups'] as $gid => $group}>
                                <div data-groupid="<{$gid}>" class="collection-item <{if in_array($gid, $role.groups)}> active<{/if}>">
                                    <{$group}>
                                </div>
                            <{/foreach}>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-action">
            <div class="button-toolbar">
                <{if !$role['system'] && $role['deleteUrl']}>
                    <div class="button-group">
                        <a class="button danger tiny" role="button" href="<{$role['deleteUrl']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                        </a>
                    </div>
                <{/if}>
                <div class="button-group">
                    <a class="button primary tiny" 
                       role="button"
                       title="<{translate id="Bearbeiten" textdomain="Core"}>"
                       href="<{$role['editUrl']}>"
                       data-module='apps/ui/informations/modal'
                       data-target="#member-edit-role-<{$role['id']}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/member/roles/RolesEdit.tpl" form=$role['form'] role=$role}>
</div>
