<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage" class="pull-updown-30">
    <div class="container full" data-cssfile="media/css/components/cards.min.css">
        <div class="row xxsmall-pushup-15">
            <div class="page-title">
                <h1>
                    <i aria-hidden="true" class="acms-icon acms-icon-userss"></i>
                    <span itemprop="name"><{$acmsContents['acmsTitle']}></span>
                </h1>
                <p itemprop="description" class="lead">
                    Bearbeiten oder erstellen Sie Benutzer-Gruppen, ändern Sie die Profil-Berechtigungen für jede Benutzergruppe.
                </p>
            </div>
        </div>
        <div class="row xxsmall-push-updown-15" data-module="ui/components/equalcolumns" data-toggle="equalcolumns" data-tallest="true" data-selector=".equal-column .card-content">
            <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                <div class="cards pull-updown-30">
                    <{foreach $acmsContents['groups'] as $group}>
                        <{include file="acmsfile:Core|admin/member/groups/Group.tpl"}>
                    <{/foreach}>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-15">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div id="groups-info-box" class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Information"}></span>
                    </h2>
                    <p>Fügen Sie neue Benutzergruppen hinzu, bearbeiten Sie vorhandene Benutzergruppen. Benutzer-Gruppen dienen dem Fein-Tuning von Benutzer-Berechtigungen. Nutzen Sie aber immer nur so viele Gruppen, wie Sie wirklich brauchen. Nachdem Sie Gruppen erstellt haben, gehen Sie zu den Gruppen-Berechtigungen und weisen Sie der Gruppe die benötigten Berechtigungen zu.</p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'member', 'item' => 'groups', 'action' => 'neu']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                            <span><{translate id="Benutzer-Gruppe hinzufügen"}></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach was Benutzer sind und wie Sie diese Einrichten können. Erfahren Sie, wie Sie die passenden Gruppen setzen und was der Unterschied zwischen den Benutzer-Typen ist." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           target="_blank"
                           href="https://www.amaryllis-support.de/core/administration/benutzer-management/benutzer-gruppen/">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-action-button">
        <a role="button"
           class="button floating large red"
           href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'member', 'item' => 'groups', 'action' => 'neu']}>"
           title="<{translate id="Eine neue Benutzer-Gruppe erstellen" textdomain="Core"}>">
            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
            <span class="src-only"><{translate id="Neue Benutzer-Gruppe" textdomain="Core"}></span>
        </a>
    </div>
</section>
