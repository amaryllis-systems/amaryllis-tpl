<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
<div class="container">
    <div class="row">
        <div class="column xxsmall-12">
            <div class="xxxlarge center aligned icon page-title v7">
                <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                <h1>
                    <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                </h1>
                <p class="sub-title" itemprop="description">
                    <{translate id="Verwalten Sie Ihre Benutzer. Jeder Benutzer mit Ausnahme von System-Benutzern kann sich im Web Portal anmelden." textdomain="Core"}>
                </p>
            </div>
        </div>
    </div>
    
    <div class="row xxsmall-push-updown-30">
        <div class="column xxsmall-12 text-center">
            <ul class="list-alphabetical v1">
                <{foreach $acmsContents['letterlist'] as $letter}>
                    <li class="letter <{$letter['value']}><{if $letter['value'] == $acmsContents['currentLetter']}> active<{/if}>">
                        <a
                            href="<{$letter['url']}>"
                            title="<{$letter['value']}>"
                            class="list-letter">
                            <{$letter['value']}>
                        </a>
                    </li>
                <{/foreach}>
            </ul>
        </div>
    </div>
    <{*Ü<div class="row">
        <pre>
        <{dump var=$acmsContents['filterBar']}>
        </pre>
    </div>*}>
    <div class="row pull-updown-30">
        <div class="column xxsmall-12">
            <{if $acmsContents['users']}>
                <div class="centered cards">
                <{foreach $acmsContents['users'] as $user}>
                    <{include file="acmsfile:Core|admin/member/user/User.tpl" user=$user}>
                <{/foreach}>
                </div>
            <{else}>
                <div class="card v2 bg-red white-text">
                    <div class="card-content">
                        <div class="card-title white-text">
                            <{translate id="Kein Ergebnis"}>
                        </div>
                        <p><{translate id="Es wurden keine Benutzer mit den Kriterien gefunden"}></p>
                    </div>
                </div>
            <{/if}>
        </div>
    </div>
    <div class="row">
        <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-user-plus text-muted"></i>
                    <span><{translate id="Benutzer anlegen" textdomain="Core"}></span>
                </h2>
                <p>
                    <{translate id="Legen Sie neue Benutzer an. Der Benutzer kann sich dann im Portal mit dem erstellten Profil anmelden. Benutzer, die ein Administrator zufügt brauchen keine Bestätigung mehr der E-Mail Adresse und werden ab Beginn als valider Benutzer angesehen. erstellen Sie keine Benutzer, ohne sich über die Identität zu informieren." textdomain="Core"}>
                </p>
                <div class="button-toolbar clearfix compact">
                    <a class="right button primary small" 
                       title="<{translate id="Benutzer anlegen" textdomain="Core"}>"
                       href="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'member', 'item' => 'user', 'action' => 'neu']}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-user-plus"></i>
                        <span>Benutzer anlegen</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                    <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                </h2>
                <p>
                    <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach was Benutzer sind und wie Sie diese Einrichten können. Erfahren Sie, wie Sie die passenden Gruppen setzen und was der Unterschied zwischen den Benutzer-Typen ist." textdomain="Core"}>
                </p>
                <div class="button-toolbar compact clearfix">
                    <a class="button small primary right" 
                       href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/benutzer-management/benutzer-rollen/"
                       target="_blank">
                        <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                        <span>Dokumentation</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
