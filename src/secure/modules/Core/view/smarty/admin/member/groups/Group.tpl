<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="card v2">
    <{if $group.deleteUrl}>
        <div class="no-space extra card-content">
        <a class="right floated small no-border avatar"
           href="<{$group.deleteUrl}>"
           title="<{translate id="Lösche %s" textdomain="Core" args=$group.title}>"
           >
            <i aria-hidden="true" class="acms-icon acms-icon-trash acms-icon-border danger text-danger"></i>
        </a>
        </div>
    <{/if}>
    <div class="card-content">
        <div class="card-title">
            <{$group.title}> (ID: <{$group['id']}>)&nbsp;
            <span class="right floated badge cursor-pointer">
                <span class="src-only">
                    <{translate id="Die Gruppe %s hat derzeit %s Mitglieder" textdomain="Core" args=[$group['title'], $group['members']]}>
                </span>
                <{$group['members']}>
            </span>
        </div>
        <div class="card-meta">
            <{$group['description']}>
        </div>
    </div>
    <div class="no-space extra card-content">
        <div class="two bottom plugged no-space buttons">
            <{if $group.editUrl}>
                <a class="button small info"
                   href="<{$group.editUrl}>"
                   title="<{translate id="Bearbeite %s" textdomain="Core" args=$group.title}>"
                   >
                    <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                    <span>Bearbeiten</span>
                </a>
            <{/if}>

            <{if $group.permUrl}>
                <a class="small primary button"
                   href="<{$group.permUrl}>"
                   title="<{translate id="Profil-Berechtigungen der Gruppe %s bearbeiten" textdomain="Core" args=$group.title}>"
                   >
                    <i aria-hidden="true" class="acms-icon acms-icon-lock"></i>
                    <span>Profil</span>
                </a>
            <{/if}>
        </div>
    </div>
</div>
