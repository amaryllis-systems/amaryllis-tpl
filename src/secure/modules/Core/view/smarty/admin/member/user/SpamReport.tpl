<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <span class="acms-icon-stack">
                            <i aria-hidden="true" class="acms-icon acms-icon-address-card v1acms-icon-stack-1x"></i>
                            <i aria-hidden="true" class="acms-icon acms-icon-ban acms-icon-stack-2x text-danger"></i>
                        </span>
                        <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <{if $acmsContents['spamreports']}>
                <{foreach $acmsContents['spamreports'] as $spamreport}>
                    <div class="column xxsmall-12">
                        <h2><{translate id="Spamreport" textdomain="Core"}> <{$spamreport['spamuser']['username']}></h2>
                        <{include file="acmsfile:Core|admin/member/user/User.tpl" user=$spamreport['spamuser']}>
                    </div>
                <{/foreach}>
            <{else}>
                <div class="column xxsmall-12 medium-12 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                    <div class="content-block v6 push-10">
                        <h2>
                            <i aria-hidden="true" class="acms-icon acms-icon-check-circle regular text-success"></i>
                            <span><{translate id="Kein Ergebnis"}></span>
                        </h2>
                        <p><{translate id="Es wurden keine Benutzer mit den Kriterien gefunden" textdomain="Core"}></p>
                    </div>
                </div>
            <{/if}>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Spam Benutzer" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Benutzer, die von anderen Benutzern als Spam gemeldet/markiert wurden. Sie haben die Wahl - Verbannen Sie die Benutzer oder löschen Sie die Spam-Markierung. Bevor Sie eine Markierung löschen, lesen Sie, warum der Benutzer gemedet wurde und besuchen Sie auch das Benutzer-Profil um die Aktivitäten des Nutzers anzusehen!"}>
                    </p>
                    
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           href="<{generateUrl name="core_admin_pages" parameters=['module' => 'core', 'type' => 'member', 'item' => 'user']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                            <span><{translate id="Zurück zur Benutzer-Übersicht"}></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach was Benutzer sind und wie Sie diese Einrichten können. Erfahren Sie, wie Sie die passenden Gruppen setzen und was der Unterschied zwischen den Benutzer-Typen ist." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           target="_blank"
                           href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/benutzer-management/benutzer/">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
