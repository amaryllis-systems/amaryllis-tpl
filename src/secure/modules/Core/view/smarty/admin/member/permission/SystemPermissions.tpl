<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div class="container">
        <div class ="row">
            <div class="column xxsmall-12">
                <!-- general admin page title -->
                <div class="page-title">
                    <h1>
                        <i aria-hidden='true' class="acms-icon acms-icon-lock text-muted"></i>
                        <span itemprop="name"><{$acmsContents['acmsTitle']}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        System-Berechtigungen für die einzelnen Benutzer-Gruppen.
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden='true' class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                        <span>System-Berechtigungen setzen</span>
                    </h2>
                    <p>
                        Gleichen Sie die Berechtigungen sinnvoll an und bedenken Sie prinzipiell die Risiken, wenn Sie Benutzern oder Gästen Aktionen freischalten! Stellen Sie die Freigaben immer gut Durchdacht ein.
                    </p>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden='true' class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span>Lesen Sie die Dokumentation</span>
                    </h2>
                    <p>
                        Systemberechtigungen sind ein sehr sensibler Bereich, der nur von Experten bearbeitet werden sollte. 
                        Wir empfehlen, die Dokumentation vorher ausführlich zu studieren und an einem Trainings-Kurs zum Thema "Benutzer und Berechtigungen" 
                        teilzunehmen.
                    </p>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <!-- Admin Form -->
            <{if $acmsContents['acms_form']}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <{include file=$acmsContents['acms_form']['template'] form=$acmsContents['acms_form']}>
                </div>
            <{elseif $acmsContents['acmsForm']}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <{include file=$acmsContents['acmsForm']['template'] form=$acmsContents['acmsForm']}>
                </div>
            <{elseif $acmsContents['message']}>
                <div class="column xxsmall-12 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
                    <{$acmsContents->message}>
                </div>
            <{/if}>
        </div>
    </div>
</section>
