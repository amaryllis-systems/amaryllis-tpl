<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="card v2"
     itemscope=""
     itemtype="http://schema.org/Person"
     data-module="ui/components/cards">
    <div class="card-content">
        <div class="right floated avatar" data-toggle="activator">
            <img src="<{$user.avatar}>">
        </div>
        <div class="card-title" data-toggle="activator">
            <span itemprop="alternateName"><{$user.username}></span>
        </div>
        <div class="card-meta">
            <{if $user.level == 8}>
                <span class="xxsmall label v2 info">
                    <{translate id="System Benutzer" textdomain="Core"}>
                </span>
            <{elseif $user.level == 7}>
                <span class="xxsmall label v2 primary">
                    <{translate id="System Admin" textdomain="Core"}>
                </span>
            <{elseif $user.level == 3}>
                <span class="xxsmall label v2 success">
                    <{translate id="Aktiv" textdomain="Core"}>
                </span>
            <{elseif $user.level == 2}>
                <span class="xxsmall label v2 warning">
                    <{translate id="Inaktiv" textdomain="Core"}>
                </span>
            <{elseif $user.level == 1}>
                <span class="xxsmall label v2 danger">
                    <{translate id="Gebannt" textdomain="Core"}>
                </span>
            <{elseif $user.level == 0 || !$user.level}>
                <span class="xxsmall label v2 default">
                    <{translate id="Gelöscht" textdomain="Core"}>
                </span>
            <{/if}>
        </div>
    </div>
    <div class="extra card-content">
        <small><{$user.first_name}> <{$user.last_name}></small> 
        <span class="default xxsmall label v2">
            ID: <{$user['id']}>
        </span>
    </div>

    <div class="card-reveal">

        <i class="right floated acms-icon acms-icon-times-circle regular red-text cursor-pointer text-lighten-2" aria-label="<{translate id="Schließen" textdomain="Core"}>" aria-hidden="true" data-toggle="close"></i>

        <ul class="small divided list">
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-at"></i>
                <div class="content">
                    <div class="heading"><{translate id="E-Mail" textdomain="Core"}></div>
                    <div class="description">
                        <a class="text-small" href="<{getMailto email=$user.email_raw}>">
                            <{$user.email_raw}>
                        </a>
                    </div>
                </div>
            </li>
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
                <div class="content">
                    <div class="heading">
                        <{translate id="Name" textdomain="Core"}>
                    </div>
                    <div class="description">
                        <{$user.last_name}>, <{$user.first_name}>
                    </div>
                </div>
            </li>
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-user-lock"></i>
                <div class="content">
                    <div class="heading">
                        <{translate id="Login-Name" textdomain="Core"}>
                    </div>
                    <div class="description">
                        <{$user.login_name}>
                    </div>
                </div>
            </li>
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-calendar-check"></i>
                <div class="content">
                    <div class="heading">
                        <{translate id="Registrierungs-Datum" textdomain="Core"}>
                    </div>
                    <div class="description">
                        <{$user['registration_date-simple']}>
                    </div>
                </div>
            </li>
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-user-clock"></i>
                <div class="content">
                    <div class="heading">
                        <{translate id="Letzte Aktivität" textdomain="Core"}>
                    </div>
                    <div class="description">
                        <{$user['last_activity-short']}>
                    </div>
                </div>
            </li>
            <li class="item">
                <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                <div class="content">
                    <div class="heading">
                        <{translate id="Zuletzt aktualisiert am" textdomain="Core"}>
                    </div>
                    <div class="description">
                        <{$user['last_updated-short']}>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="extra card-content">
        <div class="six buttons" role="toolbar" aria-label="Benutzer-Links">
            <a class="icon button primary"
               title="<{$user.visitProfile}>"
               href="<{$user.itemUrl}>"
               >
                <i aria-hidden="true" class="acms-icon acms-icon-user-circle"></i>
            </a>
            <a class="icon button secondary"
               title="<{translate id="Profil bearbeiten" textdomain="Core"}>"
               href="<{$user.editUrl}>"
               >
                <i aria-hidden="true" class="acms-icon acms-icon-user-edit"></i>
            </a>
            <a class="icon button inverse"
               title="<{translate id="Profil-Einstellungen bearbeiten" textdomain="Core"}>"
               href="<{$user.configUrl}>"
               >
                <i aria-hidden="true" class="acms-icon acms-icon-user-cog"></i>
            </a>
            <{if $user.deleteUrl}>
                <a class="icon button danger"
                   title="<{translate id='Löschen' textdomain='Core'}>"
                   href="<{$user.deleteUrl}>"
                   >
                    <i aria-hidden="true" class="acms-icon acms-icon-user-times"></i>
                </a>
            <{/if}>
        </div>
    </div>
    <{if $user.banUrl}>
        <button class="default bottom plugged icon button"
                type="button"
                data-remote="<{$user.banUrl}>"
                data-op="ban"
                data-module="view/profile/actions/ban-action"
                aria-label="<{translate id='verbannen' textdomain='Core'}>"
                >
            <span class="acms-icon acms-icon-stack">
                <i aria-hidden="true" class="acms-icon acms-icon-user acms-icon-stack-1x"></i>
                <i aria-hidden="true" class="acms-icon acms-icon-ban acms-icon-stack-2x text-danger"></i>
            </span>
            <span class="content"><{translate id='verbannen' textdomain='Core'}></span>
        </button>
    <{elseif $user.unbanUrl}>
        <button class="default bottom plugged icon button"
                type="button"
                data-remote="<{$user.unbanUrl}>"
                data-op="unban"
                data-module="view/profile/actions/ban-action"
                aria-label="Benutzer reaktivieren"
                >
            <span class="acms-icon acms-icon-stack">
                <i aria-hidden="true" class="acms-icon acms-icon-user acms-icon-stack-1x"></i>
                <i aria-hidden="true" class="acms-icon acms-icon-circle regular acms-icon-stack-2x text-success"></i>
            </span>
            <span class="content"><{translate id='reaktivieren' textdomain='Core'}></span>
        </button>
    <{/if}>
</div>
