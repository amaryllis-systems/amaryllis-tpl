<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-30">
    <div class="container">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-folder text-muted"></i>
                        <span itemprop="name"><{$acmsContents['acmsTitle']}></span>
                    </h1>
                    <p class="lead" itemprop="description">
                        <{translate id="Verwalten Sie die Profil-Kategorien und enthaltene Profil-Felder"}>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container full">
        <div class="row xxsmall-push-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                        <span>Die Profil-Kategorien</span>
                    </h2>
                    <p>
                        Bei der Präsentation von Profil-Feldern und Profil-Informationen 
                        werden diese in die hier aufgezeigten Kategorien gruppiert. 
                        Legen Sie neue Kategorien an oder entfernen Sie bestehende 
                        Kategorien. Ausschließlich die Administrative Kategorie muss 
                        erhalten bleiben Diese sollte immer nur Profil-Administratoren 
                        zugänglich gemacht werden.
                    </p>
                    <div class="button-toolbar clearfix compact">
                        <button class="button primary small right"
                                data-module="apps/ui/informations/modal"
                                data-target="#add-category-form"
                                data-toggle="modal" 
                                aria-label="<{translate id="Erstellen Sie einen neuen Registrierungs-Schritt" textdomain="Core"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                            <span>
                                <{translate id="Neue Profil-Kategorie" textdomain="Core"}>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Sie brauchen Hilfe?"}></span>
                    </h2>
                    <p>
                        Lesen Sie unsere Dokumentation und erfahren Sie, wie Sie die 
                        Profil-Kategorien einrichten, was die Änderungen bedeuten 
                        und wie sich die Änderungen im Layout bemerkbar machen. 
                        Erfahren Sie auch mehr über die administrative Profil-
                        Kategorie und ihre Bedeutung im System.
                    </p>
                    <div class="button-toolbar clearfix compact">
                        <a class="button primary small right" href="https://www.amaryllis-dokumentation.de" title="<{translate id="Lesen Sie die Dokumentation"}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                            <span><{translate id="Dokumentation"}></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container full" data-module="view/admin/profile/profile-categories">
        <div class="row xxsmall-push-updown-30" id="profile-category-admin">
            <div class="column xxsmall-12">
                <ol data-remoteurl="<{$acmsContents['actionUrl']}>" id="available-categories" class="connected-list list-unstyled">
                    <{foreach $acmsContents['categories'] as $cat}>
                        <{include file="acmsfile:Core|admin/member/profile/category/ProfileCategory.tpl" category=$cat}>
                    <{/foreach}>
                </ol>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30">
            <ul class="collection v1 profile-fields" id="available-profilefields" data-action="<{$acmsContents['fieldActionUrl']}>" data-cat="0" data-catweight="0" draggable="true">
                <{if isset($acmsContents['fields'][0])}>
                    <{foreach $acmsContents['fields'][0] as $field}>
                        <li class="collection-item sortable-field <{if $field.show}>success<{else}>warning<{/if}>"
                            data-cat="<{$cat.id}>" data-catweight="<{$field.weight}>" data-fieldstate="<{$field.show}>" data-fieldid="<{$field.id}>">
                            <div class="title">
                                <{$field.title}>
                            </div>
                            <p>
                                <span class="label v1 default">ID: <{$field.id}></span>
                            </p>
                            <span class="secondary-content">
                                <i aria-hidden="true" class="acms-icon acms-icon-expand-arrows-alt text-bold cursor-move"></i>
                            </span>
                        </li>
                    <{/foreach}>
                <{/if}>
            </ul>
        </div>

        <div class="modal" id="edit-regcat" tabindex="-1" role="dialog" aria-labelledby="edit-regcat-title" aria-hidden="true">
            <div class="modal-dialog">
                <div class="content">
                    <div class="header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="src-only">
                                <{translate id="Schließen" textdomain="Core"}>
                            </span>
                        </button>
                        <h4 class="title" id="edit-regcat-title">&nbsp;</h4>
                    </div>
                    <div class="main text-left">
                        &nbsp;
                    </div>
                    <div class="footer">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="add-category-form" tabindex="-1" role="dialog" aria-labelledby="add-category-title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="content">
                <form role="form" <{$acmsContents->addCategoryForm['attributes']}>>
                    <div class="header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                        </button>
                        <h4 class="title" id="add-category-title"><{translate id="Neue Profil-Kategorie"}></h4>
                    </div>
                    <div class="main text-left">
                        <{foreach $acmsContents->addCategoryForm['fields'] as $field}>
                            <{include file=$field['template'] field=$field}>
                        <{/foreach}>
                    </div>
                    <div class="footer clearfix">
                        <{include file=$acmsContents->addCategoryForm['buttons']['template'] field=$acmsContents->addCategoryForm['buttons']}>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
