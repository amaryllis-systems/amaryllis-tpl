<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div id="profile-fields" class="container full">
        <div class="row">
            <div class="column xxsmall-12 text-center">
                <div class="icon page-title v7">
                    <i class="acms-icon acms-icon-database"></i>
                    <div class="content">
                        <h1 class="title"><{$acmsContents['acmsTitle']}></h1>
                        <h2 class="sub-title">Erstellen und bearbeiten von Profil Formular-Feldern</h2>
                    </div>
                </div>
                <div class="center aligned short line v1"></div>
            </div>
        </div>
        <div class="row" data-module="view/admin/profile/profile-fields">
            <div class="column xxsmall-12">
                <div class="display-block center-block percentage-60">
                    <div class="collection v1">
                        <{foreach $acmsContents->fields as $field}>
                            <{include file="acmsfile:Core|admin/member/profile/field/Field.tpl" field=$field disabled=$acmsContents->disabled}>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <div class="display-block center-block percentage-60">
                    <div class="button-toolbar">
                        <{foreach $acmsContents->fieldTypes as $ftype => $conf}>
                            <div class="button-group">
                                <button
                                   class="button small default"
                                   data-target="#field-add-<{$conf['ftype']}>"
                                   data-module="apps/ui/informations/modal"
                                   title="<{translate id='Ein neues Feld erstellen' textdomain='Core'}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                                    <span><{$conf['caption']}></span>
                                </button>
                                <{include file="acmsfile:Core|admin/form/ProfileFieldModalForm.tpl" field=$conf['field'] form=$conf['form']}>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
