<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{assign var=isDisabled value=isset($disabled[$field['id']])}>

<div class="collection-item hoverable">
    <div class="title">
        <span class="text-primary"><{$field['title']}></span> <small class="text-muted"><{$field['name']}></small>
    </div>
    
    <div class="secondary-content">
        <div class="dropdown" data-module="ui/navs/dropdown">
            <span class="dropdown-toggle">
                <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i>
            </span>
            <div class="dropdown-content">
                <ul>
                    <li>
                        <a href="#" 
                           data-target="#field-edit-<{$field.id}>"
                           data-module="apps/ui/informations/modal"
                           title="<{translate id="%s bearbeiten" textdomain="Core" args=$field['title']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                            <span><{translate id="%s bearbeiten" textdomain="Core" args=$field['title']}></span>
                        </a>
                    </li>
                    <li>
                        <a href="#"
                           title="<{translate id="%s löschen" textdomain="Core" args=$field['title']}>"
                           <{if $isDisabled}>disabled="disabled" class="disabled"<{/if}>>
                            <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                            <span><{translate id="%s löschen" textdomain="Core" args=$field['title']}></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/form/ProfileFieldModalForm.tpl" field=$field form=$field['form']}>
</div>
