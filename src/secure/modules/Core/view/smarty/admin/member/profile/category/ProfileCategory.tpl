<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<li class="sortable-category" id="sort-<{$category.id}>" data-weight="<{$category.weight}>" data-id="<{$category.id}>" draggable="true">
    <div class="panel v1 collapsible collapsed" data-module="apps/ui/components/panel" role="region" aria-label="Profil-Kategorie <{$category.title}>">
        <div class="heading text-center">
            <h4 class="title white-text">
                <span class="left floated sortable-handle cursor-move">
                    <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i>
                    <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i>
                </span>
                <span><{$category.title}></span>
                <small>
                    <span class="label v1 <{if $category.active}>default<{else}>danger<{/if}>">
                        <{if $category.active}>
                            <{translate id="Aktiv" textdomain="Core"}>
                        <{else}>
                            <{translate id="Inaktiv" textdomain="Core"}>
                        <{/if}>
                    </span>
                </small>
            </h4>
        </div>
        <div class="content hiding" aria-hidden="true">
            <div>
                <small>
                    <{translate id ="Font-Icon" textdomain="Core"}>: <{if $category.fonticon}><{$category.fonticon}> (<span class="<{$category.fonticon}>"></span>)
                    <{else}>
                        <{translate id="Kein Icon gesetzt" textdomain="Core"}>
                    <{/if}>
                </small>
            </div>
            <{$category.teaser}>

            <div class="field-list">
                <p><strong><u><{translate id="Zugewiesene Felder" textdomain="Core"}></u></strong></p>
                <ul class="collection v1 <{if $category.active}>profile-fields<{/if}>" 
                    id="connected-fields-<{$category.id}>" 
                    data-cat="<{$category.id}>" 
                    data-weight="<{$category.weight}>"
                    draggable="true">
                    <{if isset($acmsContents['fields'][$category.id])}>
                        <{foreach $acmsContents['fields'][$category.id] as $field}>
                            <li class="collection-item sortable-field <{if $field.show}>success<{else}>warning<{/if}>"
                                data-cat="<{$category.id}>" data-catweight="<{$field.weight}>" data-fieldstate="<{$field.show}>" data-fieldid="<{$field.id}>">
                                <div class="title">
                                    <{$field.title}>
                                </div>
                                <p>
                                    <span class="label v1 default">ID: <{$field.id}></span>
                                </p>
                                <span class="secondary-content">
                                    <i aria-hidden="true" class="acms-icon acms-icon-expand-arrows-alt text-bold cursor-move"></i>
                                </span>
                            </li>
                        <{/foreach}>
                    <{/if}>
                </ul>
            </div>

        </div>
        <div class="footer">
            <div class="button-toolbar clearfix compact">
                <a role="button" href="<{$category.editUrl}>" class="button tiny default" title="<{translate id='Bearbeiten' textdomain='Core'}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-edit"></i>
                    <span class="src-only"><{translate id="Bearbeite %s" textdomain="Core" args=[$category.title]}></span>
                </a>
                <a role="button" href="<{$category.toggleURL}>" class="button tiny default"  title="<{translate id='Wechseln des Aktiv-Status' textdomain='Core'}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-<{if $category.active}>ban<{else}>ok<{/if}>"></i>
                    <span class="src-only"><{translate id="Toggle Aktiv-Status" textdomain="Core"}></span>
                </a>
                <a role="button" href="<{$category.deleteUrl}>" class="button tiny default" title="<{translate id='Löschen'}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                    <span class="src-only"><{translate id="Lösche %s" textdomain="Core" args=[$category.title]}></span>
                </a>
            </div>
        </div>
    </div>
</li>
