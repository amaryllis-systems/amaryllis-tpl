<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <div class="container">
        <div class="row">
            <div class="column xxsmall-12">
                <div class="page-title v0">
                    <h1>
                        <i aria-hidden="true" class="acms-icon acms-icon-eye text-muted"></i>
                        <{$acmsContents->acmsTitle}>
                    </h1>
                    <p class="lead" itemprop="description"><{$acmsContents->hint}></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12">
                <{include file=$acmsContents->filterBar['template'] form=$acmsContents->filterBar}>
            </div>
        </div>
    </div>
    <div class="container pull-updown-30" 
         data-module="view/admin/member/profile/visibility"
         data-remote="<{$acmsContents['actionUrl']}>"
         data-fieldid="<{$acmsContents->field}>"
         >
        <div class="row push-updown-30">
            <div class="column xxsmall-12">
                 <h1 class="text-center">Profil-Feld &raquo; <{$acmsContents->fieldLabel}> &laquo;</h1>
            </div>
            <div class="column xxsmall-12">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                        <span>Profil-Sichtbarkeit</span>
                    </h2>
                    <p>Wählen Sie alle Benutzer-Gruppen, die das Feld in der in der Filter-Bar gewählten Benutzer-Gruppe sehen können.</p>
                </div>
                <div class="row full">
                    <{foreach $acmsContents->groups as $gid => $group}>
                        <div class="column xxsmall-12 medium-6 large-4 xlarge-3 xxlarge-3">
                            <div class="push-10">
                                <h3><{$group}></h3>
                            </div>
                        </div>
                        <div class="column xxsmall-12 medium-6 large-8 xlarge-9 xxlarge-9">
                            <div class="push-10 group-list">
                                <div data-actionurl="<{$acmsContents['actionUrl']}>"
                                     class="collection v1 call-to-action v2"
                                     data-toggle="actionlistgroup">
                                    <div class="collection-header">
                                        <span class="text-bold"><{translate id="Auswählbar von" textdomain="Core"}></span>
                                    </div>
                                    <{foreach $acmsContents['groups'] as $pgid => $pgroup}>
                                        <div data-ugroup="<{$gid}>" data-pgroup="<{$pgid}>" data-vistype="1" class="collection-item <{if isset($acmsContents->visibility[1][$gid]) && in_array($pgid, $acmsContents->visibility[1][$gid])}> active<{/if}>">
                                            <{$pgroup}>
                                        </div>
                                    <{/foreach}>
                                </div>
                            </div>
                        </div>
                        <hr />
                    <{/foreach}>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                        <span>Profil-Editierbarkeit</span>
                    </h2>
                    <p>Wählen Sie alle Benutzer-Gruppen, die das Feld in der in der Filter-Bar gewählten Benutzer-Gruppe sehen können.</p>
                </div>
                <div class="group-list-small">
                    <div class="collection v1 call-to-action v2"
                         data-toggle="actionlistgroup">
                        <{foreach $acmsContents['groups'] as $gid => $group}>
                            <div data-ugroup="<{$gid}>" data-pgroup="0" data-vistype="2" class="collection-item<{if isset($acmsContents->visibility[2][$gid])}> active<{/if}>">
                                <{$group}>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 compact push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                        <span>Profil-Durchsuchbarkeit</span>
                    </h2>
                    <p>Wählen Sie alle Benutzer-Gruppen, die das Feld in der erweiterten Profil-Suche verwenden dürfen.</p>
                </div>
                <div class="group-list-small">
                    <div class="collection v1 call-to-action v2"
                         data-toggle="actionlistgroup">
                        <{foreach $acmsContents['groups'] as $gid => $group}>
                            <div data-ugroup="<{$gid}>" data-pgroup="0" data-vistype="3" class="collection-item<{if isset($acmsContents->visibility[3][$gid])}> active<{/if}>">
                                <{$group}>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
