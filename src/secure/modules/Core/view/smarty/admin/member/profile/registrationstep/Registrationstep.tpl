<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<li class="registration-step" id="sort-<{$step.id}>" data-id="<{$step.id}>" data-weight="<{$step.weight}>">

    <div class="panel v1 collapsible collapsed" data-module="apps/ui/components/panel" role="region" aria-label="Registrierungs-Schritt <{$step.title}>">
        <div class="heading text-center">
            <h4 class="title white-text">
                <span class="left floated sortable-handle cursor-move">
                    <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i>
                    <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i>
                </span>
                <span><{$step.title}></span>
                <small>
                    <span class="label v1 <{if $step.active}>default<{else}>danger<{/if}>">
                        <{if $step.active}>
                            <{translate id="Aktiv" textdomain="Core"}>
                        <{else}>
                            <{translate id="Inaktiv" textdomain="Core"}>
                        <{/if}>
                    </span>
                </small>
            </h4>
        </div>
        <div class="content hiding" aria-hidden="true">
            <div>
                <small>
                    <{translate id ="Font-Icon" textdomain="Core"}>: <{if $step.fonticon}><{$step.fonticon}> (<span class="<{$step.fonticon}>"></span>)
                    <{else}>
                        <{translate id="Keins" textdomain="Core"}>
                    <{/if}>
                </small>
            </div>
            <{$step.teaser}>

            <div class="field-list">
                <p><strong><u><{translate id="Felder in diesem Schritt" textdomain="Core"}></u></strong></p>
                <ul class="collection v1 profile-fields" id="connected-fields-<{$step.id}>" data-step="<{$step.id}>">
                    <{if isset($acmsContents['fields'][$step.id]) && $acmsContents['fields'][$step.id]}>
                        <{foreach $acmsContents['fields'][$step.id] as $field}>
                            <li class="collection-item sortable-field <{if $field.field_show}>success<{else}>warning<{/if}>" 
                                data-step="<{$step.id}>"
                                data-stepweight="<{$field.step_weight}>" data-fieldstate="<{$field.field_show}>" data-fieldid="<{$field.id}>">
                                <div class="title">
                                    <{$field.title}>
                                </div>
                                <p>
                                    <span class="label v1 default">ID: <{$field.id}></span>
                                </p>
                                <span class="secondary-content sortable-handle">
                                    <i aria-hidden="true" class="acms-icon acms-icon-expand-arrows-alt"></i>
                                </span>
                            </li>
                        <{/foreach}>
                    <{/if}>
                </ul>
            </div>

        </div>
        <div class="footer">
            <div class="button-toolbar compact clearfix">
                <a role="button" class="button tiny default" href="<{$step.editUrl}>" data-target="#edit-registrationstep-<{$step['editForm']['name']}>-form" data-module="apps/ui/informations/modal" title="<{translate id='Bearbeiten' textdomain='Core'}>">
                    <span class="acms-icon acms-icon-edit"></span>
                    <span class="src-only"><{translate id="Bearbeite %s" textdomain="Core" args=[$step.title]}></span>
                </a>
                <a role="button" class="button tiny default" href="<{$step.deleteUrl}>" title="<{translate id='Löschen'}>">
                    <span class="acms-icon acms-icon-trash"></span>
                    <span class="src-only"><{translate id="Lösche %s" textdomain="Core" args=[$step.title]}></span>
                </a>
                <a role="button" class="button tiny default" href="<{$step.toggleURL}>" title="<{translate id='Toggle Aktivitäts-Status' textdomain='Core'}>">
                    <span class="acms-icon acms-icon-<{if $step.active}>ban<{else}>ok<{/if}>"></span>
                    <span class="src-only"><{translate id="Toggle Aktiv" textdomain="Core"}></span>
                </a>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/member/profile/registrationstep/StepModify.tpl" form=$step['editForm']}>
</li>
