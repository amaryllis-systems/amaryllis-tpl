<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container">
    <div class="row xxsmall-push-updown-30">
        <div class="column xxsmall-12">
            <div class="page-title">
                <h1>
                    <i aria-hidden="true" class="acms-icon acms-icon-magic text-muted"></i>
                    <span itemprop="name"><{$acmsContents['acmsTitle']}></span>
                </h1>
                <p class="lead" itemprop="description">
                    <{translate id="Bearbeiten Sie die Registrierungs-Schritte, fügen Sie neue hinzu oder nehmen Sie Schritte raus."}>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container full">
    <div class="row xxsmall-push-updown-30">
        <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                    <span>Die Benutzer-Registrierung</span>
                </h2>
                <p>
                    Die Benutzer-Registrierung erfolgt durch einen Prozess basierend auf den hier erstellten Schritten und der daraufhin erfolgenden Aktivierung des Benutzers. 
                    Bedenken Sie, dass im ersten Schritt auch die absolut erforderlichen Felder wie Benutzer-Name, Login-Name, E-Mail Adresse und Passwort gefragt sind. Bei 
                    Seiten mit erforderlichem Mindest-Alter fügt sich diesen Schritten auch das Geburts-Datum an.
                </p>
                <div class="button-toolbar clearfix compact">
                    <button class="button primary small right"
                            data-module="apps/ui/informations/modal"
                            data-target="#add-registrationstep-form"
                            data-toggle="modal" 
                            aria-label="<{translate id="Erstellen Sie einen neuen Registrierungs-Schritt" textdomain="Core"}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                        <span>
                            <{translate id="Neuer Registrierungs-Schritt" textdomain="Core"}>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
            <div class="content-block v6 push-10">
                <h2>
                    <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                    <span><{translate id="Sie brauchen Hilfe?"}></span>
                </h2>
                <p>
                    Lesen Sie unsere Dokumentation und erfahren Sie, wie Sie die 
                    Profil-Registrierung einrichten, was die Änderungen bedeuten 
                    und wie sich die Änderungen im Layout bemerkbar machen
                </p>
                <div class="button-toolbar clearfix compact">
                    <a class="button primary small right" href="https://www.amaryllis-dokumentation.de" title="<{translate id="Lesen Sie die Dokumentation"}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                        <span><{translate id="Dokumentation"}></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="profile-regstep" class="container full" data-module="view/admin/profile/profile-registrationsteps">
    <div class="row xxsmall-push-updown-30">
        <ol data-remoteurl="<{$acmsContents['actionUrl']}>" id="available-steps" class="registration-steps">
            <{foreach $acmsContents['registrationSteps'] as $step}>
                <{include file="acmsfile:Core|admin/member/profile/registrationstep/Registrationstep.tpl" step=$step nocache}>
            <{/foreach}>
        </ol>
    </div>
    <div class="row xxsmall-push-updown-30">
        <ul class="boxed horizontal collection v1 profile-fields" id="available-profilefields" data-action="<{$acmsContents['fieldActionUrl']}>" data-step="0" data-stepweight="0" draggable="true">
            <{if isset($acmsContents['fields'][0])}>
                <{foreach $acmsContents['fields'][0] as $field}>
                    <li class="available-field collection-item sortable-field collection-item <{if $field.field_show}>success<{else}>warning<{/if}>"
                        data-step="<{$step.id}>" data-stepweight="<{$field.step_weight}>" data-fieldstate="<{$field.field_show}>" data-fieldid="<{$field.id}>">
                        <div class="title">
                            <{$field.title}>
                        </div>
                        <p>
                            <span class="label v1 default">ID: <{$field.id}></span>
                        </p>
                        <span class="secondary-content sortable-handle">
                            <i aria-hidden="true" class="acms-icon acms-icon-expand-arrows-alt"></i>
                        </span>
                    </li>
                <{/foreach}>
            <{/if}>
        </ul>
    </div>
</div>

<div class="modal" id="add-registrationstep-form" tabindex="-1" role="dialog" aria-labelledby="add-registrationstep-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" <{$acmsContents->addStepForm['attributes']}>>
                <div class="header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                    </button>
                    <h4 class="title" id="add-registrationstep-title"><{translate id="Neuer Registrierungs-Schritt"}></h4>
                </div>
                <div class="main text-left">
                    <{foreach $acmsContents->addStepForm['fields'] as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
                <div class="footer clearfix">
                    <{include file=$acmsContents->addStepForm['buttons']['template'] field=$acmsContents->addStepForm['buttons']}>
                </div>
            </form>
        </div>
    </div>
</div>
