<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <section class="container fixed-1200">
        <div class="full row push-updown-35">
            <div class="large page-title v7">
                <div class="middle aligned content">
                    <h1 class="middle aligned title">
                        <{$acmsContents['acmsTitle']}>
                    </h1>
                    <p class="sub-title"><{$acmsContents['subtitle']}></p>
                </div>
            </div>
        </div>
    </section>
    <section class="container container fixed-1200" data-module="apps/admin/member/session/session-admin" data-remote="<{$acmsContents->remote}>">
        <div class="full row push-updown-35">
            <div class="column xxsmall-12">
                <div class="pull-updown-20" data-role="filter-bar">
                    <form role="form">
                        <div class="fields fields-3">
                            <div class="icon form-group">
                                <div class="button-group">
                                    <label data-role="guest-changer" data-ordername="title" class="<{if $acmsContents->filterOptions['guest'] == 1}>active<{/if}> primary border-button button" role="button" type="button" value="1">
                                        <span><{translate id="Gäste" textdomain="Core"}></span>
                                        <input class="src-only" type="radio" name="guest" value="1"<{if $acmsContents->filterOptions['guest'] == 1}> checked="checked"<{/if}>>
                                    </label>
                                    <label data-role="guest-changer" class="<{if $acmsContents->filterOptions['guest'] == 2}>active <{/if}>primary border-button button" role="button" type="button" value="2">
                                        <span><{translate id="Benutzer" textdomain="Core"}></span>
                                        <input class="src-only" type="radio" name="guest" value="2"<{if $acmsContents->filterOptions['guest'] == 2}> checked="checked"<{/if}>>
                                    </label>
                                    <label data-role="guest-changer" class="<{if $acmsContents->filterOptions['guest'] == 3}>active <{/if}>primary border-button button" role="button" type="button" value="3">
                                        <span><{translate id="Beide" textdomain="Core"}></span>
                                        <input class="src-only" type="radio" name="guest" value="3" <{if $acmsContents->filterOptions['guest'] == 3}> checked="checked"<{/if}>>
                                    </label>
                                </div>
                            </div>
                            <div class="icon form-group">
                                <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                                <input class="field" type="search" name="term" placeholder="<{translate id="username, ip" textdomain="Core"}>" data-role="search" value="<{$acmsContents->filterOptions['term']}>">
                            </div>
                        </div>
                    </form>
                </div>
                <table class="middle aligned compact striped responsive table">
                    <thead>
                        <tr>
                            <th><{translate id="Benutzer" textdomain="Core"}></th>
                            <th class="size-200"><{translate id="IP-Adresse" textdomain="Core"}></th>
                            <th class="size-200"><{translate id="Zugriffs-Zeit" textdomain="Core"}></th>
                            <th class="size-150">
                                <span class="src-only"><{translate id="Aktionen" textdomain="Core"}></span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <{foreach $acmsContents->sessions as $entry}>
                            <tr class="z-depth-3-hover" data-role="table-row" data-user="<{$entry['user_id']}>" data-sid="<{$entry['sid']}>">
                                <td data-th="<{translate id="Benutzer" textdomain="Core"}>">
                                    <{if $entry['user_id'] > 0}>
                                        <div class="middle aligned">
                                            <a class="small inline-avatar avatar" href="<{$entry['user']['itemUrl']}>" title="<{$entry['user']['username']}>">
                                                <img src="<{$entry['user']['avatar']}>" alt="Avatar" title="<{$entry['user']['displayname']}>">
                                            </a>
                                            <a href="<{$entry['user']['itemUrl']}>" title="<{$entry['user']['username']}>">
                                                <{$entry['user']['displayname']}>
                                            </a>
                                        </div>
                                    <{else}>
                                        <div class="middle aligned">
                                            <div class="small inline-avatar avatar">
                                                <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
                                            </div>
                                            <div class="display-inline">
                                                <{$entry['user']['displayname']}>
                                            </div>
                                        </div>
                                    <{/if}>
                                </td>
                                <td data-th="<{translate id="IP-Adresse" textdomain="Core"}>">
                                    <{$entry['ip']}>
                                </td>
                                <td data-th="<{translate id="Zugriffs-Zeit" textdomain="Core"}>">
                                    <{$entry['access']->format('d-m-Y H:i:s')}>
                                </td>
                                <td data-th="<{translate id="Aktionen" textdomain="Core"}>">
                                    <{if $entry['user_id'] != $acmsData['user']['id']}>
                                        <button class="small warning danger-hover icon button" type="button" data-trigger="destroy" aria-label="Kill Session" data-value="<{$entry['sid']}>">
                                            <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                        </button>
                                        <{if $entry['user_id'] > 0}>
                                            <button class="small warning danger-hover icon button" type="button" data-trigger="destroyUser" aria-label="Kill all User Sessions" data-value="<{$entry['user_id']}>">
                                                <i aria-hidden="true" class="acms-icon acms-icon-user-times"></i>
                                            </button>
                                        <{/if}>
                                    <{/if}>
                                </td>
                            </tr>
                        <{/foreach}>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <span class="small right floated primary label v2">
                                    <b data-role="total-sessions"><{$acmsContents->total}></b> Sessions
                                </span>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <{if $acmsContents->acmsPagination['pagination']}>
                    <div class="pagination-container v1">
                        <{$acmsContents->acmsPagination['pagination']}>
                    </div>
                <{/if}>
            </div>
        </div>
    </section>
</section>
