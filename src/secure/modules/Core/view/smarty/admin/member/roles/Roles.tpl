<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section data-module="view/admin/member/roles"
         data-remote="<{$acmsContents['actionActionRoute']}>"
         class="pull-updown-30">
    <div class="container full">
        <div class ="row">
            <div class="column xxsmall-12">
            <!-- general admin page title -->
            <div class="page-title">
                <h1 itemprop='name'>
                    <i aria-hidden="true" class="acms-icon acms-icon-chart-pie text-muted"></i>
                    <span><{$acmsContents['acmsTitle']}></span>
                </h1>
                <p class="lead" itemprop="description">
                    Benutzer-Rollen dienen der Gruppierung von Benutzer-Gruppen für eine einfache Berechtigungs-Verteilung in Inhalten.
                </p>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12">
                
                <div class="form-wrapper pull-0 content-block v6 compact">
                    <form role="form" <{$acmsContents['filterBar']['attributes']}>>
                        <{if $acmsContents['filterBar']['title']}>
                            <legend><{$acmsContents['filterBar']['title']}></legend>
                        <{/if}>
                        <{foreach $acmsContents['filterBar']['fields'] as $field}>
                            <{include file=$field['template'] field=$field nocache}>
                        <{/foreach}>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container full" id="roles-view">
        <div class="row">
            <{foreach $acmsContents->get('roles') as $role}>
                <{include file="acmsfile:Core|admin/member/roles/Role.tpl" role=$role}>
            <{/foreach}>
        </div>
        <div class="row pull-updown-30">
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden='true' class="acms-icon acms-icon-chart-pie text-muted"></i>
                        <span>Benutzer-Rollen</span>
                    </h2>
                    <p>
                        Benutzer-Rollen sind eine Zusammenfassung der Benutzergruppen. 
                        Während die Benutzergruppen einem ein Fein-Tuning der Berechtigungen erlauben, 
                        stellen Benutzerrollen mehr eine grobe Berechtigung dar. 
                        Benutzerrollen werden nur in Modulen eingesetzt, um dem Autoren des Inhaltes eine 
                        Zielgruppe zuweisen zu können. Hat eine Gruppe die Erlaubnis, private Inhalte übermitteln 
                        zu können, sind diese nur für diesen Benutzer zu sehen (und für Administratoren).
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           target="_blank"
                           data-module="apps/ui/informations/modal"
                           data-target="#member-add-role"
                           href="#">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                            <span><{translate id="Neue Benutzer-Rolle" textdomain="Core"}></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-6 xlarge-6 xxlarge-6">
                <div class="content-block v6 push-10">
                    <h2>
                        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i>
                        <span><{translate id="Lesen Sie die Dokumentation" textdomain="Core"}></span>
                    </h2>
                    <p>
                        <{translate id="Sie sind sich nicht sicher was Sie machen sollen oder was Sie auswählen sollen? Schlagen Sie in der Amaryllis-CMS Dokumentation nach was Benutzer-Rollen sind. Dort erfahren Sie, wie Sie die passenden Benutzer-Gruppen setzen, diese für entsprechende Zugriffe einrichten und wie sich die Rollen im System sowie in den Modulen, die Zugriffs-Berechtigungen per Benutzer-Rolle unterstützen, verhalten." textdomain="Core"}>
                    </p>
                    <div class="button-toolbar compact clearfix">
                        <a class="button small primary right" 
                           target="_blank"
                           href="https://www.amaryllis-dokumentation.de/endbenutzer/administration/benutzer-management/benutzer-rollen/">
                            <i aria-hidden="true" class="acms-icon acms-icon-book"></i>
                            <span>Dokumentation</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <{include file="acmsfile:Core|admin/member/roles/RolesAdd.tpl"}>
</section>
