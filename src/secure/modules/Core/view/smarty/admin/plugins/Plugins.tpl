<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<!-- Page Title -->
<div class="container fixed-1200 fixed-1600">
    <div class="full row">
        <div class="xxlarge page-title v7">
            <i aria-hidden="true" class="acms-icon acms-icon-plug"></i>
            <div class="content">
                <h1 class="title"><{$acmsContents->acmsTitle}></h1>
                <div class="sub-title"><{translate id="Hier können Sie die installierten Plugins aus dem System verwalten." textdomain="Core"}></div>
            </div>
        </div>
    </div>
    <!-- Filter Bar -->
    <{if $acmsContents->filterBar}>
        <div class="full row push-updown-35">
            <div class="column xxsmall-12">
                <{include file=$acmsContents['filterBar']['template'] form=$acmsContents['filterBar']}>
            </div>
        </div>
    <{/if}>
</div>

<div class="container fixed-1200 fixed-1600 pull-updown-35"
     itemscope="" itemtype="http://schema.org/ControlAction">
    <div class="full row push-updown-35"
         itemprop="target" itemscope="" itemtype="http://schema.org/EntryPoint"
         >
        <div class="column xxsmall-12" itemprop="application" itemscope="" itemtype="http://schema.org/SoftwareApplication">
            <{if $acmsContents->get('categories')}>

                <div class="collection v1 has-image">
                    <{foreach $acmsContents->get('categories') as $name => $category}>
                        <div class="collection-item bordered border-solid push-5">
                            <i aria-hidden="true" class="item-image acms-icon <{$category['icon']}>"></i>
                            <h4 class="title"><a href="<{$category['link']}>" title="<{$category['description']}>" class="display-block"><{$category['caption']}></a></h4>
                            <p class="text-muted text-small"><{$category['description']}> | <{translate id="Kategorien-Name: %s" textdomain="Core" args=$name}></p>
                        </div>
                    <{/foreach}>
                </div>
            <{elseif $acmsContents->plugins}>
                <div class="xxlarge page-title v7">
                    <i aria-hidden="true" class="acms-icon <{$acmsContents->category['icon']}>"></i>
                    <div class="content">
                        <h1 class="title">
                            <{$acmsContents->category['caption']}>
                        </h1>
                        <div class="sub-title"><{$acmsContents->category['description']}></div>
                    </div>
                </div>
                <div class="cards">
                    <{foreach $acmsContents->plugins as $name => $plugin}>
                        <div class="card v2">
                            <div class="card-content">
                                <div class="card-title">
                                    <a href="<{$plugin.link}>" title="<{$plugin.name}>">
                                        <{$plugin.fullname}>
                                    </a>
                                </div>
                                <div class="card-meta">
                                    <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i> 
                                    <{$plugin.releaseDate}>
                                </div>
                            </div>
                            <a class="bottom pluggend default button" href="<{$plugin.link}>" title="Konfigurieren">
                                <i aria-hidden="true" class="acms-icon acms-icon-cogs"></i>
                                <span>Konfigurieren</span>
                            </a>
                        </div>
                    <{/foreach}>
                </div>
            <{elseif $acmsContents->pluginForm}>
                <div class="xxlarge page-title v7">
                    <i aria-hidden="true" class="acms-icon <{$acmsContents->category['icon']}>"></i>
                    <div class="content">
                        <h1 class="title">
                            <{$acmsContents->category['caption']}>
                        </h1>
                        <div class="sub-title"><{$acmsContents->category['description']}></div>
                    </div>
                </div>
                <div class="row">
                    <div class="column xxsmall-12 large-10 large-offset-1 xlarge-8 xlarge-offset-2">
                        <{include $acmsContents->pluginForm['template'] form=$acmsContents->pluginForm}>
                    </div>
                </div>
            <{/if}>
        </div>
    </div>
</div>
