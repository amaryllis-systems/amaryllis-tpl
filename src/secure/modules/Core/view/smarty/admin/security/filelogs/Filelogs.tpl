<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container pull-updown-30">
    <div class ="row">
        <div class="column xxsmall-12 xxsmall-offset-0 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <div class="page-title v1">
                <h1 itemprop="name"><{$acmsContents['acmsTitle']}></h1>
                <p>Filelogger Inhalte online prüfen. Hier sind aktuell ausschließlich die Error-Einträge des Standard Error Logs enthalten.</p>
            </div>
        </div>
    </div>
    <div class ="row">
        <div class="column xxsmall-12 xxsmall-offset-0 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            
        </div>
    </div>
    <div class ="row xxsmall-push-updown-30 push-right-20" data-cssfile="media/css/view/security">
        <div class="column xxsmall-12">
            <{if $acmsContents->logContent}>
                <div class="log-wrapper">
                    <textarea name="log-content" id="log-area" rows="70" cols="100"><{$acmsContents->logContent}></textarea>
                </div>
            <{/if}>
        </div>
    </div>
</div>
