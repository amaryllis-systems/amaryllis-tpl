<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container">
    <div class ="row">
        <div class="column xxsmall-12 xxsmall-offset-0 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <div class="page-title">
                <h1><{$acmsContents['acmsTitle']}></h1>
            </div>
        </div>
    </div>
    <div class ="row">
        <div class="column xxsmall-12 xxsmall-offset-0 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <table class="table striped compact hover" 
                   data-module="view/admin/security/iptables"
                   data-remote="<{generateUrl name="core_admin_action" parameters=['module' => 'core', 'type' => 'security', 'item' => 'iptables', 'action' => 'action']}>">
                <thead>
                    <tr>
                        <th><{translate id="IP-Adresse" textdomain="Core"}></th>
                        <th><{translate id="Vertrauenswürdig" textdomain="Core"}></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <{foreach $acmsContents->iptables as $entry}>
                        <tr data-id="<{$entry['id']}>">
                            <td>
                                <{$entry['ip']}>
                            </td>
                            <td>
                                <{if $entry['reliable']}>
                                    <span>
                                    <i aria-hidden="true" class="acms-icon acms-icon-check-circle text-success"></i>
                                    <span class="src-only"><{translate id="Zugelassen" textdomain="Core"}></span>
                                    </span>
                                <{else}>
                                    <span>
                                    <i aria-hidden="true" class="acms-icon acms-icon-ban text-danger"></i>
                                    <span class="src-only"><{translate id="Blockiert" textdomain="Core"}></span>
                                    </span>
                                <{/if}>
                            </td>
                            <td>
                                <button class="button tiny danger" type="button" data-trigger="delete" data-id="<{$entry['id']}>" data-remote="<{$entry['deleteUrl']}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                </button>
                            </td>
                        </tr>
                    <{foreachelse}>
                        <tr class="success">
                            <td><i aria-hidden="true" class="acms-icon acms-icon-check-circle"></i> <{translate id="Keine Einträge vorhanden" textdomain="Core"}></td>
                        </tr>
                    <{/foreach}>
                </tbody>
            </table>
        </div>
    </div>
    <div class ="row xxsmall-push-updown-30">
        <div class="column xxsmall-12 xxsmall-offset-0 large-10 large-offset-1 xlarge-10 xlarge-offset-1 xxlarge-10 xxlarge-offset-1">
            <div class="button-toolbar">
                <button class="button small primary" data-toggle="modal" data-target="#iptables-modal-form-add" data-module="apps/ui/informations/modal">
                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i> <{translate id="Neuer Eintrag" textdomain="Core"}>
                </button>
            </div>
        </div>
    </div>
</div>
<{* Neuer Eintrag *}>
<div class="modal bottom sheet"
     id="iptables-modal-form-add"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="iptables-modal-form-add-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" method="post" action="#">
                <div class="header">
                    <span role="button" class="close right" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                    </span>
                    <h4 class="title" id="iptables-modal-form-add-title">
                        <{translate id="Eintrag erstellen" textdomain="Core"}>
                    </h4>
                </div>
                <div class="main">
                    <{foreach $acmsContents->acms_form['fields'] as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                    <input type="hidden" name="id" value="0" />
                    <input type="hidden" name="op" value="speichern" />
                </div>
                <div class="footer">
                    <div class="footer">
                        <button class="button danger small" type="submit" data-trigger="submit">
                            <span>speichern</span>
                        </button>
                        <button class="button border-button small default" type="reset" data-trigger="abort" data-dismiss="modal">
                            <span>abbrechen</span>
                        </button>
                    </div>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>
<{* Eintrag entfernen *}>
<div class="modal bottom sheet"
     id="iptables-modal-form-delete"
     data-modal-index="1"
     tabindex="-1"
     role="dialog"
     aria-labelledby="iptables-modal-form-delete-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form role="form" method="post" action="action">
                <div class="header">
                    <span role="button" class="close right" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                    </span>
                    <h4 class="title" id="iptables-modal-form-delete-title">
                        <{translate id="Eintrag entfernen" textdomain="Core"}>
                    </h4>
                </div>
                <div class="main">
                    <{translate id="Sind Sie sicher, dass Sie den Eintrag löschen möchten?" textdomain="Core"}>
                    <input type="hidden" name="id" value="0" />
                    <input type="hidden" name="op" value="entferne" />
                </div>
                <div class="footer">
                    <button class="button danger small" type="submit" data-trigger="submit">
                        <span>entfernen</span>
                    </button>
                    <button class="button border-button small default" type="reset" data-trigger="abort" data-dismiss="modal">
                        <span>abbrechen</span>
                    </button>
                </div>
            </form>
        </div><!-- /.content -->
    </div><!-- /.modal-dialog -->
</div>
