<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="dashboard" data-view="view/admin/dashboard">
    <div class="container fixed-1200 fixed-1024" data-cssfile="media/css/admin/dashboard.min.css">
        <div class="row">
            <div class="page-title">
                <h1><span><{$acmsContents['acmsTitle']}></span> <small class="text-capitalize"><{translate id="Dashboard" textdomain="Core"}></small></h1>
                <p class="lead">
                    Gleichen Sie die Sicherheits-Einstellungen für Ihren Bedarf an
                </p>
            </div>
        </div>
        <div class="row">
            <div class="column xxsmall-12 xxsmall-offset-0 medium-10 medium-offset-1 large-6 large-offset-3 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
                <{include file="acmsfile:Core|admin/helpers/DashboardUserCard.tpl" nocache}>
            </div>
        </div>
        <div class="row xxsmall-push-updown-30" data-cssfile="media/css/components/cards.min.css">
            <div class="column xxsmall-12">
                <h2><{translate id="Was möchten Sie als nächstes bearbeiten?" textdomain="Core"}></h2>
            </div>
            <{foreach $acmsContents->bookmarks as $component}>
                <div class="column xxsmall-12 small-6 medium-4 large-4 xlarge-3 xxlarge-3">
                    <div class="display-block percentage-80 center-block push-20">
                        <div class="card v1 card-panel v1 <{$component['class']}> hoverable">
                            <a href="<{$component['url']}>" title="<{$component['title']}>">
                                <div class="display-block text-center">
                                    <i class="<{$component['fonticon']}> acms-icon-4x"></i><br>
                                    <small><{$component['title']}></small>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <{/foreach}>
        </div>
    </div>
</div>
