<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="core-changelog-modal" 
     class="modal large" 
     role="dialog" 
     aria-labelledby="core-changelog-modal-title" 
     aria-hidden="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="content">
            <div class="heading">
                <button class="close" 
                        type="button" 
                        data-dismiss="modal" 
                        aria-label="<{translate id="Schließen" textdomain="Core"}>"
                        >
                    <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                    <span class="src-only">
                        <{translate id="Schließen" textdomain="Core"}>
                    </span>
                </button>
                <h3 class="title" id="core-changelog-modal-title">
                    <{translate id="Changelog" textdomain="Core"}>
                </h3>
            </div>
            <div class="main">
                <{$acmsContents->acms['changelog']}>
            </div>
            <div class="footer">
                <button class="button primary small" type="button" data-dismiss="modal">
                    <span><{translate id="Schließen" textdomain="Core"}></span>
                </button>
            </div>
        </div>
    </div>
</div>
