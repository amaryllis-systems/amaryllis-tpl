<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="send-feedback-modal" 
     class="modal large" 
     role="dialog" 
     aria-labelledby="send-feedback-modal-title" 
     aria-hidden="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="content">
            <form class="form" action="<{$acmsContents['feedbackUrl']}>" data-module="view/admin/feedback">
                <div class="heading">
                    <button class="close" 
                            type="button" 
                            data-dismiss="modal" 
                            aria-label="<{translate id="Schließen" textdomain="Core"}>"
                            >
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">
                            <{translate id="Schließen" textdomain="Core"}>
                        </span>
                    </button>
                    <h3 class="title" id="send-feedback-modal-title">
                        <{translate id="Feedback senden" textdomain="Core"}></h3>
                </div>
                <div class="main">
                    <div id="acms-feedback-form" class="content-block v6 wide blue-grey-text">
                        <legend><{translate id="Feedback" textdomain="Core"}></legend>
                        <div class="form-group">
                            <label class="src-only">
                                <{translate id="Nachname, Vorname" textdomain="Core"}>
                            </label>
                            <div class="field-group">
                                <div class="field-icon">
                                    <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
                                </div>
                                <input name="name"
                                       type="text"
                                       class="field"
                                       required="required"
                                       placeholder="Ihr Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="src-only">
                                <{translate id="E-Mail Adresse" textdomain="Core"}>
                            </label>
                            <div class="field-group">
                                <div class="field-icon">
                                    <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>
                                </div>
                                <input name="email"
                                       type="email"
                                       required="required"
                                       class="field"
                                       placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="src-only">
                                <{translate id="Feedback-Typ" textdomain="Core"}>
                            </label>
                            <div class="row full">
                                <div class="column xxsmall-6">
                                    <input id="feedback-feedback" 
                                           class="radio-custom"
                                           name="feedback"
                                           type="radio"
                                           value="1"
                                           checked="checked"
                                           >
                                    <label for="feedback-feedback" class="radio-custom-label"><i aria-hidden="true" class="acms-icon acms-icon-heart"></i> <{translate id="Feedback" textdomain="Core"}></label>
                                </div>
                                <div class="column xxsmall-6">
                                    <input id="feedback-bug" 
                                           class="radio-custom"
                                           name="feedback"
                                           type="radio"
                                           value="2"
                                           >
                                    <label for="feedback-bug" class="radio-custom-label"><i aria-hidden="true" class="acms-icon acms-icon-bug"></i> <{translate id="Bug Report" textdomain="Core"}></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="field-label">
                                <{translate id="Geben Sie Ihr Feedback ein" textdomain="Core"}>
                            </label>
                            <textarea name="text"
                                      class="field"
                                      required="required" 
                                      placeholder="Geben Sie Ihr Feedback ein"
                                      rows="15"></textarea>
                        </div>
                        <div class="field-hint">
                            Mit dem Absenden des Feedback Formulars erklären Sie 
                            sich mit der Speicherung und Verarbeitung der 
                            gesendeten Daten einverstanden. Des weiteren 
                            erklären Sie sich einverstanden, dass Amaryllis 
                            Systems GmbH Sie bezüglich des Feedbacks 
                            kontaktieren darf.
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <button class="button border-button small" type="reset" data-dismiss="modal">
                        <span><{translate id="Abbrechen" textdomain="Core"}></span>
                    </button>
                    <button type="submit" class="button primary small">
                        <i aria-hidden="true" class="acms-icon acms-icon-paper-plane regular"></i>
                        <span><{translate id="Absenden" textdomain="Core"}></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
