<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="change-apikey-modal" 
     class="modal large" 
     role="dialog" 
     aria-labelledby="change-apikey-modal-title" 
     aria-hidden="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="content">
            <form class="form" action="<{$acmsContents['changeApiUrl']}>" data-module="view/admin/version/change-apikey">
                <div class="heading">
                    <button class="close" 
                            type="button" 
                            data-dismiss="modal" 
                            aria-label="<{translate id="Schließen" textdomain="Core"}>"
                            >
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">
                            <{translate id="Schließen" textdomain="Core"}>
                        </span>
                    </button>
                    <h3 class="title" id="change-apikey-modal-title">
                        <{translate id="API-Schlüssel" textdomain="Core"}></h3>
                </div>
                <div class="main">
                    <div id="acms-feedback-form" class="content-block v6 wide blue-grey-text">
                        <legend><{translate id="Ändern Sie bei Bedarf den API-Schlüssel" textdomain="Core"}></legend>
                        <div class="form-group">
                            <label class="src-only">
                                <{translate id="API-Schlüssel" textdomain="Core"}>
                            </label>
                            <div class="field-group">
                                <div class="field-icon">
                                    <i aria-hidden="true" class="acms-icon acms-icon-key"></i>
                                </div>
                                <input name="api"
                                       id="apikey"
                                       type="text"
                                       class="field"
                                       required="required"
                                       value="<{if $acmsContents->acms['apikey']}><{$acmsContents->acms['apikey']}><{/if}>"
                                       placeholder="Ihr neuer API-Schlüssel" />
                            </div>
                        </div>
                        <div class="field-hint">
                            Mit dem Ändern des API-Schlüssels und dem daraufhin 
                            erfolgenden Absenden des Formulars erklären Sie 
                            sich mit der Speicherung und Verarbeitung der 
                            gesendeten Daten einverstanden.
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <button class="button border-button small" type="reset" data-dismiss="modal">
                        <span><{translate id="Schließen" textdomain="Core"}></span>
                    </button>
                    <button type="submit" class="button primary small">
                        <i aria-hidden="true" class="acms-icon acms-icon-paper-plane regular"></i>
                        <span><{translate id="Absenden" textdomain="Core"}></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
