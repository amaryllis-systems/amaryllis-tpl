<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{*
====== ACHTUNG ======

DIESES TEMPLATE ENTHÄLT INFORMATIONEN ÜBER DAS SYSTEM UND DEN
HERSTELLER/AUTHOREN DES SYSTEMS. DIESE DATEN DÜRFEN NICHT MANIPULIERT ODER
VERÄNDERT WERDEN UND MÜSSEN OFFEN SICHTBAR IM TEMPLATE UND DAMIT AUF DER
AUFGERUFENENE SEITE SICHTBAR SEIN!!

*}>
<section id="core-version-informations" data-cssfile="media/css/admin/version-informations.min.css">
    <section class="primary arrow-box arrow-bottom page-section v1">
        <div class="container fixed-1200">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="large page-title v7">
                        <div class="center aligned content">
                            <h1 class="white-text title" itemprop="name">Amaryllis-CMS <small>Versions-Informationen</small></h1>
                            <p class="white-text sub-title">Informationen über Ihre aktuell installierte Amaryllis-CMS Version.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="white arrow-box arrow-bottom page-section v1">
        <div class="container fixed-1200">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="small content-block v5 text-center push-updown-35">
                        <div class="medium center aligned page-title v7">
                            <h2 class="title">
                                <img src="<{$acmsData->acmsUrl}>/media/icons/icon.png" alt="" title="Amaryllis Logo" />&nbsp;
                                <{$acmsContents->acms['synonym']}> <{$acmsContents->acms['version']}>.<{$acmsContents->acms['dbversion']}>&nbsp;
                                <{if $acmsContents->acms['isEnterprise']}>
                                    <span class="text-xxsmall label v1 royal">Enterprise</span>
                                <{elseif $acmsContents->acms['isBusiness']}>
                                    <span class="text-xxsmall label v1 primary">Business</span>
                                <{else}>
                                    <span class="text-xxsmall label v1 default">Basic</span>
                                <{/if}>
                            </h2>
                            <small class="text-xxxsmall sub-title">
                                <a title="Webseite von Amaryllis Systems GmbH" 
                                   class="provider-link" 
                                   href="https://www.amaryllis-systems.de/">
                                    <i aria-hidden="true" class="acms-icon acms-icon-industry"></i>
                                    <span>
                                        Amaryllis Systems GmbH
                                    </span>
                                </a> | 
                                <span>
                                    <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>&nbsp;
                                    <span><{$acmsContents->acms['releaseDate']}></span>
                                </span>

                            </small>
                        </div>
                        <div class="copyright">
                            <p><{translate id="Alle Rechte vorbehalten" textdomain="Core"}><br>
                                &copy; <{$acmsData->acmsYear}> Amaryllis Systems GmbH</p>
                        </div>
                        <div class="update-check" 
                             data-module="apps/admin/version/core-version" 
                             data-process="<{generateUrl name="core_update"}>"
                             data-remote="<{generateUrl name="core_update_action" parameters=['action' => 'action']}>">
                            <div class="text-center">
                                <span id="update-check-icon" style="font-size: 3rem;">
                                    <span class="acms-icon-stack acms-icon-lg">
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-check acms-icon-stack-1x text-success"></i>
                                        <i data-role="icn" aria-hidden="true" class="acms-icon acms-icon-circle regular acms-icon-stack-2x text-success"></i>
                                    </span>
                                </span>
                            </div>
                            <div class="percentage-70">
                                <div class="h3 text-center" data-role="notice">Die aktuelleste Version ist bereits installiert</div>
                            </div>
                        </div>
                        <div class="text-small button-toolbar compact presenter-footer">
                            <button type="button" class="default small left labeled icon-button button" data-module="apps/ui/informations/modal" data-target="#change-apikey-modal">
                                <i aria-hidden="true" class="acms-icon acms-icon-key"></i>
                                <span class="content"><{translate id="API-Schlüssel" textdomain="Core"}></span>
                            </button>
                            <button type="button" class="default small left labeled icon-button button" data-module="apps/ui/informations/modal" data-target="#contact-support-modal">
                                <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>
                                <span class="content">Support</span>
                            </button>
                            <a role="button" class="default small left labeled icon-button button" target="_blank" href="https://www.amaryllis-support.de/" title="System Dokumentation">
                                <i aria-hidden="true" class="acms-icon acms-icon-question-circle regular"></i>
                                <span class="content">Dokumentation</span>
                            </a>
                            <button type="button" class="default small left labeled icon-button button" data-module="apps/ui/informations/modal" data-target="#core-release-notes-modal">
                                <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
                                <span class="content">Release Informationen</span>
                            </button>
                            <button type="button" class="default small left labeled icon-button button" data-module="apps/ui/informations/modal" data-target="#core-changelog-modal">
                                <i aria-hidden="true" class="acms-icon acms-icon-history"></i>
                                <span class="content">Changelog</span>
                            </button>
                            <button type="button" class="default small left labeled icon-button button" data-module="apps/ui/informations/modal" data-target="#send-feedback-modal">
                                <i aria-hidden="true" class="acms-icon acms-icon-heart"></i>
                                <span class="content">Feedback</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <{include file="acmsfile:Core|admin/version/ContactSupport.tpl" nocache}>
        <{include file="acmsfile:Core|admin/version/Feedback.tpl" nocache}>
        <{include file="acmsfile:Core|admin/version/ChangeApi.tpl" nocache}>
        <{include file="acmsfile:Core|admin/version/Changelog.tpl" nocache}>
        <{include file="acmsfile:Core|admin/version/ReleaseNotes.tpl" nocache}>
    </section>
    <section class="primary arrow-box arrow-bottom page-section v1">
        <div class="container">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="large center aligned page-title v7">
                        <div class="center aligned content">            
                            <h2 class="white-text title">System-Informationen</h2>
                            <p class="white-text sub-title">
                                Informationen über Ihr laufendes System und PHP-Informationen 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="version-infos" class="white arrow-box arrow-bottom page-section v1">
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30" id="amaryllis-cms-version">
                <div class="column xxsmall-12 medium-10 medium-offset-1">
                    <div class="panel v1 collapsible collapsed" 
                         data-module="apps/ui/components/panel">
                        <div class="heading" aria-controls="acmsVersionInfos">
                            <h3 class="title">
                                <i aria-hidden="true" class="acms-icon acms-icon-database left"></i>
                                <span><{$acmsContents->acms['title']}></span>
                            </h3>
                        </div>
                        <div id="acmsVersionInfos" 
                             class="panel-collapse" 
                             role="region" 
                             aria-label="<{$acmsContents->acms['title']}>">
                            <div class="content hiding" aria-hidden="true">
                                
                                <ul class="collection v1 with-header">
                                    <li class="collection-header">
                                        Server-Informationen
                                    </li>
                                    <li class="collection-item">
                                        <b>Version</b>: <{$acmsContents->acms['version']}> (Build-Version: <{$acmsContents->acms['dbversion']}>)
                                    </li>
                                    <li class="collection-item">
                                        <b>PHP-Version</b>: <{$acmsContents->php['version']}>
                                    </li>
                                    <li class="collection-item">
                                        <b>PHP-Uname</b>: <{$acmsContents->php['php_uname']}>
                                    </li>
                                    <li class="collection-item">
                                        <b>PHP-Extensions</b>:<br> <{$acmsContents->php['extensions']}>
                                    </li>
                                    <li class="collection-item">
                                        <b>Webserver</b>: <{$acmsContents->php['webserver']}>
                                    </li>
                                    <li class="collection-item">
                                        <b>Datenbank-Version</b>: <{$acmsContents->database_version}>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel v1 collapsible collapsed" 
                         data-module="apps/ui/components/panel">
                        <div class="heading" aria-controls="filesystem">
                            <h3 class="title">
                                <i aria-hidden="true" class="acms-icon acms-icon-folder left"></i>
                                <span><{translate id="Dateisystem" textdomain="Core"}></span>
                            </h3>
                        </div>
                        <div id="filesystem" 
                             class="panel-collapse"
                             role="region" 
                             aria-label="<{translate id="Dateisystem" textdomain="Core"}>">
                            <div class="content hiding" aria-hidden="true">
                                <{foreach $acmsContents->acms['filesystem'] as $header => $infos}>
                                    <ul class="collection v1 with-header">
                                        <li class="collection-header"><{$header}></li>
                                            <{foreach $infos as $info}>
                                            <li class="collection-item">
                                                <div class='title'><{$info['name']}></div>
                                                <p><{$info['message']}></p>
                                                <div class="secondary-content">
                                                    <{if $info['valid']}>
                                                        <i aria-hidden="true" class="acms-icon acms-icon-check-circle text-success"></i>
                                                    <{else}>
                                                        <i aria-hidden="true" class="acms-icon acms-icon-times-circle text-danger"></i>
                                                    <{/if}>
                                                </div>
                                            </li>
                                        <{/foreach}>
                                    </ul>
                                <{/foreach}>
                            </div>
                        </div>
                    </div>
                    <div class="panel v1 collapsible collapsed" data-module="apps/ui/components/panel">
                        <div class="heading" aria-controls="phpinfo">
                            <h3 class="title">
                                <i aria-hidden="true" class="acms-icon acms-icon-code left"></i>
                                <span><{translate id="PHP Informationen" textdomain="Core"}></span>
                            </h4>
                        </div>
                        <div id="phpinfo" 
                             class="panel-collapse" style="max-height: 100%;"
                             role="region" 
                             aria-label="<{translate id="PHP Informationen" textdomain="Core"}>"
                             >
                            <div class="content hiding" aria-hidden="true">
                                <div><{$acmsContents->phpinfoString}></div>
                            </div>
                        </div>
                    </div>

                    <div class="footer">
                        <p class="text-primary">
                            Entwickelt und bereitgestellt von 
                            <a class="copyright-holder" title="Amaryllis Systems GmbH" href="https://www.amaryllis-systems.de/">Amaryllis Websolutions</a>
                            ,vertrieben von <a class="copyright-holder" title="Amaryllis Systems GmbH" href="https://www.amaryllis-systems.de/">Amaryllis Systems GmbH</a>
                            <br>
                            &copy; <{$acmsData->acmsYear}> Alle Rechte vorbehalten <a title="Amaryllis Systems GmbH" target="_blank" href="https://www.amaryllis-systems.de/">Amaryllis Systems GmbH</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
