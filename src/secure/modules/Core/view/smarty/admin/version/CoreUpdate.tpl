<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <div class="container fixed-1200" data-cssfile="media/css/widgets/update-bars.min.css">
        <section class="row full xxsmall-push-updown-30">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1 itemprop="name">Amaryllis-CMS <small>System-Update</small></h1>
                </div>
            </div>
        </section>
        <{if $acmsContents->updateInfos}>
            <section class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <div class="push-15 content-block v6">
                        <h2><i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle text-muted"></i> Achtung</h2>
                        <p>
                            Sie sind gerade dabei das System zu aktualisieren. 
                            Wir empfehlen Ihnen, vorher ein Sicherheits-Backup 
                            zu machen. Speichern Sie dazu das aktuelle Datei-
                            System in einer Backup-Location und exportieren Sie 
                            die gesamten Amaryllis-CMS Datenbanken 
                            (System und Module)
                        </p>
                    </div>
                </div>
            </section>
            <section class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
                    <div class="content-block v6 bordered border-solid">
                        <dl>
                            <dt>Aktuelle Version</dt>
                            <dd><{$acmsContents->core['version']}>.<{$acmsContents->core['buildVersion']}></dd>
                            <dt>Versions-Name</dt>
                            <dd><{$acmsContents->core['versionName']}></dd>
                        </dl>
                    </div>
                </div>
                <div class="column xxsmall-12" data-module="view/admin/version/core-update" data-can-update="true" data-remote="<{generateUrl name="core_update_action" parameters=['action' => 'action']}>">
                    <div id="core-update-wrapper" class="push-15">
                        <h2>Update Check</h2>
                        <{*
                        <div class="update-bar warning">
                            <div class="split"><i aria-hidden="true" class="acms-icon acms-icon-sync-alt"></i></div>
                            <div class="text">System <a href="#">Modulename</a> <b>v1.0.2</b> ist verfügbar! <a role='button' href="#">Jetzt aktualisieren!</a> </div>
                        </div>
                        <div class="update-bar success">
                            <div class="split"><i aria-hidden="true" class="acms-icon acms-icon-check"></i></div>
                            <div class="text">System <a href="#">Modulename</a> <b>v1.0.2</b> ist bereits die aktuellste Version! <a role='button' href="#">Versions-Informationen</a> </div>
                        </div>
                        *}>
                    </div>
                </div>
            </section>
        <{elseif $acmsContents->updateForm}>
            <section class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <{include $acmsContents->updateForm['template'] form=$acmsContents->updateForm nocache}>
                </div>
            </section>
        <{elseif $acmsContents->updateProtocol}>

        <{/if}>
    </div>
</section>
