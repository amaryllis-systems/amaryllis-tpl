<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="contact-support-modal" 
     class="modal" 
     role="dialog" 
     aria-labelledby="contact-support-modal-title" 
     aria-hidden="true" tabindex="-1">
    <div class="modal-dialog">
        <div class="content">
            <div class="heading">
                <button class="close" 
                        type="button" 
                        data-dismiss="modal" 
                        aria-label="<{translate id="Schließen" textdomain="Core"}>"
                        >
                    <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                    <span class="src-only">
                        <{translate id="Schließen" textdomain="Core"}>
                    </span>
                </button>
                <h3 class="title" id="contact-support-modal-title">
                    <{translate id="Support beziehen" textdomain="Core"}></h3>
            </div>
            <div class="main">
                <div class="amaryllis-kontakt">
                    <h4>Support-Anfragen können Sie über die folgenden Kontakt-Möglichkeiten stellen:</h4>
                    <address>
                        <div itemscope itemtype="http://schema.org/ProfessionalService">
                            <div class="row full xxsmall-push-updown-30">
                                <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                                    <h3 itemprop="name">Amaryllis Systems GmbH</h3>
                                    <div class="push-updown-10" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span class="acms-icon acms-icon-map-marker"></span>
                                        <span itemprop="streetAddress">Delbrückstraße 10</span><br>
                                        <span style="padding-left: 14px;">D-</span><span itemprop="postalCode">25541</span> <span itemprop="addressLocality">Brunsbüttel</span>,<br>
                                        <span style="padding-left: 14px;" itemprop="addressRegion">Schleswig-Holstein</span>
                                    </div>
                                    <div class="push-updown-10">
                                        <span class="acms-icon acms-icon-phone"></span> <span itemprop="telephone">+49 (800) 72 45 68 7</span> <br>
                                        <span style="padding-left: 14px;" class="text-small text-warning">(kostenpflichtiges Abonnement notwendig)</span><br>
                                        <span class="acms-icon acms-icon-envelope"></span>&nbsp;
                                        <{getMailto email="kontakt@amaryllis-systems.de" subject="Support-Anfrage" assign="mailSupport"}>
                                        <a href="<{$mailSupport}>" title="Support-Anfrage stellen">
                                            <span itemprop="email">kontakt@amaryllis-systems.de</span>
                                        </a><br>
                                        <span style="padding-left: 14px;" class="text-small text-muted">(kein Abonnement notwendig)</span><br>
                                        <i aria-hidden="true" class="acms-icon acms-icon-external-link-alt"></i> &nbsp;
                                        <a itemprop="url"
                                           target="_blank" 
                                           href="https://www.amaryllis-systems.de/"
                                           title="Amaryllis Systems GmbH">
                                            www.amaryllis-systems.de
                                        </a>
                                    </div>
                                </div>
                                <div class="column xxsmall-12 medium-6 large-6 xlarge-6 xxlarge-6">
                                    <img class="responsive" 
                                         itemprop="logo"
                                         src="http://www.amaryllis-systems.de/logo.png"
                                         title="Amaryllis Systems GmbH"
                                         alt="Logo" />
                                </div>
                            </div>
                        </div>
                    </address>
                </div>
            </div>
            <div class="footer">
                <button class="button primary small" type="button" data-dismiss="modal">
                    <span><{translate id="Schließen" textdomain="Core"}></span>
                </button>
            </div>
        </div>
    </div>
</div>
