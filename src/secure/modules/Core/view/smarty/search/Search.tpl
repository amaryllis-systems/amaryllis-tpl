<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1024 fixed-1200">
            <div class="full row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="primary page-section v1">
        <div class="container fixed-1024 fixed-1200">
            <div class ="full row xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <div class="large center aligned icon page-title v7">
                        <i aria-hidden="true" class="circular acms-icon acms-icon-search white-text text-xlarge"></i>
                        <div class="content">
                            <h1 class="title white-text"><{$acmsContents->get('acmsTitle')}></h1>
                            <p class="sub-title"><{translate id="Durchsuchen Sie die Inhalte unserer Webseite"}></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container fixed-1024 fixed-1200">
            <div class="full row xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <{* include file="acmsfile:Core|search/form/SearchForm.tpl" form=$acmsContents->searchForm nocache *}>
                    <div class="form-wrapper">
                        <form method="GET" role="search" name="core-search" class="form" action="<{$acmsContents->searchAction}>">
                            <div class="form-group ">
                                <label for="search" class="field-label">
                                    Suche 
                                    <a class="acms-helptip" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true" data-title="Geben Sie einzelne Suchwörter ein und separieren Sie einzelne Suchzeichen mit einem Leerzeichen.">
                                        <span class="cursor-pointer acms-icon acms-icon-question-circle regular floated right"></span>
                                    </a>
                                </label>
                                <div class="field-group">
                                    <input type="search" value="<{$acmsContents['options']->getOption('keyword')}>" name="search" data-title="Suche" data-content="Geben Sie einzelne Suchwörter ein und separieren Sie einzelne Suchzeichen mit einem Leerzeichen." placeholder="Suche" class="field">
                                    <button class="field-icon" data-trigger="search" type="submit" aria-label="<{translate id="Suchen" textdomain="Core"}>...">
                                        <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <p class="text-right display-block">
                                    <span class="cursor-pointer text-muted" role="button" aria-expanded="false" aria-controls="extended-search-wrapper" data-module="apps/ui/helpers/collapse" data-target="#extended-search-wrapper" title="Eweitern Sie die Such-Optionen">
                                        <i aria-hidden="true" class="acms-icon acms-icon-cogs"></i>
                                        <span><{translate id="Erweiterte Suche" textdomain="Core"}></span>
                                    </span>
                                </p>
                                <div id="extended-search-wrapper" class="hiding" role="region" aria-label="Erweiterte Such-Optionen" aria-expanded="false">
                                    <div class="form-group">
                                        <label for="search-module">
                                            <{translate id="Sektionen" textdomain="Core"}>
                                        </label>
                                        <ul class="statinfo v1">
                                            <{assign var="checkedModules" value=$acmsContents['options']->getModule()}>
                                            <{foreach $acmsContents['options']->getModuleWhitelist() as $dirname => $name}>
                                                <li class="item">
                                                    <div class="text"><{$name}></div>
                                                    <div class="statinfo">
                                                        <div class="small toggle-variant switch v2">
                                                            <input type="checkbox" name="section[]" value="<{$dirname}>" id="module_<{$dirname}>" <{if in_array($dirname, $checkedModules)}>checked="checked"<{/if}> />
                                                            <label for="module_<{$dirname}>" aria-label="<{translate id="Sektion %s durchsuchen" textdomain="Core" args=$name}>"></label>
                                                        </div>
                                                    </div>
                                                </li>
                                            <{/foreach}>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <{if $acmsContents->result}>
            <div class ="full row xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <div class="push-10" role="region" aria-label="Such-Ergebnisse">
                        <h2>Such-Ergebnisse <{if $acmsContents->totalResult}><small class="right floated"><span class="badge v1 primary" aria-label="Anzahl der Ergebnisse"><{$acmsContents->totalResult}></span></small><{/if}></h2>
                        <div class="boxed items v1">
                            <{foreach $acmsContents->results->getPreparedResult() as $result}>
                                <{include file=$result['template'] item=$result}>
                            <{/foreach}>
                        </div>
                    </div>
                </div>
            </div>
            <{/if}>
        </div>
    </section>
</section>
