<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>


<div class="item">
    <div class="small image">
        <img src="<{$item->getImage()}>" title="<{$item->getTitle()}>" alt="" />
    </div>
    <div class="content">
        <a class="header"
            href="<{$item->getUrl()}>"
            title="<{$item->getTitle()}>">
            <{$item->getTitle()}>
        </a>
        <div class="meta">
            <span>
                <{if $item->getSubmitter()}>
                    <a class="round tag v1" href="<{$item['submitter']['itemUrl']}>" title="<{$item['submitter']['username']}>"><span class="tag-image"><img src="<{$item['submitter']['avatar']}>" alt="" title="<{$item['submitter']['username']}>" /></span> <span><{$item['submitter']['username']}></span></a>
                <{/if}>
                <{if $item->getTags()}>
                    <{foreach $item->getTags() as $tag}>
                        <a class="tag v1" href="<{$tag['url']}>"><span class="tag-image"><i aria-hidden="true" class="acms-icon acms-icon-tag"></i></span> <span><{$tag['title']}></span></a>
                    <{/foreach}>
                <{/if}>
            </span>
            <span class="tag v1">
                <span class="tag-image"><i aria-hidden="true" class="acms-icon acms-icon-calendar"></i></span> <{$item->searchDate}>
            </span>
            <span class="right floated">
                <i aria-hidden="true" class="acms-icon acms-icon-rss"></i>
            </span>
        </div>
        <div class="description">
            <{$item->getTeaser()}>
        </div>
        <div class="extra">
            <a class="right floated primary icon-right icon button" href="<{$item->getUrl()}>" title="<{$item->getTitle()}>">
                <span><{translate id="mehr erfahren" textdomain="Core"}></span>
                <i aria-hidden="true" class="acms-icon acms-icon-chevron-right icon-right"></i>
            </a>
        </div>
    </div>
</div>
