<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="error-<{$acmsContents->errorCode}>" class="cover v1">
    <div class="container">
        <div class="page-title">
            <h1>
		<span class="text-danger"> <span class="acms-icon acms-icon-ban"> </span> </span>
                <span class="text-danger"><{translate id="Keine Berechtigung" textdomain="Core"}></span>
            </h1>
        </div>
        <div class="alert v1 danger" role="alert" data-cssfile="media/css/components/alert.min.css">
            <p>
                <{if $acmsContents->errorMessage}>
                    <{$acmsContents->errorMessage}>
                <{else}>
                    <{translate id="NO_PERMS" textdomain="Core"}>
                <{/if}>
            </p>
        </div>
        <div class="row">
            <div class="error-actions column xxsmall-12">
                <a href="<{$acmsData->acmsUrl}>"
                   class="button primary">
                    <i aria-hidden="true" class="acms-icon acms-icon-home"></i>
                    <{translate id="Zurück auf die Startseite" textdomain="Core"}>
                </a>
                <a href="mailto:<{$acmsContents->contact_mail}>" class="button default">
                    <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>
                    <{translate id="Support kontaktieren" textdomain="Core"}>
                </a>
            </div>
        </div>
    </div>
</div>
