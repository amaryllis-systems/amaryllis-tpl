<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="error-<{$acmsContents->errorCode}>" class="cover v1">
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12 text-center">
                <div class="h1"><{translate id="Ups..." textdomain="Core"}></div>
            </div>
            <div class="column xxsmall-12 text-center">
                <div class="text-center floating button large red white-text">
                    <span><{$acmsContents->errorCode}></span>
                    <span class="white-text"><i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle-sign"></i></span>
                </div>
            </div>
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1>
                        <span class="text-danger"> <i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle"></i> </span>
                        <span class="text-danger"><{translate id="Fehler" textdomain="Core"}></span>
                    </h1>
                </div>
                <div class="alert v1 danger" role="alert" data-cssfile="media/css/components/alert.min.css">
                    <span class="alert-title">
                        <{$acmsContents->errorTitle}>
                    </span>
                    <p><{$acmsContents->errorMessage}></p>
                </div>
            </div>
        </div>
        <div class="row full">
            <div class="error-actions column xxsmall-6">
                <a href="<{$acmsData->acmsUrl}>" class="button primary">
                    <i aria-hidden="true" class="acms-icon acms-icon-home"></i>
                    <{translate id="Zurück auf die Startseite" textdomain="Core"}>
                </a>
                <a href="mailto:<{$acmsContents->contact_mail}>" class="button default">
                    <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>
                    <{translate id="Support kontaktieren" textdomain="Core"}>
                </a>
            </div>
        </div>
    </div>
</div>
