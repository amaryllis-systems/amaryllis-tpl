<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="error-<{$errorCode}>" class="cover v1">
    <div class="container">
        <div class="center aligned icon page-title v7">
            <i aria-hidden="true" class="acms-icon acms-icon-ban text-danger"></i>
            <div class="content">
                <h1 class="text-danger">
                    <span class="text-danger">
                        <{if $errorTitle}><{$errorTitle}><{else}><{translate id="Keine Berechtigung" textdomain="Core"}><{/if}>
                    </span>
                </h1>
            </div>
        </div>
        <div class="danger bordered alert v3" role="alert">
            <h2><{if $errorMessage}><{$errorMessage}><{else}><{translate id="NO_PERMS" textdomain="Core"}><{/if}></h2>
        </div>
        <div class="full row">
            <div class="error-actions column medium-6 medium-offset-3">
                <a href="<{$acmsUrl}>" class="large primary button">
                    <i class="acms-icon acms-icon-home"></i>
                    <span><{translate id="Zurück zur Startseite" textdomain="Core"}></span>
                </a>
                <a href="mailto:<{$contact_mail}>" class="default large button">
                    <i class="acms-icon acms-icon-envelope"></i>
                    <span><{translate id="Support kontaktieren" textdomain="Core"}></span>
                </a>
            </div>
        </div>
    </div>
</div>
