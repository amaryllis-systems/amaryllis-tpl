<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container" id="<{$table_id}>">
    <!-- Table Header -->
    <{if $acms_table_header}>
        <div class="row">
            <div class="column xxsmall-12">
                <div class="acms-pre-table content-block v6 wide">
                    <p><{$acms_table_header}></p>
                </div>
            </div>
        </div>
    <{/if}>
    <!-- intro buttons -->
    <div class="row hidden">
        <{if $acms_main_filter_form || $acms_quicksearch || $acms_custom_filter_form}>
            <div class="column xxsmall-12 medium-8 large-9 xlarge-9 xxlarge-9">
                <{if $acms_main_filter_form}>
                <div class="push-5">
                    <{include file=$acms_main_filter_form['template'] form=$acms_main_filter_form}>
                </div>
                <{/if}>
                <{if $acms_quicksearch || $acms_custom_filter_form}>
                    <div class="push-5">
                        <{if $acms_custom_filter_form}>
                            <{include file=$acms_custom_filter_form['template'] form=$acms_custom_filter_form}>
                        <{/if}>
                    </div>
                <{/if}>
            </div>
        <{/if}>
        <{if $acms_intro_buttons || $acms_pagenav.pagination}>
            <div class="column xxsmall-12 medium-4 medium-offset-8 large-3 xlarge-3 xxlarge-3">
                <div class="button-toolbar">
                    <{if $acms_intro_buttons}>
                        <div class="intro-buttons right">
                            <{foreach $acms_intro_buttons as $introButton}>
                                <a href="<{$introButton['url']}>" class="button small <{if $introButton['class']}> <{$introButton['class']}><{/if}>" title="<{$introButton['title']}>">
                                    <i class="<{$introButton.fonticon}>"></i> 
                                    <span><{$introButton.caption}></span>
                                </a>
                            <{/foreach}>
                        </div>
                    <{/if}>

                    <{if $acms_pagenav.pagination}>
                        <div class="acms-pagenav-wrapper">
                            <{$acms_pagenav.pagination}>
                        </div>
                    <{/if}>
                </div>
            </div>
        <{/if}>
    </div>
    <div class="row">
    
    </div>
    <div class="row">
    <{if $acms_action_buttons || $acms_batch_actions}>
        <form id="form_<{$table_id}>" action="<{$acms_form_action_url}>" method="post">
            <input type='hidden' name='op' id='op' value='' />
    <{/if}>
        <div class="column xxsmall-12 table-responsive">
        <table class='table striped hover compact' data-module="view/table/table">
            <thead>
            <tr>
             <{foreach $acms_columns as $column}>
                <{if $column.name == "checked"}>
                    <th <{$column.attributes}>>
                        <button type ="button" name="Toggle_All" class="button tiny default acms-table-toggle-all">
                            <span title="<{translate id='Alles toggeln' textdomain='core'}>" class="acms-icon acms-icon-square regular"></span>
                        </button>
                    </th>
                <{else}>
                    <th <{$column['attributes']}>>
                        <{if $column['links']}>
                            <a class="acms-table-action<{if $column['links']['asc']['isSort'] && $column['isOrder']}> text-success acms-table-sort-active<{/if}>" href="<{$column['links']['asc']['url']}>" title="<{$column['links']['asc']['caption']}>">
                                <span class="acms-icon acms-icon-sort-alpha-up<{if $column['links']['asc']['isSort'] && $column['isOrder']}> text-success<{/if}>"></span>
                            </a>
                        <{/if}>
                        <strong>
                            <span class="<{if $column['isOrder']}>text-success<{else}>text-muted<{/if}>"><{$column['caption']}></span>
                        </strong>
                        <{if $column['links']}>
                            <a class="acms-table-action<{if $column['links']['desc']['isSort'] && $column['isOrder']}> text-success acms-table-sort-active<{/if}>" href="<{$column['links']['desc']['url']}>" title="<{$column['links']['desc']['caption']}>">
                                <span class="acms-icon acms-icon-sort-alpha-down<{if $column['links']['desc']['isSort'] && $column['isOrder']}> text-success<{/if}>"></span>
                            </a>
                        <{/if}>
                    </th>
                <{/if}>
             <{/foreach}>
             <{if $acms_has_actions}>
                <th width='<{$acms_actions_column_width}>' align='center'>
                    <{if $acms_show_action_column_title}>
                        <strong><{translate id='Aktionen' textdomain="Core"}></strong>
                    <{/if}>
                </th>
             <{/if}>
            </tr>
            </thead>
            <tfoot>
                <tr>
                    <{if $acms_batch_actions}>
                        <td>

                        </td>
                    <{/if}>
                 </tr>
            </tfoot>
            <{if $acms_models}>
                <{foreach from=$acms_models item=model}>
                    <{if $acms_action_buttons}>
                        <input type='hidden' name='<{$table_id}>_models[]' class='listed-models' value='<{$model.id}>' />
                    <{/if}>
                    <tr>
                        <{foreach from=$model.columns item=column}>
                            <td <{$column.attributes}>><{$column.value}></td>
                        <{/foreach}>
                        <{if $model.actions}>
                            <td class="<{* $model.class *}>" align='center'>
                                <{foreach from=$model.actions item=action}>
                                    <{$action}>
                                <{/foreach}>
                            </td>
                        <{/if}>
                    </tr>
                <{/foreach}>
            <{else}>
                <tr>
                    <td class='head' style='text-align: center; font-weight: bold;' colspan="<{$acms_colspan}>"><{translate id='Kein Ergebnis' textdomain="acms" }></td>
                </tr>
            <{/if}>

        </table>
        <{*
        <div class="button-group dropdown" data-module="ui/navs/dropdown">
          <button class="button small primary dropdown-toggle" type="button">
            <{translate id='Wähle eine Aktion' textdomain="Core"}>
          </button>
          <div class="dropdown-content">
          <ul>
             <{foreach $acms_batch_actions as $key => $action}>
                 <li class="dropdown-hover clearfix cursor-pointer <{$action.class}>" data-action="<{$action.op}>">
                    <span class="<{$action.fonticon}> left"></span> <span class="right floated"><{$action.text}></span>
                 </li>
             <{/foreach}>
          </ul>
          </div>
        </div>
        *}>
        </div>
        
        <{if  $acms_action_buttons || $acms_batch_actions}>
            <{if $acms_action_buttons}>
                <div class="button-group">
                    <{foreach from=$acms_action_buttons item=actionButton}>
                        <input type="submit" name="<{$actionButton.op}>" onclick="this.form.elements.op.value='<{$actionButton.op}>'" value="<{$actionButton.text}>" />
                    <{/foreach}>
                </div>
            <{/if}>
        </form>
        <{/if}>
    </div>
        <{if $acms_intro_buttons || $acms_pagenav.pagination}>
            <div class="row">
                <{if $acms_intro_buttons}>
                    <div class="column xxsmall-12 text-right">
                        <{foreach from=$acms_intro_buttons item=introButton}>
                            <a href="<{$introButton.url}>" class="button small <{if $introButton.class}> <{$introButton.class}><{/if}>" title="<{$introButton.title}>">
                                <i class="<{$introButton.fonticon}>"></i> <span><{$introButton.caption}></span>
                            </a>
                        <{/foreach}>
                    </div>
                <{/if}>
                <!-- Pagination -->
                <{if $acms_pagenav.pagination}>
                    <{$acms_pagenav.pagination}>
                <{/if}>
            </div>
        <{/if}>
</div>
