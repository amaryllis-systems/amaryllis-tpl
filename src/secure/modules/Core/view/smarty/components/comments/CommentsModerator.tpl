<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-pushupdown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container fixed-1200">
            <div class="row full xxsmall-pushupdown-30">
                <div class="column xxsmall-12">
                    <div class="large center aligned icon page-title v7">
                        <i aria-hidden="true" class="circular acms-icon acms-icon-tachometer-alt white-text text-xlarge"></i>
                        <div class="content">
                            <h1 class="title white-text">
                                <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                            </h1>
                            <p class="sub-title text-muted">Dashboard für unsere Kommentar Moderatoren</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container fixed-1200">
            <div class="row full xxsmall-pushupdown-30">
                <div class="column xxsmall-12 medium-12 large-8 xlarge-8 xxlarge-8">
					<div class="page-title v7">
						<i aria-hidden="true" class="acms-icon acms-icon-unlock"></i>
						<div class="content">
                            <h1 class="title">
                                <span itemprop="name"><{translate id="Abwartende Genehmigungen"}></span>
                            </h1>
                            <p class="sub-title text-muted"><{translate id="Abwartende Genehmigungen für eingereichte Kommentare"}></p>
                        </div>
					</div>
                    <div class="push-10 cards">
                        <{if $acmsContents->artikel}>
							<{generateUrl name="core_comments_action" parameters=['action' => 'action'] assign="commentActionUrl"}>
							<{foreach $acmsContents->comments as $comment}>
								<div class="card v2" data-module="apps/apf/approval/approval" data-remote="<{$commentActionUrl}>" data-id="<{$comment['id']}>">
									<div class="card-content">
										<div class="right floated small avatar">
											<i class="acms-icon acms-icon-exclamation-circle text-danger"></i>
										</div>
										<a class="card-title text-danger" href="<{$comment['itemUrl']}>" title="Beitrag aufrufen">
											<{$comment['title']}>
										</a>
										<div class="card-meta">
											<{translate id="Genehmigung ausstehend" textdomain="Core"}>
										</div>
										<div class="description">
											<{translate id="%s hat diesen Beitrag eingereicht und wartet auf deine Genehmigung zur Veröffentlichung." textdomain="Core" args=[$comment['commentSubmitter']['username']]}>
										</div>
                                        <div class="extra card-content">
                                            <{$comment['comment']}>
                                        </div>
									</div>
									<div class="extra card-content">
										<a class="stacked primary icon border-button button" href="<{$comment['itemUrl']}>" title="Beitrag in neuem Fenster aufrufen" target="_blank">
											<span>aufrufen</span>
											<i class="icon-right acms-icon acms-icon-external-link-alt"></i>
										</a>
									</div>
									<div class="extra card-content">
										<div class="two buttons">
											<button class="button small success icon-button" data-trigger="approve">
												<i class="acms-icon acms-icon-check"></i>
												<span class="content"><{translate id="genehmigen" textdomain="Core"}></span>
											</button>
											<button class="button small danger icon-button border-button" data-trigger="decline" data-title="Artikel Ablehnen">
												<i class="acms-icon acms-icon-ban"></i>
												<span class="content"><{translate id="ablehnen" textdomain="Core"}></span>
											</button>
										</div>
									</div>
								</div>
							<{/foreach}>
						<{else}>
							<div class="text-center" data-cssfile="media/css/ui/update-bars">
								<div class="update-bar v1 success">
									<div class="split">
										<i aria-hidden="true" class="acms-icon acms-icon-check-circle"></i>
									</div>
									<div class="text">
										<{translate id="Alles erledigt" textdomain="Core"}>
									</div>
								</div>
							</div>
						<{/if}>
                    </div>
					<div class="row full xxsmall-pushupdown-25">
						<div class="column xxsmall-12">
							<h2>
								<i aria-hidden="true" class="acms-icon acms-icon-database"></i>
								Datensicherung
							</h2>
						</div>
						<div class="column xxsmall-12">
							<div class="button-toolbar compact bordered push-10" data-module="apps/admin/common-export">
								<{foreach $acmsContents->exports as $export}>
									<button data-trigger="export" class="button small default" data-remote="<{$export['uri']}>" data-format="<{$export['format']}>">
										<i class="acms-icon <{$export['fonticon']}>"></i>
										<span><{translate id="Export als %s" args=$export['format']}></span>
									</button>
								<{/foreach}>
							</div>
						</div>
					</div>
					<div class="full row push-updown-35">
						<div class="column xxsmall-12 pull-updown-35">
							<div class="compact button-toolbar">
								<a class="small default button" href="<{generateUrl name="core_comments_index"}>" title="Kommentare Übersicht">
									<i class="acms-icon acms-icon-chevron-left"></i>
									<span><{translate id="zurück"}></span>
								</a>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>
</section>
