<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="comment-main-level">
    <div class="comment" data-role="comment" data-id="<{$comment['id']}>">
        <div class="comment-avatar">
            <a class="avatar" href="<{$comment['commentSubmitter']['itemUrl']}>" 
               title="<{$comment['commentSubmitter']['username']}>">
                <img src="<{$comment['commentSubmitter']['avatar']}>" 
                     alt="<{translate id="Avatar" textdomain="Core"}>"
                     title="<{$comment['commentSubmitter']['username']}>">
            </a>
        </div>
        <div class="comment-box">
            <div class="heading">
                <div class="info">
                    <{if $comment['commentSubmitter']}>
                        <a href="<{$comment['commentSubmitter']['itemUrl']}>" title="<{$comment['commentSubmitter']['username']}>">
                            <{if $comment['commentSubmitter']['config']['display_name'] == 1}>
                                <span><{$comment['commentSubmitter']['username']}></span>
                            <{else if $comment['commentSubmitter']['config']['display_name'] == 2}>
                                <span><{$comment['commentSubmitter']['first_name']}> <{$comment['commentSubmitter']['last_name']}></span>
                            <{else if $comment['commentSubmitter']['config']['display_name'] == 3}>
                                <span><{$comment['commentSubmitter']['first_name']}> <{$comment['commentSubmitter']['last_name']}></span> <small><{$comment['commentSubmitter']['username']}></small>
                            <{/if}>
                        </a>
                    <{else}>
                        <span><{$acmsData.localeData.anonymous}></span>
                    <{/if}> <span class="date"><{$comment['pdate-relative']}></span>
                </div>
                <span class="actions">
                    <{if $comment['replyform']}>
                        <a href="#"  data-module="apps/ui/helpers/collapse" data-toggle="collapse" data-target="#comment-replyform-<{$comment['id']}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-reply"></i>
                        </a>
                    <{/if}>
                    <span data-role="comment-vote" data-trigger="upvote" class="cursor-pointer<{if $comment['uservote']}> voted <{$comment['uservote']}><{/if}>" data-id="<{$comment['id']}>">
                        <i class="acms-icon acms-icon-heart"></i> <span class="text-muted" data-role="vote-counter"><{$comment['votes']['upvotes']}></span>
                    </span>
                </i>
            </div>
            <div class="content">
                <div id="comment-text-<{$comment['id']}>" class="text">
                    <{$comment['comment']}>
                </div>
            </div>
            <{if $comment['replyform']}>
                <div class="collapse" id="comment-replyform-<{$comment['id']}>">
                    <{include file=$comment['replyform']['template'] form=$comment['replyform']}>
                </div>
            <{/if}>
        </div>
    </div>
</div>
<{if $comment['subs']}>
    <div class="comment-list replies">
        <{foreach $comment['subs'] as $reply}>
            <{include file="acmsfile:Core|components/comments/comment/NestCommentReply.tpl" comment=$reply}>
        <{/foreach}>
    </div>
<{/if}>
