<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="comments v1" data-module="apps/components/comments/comments" data-remote="<{generateUrl name='core_comments_action' parameters=['action' => 'action']}>">
    <div class="page-title v2">
        <h3 class="title">Kommentare</h3>
        <div class="line v1 short left"></div>
    </div>
    <div class="comment-list">
        <{if $comments}>
            <{foreach $comments as $comment}>
                <{include file="acmsfile:Core|components/comments/comment/NestComment.tpl" comment=$comment}>
            <{/foreach}>
        <{else}>
            <div class="alert v2 info bordered">
                <i class="acms-icon acms-icon-info-circle"></i>
                <div class="alert-content">
                    <p>Leider nocht keine Kommentare vorhanden.. Schreiben Sie jetzt den ersten Kommentar</p>
                </div>
            </div>
        <{/if}>
    </div>
    <div class="page-title v7">
        <h3 class="title">
            <{translate id="Kommentar hinterlassen" textdomain="Core"}>
        </h3>
    </div>
    <{if $commentform && $commentform['template']}>
        <{include file=$commentform['template'] form=$commentform}>
    <{/if}>
</div>
