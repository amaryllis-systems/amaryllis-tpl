<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container">
    <div class="row">
        <div class="column xxsmall-12">
            <div class="page-title v7">
                <h3 class="reviews"><{translate id="Hinterlassen Sie Ihren Kommentar" textdomain="Core"}></h3>
            </div>
            <div class="comment-tabs tab-container" data-module="ui/navs/tabs" data-tab=".comment-tab">
                <ul class="tabs pills" role="tablist">
                    <li class="text-center active comment-tab">
                        <a href="#" data-target="#comments-panel" role="tab" data-toggle="tab">
                            <i aria-hidden="true" class="acms-icon acms-icon-comments acms-icon-3x"></i><br> <{translate id="Kommentare" textdomain="Core"}>
                        </a>
                    </li>
                    <li class="comment-tab text-center">
                        <a class="text-center" href="#" data-target="#add-comment" role="tab" data-toggle="tab">
                            <i aria-hidden="true" class="acms-icon acms-icon-comment-alt regular acms-icon-3x"></i><br> <{translate id="Neuer Kommentar" textdomain="Core"}>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-panel v1 active" id="comments-panel">
                        <section class="comment-list comments v2">
                            <{if $comments}>
                            <{foreach $comments as $comment}>
                                <{include file="acmsfile:Core|components/comments/comment/FlatComment.tpl" comment=$comment}>
                            <{/foreach}>
                            <{else}>
                                <div class="alert v1 info" data-cssfile="media/css/components/alert.min.css">
                                    <p>Leider nocht keine Kommentare vorhanden.. Schreiben Sie jetzt den ersten Kommentar</p>
                                </div>
                            <{/if}>
                        </section>
                    </div>
                    <div class="tab-panel" id="add-comment">
                        <{if $commentform && $commentform['template']}>
                            <{include file=$commentform['template'] form=$commentform}>
                        <{/if}>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
