<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="comment" data-role="comment" data-id="<{$comment['id']}>">
    <a class="medium avatar" href="<{$comment['commentSubmitter']['itemUrl']}>" title="<{$comment['commentSubmitter']['username']}>">
        <img class="" alt="<{$comment['commentSubmitter']['username']}>" title="<{$comment['commentSubmitter']['username']}>" src="<{$comment['commentSubmitter']['avatar']}>" />
    </a>
    <div class="content">
        <a href="<{$comment['commentSubmitter']['itemUrl']}>" title="<{$comment['commentSubmitter']['username']}>" class="author"><{$comment['commentSubmitter']['username']}></a>
        <div class="info">
            <time class="date" datetime="<{$comment['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$comment['pdate-relative']}></time>
        </div>
        <div class="text">
            <{$comment['comment']}>
        </div>
        <div class="actions">
            <span data-role="comment-vote" data-trigger="upvote" class="cursor-pointer<{if $comment['uservote']}> voted <{$comment['uservote']}><{/if}>">
                <i class="acms-icon acms-icon-heart"></i> <span class="text-muted" data-role="vote-counter"><{$comment['votes']['upvotes']}></span>
            </span>
        </div>
    </div>
</div>
