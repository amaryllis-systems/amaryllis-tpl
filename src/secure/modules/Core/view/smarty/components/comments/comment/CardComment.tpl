<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="card v2" data-module="ui/components/cards">
    <div class="middle aligned card-content">
        <a class="inline-avatar medium avatar card-image push-leftright-15" href="<{$comment['commentSubmitter']['itemUrl']}>">
            <img class="" src="<{$comment['commentSubmitter']['avatar']}>" title="<{$comment['commentSubmitter']['username']}>" alt="Avatar">
        </a>
        <a class="card-meta" href="<{$comment['commentSubmitter']['itemUrl']}>">
            <{$comment['commentSubmitter']['username']}>
        </a>
    </div>
    <div class="card-content">
        <a href="<{$comment['itemModel']['itemUrl']}>" title="<{$comment['itemModel']['title']}>" class="card-title h4 push-0 pull-0">
            <{$comment['title']}>
        </a>
        <div class="card-extra">
            <p class="text-xsmall push-0 text-muted">
                <time class="date" datetime="<{$comment['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$comment['pdate-relative']}></time>
                <br>
                <i aria-hidden="true" class="acms-icon acms-icon-user"></i> <{$comment['commentSubmitter']['username']}>
                <br>
                <i aria-hidden="true" class="acms-icon acms-icon-heart"></i> <{$comment['votes']['upvotes']}>
            </p>
            <{if $acmsContents->isCommentManager}>
                <{if $comment['approved'] == 1}>
                    <span class="right floated xxsmall danger label v2"><{translate id="Abgelehnt"}></span>
                <{elseif $comment['approved'] == 2}>
                    <span class="right floated xxsmall warning label v2"><{translate id="Abwartend"}></span>
                <{else}>
                    <span class="right floated xxsmall success label v2"><{translate id="Genehmigt"}></span>
                <{/if}>
            <{/if}>
        </div>
    </div>
    <div class="card-content">
        <{$comment['comment']}>
    </div>
</div>
