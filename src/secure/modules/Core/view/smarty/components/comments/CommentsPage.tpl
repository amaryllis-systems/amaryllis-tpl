<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="full row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container fixed-1200">
            <div class="full row xxsmall-pushupdown-30">
                <div class="column xxsmall-12">
                    <div class="large center aligned icon page-title v7">
                        <i aria-hidden="true" class="circular acms-icon acms-icon-comments white-text text-xlarge"></i>
                        <div class="content">
                            <h1 class="title white-text">
                                <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                            </h1>
                            <p class="sub-title text-muted">Die neuesten Kommentare unserer Mitglieder</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container fixed-1200 pull-updown-35" itemscope="" itemtype="http://schema.org/WebPage">
        <div class="full row push-updown-35">
            <div class="column xxsmall-12">
                <div class="cards" data-cssfile="media/css/ui/cards">
                    <{if $acmsContents->comments}>
                        <{foreach $acmsContents->comments as $comment}>
                            <{include file="acmsfile:Core|components/comments/comment/CardComment.tpl" comment=$comment}>
                        <{/foreach}>
                    <{/if}>
                </div>
            </div>
        </div>
        <{if $acmsContents->isCommentManager}>
            <div class="full row push-updown-35">
                <div class="column xxsmall-12 pull-updown-35">
                    <div class="compact button-toolbar">
                        <a class="small default button" href="<{generateUrl name="core_comments_moderate"}>" title="Kommentare Moderieren">
                            <span>Kommentare genehmigen</span>
                        </a>
                    </div>
                </div>
            </div>
        <{/if}>
    </div>
</section>
