<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<button role="button" type="button" class="button tiny info" data-module="apps/ui/informations/modal" data-target="#tag-quick-info-<{$tag.id}>">
    <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
</button>
<div class="modal"
     id="tag-quick-info-<{$tag.id}>"
     tabindex="-1"
     role="dialog"
     aria-labelledby="tag-quick-info-<{$tag.id}>-title"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <div class="header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span>
                </button>
                <h4 class="title" id="tag-quick-info-<{$tag.id}>-title"><{$tag.title}></h4>
            </div>
            <div class="main text-left">
                <dl>
                    <dt>
                        <{translate id="Tag Name" texdomain="Core"}>
                    </dt>
                    <dd>
                        <i aria-hidden="true" class="acms-icon acms-icon-tag"></i> <span><{$tag.title}></span>
                    </dd>
                    <dt><{translate id="Genehmigt" texdomain="Core"}></dt>
                    <dd>
                        <{if $tag.approved == 1}>
                            <span class="label v1 danger">
                                <{translate id="Abgelehnt" texdomain="Core"}>
                            </span>
                        <{elseif $tag.approved == 2}>
                            <span class="label v1 warning">
                                <{translate id="Abwartend" texdomain="Core"}>
                            </span>
                        <{elseif $tag.approved == 3}>
                            <span class="label v1 success">
                                <{translate id="Genehmigt" texdomain="Core"}>
                            </span>
                        <{/if}>
                    </dd>
                    <dt>
                        <{translate id='Übermittelt von' textdomain="Core"}>
                    </dt>
                    <dd>
                        <i aria-hidden="true" class="acms-icon acms-icon-user"></i> <span class="tag v1 round">
                                                                    <img title="<{$tag.submitter.username}>"
                                                                         alt=""
                                                                         src="<{$tag.submitter.avatar}>" />
                                                                        <a href="<{$tag.submitter.itemUrl}>"
                                                                          title="<{$tag.submitter.username}>">
                                                                            <{$tag.submitter.username}>
                                                                        </a>
                                                                 </span>
                    </dd>
                    <dt>
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i> <def><{translate id='Veröffentlicht am' textdomain="Core"}></def>
                    </dt>
                    <dd>
                        <span><{$tag['pdate']}></span>
                    </dd>
                    <dt>
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i> <def><{translate id='Häufigkeit' textdomain="Core"}></def>
                    </dt>
                    <dd>
                        <span><{if $tag.frequency}><{$tag.frequency}><{else}>0<{/if}></span>
                    </dd>
                    <dt>
                        <i aria-hidden="true" class="acms-icon acms-icon-globe"></i> <def><{translate id='Sprache'}></def>
                    </dt>
                    <dd>
                        <span><{$tag.language}></span>
                    </dd>

                    <dt>
                        <i aria-hidden="true" class="acms-icon acms-icon-file"></i> <def><{translate id='Inhalt'}></def>
                    </dt>
                    <dd>
                        <p><{$tag.body}></p>
                    </dd>
                </dl>
            </div>
            <div class="footer">
                <button type="button" class="button primary" data-dismiss="modal"><{translate id="Schließen" textdomain="Core"}></button>
            </div>
        </div>
    </div>
</div>
