<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="page-section v1 primary">
        <div class="container fixed-1200">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h1><i aria-hidden="true" class="acms-icon acms-icon-tags"></i> <span itemprop="name"><{$acmsContents->acmsTitle}></span></h1>
                        <p class="lead">Durchstöbern Sie unsere Tags nach passenden Inhalten</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <{if $acmsContents->indexPage}>
        <section class="white">
            <div class="container fixed-1200">

                <div class="row full xxsmall-push-updown-30">
                    <div class="column xxsmall-12">
                        <{if $acmsContents->indexPage['thumbUrl']}>
                            <div class="left floated">
                                <div class="image-container">
                                    <img src="<{$acmsContents->indexPage['thumbUrl']}>" title="<{$acmsContents->indexPage['header']}>" alt="" />
                                </div>
                            </div>
                        <{/if}>
                        <div class="display-inline text-center">
                            <h2><{$acmsContents->indexPage['header']}></h2>
                        </div>
                    </div>
                    <{if $acmsContents->indexPage['content']}>
                        <div class="column xxsmall-12">
                            <p class="lead" itemprop="description"><{$acmsContents->indexPage['content']}></p>
                        </div>
                    <{/if}>
                </div>
            </div>
        </section>
    <{/if}>
    <section>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <{if $acmsContents->tags}>
                        <div class="collection v1 row full">
                            <{foreach $acmsContents->tags as $id => $tag}>
                                <div class="collection-item hoverable z-depth-1 column xxsmall-12 medium-3 large-3 xlarge-2 xxlarge-2 push-5">
                                    <a class="text"
                                       data-trigger="hover focus"
                                       data-module="apps/ui/informations/tooltip"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       href="<{$tag['itemUrl']}>"
                                       title="<{if $tag['body']}><{$tag['body']}><{else}><{translate id='Tag' textdomain='Core'}>: <{$tag['title']}><{/if}>">
                                        <{$tag['title']}>
                                    </a>
                                    <span class="secondary-content badge v1">
                                        <{if $tag.frequency}><{$tag.frequency}><{else}>0<{/if}>
                                    </span>
                                </div>
                            <{/foreach}>
                        </div>
                    <{else}>
                        <div class="alert v2 info bordered size-500" data-cssfile="media/css/ui/alert">
                            <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
                            <div class="alert-title">
                                <h4><{translate id="Keine Tags gefunden" textdomain="Core"}></h4>
                            </div>
                            <div class="alert-content">
                                <p>
                                    <{translate
                                        id="Es wurden leider noch keine Tags übermittelt. bitte schauen Sie zu einem späteren Zeitpunkt noch einmal vorbei."
                                        textdomain="Core"
                                    }>
                                </p>
                            </div>
                        </div>
                    <{/if}>
                </div>
            </div>
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <{include file="acmsfile:Core|helpers/SearchHint.tpl"}>
                </div>
            </div>
        </div>
    </section>

    <{if $acmsContents->indexPage && $acmsContents->indexPage['footer']}>
        <section class="white">
            <div class="container fixed-1200">
                <div class="row full">
                    <div class="column xxsmall-12">
                        <p><{$acmsContents->indexPage['footer']}></p>
                    </div>
                </div>
            </div>
        </section>
    <{/if}>
</section>
