<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $rating['mode'] == 1}>
    <div>
        <{translate id="%s hat den Beitrag aufgewertet" textdomain="Core" args=[$rating['user']['username']]}>
    </div>
<{elseif $rating['mode'] == 2}>
    <div>
        <{translate id="%s hat den Beitrag abgewertet" textdomain="Core" args=[$rating['user']['username']]}>
    </div>
<{else}>
    <div class="ratings">
        <fieldset class="static rate">
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating10" name="rate" value="10" <{if $rating['rate'] == '10'}>checked<{/if}> />
            <label for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating10" title="5 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating9" name="rate" value="9" <{if $rating['rate'] == '9'}>checked<{/if}> />
            <label class="half" for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating9" title="4 1/2 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating8" name="rate" value="8" <{if $rating['rate'] == '8'}>checked<{/if}> />
            <label for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating8" title="4 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating7" name="rate" value="7" <{if $rating['rate'] == '7'}>checked<{/if}> />
            <label class="half" for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating7" title="3 1/2 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating6" name="rate" value="6" <{if $rating['rate'] == '6'}>checked<{/if}> />
            <label for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating6" title="3 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating5" name="rate" value="5" <{if $rating['rate'] == '5'}>checked<{/if}> />
            <label class="half" for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating5" title="2 1/2 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating4" name="rate" value="4" <{if $rating['rate'] == '4'}>checked<{/if}> />
            <label for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating4" title="2 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating3" name="rate" value="3" <{if $rating['rate'] == '3'}>checked<{/if}> />
            <label class="half" for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating3" title="1 1/2 stars"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating2" name="rate" value="2" <{if $rating['rate'] == '2'}>checked<{/if}> />
            <label for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating2" title="1 star"></label>
            <input disabled="disabled" type="radio" id="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating1" name="rate" value="1" <{if $rating['rate'] == '1'}>checked<{/if}> />
            <label class="half" for="<{$rating['module']}>-<{$rating['item']}>-<{$rating['itemid']}>-rating1" title="1/2 star"></label>
        </fieldset>
    </div>
<{/if}>
