<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container fixed-1200 fixed-1600">
            <div class="full row">
                <div class="column xxsmall-12">
                    <div class="large page-title v7">
                        <i aria-hidden="true" class="acms-icon acms-icon-star"></i>
                        <div class="content">
                            <h1 class="title"><{$acmsContents->acmsTitle}></h1>
                            <div class="sub-title">
                                <{translate id="Üersicht der letzten Bewertungen"}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="page-section v1">
        <div class="container fixed-1200 fixed-1600">
            <div class="full row">
                <div class="column xxsmall-12">
                    <div class="feed-collection v1" data-module="apps/components/ratings/ratings-view">
                        <{foreach $acmsContents->ratings as $rating}>
                            <div class="card v2" data-role="rating" data-id="<{$rating['id']}>">
                                <div class="card-content">
                                    <div class="right floated small avatar">
                                        <img class="image" alt="Avatar" title="<{$rating['user']['username']}>" src="<{$rating['user']['avatar']}>">
                                    </div>
                                    <div class="card-title">
                                        <a href="<{$rating['user']['itemUrl']}>" title="Profil aufrufen">
                                            <{$rating['user']['username']}>
                                        </a>
                                    </div>
                                    <div class="card-meta text-xsmall">
                                        <{$rating['moduleObject']['name']}> > 
                                        <a class="text-muted" href="<{$rating['ratedItem']['itemUrl']}>" title="Beitrag aufrufen">
                                            <{$rating['ratedItem']['title']}>
                                        </a>
                                    </div>
                                    <div class="description">
                                        <{if $rating['body']}><{$rating['body']}><{else}><span class="text-muted">- Kein Text -</span><{/if}>
                                    </div>
                                    <div class="card-meta text-xsmall">
                                        <{include file="acmsfile:Core|components/ratings/helpers/DisplayRating.tpl" rating=$rating}>
                                    </div>
                                </div>
                                <div class="card-content text-xsmall text-muted">
                                    <span class="right floated">
                                        <i class="acms-icon acms-icon-thumbs-up"></i>
                                        <{$rating['useful']}> <{translate id="fanden diese Bewertung hilfreich" textdomain="Core"}>
                                    </span>
                                    <i class="acms-icon acms-icon-comments"></i>
                                    <{$rating['answers']|@count}> <{translate id="Antworten" textdomain="Core"}>
                                </div>
                                <{if $acmsContents->canAnswer}>
                                    <div class="extra card-content">
                                        <form data-role="reply-form">
                                        <div class="large transparent left icon form-group">
                                            <i class="acms-icon acms-icon-comments"></i>
                                            <textarea placeholder="Antworten..." name="body" type="text" class="field" data-role="reply-input"></textarea>
                                        </div>
                                        <input type="hidden" name="body-ctype" value="3">
                                        <div class="compact button-toolbar">
                                            <button class="small default primary-hover button" data-trigger="reply-button">
                                                <span class="content">Absenden</span>
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                <{/if}>
                                <{if $acmsContents->isModerator || $acmsContents->canDelete}>
                                    <div class="extra card-content">
                                        <button class="plugged bottom small danger icon-button button" data-trigger="delete-rating">
                                            <i class="acms-icon acms-icon-trash"></i>
                                            <span class="content"><{translate id="Löschen" textdomain="Core"}></span>
                                        </button>
                                    </div>
                                <{/if}>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
