<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="ratings"
     data-module="apps/components/ratings/ratings-vote"
     data-dirname="<{$module}>"
     data-item="<{$item}>"
     data-itemid="<{$itemid}>"
     data-remote="<{generateUrl name="core_ratings_action" parameters=['action' => 'action']}>">
    <button class="vote-up default icon-button button" data-trigger="voteup" aria-label="<{translate id="Aufwerten" textdomain="Core"}>">
        <i aria-hidden="true" class="acms-icon acms-icon-thumbs-up acms-icon-3x"></i>
    </button>
    <button class="vote-down default icon-button button" data-trigger="votedown" aria-label="<{translate id="Abwerten" textdomain="Core"}>">
        <i aria-hidden="true" class="acms-icon acms-icon-thumbs-down acms-icon-3x"></i>
    </button>
</div>
