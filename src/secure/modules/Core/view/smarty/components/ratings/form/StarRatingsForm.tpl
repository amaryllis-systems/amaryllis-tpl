<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="ratings" 
     data-module="apps/components/ratings/ratings-star" 
     data-dirname="<{$module}>"
     data-item="<{$item}>"
     data-itemid="<{$itemid}>"
     data-remote="<{generateUrl name="core_ratings_action" parameters=['action' => 'action']}>">
    <fieldset class="rate text-theme">
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating10" name="rate" value="10" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating10" title="5 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating9" name="rate" value="9" /><label data-role="star" class="half" for="<{$module}>-<{$item}>-<{$itemid}>-rating9" title="4 1/2 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating8" name="rate" value="8" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating8" title="4 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating7" name="rate" value="7" /><label data-role="star" class="half" for="<{$module}>-<{$item}>-<{$itemid}>-rating7" title="3 1/2 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating6" name="rate" value="6" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating6" title="3 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating5" name="rate" value="5" /><label data-role="star" class="half" for="<{$module}>-<{$item}>-<{$itemid}>-rating5" title="2 1/2 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating4" name="rate" value="4" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating4" title="2 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating3" name="rate" value="3" /><label data-role="star" class="half" for="<{$module}>-<{$item}>-<{$itemid}>-rating3" title="1 1/2 stars"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating2" name="rate" value="2" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating2" title="1 star"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating1" name="rate" value="1" /><label data-role="star" class="half" for="<{$module}>-<{$item}>-<{$itemid}>-rating1" title="1/2 star"></label>
        <input type="radio" id="<{$module}>-<{$item}>-<{$itemid}>-rating0" name="rate" value="0" /><label data-role="star" for="<{$module}>-<{$item}>-<{$itemid}>-rating0" title="No star"></label>
    </fieldset>
    <div class="form-group">
        <label for="<{$module}>-<{$item}>-<{$itemid}>-rating-body" class="field-label">Bewertung</label>
        <textarea class="field" style="min-height: auto;" id="<{$module}>-<{$item}>-<{$itemid}>-rating-body" name="body"></textarea>
    </div>
    <input type="hidden" name="body-ctype" value="3">
    <div class="button-toolbar">
        <button class="small default primary-hover button" disabled="disabled" data-trigger="send-rating">
            <span class="content">Absenden</span>
        </button>
    </div>
</div>
