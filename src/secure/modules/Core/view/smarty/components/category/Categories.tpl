<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="page-section arrow-box arrow-bottom v1 primary">
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h1>
                            <i aria-hidden="true" class="acms-icon acms-icon-<{if $acmsContents->get('categories')}>folder<{else}>folder-open<{/if}> text-muted"></i>&nbsp;
                            <span itemprop="name"><{$acmsContents->acmsTitle}></span>
                        </h1>
                        <p class="lead">Nachrichten-Stream von <{$acmsData->acmsSitename}></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <{if $acmsContents->indexPage}>
        <section class="primary page-section v1">
            <div class="container fixed-1200">
                <div class="row full xxsmall-push-updown-25">
                    <div class="column xxsmall-12">
                        <{if $acmsContents->indexPage['thumbUrl']}>
                            <div class="left floated">
                                <div class="image-container">
                                    <img src="<{$acmsContents->indexPage['thumbUrl']}>" title="<{$acmsContents->indexPage['header']}>" alt="" />
                                </div>
                            </div>
                        <{/if}>
                        <div class="display-inline text-center">
                            <h2><{$acmsContents->indexPage['header']}></h2>
                        </div>
                    </div>
                    <{if $acmsContents->indexPage['content']}>
                        <div class="column xxsmall-12">
                            <p class="lead"><{$acmsContents->indexPage['content']}></p>
                        </div>
                    <{/if}>
                </div>
            </div>
        </section>
    <{/if}>
    <div class="container fixed-1200 fixed-1600 pull-updown-30">
        <div class="row full push-updown-30">
            <div class="column xxsmall-12 large-9 xlarge-9 xxlarge-9">
                <div class="push-20">

                    <div class="row full xxsmall-push-updown-15">
                        <div class="column xxsmall-12" data-module="ui/components/action-buttons">
                            <{if $acmsContents->get('categories')}>
                                <{foreach $acmsContents->get('categories') as $category}>
                                    <{if $category@iteration % 2 == 1 || $category@first}>
                                        <div class="call-to-action v2 center-block">
                                            <div class="action-items">
                                    <{/if}>
                                    <{include file="acmsfile:Core|components/category/Category.tpl" category=$category}>
                                    <{if $category@iteration % 2 != 1 && !$category@first || $category@last}>
                                            </div>
                                        </div>
                                    <{/if}>
                                <{/foreach}>
                            <{/if}>
                            <{if $acmsContents->acmsPagination["pagination"]}>
                                <{$acmsContents->acmsPagination["pagination"]}>
                            <{/if}>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column xxsmall-12 medium-12 large-3 xlarge-3 xxlarge-3">
                <{include file="acmsfile:Core|components/category/CategorySidebar.tpl"}>
            </div>
        </div>
        <{if $acmsContents->indexPage && $acmsContents->indexPage['footer']}>
            <div class="row full">
                <div class="column xxsmall-12">
                    <p><{$acmsContents->indexPage['footer']}></p>
                </div>
            </div>
        <{/if}>
    </div>
    <{if ! $acmsContents->has('preventCategoryActions') && $acmsContents['userCanSubmit']}>
        <{include file="acmsfile:Core|components/category/CategoryActions.tpl"}>
    <{/if}>
</section>
