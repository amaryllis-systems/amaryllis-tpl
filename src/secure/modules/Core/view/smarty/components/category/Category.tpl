<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<article class="action-button">
    <div class="figures">
        <figure class="figure">
            <{if $category['image']}>
                <img src="<{$category['image']}>?width=160" title="" alt="" />
            <{else}>
                <i aria-hidden="true" class="acms-icon acms-icon-folder"></i>
            <{/if}>
        </figure>
    </div>
    <div class="content">
        <div class="headline"><{$category['title']}></div>
        <div class="text"><p><{$category['teaser']}></p></div>
    </div>
    <ul class="links">
        <li>
            <a title="<{$category['title']}>" href="<{$category['itemUrl']}>"></a>
        </li>
    </ul>
</article>
