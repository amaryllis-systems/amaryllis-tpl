<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $acmsContents->get('singleCategory')}>
    <div class="fixed-action-button" data-module="ui/navs/floating-button" data-nav="floating-button">
        <a role="button" class="bg-red large floating button" href="#" title="<{translate id="Aktionen" textdomain="Core"}>" data-toggle="floating-menu">
            <i aria-hidden="true" class="acms-icon acms-icon-bolt white-text"></i>
        </a>
        <ul>
            <li>
                <a class="large primary floating button"
                   title="<{translate id="Kategorie bearbeiten" textdomain="Core"}>"
                   href="<{$acmsContents['singleCategory']['editUrl']}>"
                   >
                    <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                </a>
            </li>
            <li>
                <a class="large light floating button"
                   title="<{translate id="Kategorie klonen" textdomain="Core"}>"
                   href="<{$acmsContents['singleCategory']['cloneUrl']}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-clone"></i>
                </a>
            </li>
            <li>
                <a class="large secondary floating button"
                   title="<{translate id="Neue Kategorie" textdomain="Core"}>"
                   href="<{$acmsContents->get('categorySubmitUrl')}><{if $acmsContents->get('singleCategory')}>?parentid=<{$acmsContents['singleCategory']['id']}><{/if}>">

                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                </a>
            </li>
            <li>
                <a class="large danger floating button"
                   title="<{translate id="Kategorie löschen" textdomain="Core"}>"
                   href="<{$acmsContents['singleCategory']['deleteUrl']}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                </a>
            </li>
        </ul>
    </div>
<{else}>
    <div class="fixed-action-button">
        <a class="large primary floating button"
            title="<{translate id="Neue Kategorie" textdomain="Core"}>"
            href="<{$acmsContents->get('categorySubmitUrl')}><{if $acmsContents->get('singleCategory')}>?parentid=<{$acmsContents['singleCategory']['id']}><{/if}>">

             <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
        </a>
    </div>
<{/if}>
