<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper category-form-wrapper">
    <div class="row full">
        <form role="form" <{$form['attributes']}>>
            <{if $form['fieldsetList']}>
            <div class="tab-container" data-module="ui/navs/tabs" data-effect="" data-outeffect="">
                <ul class="tabs tabs-horizontal tabs-stacked tabs-right large" role="tablist">
                    <{foreach $form['fieldsetList'] as $fieldset}>
                        <li class="tab fieldset-tab<{if $fieldset['active']}> active<{/if}>">
                            <a role="tab" data-target="#<{$fieldset['target']}>">
                                <{$fieldset['title']}>
                                <{if $fieldset['required']}>
                                    <span class="acms-field-required text-danger right">*</span>
                                <{/if}>
                            </a>
                        </li>
                    <{/foreach}>
                </ul>
                <div class="tab-content">
                    <{foreach $form.fields as $field}>
                        <{include file=$field['template'] field=$field}>
                    <{/foreach}>
                </div>
            </div>
            <{else}>
                <{foreach $form.fields as $field}>
                    <{include file=$field['template'] field=$field}>
                <{/foreach}>
            <{/if}>
            <{include file=$form['buttons']['template'] field=$form['buttons']}>
        </form>
    </div>
</div>
