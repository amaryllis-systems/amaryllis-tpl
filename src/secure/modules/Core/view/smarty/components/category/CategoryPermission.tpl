<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="container fixed-1024 fixed-1200" itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="row full xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <{include file=$acmsContents['acmsBreadCrumb']['template'] nocache}>
            </div>
        </div>
    <{/if}>
    <div class ="row full">
        <div class="column xxsmall-12 medium-8 large-9 xlarge-9 xxlarge-9">
            <!-- general admin page title -->
            <{if $acmsContents->get('acmsTitle')}>
                <div class="column xxsmall-12">
                    <div class="page-title v7">
                        <h1 itemprop="name"><{$acmsContents->get('acmsTitle')}></h1>
                    </div>
                </div>
            <{/if}>
            <div class="column xxsmall-12">
                <!-- Admin Form -->
                <{if $acmsContents->get('acmsForm')}>
                    <{include file=$acmsContents['acmsForm']['template'] form=$acmsContents['acmsForm'] nocache}>
                <{/if}>
            </div>
        </div>
        <div class="column xxsmall-12 medium-4 large-3 xlarge-3 xxlarge-3">
            <{include file="acmsfile:Core|components/category/CategorySidebar.tpl" nocache}>
        </div>
    </div>
</div>
