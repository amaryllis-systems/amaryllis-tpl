<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">

    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row xxsmall-pushupdown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <main>
    <section class="page-section arrow-box arrow-bottom v1 primary">
        <div class="container fixed-1024 fixed-1200">
            <div class ="row full xxsmall-pushupdown-30">
                <!-- general admin page title -->
                <div class="column xxsmall-12">
                    <div class="large page-title v7 push-10">
                        <i aria-hidden="true" class="acms-icon acms-icon-trash text-xlarge"></i>
                        <div class="content">
                            <h2 class="title"><{$acmsContents->get('acmsTitle')}></h2>
                            <div class="sub-title"><{translate id="Hier können Sie Kategorie &raquo;%s&laquo; entfernen." args=[$acmsContents->model['title']]}></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container fixed-1024 fixed-1200 pull-updown-30">
        <div class="row full xxsmall-pushupdown-30">
            <div class="column xxsmall-12">
                <!-- Form -->
                <div class="form-wrapper push-10 content-block v5">
                    <form class="core-delete-form" 
                          name="core-delete-form" 
                          action="<{$acmsContents->action}>" 
                          data-module="view/form/model-form">
                        <div class="form-group">
                            <label for="agree">
                                <{translate id="Sind Sie sicher, dass Sie die Kategorie entfernen möchten?"}>
                            </label>
                            <div class="switch v2 success yesno-variant">
                                <input id="agree" value="1" name="agree" type="checkbox">
                                <label for="agree" data-on="Ja" data-off="Nein"></label>
                            </div>
                        </div>
                        <{if $acmsContents->total}>
                            <p class="field-static text-danger">
                                Wenn Sie diese Kategorie löschen, werden auch die folgenden <{$acmsContents->total}> Unter-Kategorien mitgelöscht:
                            </p>
                            <ul class="bullet-list round-bullet">
                                <{foreach $acmsContents->categories as $category}>
                                    <{include file="acmsfile:Core|components/category/helpers/CategorySimpleMenuItem.tpl" category=$category}>
                                <{/foreach}>
                            </ul>
                            <div class="form-group">
                                <label for="agree_subcategorys">
                                    <{translate id="Sind Sie sicher, dass Sie die Unter-Kategorieen alle entfernen möchten?"}>
                                </label>
                                <div class="switch v2 success yesno-variant">
                                    <input id="agree_subcategories" value="1" name="agree_subcategories" type="checkbox">
                                    <label for="agree_subcategories" data-on="Ja" data-off="Nein"></label>
                                </div>
                            </div>
                        <{/if}>
                        <{if $acmsContents->linked}>
                            <p class="field-static text-danger">
                                Wenn Sie diese Kategorie löschen, werden auch die <{$acmsContents->linked}> Verlinkungen zu dieser Kategorie entfernt. 
                                Es wird <b>dringend</b> empfohlen, diese Verlinkungen zuerst in eine neue Kategorie zu schieben!
                            </p>
                            
                            <div class="form-group">
                                <label for="agree_subcategorys">
                                    <{translate id="Sind Sie sicher, dass Sie die Verlinkungen alle entfernen möchten?"}>
                                </label>
                                <div class="switch v2 success yesno-variant">
                                    <input id="agree_linked" value="1" name="agree_linked" type="checkbox">
                                    <label for="agree_linked" data-on="Ja" data-off="Nein"></label>
                                </div>
                            </div>
                        <{/if}>
                        <input type="hidden" name="op" value="dodelete">
                        <input type="hidden" name="token" value="<{$acmsContents->token}>">
                        <input type="hidden" name="category_id" value="<{$acmsContents->model['id']}>">
                        <div class="button-toolbar">
                            <button data-trigger="save-close" type="submit" class="button danger border-button">
                                <span>Löschen</span>
                            </button>
                            <button data-trigger="abort" type="reset" class="button primary">
                                <span>Abbrechen</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    </main>
</section>
