<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">

    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row xxsmall-pushupdown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <main>
    <section class="page-section arrow-box arrow-bottom v1 primary">
        <div class="container fixed-1024 fixed-1200">
            <div class ="row full xxsmall-pushupdown-30">
                <!-- general admin page title -->
                <div class="column xxsmall-12">
                    <div class="large page-title v7 push-10">
                        <i aria-hidden="true" class="acms-icon acms-icon-clone text-xlarge"></i>
                        <div class="content">
                            <h2 class="title"><{$acmsContents->get('acmsTitle')}></h2>
                            <div class="sub-title"><{translate id="Hier können Sie Kategorie &raquo;%s&laquo; kopieren." args=[$acmsContents->category['title']]}></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container fixed-1024 fixed-1200 pull-updown-30">
        <div class="row full xxsmall-pushupdown-30">
            <div class="column xxsmall-12">
                <!-- Admin Form -->
                <{if $acmsContents->get('acmsForm')}>
                    <{include file=$acmsContents['acmsForm']['template'] form=$acmsContents['acmsForm']}>
                <{/if}>
            </div>
        </div>
    </section>
    </main>
</section>
