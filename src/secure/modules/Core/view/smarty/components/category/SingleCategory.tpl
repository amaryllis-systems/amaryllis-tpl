<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="arrow-bottom arrow-box page-section v1 primary">
        <div class="container fixed-1200 fixed-1600">
            <div class="row full">
                <div class="column xxsmall-12">
                    <div class="large center aligned icon page-title v7">
                        <i aria-hidden="true" class="circular acms-icon acms-icon-folder-open text-xxlarge"></i>&nbsp;
                        <div class="content">
                            <h1 class="title">
                                <span itemprop="name"><{$acmsContents->get('acmsTitle')}></span>
                            </h1>
                            <div class="sub-title"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container fixed-1200 fixed-1600 pull-updown-30">
        <div class="row full push-updown-30">
            <div class="column xxsmall-12 medium-12 large-8 xlarge-9 xxlarge-9">
                <div class="push-leftright-35">

                    <{if $acmsContents->get('singleCategory')}>
                        <{if $acmsContents['singleCategory']['body']
                            || $acmsContents['singleCategory']['image']}>
                        <div class="content-block v6 wide clearfix">
                            <{if $acmsContents['singleCategory']['image']}>
                                <div class="image-container left">
                                    <img class="image" 
                                         itemprop="image"
                                         src="<{$acmsContents['singleCategory']['image']}>"
                                         title="<{$acmsContents['singleCategory']['title']}>"
                                         alt=""/>
                                </div>
                            <{else}>
                                <div class="display-inline left floated pull-right-15">
                                    <i aria-hidden="true" class="acms-icon acms-icon-folder-open acms-icon-5x"></i>
                                </div>
                            <{/if}>
                            <div itemprop="description">
                                <{if $acmsContents['singleCategory']['body']}>
                                    <{$acmsContents['singleCategory']['body']}>
                                <{/if}>
                            </div>
                        </div>
                    <{/if}>
                    <{if $acmsContents->subCategories}>
                        <div class="subcategories" data-module="ui/components/action-buttons">
                            <div class="page-title v3">
                                <h2><{translate id="Unterkategorien" textdomain="Core"}></h2>
                            </div>
                            <{foreach $acmsContents->get('subCategories') as $id => $category}>
                                <{if $category@iteration % 2 == 1 || $category@first}>
                                    <div class="call-to-action v2 center-block">
                                        <div class="action-items">
                                <{/if}>
                                <{include file="acmsfile:Core|components/category/Category.tpl" category=$category}>
                                <{if $category@iteration % 2 != 1 && !$category@first || $category@last}>
                                        </div>
                                    </div>
                                <{/if}>
                            <{/foreach}>
                        </div>
                    <{/if}>
                    <{if $acmsContents->items}>
                        <div class="category-items">
                            <div class="page-title v3">
                                <h2><{translate id="Inhalte" textdomain="Core"}></h2>
                            </div>
                            <div class="cards" data-cssfile="media/css/ui/cards">
                                <{foreach $acmsContents->get('items') as $item}>
                                    <div class="card v2 hoverable">
                                        <div class="card-content">
                                            <div class="card-title">
                                                <a href="<{$item->item['itemUrl']}>" title="<{$item->item['title']}>">
                                                    <{$item->item['title']}>
                                                </a>
                                                <span class="right floated">
                                                    <i aria-hidden="true" class="acms-icon acms-icon-rss"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <{if $item->item['thumbUrl']}>
                                            <div class="card-image">
                                                <img src="<{$item->item['thumbUrl']}>" title="<{$item->item['title']}>" alt="" />
                                            </div>
                                        <{/if}>
                                        <div class="card-content">
                                            <{$item->item['teaser']}>
                                        </div>
                                    </div>
                                <{/foreach}>
                            </div>
                        </div>
                    <{/if}>
                    <{if $acmsContents->acmsPagination["pagination"]}>
                        <{$acmsContents->acmsPagination["pagination"]}>
                    <{/if}>
                    <{/if}>
                    </div>
                </div>
                <div class="column xxsmall-12 medium-12 large-4 xlarge-3 xxlarge-3">
                    <{include file="acmsfile:Core|components/category/CategorySidebar.tpl"}>
                </div>
            </div>
        </div>
        <{if $acmsContents['singleCategory']['userCanDelete']
        || $acmsContents['singleCategory']['userCanEdit']
        || $acmsContents['singleCategory']['userCanSubmit']
        || $acmsContents->get('userCanSubmit')}>
        <{include file="acmsfile:Core|components/category/CategoryActions.tpl" nocache}>
        <{/if}>
</section>
