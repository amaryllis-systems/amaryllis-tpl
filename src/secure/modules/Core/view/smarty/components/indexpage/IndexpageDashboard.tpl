<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section>
    <section class="primary arrow-bottom arrow-box page-section v1">
        <div class="container fixed-1200">
            <div class="full row">
                <div class="column xxsmall-12">
                    <div class="large page-title v7">
                        <div class="center aligned content">
                            <h1 class="white-text title" itemprop="name"><{$acmsContents->acmsTitle}></h1>
                            <p class="white-text sub-title"><{$acmsContents->description}></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="page-section v1">
        <div class="container fixed-1200">
            <div class="full row">
                <div class="column xxsmall-12">
                    <div class="push-10">
                        <table class="middle aligned responsive striped compact table v1">
                            <thead>
                                <tr>
                                    <th class="size-100">ID</th>
                                    <th>Header</th>
                                    <th>Modul</th>
                                    <th>Item</th>
                                    <th>Sprache</th>
                                    <th class="size-150"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <{foreach $acmsContents->indexpages as $indexpage}>
                                    <tr>
                                        <td data-th="ID">
                                            <{$indexpage['id']}>
                                        </td>
                                        <td data-th="Titel">
                                            <{$indexpage['header']}>
                                        </td>
                                        <td data-th="Modul">
                                            <{$indexpage['module']}>
                                        </td>
                                        <td data-th="Item">
                                            <{$indexpage['item']}>
                                        </td>
                                        <td data-th="Sprache">
                                            <{$indexpage['language']}>
                                        </td>
                                        <td>
                                            <a href="<{$indexpage['editUrl']}>" class="small secondary icon button" data-trigger="edit">
                                                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                                            </a>
                                            <button class="small default danger-hover icon button" data-trigger="delete">
                                                <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <{/foreach}>
                            </tbody>
                        </table>
                    </div>
                    <div class="button-toolbar">
                        <a class="small primary button" title="Neu" href="<{$acmsContents->submitUrl}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                            <span class="content">Neu erstellen</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
