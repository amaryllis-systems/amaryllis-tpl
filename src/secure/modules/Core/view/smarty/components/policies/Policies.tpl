<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12">
                <div class="row full">
                    <div class="collection v1 has-image">
                    <{foreach $acmsContents->terms as $term}>
                        <div class="collection-item">
                            <i aria-hidden="true" class="acms-icon acms-icon-gavel item-image"></i>
                            <div class="title">
                                <a class=""
                                   title="<{$term.title}>"
                                   href="<{$term.itemUrl}>"
                                   role="presentation"
                                   itemprop="url">
                                    <span class="name"><{$term.title}></span>
                                </a>
                            </div>
                        </div>
                    <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
