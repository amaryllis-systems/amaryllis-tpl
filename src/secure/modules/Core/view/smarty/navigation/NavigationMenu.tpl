<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $menu}>
    <ul role="menu"
        itemscope="itemscope"
        itemtype="http://schema.org/SiteNavigationElement"
        class="vertical nav v1"
    >
        <{if $menu.items}>
            <{function name=navigationsList}>
                <{foreach $items as $item}>
                    <li class="acms-menu-item item <{if $item.isActive}>active<{/if}>">
                        <a itemprop="url"
                           href="<{$item.url}>"
                           title="<{if $item.description}><{$item.description}><{else}><{$item.title}><{/if}>"
                        >
                            <span itemprop="name" class="item-title">
                                <{$item.title}>
                            </span>
                            <{if $item.description}>
                                <span itemprop="description" aria-hidden="true" class="item-description hidden">
                                    <{$item.description}>
                                </span>
                            <{/if}>
                        </a>
                        <{if $item['subs']}>
                            <ul>
                            <{call name=navigationsList items=$item['subs']}>
                            </ul>
                        <{/if}>
                    </li>
                <{/foreach}>
            <{/function}>
            <{call name=navigationsList items=$menu.items}>
        <{/if}>
    </ul>
<{/if}>
