<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="user-profile" itemscope="" itemtype="http://schema.org/ProfilePage">

<{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>

<div class="container fixed-1200" data-profileview="board">
    <div class="row full">
        <div class="column xxsmall-12 medium-3 large-3 xlarge-3 xxlarge-3">
            <aside role="complementary" class="profile-sidebar">
                <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
            </aside>
        </div>
        <div class="column xxsmall-12 medium-8 large-8 xlarge-9 xxlarge-9">
            <{if $acmsContents->confirmDeletion}>
                
            <{else}>
                
            <{/if}>
        </div>
    </div>
</div>
</section>
