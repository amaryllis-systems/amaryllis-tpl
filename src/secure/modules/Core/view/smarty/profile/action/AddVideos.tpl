<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>

<div id="profile" class="container full profile-content profile-clubs profile-clubs-action" data-role="profile"
	 data-profileview="clubs" data-owner="<{if $acmsContents->get('isOwner')}>true<{else}>false<{/if}>"
>
	<div class="row">
            <div class="column xxsmall-12 large-8 xlarge-8 large-offset-2 xlarge-offset-2">
                <{include file=$acmsContents['form']['template'] form=$acmsContents['form']}>
            </div>
	</div>
</div>
