<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="row full">
    <div class="column xxsmall-12 medium-8">
        <button type="button" id="pmcheckall" class="button info tiny" title="<{translate id='Schnellwahl' textdomain='Core'}>">
            <i aria-hidden="true" class="acms-icon acms-icon-check-square regular"></i>
        </button>
    <div class="button-group">
        <button class="button default small split">
            <span><{translate id="Aktionen" textdomain="Core"}></span> 
            <span class="splitted"></span>
        </button>
            <ul class="dropdown-box" style="top: 85%;">
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="all"><{translate id="Alle" textdomain="Core"}></a></li>
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="none"><{translate id="Keine" textdomain="Core"}></a></li>
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="read"><{translate id="Gelesene" textdomain="Core"}></a></li>
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="unread"><{translate id="Ungelesene" textdomain="Core"}></a></li>
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="important"><{translate id="Wichtige" textdomain="Core"}></a></li>
            <li><a class="check-menu-item dropdown-box-link" href="#" data-action="unimportant"><{translate id="Unwichtige" textdomain="Core"}></a></li>
        </ul>
    </div>
        <button id="inbox-refresh" class="button default tiny" data-title="<{translate id='Neu laden' textdomain='Core'}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip">
            <span class="acms-icon acms-icon-sync-alt"></span>
        </button>
            <div class="button-group">
        <button class="button default small split">
            <span><{translate id="Mehr" textdomain="Core"}></span> 
            <span class="splitted"></span>
        </button>
        <ul class="dropdown-box" style="top: 85%;">
            <li role="presentation">
                <a class="check-menu-item dropdown-box-link" role="menuitem" tabindex="-1" href="#"><{translate id="Markiere alle als gelesen" textdomain="Core"}></a>
            </li>
            <li role="presentation">
                <a class="check-menu-item dropdown-box-link" role="menuitem" tabindex="-1" href="#"><{translate id="Lösche alle" textdomain="Core"}></a>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation" class="disabled">
                <a class="check-menu-item dropdown-box-link" role="menuitem" id="placeholder" tabindex="-1" href="#">
                    <{translate id="Wählen Sie Nachrichten aus um mehr zu sehen" textdomain="Core"}>
                </a>
            </li>
            <li role="presentation" class="hidden">
                <a class="check-menu-item dropdown-box-link" role="menuitem" tabindex="-1" href="#">
                    <{translate id="Lösche alle markierten" textdomain="Core"}>
                </a>
            </li>
            <li role="presentation" class="hidden">
                <a class="check-menu-item dropdown-box-link" role="menuitem" tabindex="-1" href="#">
                    <{translate id="Markiere alle Gewählten als wichtig" textdomain="Core"}>
                </a>
            </li>
            <li role="presentation" class="hidden">
                <a class="check-menu-item dropdown-box-link" role="menuitem" tabindex="-1" href="#">
                    <{translate id="Markiere alle Gewählten als unwichtig" textdomain="Core"}>
                </a>
            </li>
        </ul>
            </div>
    </div>
    
    <div class="column xxsmall-12 medium-4 large-4 text-right">
        <button id="clear-trash" class="button danger small">
            <i aria-hidden="true" class="acms-icon acms-icon-trash-alt regular"></i>
            <span class="text-small"><{translate id="Leere Papierkorb" textdomain="Core"}></span>
        </button>
    </div>
</div>
