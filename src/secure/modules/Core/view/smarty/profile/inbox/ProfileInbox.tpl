<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" itemtype="http://schema.org/ProfilePage">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-change-email"
         data-role="profile"
         data-profileview="inbox" 
         data-owner="<{if $acmsContents->isOwner}>true<{else}>false<{/if}>">
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>

        <section class="push-updown-30 pull-updown-35 large-pull-35 large-push-30">
            <div class="profile-inbox content-block v5" 
                 data-cssfile="media/css/profile/profile-inbox"
                 data-module="apps/profile/inbox/inbox"
                 data-remote="<{generateUrl name="core_inbox_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>" 
                 data-view="<{$acmsContents->subview}>">
                 <{* Sidebar *}>
                <{include file="acmsfile:Core|profile/inbox/InboxSidebar.tpl"}>
                <{* inbox contents *}>
                <div class="profile-inbox-content" role="section">
                    <div class="inner">
                        <div class="inbox-content" data-role="pm-list-wrapper">
                            <{if $acmsContents->pm}>
                                <{include file=$acmsContents->inboxTpl pm=$acmsContents->pm replies=$acmsContents->inbox nocache}>
                            <{else}>
                                <{include file=$acmsContents->inboxTpl inbox=$acmsContents->inbox nocache}>
                            <{/if}>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<{include file="acmsfile:Core|profile/form/PrivatemessageForm.tpl"}>

<{if !$acmsData->deviceIsDesktop}>
    <div class="fixed-action-button">
        <button type="button" class="large primary floating button"
           aria-label="<{translate id="Nachricht verfassen" textdomain="Core"}>"
           data-toggle="modal"
           data-target="#compose-message"
           data-module="apps/ui/informations/modal"
           >
            <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt "></i>
        </button>
    </div>
<{/if}>
 