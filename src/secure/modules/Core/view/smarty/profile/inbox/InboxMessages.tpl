<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="pm-inbox" class="pm-inbox collection has-image">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item<{if !$pm.read}> read<{/if}>"
            data-id="<{$pm['id']}>" data-read="<{$pm['read']}>"
            data-removeurl="<{$pm['deleteUrl']}>"
            >
            <img src="<{$pm['from']['avatar']}>" class="circle item-image" alt="" title="" />
            <div class="title">
                <span class="from-name display-inline-block">
                    <a href="<{$pm['from']['itemUrl']}>"
                       class="pm-link text-default"
                       title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                        <{$pm['from']['username']}>
                    </a>
                </span>
                <a class="private-message-link private-message-subject<{if !$pm['read']}> text-bold unread<{/if}>" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
                <{if $pm['type'] == 1 && $acmsContents->subview == 'inbox-inbox' && $pm['status'] == 1}>
                    <span class="label v1 warning">ungelesen</span>
                <{/if}>
            </div>
            <p class="clearfix">
                <small class="text-muted">- <{$pm['teaser']}></small>
                <span class="badge v1 right"><{$pm['pdate-fix']}></span>
            </p>
            <div class="secondary-content">
            <{if $pm['type'] == 1 && $acmsContents->subview == 'inbox-inbox'}>
            <a href="#" 
               data-module="view/profile/inbox/pm-important"
               data-remote="<{generateUrl name="core_inbox_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>"
               data-trigger="starring" 
               data-id="<{$pm.id}>" 
               class="<{if $pm.important}>important<{/if}>"
               title="<{translate id='Klicken Sie auf den Stern um eine Nachricht als wichtig/unwichtig zu markieren.' textdomain='Core'}>">
                <i class="pm-favorite acms-icon acms-icon-star<{if !$pm.important}>-o<{else}> purple-text<{/if}>"></i>
            </a>
            <{/if}>
            <a href="#" 
               data-module="view/profile/inbox/pm-delete"
               data-remote="<{generateUrl name="core_inbox_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>"
               data-trigger="starring" 
               data-id="<{$pm.id}>" 
               class="<{if $pm.important}>important<{/if}>"
               title="<{translate id='Klicken Sie um die Nachricht zu löschen.' textdomain='Core'}>">
                <i class="pm-favorite acms-icon acms-icon-trash text-danger"></i>
            </a>
            </div>
        </div>
    <{/foreach}>
</div>
