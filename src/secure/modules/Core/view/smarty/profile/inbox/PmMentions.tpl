<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="pm-inbox collection v1 <{if $pm['from']}>has-avatar<{else}>has-image<{/if}>">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item<{if !$pm.read}> read<{/if}>"
            data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-read="<{$pm['read']}>" data-role="private-message">
            <{if $pm['from']}>
                <div class="avatar item-image">
                    <img src="<{$pm['from']['avatar']}>" class="item-image" alt="" title="" />
                </div>
            <{else}>
                <i aria-hidden="true" class="acms-icon acms-icon-bullhorn item-image"></i>
            <{/if}>
            <div class="title">
                <{if $pm['from']}>
                    <span class="from-name display-inline-block">
                        <a href="<{$pm['from']['itemUrl']}>"
                        class="pm-link text-default"
                        title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                            <{$pm['from']['username']}>
                        </a>
                    </span>
                <{/if}>
                <a class="private-message-link private-message-subject<{if !$pm['read']}> text-bold unread<{/if}>" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <p class="clearfix text-xsmall">
                <small class="text-muted">- <{$pm['teaser']}></small><br>
                <span class class="text-muted"><i aria-hidden="true" class="acms-icon acms-icon-calendar"></i> <{$pm['pdate-relative']}></span>
            </p>
            <div class="secondary-content text-xlarge">
                <span class="delete-trigger cursor-pointer" data-trigger="trash" 
                      aria-label="<{translate id='Klicken Sie um die Nachricht zu löschen.' textdomain='Core'}>">
                    <i class="acms-icon acms-icon-trash text-danger"></i>
                </span>
            </div>
        </div>
    <{foreachelse}>
        <div class="large center aligned content-block v5 pull-updown-35">
            <div class="large center aligned sucess icon page-title v7 pull-updown-20">
                <i class="text-success circular acms-icon acms-icon-check-circle"></i>
                <div class="content">
                    <h3 class="title text-success"><{translate id="Erwähnungen" textdomain="Core"}></h3>
                    <h4 class="sub-title"><{translate id="Sie haben aktuell keine Erwähnungen in Ihrem Posteingang." textdomain="Core"}></h4>
                </div>
            </div>
        </div>
    <{/foreach}>
</div>
