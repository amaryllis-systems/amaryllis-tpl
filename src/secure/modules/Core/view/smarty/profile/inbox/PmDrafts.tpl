<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="pm-inbox collection v1 has-image">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item" data-id="<{$pm['id']}>" data-role="draft" data-draft="<{$pm['json']}>">
            <i aria-hidden="true" class="item-image acms-icon acms-icon-pencil-ruler"></i>
            <div class="title">
                <a class="private-message-link private-message-subject" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <p class="clearfix push-0">
                <span class="text-muted">Erstellt am <{$pm['pdate-short']}></span> 
                <i aria-hidden="true" class="acms-icon acms-icon-users"></i> 
                <{translate id="Empfänger" textdomain="Core"}>: 
                <{foreach $pm['to_user'] as $to}>
                    <span class="tag v1 round default">
                        <img src="<{$to['avatar']}>" class="" alt="" title="<{$to['username']}>" /> <{$to['username']}>
                    </span>
                <{foreachelse}>
                    <i>Keine Empfänger angegeben</i>
                <{/foreach}>
            </p>
            <div class="secondary-content">
                <span class="cursor-pointer push-right-10"
                        aria-label="<{translate id='Klicken Sie um die Nachricht zu bearbeiten.' textdomain='Core'}>"
                        data-apply="edit">
                        <i class="acms-icon acms-icon-pencil-alt text-primary"></i>
                </span>
                <span class="cursor-pointer push-right-10"
                        aria-label="<{translate id='Klicken Sie um die Nachricht zu löschen.' textdomain='Core'}>"
                        data-trigger="deleteDraft" data-id="<{$pm['id']}>">
                        <i class="acms-icon acms-icon-trash text-danger"></i>
                </span>
            </div>
        </div>
    <{foreachelse}>
        <div class="large center aligned content-block v5 pull-updown-35">
            <div class="large center aligned icon page-title v7 pull-updown-20">
                <i class="text-dark circular acms-icon acms-icon-pencil-ruler"></i>
                <div class="content">
                    <h3 class="title"><{translate id="Entwürfe" textdomain="Core"}></h3>
                    <h4 class="sub-title"><{translate id="Sie haben aktuell keine Nachrichten als Entwurf gespeichert." textdomain="Core"}></h4>
                </div>
            </div>
            <button class="primary rounded-light icon button push-updown-0" data-toggle="modal" data-target="#compose-message" data-module="apps/ui/informations/modal">
                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                <span>Neue Nachricht</span>
            </button>
        </div>
    <{/foreach}>
</div>
