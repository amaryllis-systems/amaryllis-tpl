<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="content-block v5">
    <div class="page-title v7">
        <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
        <div class="content">
            <h3 class="title"><{translate id="Suche" textdomain="Core"}></h3>
            <div class="sub-title">Posteingang durchsuchen</div>
        </div>
    </div>
    <div class="content">
        <{if $acmsContents->keywords}>
            <{* token type "with keyword" *}>
            <{counter start=0 print=false name="with_keyword" assign="with_keyword"}>
            <{capture name="withKeyword" assign="withKeyword"}>
                <div class="text-bold">Mit Keyword:</div>
                <div class="horizontal list push-0 pull-0">
                    
                    <{foreach $acmsContents->keywords['and'] as $token}>
                        <{if !$token->isNamedField()}>
                            <{counter name="with_keyword"}>
                            <div class="item"><span class="tag v1"><{$token->toString()}></span></div>
                        <{/if}>
                    <{/foreach}>
                    <{foreach $acmsContents->keywords['or'] as $token}>
                        <{if !$token->isNamedField()}>
                            <{counter name="with_keyword"}>
                            <div class="item"><span class="tag v1"><{$token->toString()}></span></div>
                        <{/if}>
                    <{/foreach}>
                </div>
            <{/capture}>
            <{if $with_keyword}>
                <{$smarty.capture.withKeyword}>
            <{/if}>
            <{* token type "without keyword" *}>
            <{counter start=0 print=false name="without_keyword" assign="without_keyword"}>
            <{capture name="withoutKeyword" assign="withoutKeyword"}>
                <div class="text-bold">Ohne Keyword:</div>
                <div class="horizontal list push-0 pull-0">
                    <{foreach $acmsContents->keywords['not'] as $token}>
                        <{if !$token->isNamedField()}>
                            <{counter name="without_keyword"}>
                            <div class="item"><span class="tag v1"><{$token->toString()}></span></div>
                        <{/if}>
                    <{/foreach}>
                </div>
            <{/capture}>
            <{if $without_keyword}>
                <{$smarty.capture.withoutKeyword}>
            <{/if}>
            <{* token type "skipped keyword" *}>
            <{counter start=0 print=false name="skipped_keyword" assign="skipped_keyword"}>
            <{capture name="skippedKeyword" assign="skippedKeyword"}>
                <div class="text-bold">Skipped Keyword:</div>
                <div class="horizontal list push-0 pull-0">
                    <{foreach $acmsContents->keywords['skipped'] as $token}>
                        <{if !$token->isNamedField()}>
                            <{counter name="skipped_keyword"}>
                            <div class="item"><span class="tag v1"><{$token->toString()}></span></div>
                        <{/if}>
                    <{/foreach}>
                </div>
            <{/capture}>
            <{if $skipped_keyword}>
                <{$smarty.capture.skippedKeyword}>
            <{/if}>
            <{* token type "named keyword" *}>
            <{counter start=0 print=false name="named_keyword" assign="named_keyword"}>
            <{capture name="namedKeyword" assign="namedKeyword"}>
                <div class="text-bold">Named Keyword:</div>
                    <{foreach $acmsContents->keywords['named'] as $token}>
                        <{counter name="named_keyword"}>
                        <div class="horizontal list push-0 pull-0">
                            <div class="item">
                                <span class="default right arrow label v2"><{$token->getField()}></span>
                            </div>
                            <{if !is_array($token->getValue())}>
                                <div class="item"><span class="tag v1"><{$token->getValue()}></span></div>
                            <{else}>
                                <{foreach $token->getValue() as $value}>
                                    <div class="item"><span class="tag v1"><{$value}></span></div>
                                <{/foreach}>
                            <{/if}>
                        </div>
                    <{/foreach}>
                </div>
            <{/capture}>
            <{if $named_keyword}>
                <{$smarty.capture.namedKeyword}>
            <{/if}>
        <{/if}>
    </div>
</div>
<div class="has-avatar divided collection v1 private-message-list" data-role="private-message-list">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item">
            <div class="avatar inline-avatar">
            <img src="<{$pm['from']['avatar']}>" class="item-image" alt="" title="" />
            </div>
            <div class="title">
                <span class="from-name display-inline-block">
                    <a href="<{$pm['from']['itemUrl']}>"
                       class="pm-link text-default"
                       title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                        <{$pm['from']['username']}>
                    </a>
                </span>
                <a class="private-message-link private-message-subject" href="<{$pm['url']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <p class="clearfix">
                <small class="text-muted">- <{$pm['teaser']}></small>
                <span class="xxsmall default label v2">
                    <{$pm['date']['pdate-short']}>
                    <span class="secondary-content">
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                    </span>
                </span>
            </p>
        </div>
    <{/foreach}>
</div>
