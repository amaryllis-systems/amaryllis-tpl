<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="pm-trash collection v1 has-image" data-role="private-message-list">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item<{if !$pm.read}> read<{/if}>"
            data-id="<{$pm['id']}>">
            <img src="<{$pm['from']['avatar']}>" class="item-image" alt="" title="" />
            <div class="title">
                <span class="from-name display-inline-block">
                    <a href="<{$pm['from']['itemUrl']}>"
                       class="pm-link text-default"
                       title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                        <{$pm['from']['username']}>
                    </a>
                </span>
                <a class="private-message-link private-message-subject<{if !$pm['read']}> text-bold unread<{/if}>" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <p class="clearfix">
                <small class="text-muted">- <{$pm['teaser']}></small>
                <span class="xxsmall default label v2">
                    <{$pm['pdate-short']}>
                    <span class="secondary-content">
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                    </span>
                </span>
            </p>
        </div>
    <{foreachelse}>
        <div class="large center aligned content-block v5 pull-updown-35">
            <div class="large center aligned sucess icon page-title v7 pull-updown-20">
                <i class="text-success circular acms-icon acms-icon-check-circle"></i>
                <div class="content">
                    <h3 class="title text-success"><{translate id="Jemand hat den Müll rausgebracht!" textdomain="Core"}></h3>
                    <h4 class="sub-title"><{translate id="Keine Nachrichten im Papierkorb!" textdomain="Core"}></h4>
                </div>
            </div>
        </div>
    <{/foreach}>
</div>
