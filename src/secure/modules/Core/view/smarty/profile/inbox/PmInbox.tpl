<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="has-avatar divided collection v1 private-message-list" data-role="private-message-list">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item<{if !$pm.read}> unread<{/if}>"
            data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-read="<{$pm['read']}>" data-role="private-message"
            >
            <div class="avatar inline-avatar">
                <img src="<{$pm['from']['avatar']}>" class="" alt="" title="" />
            </div>
            <div class="title">
                <span class="from-name display-inline-block">
                    <a href="<{$pm['from']['itemUrl']}>"
                       class="pm-link text-default"
                       title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                        <{$pm['from']['username']}>
                    </a>
                </span>
                <a class="private-message-link private-message-subject<{if !$pm['read']}> unread<{/if}>" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <div class="clearfix text pull-down-10">
                <{if $pm['type'] == 1 && $pm['status'] == 1}>
                    <span class="xxsmall warning label v2">ungelesen</span>
                <{/if}>
                <span class="xxsmall default label v2">
                    <{$pm['pdate-short']}>
                    <span class="secondary-content">
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                    </span>
                </span>
            </div>
            <div class="secondary-content text-xxlarge">
                <{if $pm['type'] == 1 && $acmsContents->subview == 'inbox-inbox'}>
                    <span aria-label="<{translate id='Klicken Sie auf den Stern um eine Nachricht als wichtig/unwichtig zu markieren.' textdomain='Core'}>"
                        data-trigger="<{if !$pm.important}>star<{else}>unstar<{/if}>"
                        class="cursor-pointer push-right-10 <{if $pm.important}>important<{/if}>">
                        <i class="pm-favorite acms-icon acms-icon-star<{if !$pm.important}> regular<{else}> purple-text<{/if}>"></i>
                    </span>
                <{/if}>
                <span class="cursor-pointer push-right-10"
                    aria-label="<{translate id='Klicken Sie um die Nachricht zu löschen.' textdomain='Core'}>"
                    data-trigger="trash">
                    <i class="pm-favorite acms-icon acms-icon-trash text-danger"></i>
                </span>
            </div>
        </div>
    <{foreachelse}>
        <div class="large center aligned content-block v5 pull-updown-35">
            <div class="large center aligned icon page-title v7 pull-updown-20">
                <i class="text-default circular acms-icon acms-icon-inbox"></i>
                <div class="content">
                    <h3 class="title">Post-Eingang</h3>
                    <h4 class="sub-title"><{translate id="Sie haben aktuell keine Nachrichten in Ihrem Posteingang." textdomain="Core"}></h4>
                </div>
            </div>
            <button class="primary rounded-light icon button push-updown-0" data-toggle="modal" data-target="#compose-message" data-module="apps/ui/informations/modal">
                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                <span>Neue Nachricht</span>
            </button>
        </div>
    <{/foreach}>
</div>
