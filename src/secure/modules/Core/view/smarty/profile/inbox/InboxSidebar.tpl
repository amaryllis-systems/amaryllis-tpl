<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="profile-inbox-aside" role="section" aria-label="Inbox-Navigation">
    <div class="push-updown-10 text-center">
        <button class="primary rounded-light large icon button push-updown-0" data-role="pm-form-trigger" data-toggle="modal" data-target="#compose-message" data-module="apps/ui/informations/modal">
            <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
            <span>Neue Nachricht</span>
        </button>
    </div>
    <div class="primary vertical fluid has-arrow icon nav v1">
        <{* inbox *}>
        <a class="<{if $acmsContents->subview == 'inbox-inbox'}>active <{/if}>item" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername]}>" title="<{translate id='Ihr Posteingang' textdomain='Core'}>" data-view="inbox">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-inbox"></i>
            <span class=""><{translate id="Post-Eingang" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="inbox-counter"><{$acmsContents->counter['messages']}></span>
        </a>
        <{* important *}>
        <a class="<{if $acmsContents->subview == 'inbox-important'}>active royal <{/if}>item" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'important', 'viewBy' => 'liste']}>" title="<{translate id='Ihre wichtigen Nachrichten' textdomain='Core'}>" data-view="important">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-star important"></i>
            <span class=""><{translate id="Wichtige" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="important-counter"><{$acmsContents->counter['important']}></span>
        </a>
        <{* notifications *}>
        <a class="<{if $acmsContents->subview == 'inbox-notifications'}>active info <{/if}>item" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'notifications', 'viewBy' => 'liste']}>" title="<{translate id='Ihre (System-) Benachrichtigungen' textdomain='Core'}>" data-view="notification">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-info-circle"></i>
            <span class=""><{translate id="Benachrichtigungen" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="notifications-counter"><{$acmsContents->counter['notifications']}></span>
        </a>
        <{* mentions *}>
        <a class="<{if $acmsContents->subview == 'inbox-mentions'}>active <{/if}>item" data-view="mentions" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'mentions', 'viewBy' => 'liste']}>">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-at"></i>
            <span class=""><{translate id="Erwähnungen" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="mentions-counter"><{$acmsContents->counter['mentions']}></span>
        </a>
        <{* sent *}>
        <a class="<{if $acmsContents->subview == 'inbox-sent'}>active <{/if}>item" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'sent', 'viewBy' => 'liste']}>" title="<{translate id='Gesendete Nachrichten' textdomain='Core'}>" data-view="sent">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-paper-plane regular"></i>
            <span class=""><{translate id="Gesendet" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="sent-counter"><{$acmsContents->counter['sent']}></span>
        </a>
        <{* drafts *}>
        <a class="<{if $acmsContents->subview == 'inbox-drafts'}>active <{/if}>item" data-view="drafts" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'drafts', 'viewBy' => 'liste']}>">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-pencil-ruler"></i>
            <span class=""><{translate id="Entwürfe" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter"><{$acmsContents->counter['drafts']}></span>
        </a>
        <{* trash *}>
        <a class="<{if $acmsContents->subview == 'inbox-trash'}>active danger <{/if}>item" href="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'trash', 'viewBy' => 'liste']}>" title="<{translate id='Papierkorb' textdomain='Core'}>" data-view="trash">
            <i aria-hidden="true" class="acms-icon acms-icon-2x acms-icon-trash"></i>
            <span class=""><{translate id="Papierkorb" textdomain="Core"}></span>
            <span data-role="counter" class="dark right floated badge v1 counter" data-role="trash-counter"><{$acmsContents->counter['trash']}></span>
        </a>
        <{* search *}>
        <div class="<{if $acmsContents->subview == 'inbox-search'}>active <{/if}>item">
            <form class="push-0" method="GET" action="<{generateUrl name="core_inbox_index" parameters=['username' => $acmsContents->lowerUsername, 'view' => 'search', 'viewBy' => 'liste']}>">
            <div class="icon field-group">
                <input placeholder="Search..." type="text" class="field" name="keyword" value="<{if $acmsContents->options}><{$acmsContents['options']->getOption('keyword')}><{/if}>">
                <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
            </div>
            </form>
        </div>
    </div> <!-- /.nav -->

    <div class="push-updown-10 text-center <{if 0 == $acmsContents->counter['trash']}>opacity-0<{else}>opacity-8<{/if}>" data-role="clear-cache" data-trigger="delete">
        <button class="danger rounded-light icon button push-updown-0">
            <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
            <span>Papierkorb leeren</span>
        </button>
    </div>
</div>
