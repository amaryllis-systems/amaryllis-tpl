<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="private-message">
    <div class="full row">
        <div class="column xxsmall-12 large-10">
            <div class="comments v2" data-role="private-message" data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>">
                <div class="page-title v7">
                    <h1 class="title"><{$pm['subject']}></h1>
                </div>
                <div class="comment">
                    <a class="medium avatar" href="<{$pm.from.username}>" title="<{$pm.from.username}>">
                        <img class="" alt="<{$pm.from.username}>" title="<{$pm.from.username}>" src="<{$pm.from.avatar}>" />
                    </a>
                    <div class="content">
                        <a href="<{$pm.from.username}>" title="<{$pm.from.username}>" class="author"><{$pm.from.username}></a>
                        <div class="info">
                            <time class="date" datetime="<{$pm['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$pm['pdate-relative']}></time>
                        </div>
                        <div class="text">
                            <{$pm.body}>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>
                </div>
                <div class="comments v2" data-role="replies">
                <{if $replies}>
                    <{foreach $replies as $reply}>
                        <div class="comment" data-date="<{$reply['pdate-webdate']}>">
                            <a class="medium avatar" href="<{$reply.from.itemUrl}>" title="<{$reply.from.username}>">
                                <img class="" alt="<{$reply.from.username}>" title="<{$reply.from.username}>" src="<{$reply.from.avatar}>" />
                            </a>
                            <div class="content">
                                <a href="<{$reply.from.itemUrl}>" title="<{$reply.from.username}>" class="author"><{$reply.from.username}></a>
                                <div class="info">
                                    <time class="date" datetime="<{$reply['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$reply['pdate-relative']}></time>
                                </div>
                                <div class="text">
                                    <{$reply.body}>
                                </div>
                                <div class="actions">
                                    
                                </div>
                            </div>
                        </div>
                    <{/foreach}>
                <{/if}>
                </div>
                <{if $pm['type'] == 1}>
                <div class="form-wrapper">
                    <form class="form" onsubmit="return false;"  
                          name="pm-reply" 
                          method="POST" 
                          data-role="reply-form">
                        <div class="form-group">
                            <label class="field-label src-only" for="reply-message">Nachricht</label>
                            <textarea class="field" id="reply-message" name="message"></textarea>
                        </div>
                        <input type="hidden" name="pmid" value="<{$pm.id}>">
                        <input type="hidden" name="idx" value="<{$pm.idx}>">
                        <input type="hidden" name="op" value="reply">
                        <input type="hidden" name="contentType" value="3">
                        <button class="button small primary right floated" type="submit" data-role="reply-button">
                            <i class="acms-icon acms-icon-paperplane"></i> <span>Antwort senden</span>
                        </button>
                    </form>
                </div>
                <{/if}>
            </div>

        </div>
        <div class="column xxsmall-12 large-2">
            <{if $pm['type'] == 1}>
                <button class="<{if $pm['important']}>important<{else}>default<{/if}> stacked primary-hover button" data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-trigger="<{if $pm['important']}>star<{else}>unstar<{/if}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-star<{if $pm['important']}> purple-text<{else}> regular<{/if}>"></i>
                    <span>markieren</span>
                </button>
            <{/if}>
            <button class="default stacked danger-hover button" data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-trigger="trash">
                <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                <span>Löschen</span>
            </button>
            <{if $pm['type'] == 1 && $acmsContents->receivers}>
                <div class="small page-title v7">
                    <i aria-hidden="true" class="acms-icon acms-icon-users text-xlarge"></i>
                    <div class="content">
                        <h2 class="title">Empfänger</h2>
                        <div class="sub-title text-small">alle Mitglieder in dieser Unterhaltung</div>
                    </div>
                </div>
                <div class="middle aligned divided list">
                    <{foreach $acmsContents->receivers as $receiver}>
                    <div class="item">
                        <img class="avatar image" src="<{$receiver['avatar']}>" title="<{$receiver['username']}>">
                        <div class="content">
                            <a class="header" href="<{$receiver['itemUrl']}>"><{$receiver['username']}></a>
                        </div>
                    </div>
                    <{/foreach}>
                </div>
            <{/if}>
        </div>
    </div>
</section>
