<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="pm-inbox" class="pm-inbox collection v1 has-image">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item<{if !$pm.read}> read<{/if}>" data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-read="<{$pm['read']}>" data-role="private-message">
            <img src="<{$pm['from']['avatar']}>" class="circle item-image" alt="" title="" />
            <div class="title">
                <span class="from-name display-inline-block">
                    <a href="<{$pm['from']['itemUrl']}>"
                       class="pm-link text-default"
                       title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=[$pm['from']['username']]}>">
                        <{$pm['from']['username']}>
                    </a>
                </span>
                <a class="private-message-link private-message-subject<{if !$pm['read']}> text-bold unread<{/if}>" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <p class="clearfix">
                <small class="text-muted">- <{$pm['teaser']}></small>
                <span class="xxsmall default label v2">
                    <{$pm['pdate-short']}>
                    <span class="secondary-content">
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                    </span>
                </span>
                <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                <{translate id="Empfänger" textdomain="Core"}>:
                <{foreach $pm['toUsers'] as $to}>
                    <span class="tag v1 round default">
                        <img src="<{$to['avatar']}>" class="" alt="" title="<{$to['username']}>" /> <{$to['username']}>
                    </span>
                <{/foreach}>
            </p>
            <div class="secondary-content text-xxlarge">
                <span aria-label="<{translate id='Klicken Sie auf den Stern um eine Nachricht als wichtig/unwichtig zu markieren.' textdomain='Core'}>"
                    data-trigger="<{if !$pm.important}>star<{else}>unstar<{/if}>"
                    class="cursor-pointer push-right-10 <{if $pm.important}>important<{/if}>">
                    <i class="pm-favorite acms-icon acms-icon-star<{if !$pm.important}> regular<{else}> purple-text<{/if}>"></i>
                </span>
                <span class="cursor-pointer push-right-10"
                        aria-label="<{translate id='Klicken Sie um die Nachricht zu löschen.' textdomain='Core'}>"
                        data-trigger="trash">
                        <i class="pm-favorite acms-icon acms-icon-trash text-danger"></i>
                </span>
            </div>
        </div>
    <{foreachelse}>
        <{translate id="Sie haben aktuell keine Nachrichten als wichtig markiert." textdomain="Core"}>
    <{/foreach}>
</div>
