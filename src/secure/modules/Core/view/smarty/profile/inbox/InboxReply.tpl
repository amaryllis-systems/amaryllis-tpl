<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<article class="row full single-reply" data-date="<{$reply['pdate-webdate']}>">
    <{if $reply['from']['id'] != $profileuser['id']}>
        <div class="column large-2 xlarge-2 xxlarge-2 hidden-for-medium-down">
            <figure class="thumbnail text-center">
                <img class="image circle" alt="<{$reply['from']['username']}>" title="<{$reply['from']['username']}>" src="<{$reply.from.avatar}>" />
                <figcaption class="text-center"><{$reply['from']['username']}></figcaption>
            </figure>
        </div>
    <{/if}>
    <div class="column xxsmall-12 large-10 xlarge-10 xxlarge-10">
        <div class="pm-panel v1 arrow <{if $reply.from.id == $profileuser['id']}>right<{else}>left<{/if}>">
            <div class="content bordered border-solid shadow">
                <div class="push-25">
                    <header class="text-<{if $reply.from.id == $profileuser['id']}>right<{else}>left<{/if}>">
                        <div class="comment-user"><i aria-hidden="true" class="acms-icon acms-icon-user"></i> <{$reply['from']['username']}></div>
                        <time class="comment-date" datetime="<{$reply['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$reply['pdate-relative']}></time>
                    </header>
                    <div class="comment-post">
                        <p>
                            <{$reply.body}>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <{if $reply.from.id == $profileuser['id']}>
        <div class="column large-2 xlarge-2 xxlarge-2 hidden-for-medium-down">
            <figure class="thumbnail text-center">
                <img class="image circle" alt="<{$reply['from']['username']}>" title="<{$reply['from']['username']}>" src="<{$reply.from.avatar}>" />
                <figcaption class="text-center"><{$reply['from']['username']}></figcaption>
            </figure>
        </div>
    <{/if}>
</article>
