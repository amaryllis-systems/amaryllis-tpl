<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="pm-sent collection v1 has-image private-message-list"  data-role="private-message-list">
    <{foreach $acmsContents->inbox as $pm}>
        <div class="private-message collection-item"
            data-id="<{$pm['id']}>" data-idx="<{$pm['idx']}>" data-role="private-message">
            <i aria-hidden="true" class="acms-icon acms-icon-users item-image"></i>
            <div class="title">
                <a class="private-message-link private-message-subject" href="<{$pm['itemUrl']}>" title="<{$pm['subject']}>">
                    <span class="pm-subject"><{$pm['subject']}></span>
                </a>
            </div>
            <div class="clearfix">
                <small class="text-muted">- <{$pm['teaser']}></small>
                <span class="xxsmall default label v2">
                    <{$pm['pdate-short']}>
                    <span class="secondary-content">
                        <i aria-hidden="true" class="acms-icon acms-icon-calendar"></i>
                    </span>
                </span>
                <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                <{translate id="Empfänger" textdomain="Core"}>:
                <{foreach $pm['toUsers'] as $to}>
                    <span class="tag v1 round default">
                        <img src="<{$to['avatar']}>" class="" alt="" title="<{$to['username']}>" /> <{$to['username']}>
                    </span>
                <{/foreach}>
            </div>
        </div>
    <{foreachelse}>
        <div class="large center aligned content-block v5 pull-updown-35">
            <div class="large center aligned icon page-title v7 pull-updown-20">
                <i class="text-default circular acms-icon acms-icon-paper-plane"></i>
                <div class="content">
                    <h3 class="title">Gesendete Nachrichten</h3>
                    <h4 class="sub-title"><{translate id="Sie haben aktuell keine gesendeten Nachrichten." textdomain="Core"}></h4>
                </div>
            </div>
            <button class="primary rounded-light icon button push-updown-0" data-toggle="modal" data-target="#compose-message" data-module="apps/ui/informations/modal">
                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt"></i>
                <span>Neue Nachricht</span>
            </button>
        </div>
    <{/foreach}>
</div>
