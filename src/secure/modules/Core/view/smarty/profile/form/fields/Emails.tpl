<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" 
     data-remote="<{generateUrl name="core_profile_email_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>"
     data-owner="<{$acmsContents->profileuser['id']}>" 
     data-module="view/profile/emails/emails">
    <{if $field.with_label}>
    <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
        <span class="text-danger required">*</span><{/if}>
        <{if $field.label.description && $field.show_tooltip}>
            <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                <span class="pointer acms-icon acms-icon-info-circle"></span>
            </a>
        <{/if}>
    </label>
    <{/if}>
    <div class="row full" data-section="emails">
        <{foreach $field['values'] as $type => $emails}>
            <div class="column xxsmall-12 large-6 xlarge-6 xxlarge-6 profile-emails">
                <div class="push-10">
                    <h3>
                        <{$field['types'][$type]}>
                    </h3>
                    <div class="tag-bar" data-role="profile-emails" data-type="<{$type}>">
                    <{foreach $emails as $email}>
                        <div class="profile-emails-email tag"
                             data-id="<{$email['id']}>">
                            <span><{$email['email']}></span> <i aria-hidden="true" class="acms-icon acms-icon-trash-alt cursor-pointer" data-action="delete" data-trigger="action"></i>
                        </div>
                    <{/foreach}>
                    </div>
                </div>
            </div>
        <{/foreach}>
    </div>
    <div class="row full">
        <div class="column xxsmall-4 large-3 xlarge-3 xxlarge-3">
            <select class="field" name="<{$field['name']}>-emailtype">
                <{foreach $field['types'] as $id => $type}>
                    <option value="<{$id}>" data-type="<{$type}>"><{$type}></option>
                <{/foreach}>
            </select>
        </div>
        <div class="column xxsmall-8 large-9 xlarge-9 xxlarge-9">
            <div>
                <div class="field-group">
                    <input type="email" <{$field.attributes}> />
                    <{if !$field['isRegistration']}>
                        <div class="field-icon cursor-pointer bg-theme" data-role="action-button" aria-label="E-Mail speichern">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
        <{if $field.with_label && $field.label.description && !$field.show_tooltip}>
            <span class="field-hint"><{$field.label.description}></span>
        <{/if}>
    </div>
</div>
