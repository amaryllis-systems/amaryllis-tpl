<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal large" id="compose-message" tabindex="-1" role="dialog" aria-labelledby="compose-message-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form id="pmfrom" 
                  role="form" 
                  method="POST" 
                  data-role="private-message-form"
                  data-module="apps/profile/inbox/pm-form"
                  data-remote="<{generateUrl name='core_inbox_action' parameters=['username' => $acmsData->user['username']|lower, 'action' => 'action']}>">
                
                <div class="header text-center">
                    <button type="button" class="close" data-dismiss="modal" data-trigger="abort-pm" aria-label="<{translate id="Schließen" textdomain="Core"}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                    </button>
                    <h4 class="title" id="compose-message-title"><{translate id="Neue Nachricht"}></h4>
                </div>
                <div class="main text-left">
                    <div class="form-group">
                        <label class="field-label" for="subject">
                            <{translate id="Betreff" textdomain="Core"}> 
                            <sup class="text-danger">*</sup>
                        </label>
                        <input type="text" id="subject" name="subject" class="field" value="" />
                    </div>
                    <{if isset($toUser) && $toUser}>
                        <input type="hidden" name="to" value="<{$toUser['username']}>" />
                    <{else}>
                        <div class="form-group">
                            <label class="field-label" for="to">
                                <{translate id="Sende an"}> 
                                <sup class="text-xxsmall text-danger">*</sup>
                            </label>
                            <input data-remoteurl="<{generateUrl name='core_inbox_action' parameters=['username' => $acmsData->user['username']|lower, 'action' => 'searchUser']}>"
                                data-minlength="2" 
                                data-maxitems="1" 
                                placeholder="<{translate id='Geben Sie den Benutzernamen ein' textdomain='Core'}>"
                                type="text" 
                                id="to" 
                                required="required" 
                                data-module="apps/searching/inputsearch" 
                                name="to_user" 
                                class="field"
                                value="" />
                        </div>
                    <{/if}>
                    <div class="form-group">
                        <label class="field-label" for="message">
                            <{translate id="Nachricht" textdomain="Core"}>
                            <sup class="text-xxsmall text-danger">*</sup>
                        </label>
                        <textarea rows="15" id="message" name="message" required="required" class="field"></textarea>
                    </div>
                    <input type="hidden" name="draft_id" value="0">
                    <input type="hidden" name="contentType" value="3">
                </div>
                <div class="footer">
                    <div class="button-toolbar">
                        <button type="button" 
                                class="small right floated primary icon button" 
                                id="send-pm" 
                                data-trigger="send-pm" 
                                data-dismiss="modal">
                            <i aria-hidden="true" class="acms-icon acms-icon-paper-plane regular"></i>
                            <span><{translate id="Senden" textdomain="Core"}></span>
                        </button>
                        <button type="reset" class="small left floated default icon button" data-trigger="abort-pm" data-dismiss="modal">
                            <i aria-hidden="true" class="acms-icon acms-icon-trash-alt"></i>
                            <span><{translate id="Abbrechen" textdomain="Core"}></span>
                        </button>
                        <button type="button" class="small left floated secondary icon button" data-trigger="save-draft">
                            <i aria-hidden="true" class="acms-icon acms-icon-save regular"></i>
                            <span><{translate id="Entwurf speichern" textdomain="Core"}></span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
