<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-wrapper" data-cssfile="media/css/profile/profile-configs.min.css">
    <form role="form" <{$form.attributes}>>
        
        <div class="icon page-title v7">
            <i aria-hidden="true" class="acms-icon acms-icon-user-cog"></i>
            <div class="content">
                <h1 class="title">Profil Einstellungen</h1>
                <h2 class="sub-title">Konfiguriere deinen Account nac h deinen Wünschen</h2>
            </div>
        </div>
        <div class="tab-container" data-module="ui/navs/tabs" data-tab=".config-tab">
            <div class="row full">
                <div class="column xxsmall-12">
                    <ul class="config-tablist" role="tabs">
                    <{foreach $form['tabs'] as $name => $tab}>
                        <li role="presentation" class="text-center config-tab<{if $tab['active']}> active<{/if}>"
                            <{if $tab['active']}>
                                aria-selected="true"
                            <{else}>
                                aria-selected="false"
                            <{/if}>>
                            <a class="config-tab-item" href="#<{$name}>" role="tab" data-target="#<{$name}>">
                                <i class="acms-icon acms-icon-3x <{if $tab['config']['fonticon']}><{$tab['config']['fonticon']}><{else}>acms-icon-cog<{/if}>"></i>
                                <small><{$tab['title']}></small>
                            </a>
                        </li>
                    <{/foreach}>
                    </ul>
                </div>
                <div class="column xxsmall-12">
                    <div class="tab-content">
                    <{foreach $form.fields as $field}>
                        <{include file=$field['template'] field=$field nocache}>
                    <{/foreach}>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
