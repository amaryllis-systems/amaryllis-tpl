<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="form-group" 
     data-module="view/profile/weblinks/websites"
     data-remote="<{generateUrl name="core_profile_websitelinks_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>">
    <{if $field.with_label}>
        <label class="field-label" <{$field.label.attributes}>><{$field.label.caption}> <{if $field.required}>
            <span class="text-danger required">*</span><{/if}>
            <{if $field.label.description && $field.show_tooltip}>
                <a class="acms-helptip" title="<{$field.label.description}>" data-module="apps/ui/informations/tooltip" data-toggle="tooltip" data-trigger="hover click" data-placement="top" data-html="true">
                    <span class="pointer acms-icon acms-icon-info-circle"></span>
                </a>
            <{/if}>
        </label>
    <{/if}>
    <div class="row full" data-section="weblinks">
        <div class="column xxsmall-12 large-6 xlarge-6 xxlarge-6 profile-weblinks">
            <{foreach $field['values'] as $type => $weblinks}>
                <div class="weblink-container" data-role="profile-weblinks" data-type="<{$type}>">
                    <div class="weblink-title">
                        <h3>
                            <{$field['types'][$type]}>
                        </h3>
                    </div>
                    <div class="weblink-title">
                        <{foreach $field['values'] as $weblinks => $weblink}>
                            <div class="profile-weblink tag"
                                 data-type="<{$weblink['typevalue']}>" data-id="<{$weblink['id']}>">
                                
                                <span><{$weblink['weblink']}></span> <i aria-hidden="true" class="acms-icon acms-icon-trash-alt cursor-pointer pull-10" data-action="delete" data-trigger="action"></i>
                            </div>
                        <{/foreach}>
                    </div>
                </div>
            <{/foreach}>
        </div>
    </div>
    <div class="row full">
        <div class="column xxsmall-4 large-3 xlarge-3 xxlarge-3">
            <select class="field" name="<{$field['name']}>-weblinktype">
                <{foreach $field['types'] as $id => $type}>
                    <option value="<{$id}>"><{$type}></option>
                <{/foreach}>
            </select>
        </div>
        <div class="column xxsmall-8 large-9 xlarge-9 xxlarge-9">
            <div>
                <div class="field-group">
                    <input type="url" <{$field.attributes}> />
                    <{if !$field['isRegistration']}>
                        <div class="field-icon cursor-pointer bg-theme" data-role="action-button" aria-label="Link Speichern" aria-hidden="true">
                            <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                        </div>
                    <{/if}>
                </div>
            </div>
        </div>
        <{if $field.with_label && $field.label.description && !$field.show_tooltip}>
            <span class="field-hint"><{$field.label.description}></span>
        <{/if}>
    </div>
</div>
