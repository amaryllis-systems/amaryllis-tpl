<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" data-profileview="board" itemtype="http://schema.org/ProfilePage">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-board profile-startpage">
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl" nocache}>
        <div class="light bottom arrow-box v1 pull-updown-35">
            <div class="large center aligned icon page-title v7 pull-updown-35">
                <{getDate assign="currentDate"}>
                <{assign var="hour" value=$currentDate->format('G')}>
                <{if $hour > 4 &&  $hour < 10}>
                    <{translate id="Guten Morgen, %s!" args=[$acmsData->user['username']] assign="message"}>
                <{elseif $hour >= 10 &&  $hour < 15}>
                    <{translate id="Guten Tag, %s!" args=[$acmsData->user['username']] assign="message"}>
                <{elseif $hour >= 15 &&  $hour < 16}>
                    <{translate id="Hallo, %s!" args=[$acmsData->user['username']] assign="message"}>
                <{elseif $hour >= 16 &&  $hour < 23}>
                    <{translate id="Schönen Abend, %s!" args=[$acmsData->user['username']] assign="message"}>
                <{else}>
                    <{translate id="Schöne Träume, %s!" args=[$acmsData->user['username']] assign="message"}>
                <{/if}>
                <div class="content">
                    <h2 class="title"><{$message}></h2>
                    <p class="sub-title">
                        <{if $acmsContents->isOwner}>
                            <{translate id="Willkommen auf deinem Profil!"}>
                        <{else}>
                            <{translate id="Willkommen auf meinem Profil!"}>
                        <{/if}>
                    </p>
                </div>
            </div>
        </div>
        <{if $acmsContents->isOwner}>
            <div class="light bottom arrow-box v1">
                <div class="row full pull-updown-35">
                    <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
                        <div class="display-block push-15">
                            <{include file="acmsfile:Core|profile/profile/PostToBoard.tpl" nocache}>
                        </div>
                    </div>
                </div>
            </div>
        <{/if}>
        <div class="white bottom arrow-box v1">
            <div class="row full pull-updown-35">
                <div class="column xxsmall-12 pull-updown-35">
                    <div class="center aligned primary large icon page-title v7">
                        <i aria-hidden="true" class="circular acms-icon acms-icon-history"></i>
                        <div class="content">
                            <h3 class="title"><{translate id="Letzte Aktivitäten"}></h3>
                            <div class="sub-title"><{translate id="Letzte Aktivitäten von %s" args=[$acmsContents->displayname]}></div>
                        </div>
                    </div>
                </div>
                <div class="column xxsmall-12 pull-updown-35">
                    <ul class="timeline v1"
                        data-limit="20"
                        data-remote="<{$acmsContents->get('profileTimelineRemote')}>"
                        data-module="apps/profile/board/timeline"
                        data-cssfile="media/css/profile/timeline"
                        >&nbsp;</ul>
                </div>
            </div>
        </div>
    </div>
</section>
