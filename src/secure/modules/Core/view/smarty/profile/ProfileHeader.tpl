<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $acmsContents['acmsBreadCrumb']}>
    <div class="container fixed-1200">
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <{include file=$acmsContents['acmsBreadCrumb']['template']}>
            </div>
        </div>
    </div>
<{/if}>
<div class="container" data-cssfile="media/css/profile/profile">
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12">
            <{include file="acmsfile:Core|profile/ProfileCurrentMenu.tpl"}>
        </div>
    </div>
</div>
<section class="primary bottom arrow-box v1">
    <div class="container fixed-1200">
        <div class="arrow-box-content">
            <div class="page-title v7">
                <div class="xxlarge round avatar">
                    <img src="<{$acmsContents->profileuser['avatar']}>" alt="" title="<{$acmsContents->profileuser['username']}>"  data-role="current-avatar">
                </div>
                <div class="content">
                    <h1 class="white-text"><{$acmsContents->displayname}></h1>
                    <h2 class="sub-title"></h2>
                </div>
            </div>
        </div>
    </div>
</section>
