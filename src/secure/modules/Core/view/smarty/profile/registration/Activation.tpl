<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $acmsContents['acmsBreadCrumb']}>
    <div class="container full">
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <{include file=$acmsContents['acmsBreadCrumb']['template']}>
            </div>
        </div>
    </div>
<{/if}>

<div class="container">
    <div class="row">
        <div class="column xxsmall-12">
            <div class="page-title">
                <h1><{$acmsContents->acmsTitle}></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column xxsmall-12 medium-6 medium-offset-3">
            <div class="card v1card-container center-block">
                <div class="text-center profile-img-card v1blue-grey darken-3 white-text">
                    <i class="image circle acms-icon acms-icon-user acms-icon-5x"></i>
                </div>
                <p id="profile-name" class="profile-name-card"></p>
                <div class="flip-container white blue-grey-text" data-module="ui/widgets/flipbox">
                    <div class="flipper">
                        <div class="front">
                            <{loadBlock name="core_login"}>
                            <a href="#" data-trigger="flip" class="forgot-password show-forgot">
                                <{translate id="Passwort vergessen?" textdomain="Core"}>
                                <i aria-hidden="true" class="acms-icon acms-icon-chevron-circle-right"></i>
                            </a>
                        </div>
                        <div class="back bg-warning">
                            <div class="form-wrapper">
                            <form role="form" action="#">
                                <div class="form-group">
                                    <label class="field-label">
                                        <{translate id="Ihre E-Mail Adresse"}>
                                    </label>
                                    <div class="field-group">
                                        <input class="field" type="email" placeholder="<{translate id="E-Mail"}>" />
                                        <div class="field-icon">
                                            <i aria-hidden="true" class="acms-icon acms-icon-at"></i>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="button primary">
                                    <{translate id="Absenden"}>
                                </button>
                            </form>
                            </div>
                            <a href="#" data-trigger="flip" class="forgot-password hide-forgot">
                                <i aria-hidden="true" class="acms-icon acms-icon-chevron-circle-left"></i>
                                <{translate id="Zurück zum Login-Formular" textdomain="Core"}>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
