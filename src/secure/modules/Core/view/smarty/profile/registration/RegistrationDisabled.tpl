<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container full">
            <div class="row xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>
    <section class="page-section v1 primary">
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <h1 class="white-text" itemprop="name"><{$acmsContents->acmsTitle}></h1>
                </div>
            </div>
        </div>
    </section>
    <section class="white">
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12 large-8 large-offset-2 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
                    <div class="alert v1 danger" data-cssfile="media/css/components/alert.min.css">
                        <h4 class="alert-title"><{translate id="Keine Registrierung möglich" textdomain="Core"}></h4>
                        <div class="alert-content">
                            <p>
                                Vielen Dank für Ihr Interesse sich in unserem Portal zu registrieren. Derzeit ist diese Seite ist nur für die interne Benutzung 
                                freigeschalten und eine Registrierung ist leider nicht möglich.
                            </p>
                            <p>
                                Als Kunde von <{$acmsData->acmsSitename}> können Sie sich über den <a role="button" class="button tiny primary" href="<{generateUrl name="core_login"}>" title="Zur Kundenanmeldung">Kunden-Zugang</a> anzumelden um Zugriff auf Ihre Daten zu bekommen.
                            </p>
                        </div>
                        <div class="alert-footer">
                            <p><{translate id="Ihr %s Team" textdomain="Core" args=$acmsData->acmsSitename}></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
