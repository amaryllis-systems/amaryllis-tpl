<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section role="main" itemscope="" itemtype="http://schema.org/WebPage">
    <{if $acmsContents['acmsBreadCrumb']}>
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-25">
                <div class="column xxsmall-12">
                    <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                </div>
            </div>
        </div>
    <{/if}>

    <div class="container fixed-1200">
        <div class="row full">
            <div class="column xxsmall-12">
                <div class="page-title">
                    <h1 itemprop="name"><{$acmsContents->acmsTitle}></h1>
                </div>
            </div>
        </div>
        <div class="row full">
            <div class="column xxsmall-12 large-8 large-offset-2 xlarge-8 xlarge-offset-2 xxlarge-8 xxlarge-offset-2">
                <{if $acmsContents->form['template']}>
                    <{include file=$acmsContents->form['template'] form=$acmsContents->form nocache}>
                <{/if}>
            </div>
        </div>
    </div>
</section>
