<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://schema.org/WebPage">

    <{block name="breadcrumb"}>
        <{if $acmsContents['acmsBreadCrumb']}>
            <div class="container fixed-1200">
                <div class="row full xxsmall-push-updown-25">
                    <div class="column xxsmall-12">
                        <{include file=$acmsContents['acmsBreadCrumb']['template']}>
                    </div>
                </div>
            </div>
        <{/if}>
    <{/block}>

<div class="container fixed-1200">
    <div class="row full">
        <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
            <div class="page-title v7">
                <i class="acms-icon acms-icon-key"></i>
                <div class="content">
                    <h1 class="push-0"><{translate id="Anmelden" textdomain="Core"}></h1>
                    <div class="sub-title">
                        Melde dich mit deinen Login-Daten in unserem Portal an.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row full">
        <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
            <div class="card v1 card-container center-block pull-updown-25">
                <div class="center aligned icon page-title v7 push-updown-20">
                    <i class="circular acms-icon acms-icon-user"></i>
                </div>
                
                <div class="flip-container white blue-grey-text" data-module="ui/widgets/flipbox">
                    <div class="flipper">
                        <div class="front">
                            <{block name="login_form"}>
                                <{include file=$acmsContents['loginForm']->get('template') 
                                          form=$acmsContents['loginForm'] nocache}>
                            <{/block}>
                            <a href="#" data-trigger="flip" class="forgot-password show-forgot">
                                <{translate id="Passwort vergessen?" textdomain="Core"}>
                                <i aria-hidden="true" class="acms-icon acms-icon-chevron-circle-right"></i>
                            </a>
                        </div>
                        <div class="back bg-warning">
                            <{block name="forgot_password"}>
                                <div class="form-wrapper">
                                    <form role="form" 
                                          action="<{generateUrl name="core_lostpass_action" parameters=['action' => 'reset']}>" 
                                          method="post" data-module="view/profile/forgot-password">
                                        <div class="form-group">
                                            <p class="field-static">
                                                Hier musst du die E-Mail Adresse angeben, die du für die 
                                                Registrierung verwendet hast. Nach absenden des Formulars 
                                                erhälst du eine E-Mail mit den weiteren Anweisungen.
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label class="field-label">
                                                <{translate id="Ihre E-Mail Adresse" textdomain="Core"}>
                                            </label>
                                            <div class="field-group">
                                                <input class="field" type="email" placeholder="<{translate id="E-Mail" textdomain="Core"}>" />
                                                <div class="field-icon">
                                                    <i aria-hidden="true" class="acms-icon acms-icon-at"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="button primary">
                                            <{translate id="Absenden" textdomain="Core"}>
                                        </button>
                                    </form>
                                </div>
                            <{/block}>
                            <a href="#" data-trigger="flip" class="forgot-password hide-forgot">
                                <i aria-hidden="true" class="acms-icon acms-icon-chevron-circle-left"></i>
                                <{translate id="Zurück zum Login-Formular" textdomain="Core"}>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
