<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<ul class="list-unstyled clearfix"
    data-role="all-circles"
    data-remoteactionurl="<{generateUrl name='core_profile_circle_action' parameters=['username' => $acmsContents['profileuser']['username']|lower, 'action' => 'action']}>">
    <{if $acmsContents->has('circles')}>
        <{foreach $acmsContents['circles'] as $circle}>
            <li class="column xxsmall-12 medium-4 large-3"
                data-role="circle"
                data-circle="<{$circle['title']}>"
                data-id="<{$circle['id']}>">
                <div class="circle">
                    <ul id="circle-friends-<{$circle['id']}>"
                        data-circle="<{$circle['title']}>"
                        data-id="<{$circle['id']}>"
                        data-limit="10"
                        data-friends="<{count($circle['members'])}>"
                        class="circle-friends">
                        <{foreach $acmsContents['friends'][3]['friends'] as $friend}>
                            <li class="circle-friend<{if $friend@index > 9}> hidden<{/if}>"
                                data-role="circle-friend"
                                data-username="<{$friend.username}>"
                                data-circle="<{$friend['all_circles']}>"
                                data-uid="<{$friend['friend_uid']}>"
                                >
                                <a data-role="userlink" href="<{$friend.profile}>" title="<{$friend.username}>">
                                    <img data-role="avatar" role="avatar" title="<{$friend.username}>" alt="<{$friend.username}>" src="<{$friend.avatar}>" />
                                </a>
                            </li>
                        <{/foreach}>
                    </ul>
                    <button type="button" class="circle-open" data-circleid="<{$circle['id']}>" data-role="open-circle">
                        <span data-role="title"><{$circle['title']}></span>
                        <span class="badge v1" data-role="friend-counter"><{count($circle['members'])}></span>
                    </button>
                    <a role="button" class="circle-close button link right">
                        <span class="acms-icon acms-icon-trash-alt text-danger right pointer"
                              data-role="remove-circle"></span>
                    </a>
                </div> <!-- /.circle -->
            </li>
        <{/foreach}>
    <{/if}>
</ul> <!-- / #all-circles -->
