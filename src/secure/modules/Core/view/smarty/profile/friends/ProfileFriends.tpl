<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" itemtype="http://schema.org/ProfilePage">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-friends"
         data-cssfile="media/css/profile/profile-friends"
         data-role="profile" data-friendslist="#profile-friends" data-profileview="Friends"
         data-owner="<{if $acmsContents->get('isOwner')}>true<{else}>false<{/if}>"
         >
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>
        <{if $acmsContents->get('isOwner')}>
            <{include file="acmsfile:Core|profile/friends/ProfileFriendsOwner.tpl" nocache}>
        <{else}>
            <{include file="acmsfile:Core|profile/friends/ProfileFriendsVisitor.tpl" nocache}>
        <{/if}>
        <{if $acmsContents->get('isOwner') || $acmsContents->userAcceptsFriendRequest}>
            <div class="fixed-action-button" data-module="ui/navs/floating-button" data-nav="floating-button">
                    <{if $acmsContents->get('isOwner')}>
                            <a class="button floating red large"
                               data-module="apps/ui/informations/modal"
                               data-target="#addcircle-content"
                               data-toggle="modal" title="<{translate id="Neuer Kreis" textdomain="Core"}>">
                                <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                            </a>
                    <{elseif $acmsContents->acceptFriendrequest}>
                            <a class="button floating red large"
                               title="<{translate id="Zu meinen Personen hinzufügen" textdomain="Core"}>"
                               data-role="add-to-friends"
                               data-module="apps/profile/friendship/friend-request"
                               data-remote="<{$acmsContents['actionUrl']}>"
                               data-friend="<{$acmsContents['profileuser']['id']}>"
                               >
                                <i aria-hidden="true" class="acms-icon acms-icon-user-plus"></i>
                            </a>
                    <{/if}>
                </ul>
            </div>
        <{/if}>
        <{include file="acmsfile:Core|profile/form/PrivatemessageForm.tpl"}>
        <{if $acmsContents->get('isOwner')}>
            <{include file="acmsfile:Core|profile/friends/NewCircle.tpl"}>
        <{/if}>
    </div>
</section>
