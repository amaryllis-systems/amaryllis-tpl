<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal" id="addcircle-content" data-role="addcircle" tabindex="-1" role="dialog" aria-labelledby="add-circle-heading" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form name="addcircle" 
                  method="post" 
                  role="form" 
                  action="#"
                  data-remote="<{generateUrl name="core_profile_circle_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>" 
                  data-module="view/profile/circles/circle-add">
                <div class="header">
                    <button class="close"
                            type="reset"
                            data-role="abort-circle"
                            data-dismiss="modal"
                            aria-label="<{translate id='Abbrechen' textdomain='Core'}>">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                    </button>
                    <h4 class="title" id="add-circle-heading">
                        <i aria-hidden="true" class="acms-icon acms-icon-plus-circle text-muted"></i>
                        <span class="text-theme"><{translate id='Einen neuen Kreis hinzufügen' textdomain='Core'}></span>
                    </h4>
                </div>
                <div class="main">
                    <div class="push-10">
                        <div class="form-group">
                            <label class="src-only" for="title"><{translate id='Name der Gruppe' textdomain='Core'}></label>
                            <input type="text" name="title" class="field"
                                   placeholder="<{translate id='Geben Sie den Gruppen-Namen an' textdomain='Core'}>" />
                        </div>
                    </div>
                    <input type="hidden" name="op" value="save">
                </div>
                <div class="footer">
                    <button id="submit-circle" 
                            class="button primary small" 
                            type="submit" 
                            data-role="submit-circle">
                        <i aria-hidden="true" class="acms-icon acms-icon-check"></i>
                        <span><{translate id="Erstellen" textdomain="Core"}></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</div> <!-- /.modal -->

<div class="modal xlarge" id="changecircle-membership" data-role="addcircle" tabindex="-1" role="dialog" aria-labelledby="changecircle-membership-heading" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <div class="header">
                <h4 class="title" id="changecircle-membership-heading">
                    Kreise
                </h4>
            </div>
            <div class="main">
                <div class="row full">
                    <{if $acmsContents->has('circles')}>
                        <{foreach $acmsContents['circles'] as $circle}>
                            <div class="column xxsmall-9 medium-6 large-8 xlarge-8 xxlarge-8">
                                <{$circle['title']}>
                            </div>
                            <div class="column xxsmall-3 medium-6 large-4 xlarge-4 xxlarge-4">
                                <div class="switch v2 success compact toggle-variant round">
                                    <input id="circle-select-<{$circle['id']}>"
                                           name="circle-select-<{$circle['id']}>"
                                           value="<{$circle['id']}>"
                                           type="checkbox">
                                    <label for="circle-select-<{$circle['id']}>"></label>
                                </div>
                            </div>
                        <{/foreach}>
                    <{/if}>
                </div>
            </div>
            <div class="footer">
                <div class="button-toolbar">
                    <div class="button-group">
                        <button class="button primary small" data-trigger="abort" data-dismiss="modal">
                            <{translate id="Ok" texdomain="Core"}>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
