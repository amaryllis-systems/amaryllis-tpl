<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<li data-username="<{$friend.username}>"
    data-accepted="<{$friend.status}>"
    data-accepted2="<{$friend.status2}>"
    data-circle="<{if isset($friend['all_circles'])}><{$friend['all_circles']}><{/if}>"
    data-uid="<{if $acmsData->user['id'] == $friend['friend_uid']}><{$acmsContents['profileuser']['id']}><{else}><{$friend['friend_uid']}><{/if}>">
    <div class="column xxsmall-12 medium-6 large-3 xlarge-3 xxlarge-2">
        <div class="push-15">
            <div class="card v1 profile-card">
                <div class="profile-avatar">
                    <img data-role="avatar" class="profile-avatar image circle" alt="<{$friend.username}>" src="<{$friend.avatar}>" title="<{$friend.username}>" />
                </div>
                <div class="profile-name">
                    <{if $acmsContents->get('isOwner')}>
                        <{assign username $friend.username}>
                    <{else}>
                        <{assign username $acmsContents['profileuser']['username']}>
                    <{/if}>
                    <a class="profile-friend-link" href="<{$friend.profile}>" data-role="userlink"
                       data-toggle='tooltip' title="<{translate id='Besuche %s\'s Profil' textdomain='Core' args=$username}>">
                        <span><{$friend.username}></span>
                    </a>
                    <div class="button-toolbar">
                        <{if $acmsContents->get('isOwner') || $acmsData->user['id'] == $friend['friend_uid']}>
                            <i aria-hidden="true" class="acms-icon acms-icon-trash-alt text-danger pointer"
                               data-uid="<{if $acmsContents->get('isOwner')}><{$friend['friend_uid']}><{elseif $acmsData->user['id'] == $friend['friend_uid']}><{$acmsContents['profileuser']['id']}><{/if}>"
                               data-toggle='tooltip' data-role="remove-friendship"
                               data-title="<{translate id='Beende die Freundschaft mit %s' textdomain='Core' args=$username}>"></i>&nbsp;
                        <{/if}>
                        <span class="label v1 success"><{$acmsData->user['id']}></span>
                        <{if $friend['status2'] == 2
                                && $acmsData->user['id'] == $friend['friend2']}>
                                <button type="button" 
                                        class="button tiny success" 
                                        title="<{translate id='Akzeptiere die Freundschafts-Anfrage von %s' textdomain='Core' args=$username}>"
                                        data-remote ="<{$acmsContents->actionUrl}>"
                                        data-friend="<{if $acmsContents->get('isOwner')}><{$friend['friend1']}><{elseif $acmsData->user['id'] == $friend['friend1']}><{$acmsContents['profileuser']['id']}><{/if}>"
                                        data-module="view/profile/friendship/friendship-accept">
                                    <i aria-hidden="true" class="acms-icon acms-icon-ok text-success pointer"
                                       data-uid="<{if $acmsContents->get('isOwner')}><{$friend['friend_uid']}><{elseif $acmsData->user['id'] == $friend['friend_uid']}><{$acmsContents['profileuser'].id}><{/if}>"
                                       data-toggle='tooltip' data-role="accept-friendship"
                                    ></i>&nbsp;
                                </button>
                        <{/if}>
                        <{if $acmsContents->get('isOwner')}>
                            <button type="button" class="send-friend-pm button primary tiny" data-toggle='tooltip' data-title="<{translate id='Sende eine Nachricht' textdomain='Core'}>"
                                    data-tousername="<{$friend.username}>" data-fromusername="<{$acmsContents['profileuser']['username']}>">
                                <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i>
                            </button>
                        <{/if}>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>
