<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="column xxsmall-12 medium-6 medium-offset-6 large-3 large-offset-9">
    <button type="button" class="button default" data-role="show-all">
        <{translate id="Zeige alle" textdomain="Core"}>
    </button>
</div>
<div class="single-circle-title column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
    <h3 data-role="title">&nbsp;</h3>
</div>
<div class="single-circle column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
    <ul id="single-circle">&nbsp;</ul>
</div>
