<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="row xxsmall-push-updown-30">
    <div class="column xxsmall-12 medium-6 medium-offset-6">

    </div>
</div>
<div class="row xxsmall-push-updown-30">
    <div class="column xxsmall-12" id="show-all-circles">
        <div class="display-block center-block percentage-70">
            <div id="profile-friend-tabcontainer" 
                 class="tab-container" 
                 data-module="ui/navs/tabs" 
                 data-tab=".item" 
                 data-panes=".tab-panel"
                 data-panes-container=".content-blocks"
                 data-effect="fadeIn"
                 data-outeffect=false>
                <div id="profile-friend-tabs" class="labeled icon nav v1 push-0">
                    <a class="active item"
                       href="#friend-circles"
                       data-target="#friend-circles"
                       role="tab"
                       title="<{translate id='Meine Personen-Kreise' textdomain='Core'}>"
                       >
                        <i aria-hidden="true" class="acms-icon acms-icon-users"></i>
                        <{translate id='Personen-Kreise' textdomain='Core'}>
                    </a>
                    <a class="item"
                       href="#personen-finden"
                       data-target="#personen-finden"
                       role="tab"
                       title="<{translate id='Personen' textdomain='Core'}>"
                       >
                        <i aria-hidden="true" class="acms-icon acms-icon-search-plus"></i>
                        <{translate id='Weitere Personen finden' textdomain='Core'}>
                    </a>
                </div>
                <div class="content-blocks v5 push-0">
                    <div class="content-block v5 active tab-panel" id="friend-circles">
                        <div class="push-5" data-module="apps/profile/friendship/circle-box" data-remote="<{generateUrl name="core_profile_circle_action" parameters=['username' => $acmsContents->profileuser['username']|lower, 'action' => 'action']}>">
                            <div class="accordion v1" data-module="apps/ui/navs/accordion">
                                <{if $acmsContents->get('circles')}>
                                    <{foreach $acmsContents['circles'] as $circle}>
                                        <section class="accordion-section"
                                                 data-role="circle"
                                                 data-circle="<{$circle['title']}>"
                                                 data-id="<{$circle['id']}>">
                                            <div class="heading" data-toggle="accordion">
                                                <span class="accordion-dropdown dark-text opacity-50">
                                                    <i aria-hidden="true" class="acms-icon acms-icon-caret-right"></i>
                                                </span>
                                                <div class="title">
                                                    <{$circle['title']}>
                                                </div>
                                                <span class="badge v1 default"><{$circle['totalmembers']}></span>
                                            </div>
                                            <div class="content">
                                                <ul class="circle-friends collection v1 bordered" id="circle-friends-<{$circle['id']}>">
                                                </ul>
                                            </div>
                                        </section>
                                    <{/foreach}>
                                    <section class="accordion-section"
                                             data-role="circle"
                                             data-circle="<{translate id="Kein Kreis"}>"
                                             data-id="0">
                                        <div class="heading" data-toggle="accordion">
                                            <span class="accordion-dropdown dark-text opacity-50">
                                                <i aria-hidden="true" class="acms-icon acms-icon-caret-right"></i>
                                            </span>
                                            <div class="title">
                                                <{translate id="Ohne Zuordnung"}>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <ul class="circle-friends collection v1 bordered" id="circle-friends-0">
                                            </ul>
                                        </div>
                                    </section>
                                <{/if}>
                            </div>
                        </div>
                        <{if !$acmsContents->get('circles')}>
                            <div class="push-5">
                                <div class="content-block v6 wide ">

                                    <h3 class="">
                                        <i aria-hidden="true" class="acms-icon acms-icon-hand-point-right regular text-muted"></i>
                                        <{translate id="Personen-Kreise"}>
                                    </h3>
                                    <p>
                                        Sie haben bisher noch keine Personen-Kreise erstellt. 
                                        Personen-Kreise helfen Ihnen, Ihre Kontakte zu 
                                        strukturieren und Inhalte für diese Gruppen 
                                        freizuschalten. So behalten Sie den Überblick über freigegebene Inhalte.</p>

                                    <div class="button-toolbar compact clearfix">
                                        <button class="button right small primary"
                                                type="button"
                                                data-module="apps/ui/informations/modal"
                                                data-target="#addcircle-content">
                                            <i aria-hidden="true" class="acms-icon acms-icon-plus-circle"></i>
                                            <span><{translate id="Kreis erstellen" textdomain="Core"}></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <{/if}>
                        <div class="content-blocks v5" data-module="apps/profile/friendship/friend-status" data-userid="<{$acmsContents->profileuser['id']}>" data-remote="<{$acmsContents->actionUrl}>">
                            <div class="content-block v5 pending-requests" data-owner="<{$acmsContents->profileuser['id']}>">
                                <{* pending requests *}>
                                <{countProfileFriendRequests assign="totalPending" uid=$acmsContents->profileuser['id']}>
                                <div class="page-title v7<{if $totalPending == 0}> opacity-50<{/if}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-user-friends"></i>
                                    <div class="content">
                                        <div class="title">Freundschafts-Anfragen</div>
                                        <div class="sub-title">
                                            <{translate id="Es gibt %s abwartende Anfragen von Mitgliedern" args=$totalPending}>
                                        </div>
                                    </div>
                                </div>
                                <div class="cards">
                                    <{if $totalPending > 0}>
                                        <{getProfileFriendRequests assign="pending" uid=$acmsContents->profileuser['id']}>
                                        <{foreach $pending as $fship}>
                                            <div class="card v2">
                                                <div class="card-content">
                                                    <div class="right floated medium avatar">
                                                        <img class="image" src="<{$fship['avatar']}>" alt="avatar" title="<{$fship['username']}>">
                                                    </div>
                                                    <div class="card-title">
                                                        <a class="header" href="<{$fship['data']['itemUrl']}>" title="<{$fship['username']}>">
                                                            <{$fship['username']}>
                                                        </a>
                                                    </div>
                                                    <div class="card-meta">
                                                        <{translate id="Mitglied seit %s" args=$fship['data']['registration_date-simple']}>
                                                    </div>
                                                    <div class="description">
                                                        <{translate id="%s möchte dich in Personen aufnehmen." textdomain="Core" args=$fship['username']}>
                                                    </div>
                                                </div>
                                                <div class="extra card-content">
                                                    <div class="two buttons">
                                                        <button type="button" 
                                                                class="small success icon button" 
                                                                aria-label="<{translate id='Akzeptiere die Freundschafts-Anfrage von %s' textdomain='Core' args=$fship['username']}>"
                                                                data-friend="<{if $acmsContents->get('isOwner')}><{$fship['friend_uid']}><{elseif $acmsData->user['id'] == $fship['friend1']}><{$acmsContents['profileuser']['id']}><{/if}>"
                                                                data-toggle="accept">
                                                            <i class="acms-icon acms-icon-check"></i>
                                                            <span class="content">Annehmen</span>
                                                        </button>
                                                        <button type="button" 
                                                                class="small danger broder-button icon button" 
                                                                aria-label="<{translate id='Lehne die Anfrage von %s ab' textdomain='Core' args=$fship['username']}>"
                                                                data-friend="<{if $acmsContents->get('isOwner')}><{$fship['friend_uid']}><{elseif $acmsData->user['id'] == $fship['friend1']}><{$acmsContents['profileuser']['id']}><{/if}>"
                                                                data-toggle="reject">
                                                            <i class="acms-icon acms-icon-ban"></i>
                                                            <span class="content">Ablehnen</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        <{/foreach}>
                                    <{/if}>
                                </div>
                            </div>
                            <{block name="myPendingFriendshipRequests"}>
                            <div class="content-block v5 my-pending-requests">
                                <{* my pending requests *}>
                                <{countMyProfileFriendRequests assign="myTotalPending" uid=$acmsContents->profileuser['id']}>
                                <div class="page-title v7<{if $myTotalPending == 0}> opacity-50<{/if}>">
                                    <i aria-hidden="true" class="acms-icon acms-icon-user-clock"></i>
                                    <div class="content">
                                        <div class="title">Deine Freundschafts-Anfragen</div>
                                        <div class="sub-title">
                                            
                                            <{translate id="Du hast derzeit %s abwartende Anfragen" textdomain="Core" args=$myTotalPending}>
                                        </div>
                                    </div>
                                </div>
                                <div class="cards">
                                    <{if $myTotalPending > 0}>
                                        <{getMyProfilePendingRequests assign="myPending" uid=$acmsContents->profileuser['id']}>
                                        <{foreach $myPending as $fship}>
                                            <div class="card v2">
                                                <div class="card-content">
                                                    <div class="right floated medium avatar">
                                                        <img class="image" src="<{$fship['avatar']}>" alt="avatar" title="<{$fship['username']}>">
                                                    </div>
                                                    <a class="card-title" href="<{$fship['data']['itemUrl']}>" title="<{$fship['username']}>">
                                                        <{$fship['username']}>
                                                    </a>
                                                    <div class="card-meta">
                                                        <{translate id="Mitglied seit %s" args=$fship['data']['registration_date-simple']}>
                                                    </div>
                                                    <div class="description">
                                                        <{translate id="Du möchtest %s in Personen aufnehmen, aber %s hat noch nicht reagiert." textdomain="Core" args=[$fship['username'], $fship['username']]}>
                                                    </div>
                                                </div>
                                                <button type="button" 
                                                        class="small default bottom plugged danger-hover icon button" 
                                                        aria-label="<{translate id='Lösche die Freundschafts-Anfrage an %s' textdomain='Core' args=$fship['username']}>"
                                                        data-friend="<{if $acmsContents->get('isOwner')}><{$fship['friend_uid']}><{elseif $acmsData->user['id'] == $fship['friend1']}><{$acmsContents['profileuser']['id']}><{/if}>"
                                                        data-toggle="delete">
                                                    <i class="acms-icon acms-icon-trash"></i>
                                                    <span class="content">Löschen</span>
                                                </button>
                                            </div>
                                        <{/foreach}>
                                    <{/if}>
                                </div>
                            </div>
                            <{/block}>
                        </div>
                    </div>

                    <div class="content-block v5 tab-panel" id="personen-finden" aria-hidden="true">
                        <{include file="acmsfile:Core|profile/friends/FriendSearchNew.tpl"}>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
