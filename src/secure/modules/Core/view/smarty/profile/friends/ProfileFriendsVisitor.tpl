<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section class="pull-updown-35 push-updown-35 push-leftright-35">        
    <div class="cards" data-cssfile="media/css/ui/cards">
        <{if $acmsContents->friends}>
            <{foreach $acmsContents->friends as $friend}>
                <div class="card v2">
                    <a class="card-image bg-transparent" href="<{$friend['profile']}>" title="<{$friend['username']}>">
                        <img src="<{$friend['avatar']}>" alt="" title="<{$friend['username']}>" />
                    </a>
                    <div class="card-content">
                        <h3 class="card-title">
                            <a class="display-block" href="<{$friend['profile']}>" title="<{$friend['username']}>">
                                <{$friend['username']}>
                            </a>
                        </h3>
                        <div class="card-meta">
                            <span class="date">Mitglied seit <{$friend['data']['registration_date-year']}></span>
                        </div>
                    </div>
                    <div class="extra card-content">
                        <span>
                            <i class="acms-icon acms-icon-users"></i>
                            <{$friend['data']['friends']}> Freunde
                        </span>
                    </div>
                </div>
            <{/foreach}>
        <{/if}>
    </div>
</section>
