<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class='form-wrapper' data-module="apps/profile/friendship/people-search" data-owner="<{$acmsContents->profileuser['id']}>" data-remote="<{generateUrl name="core_profile_friendship_action" parameters=['username' => $acmsContents->profileuser['username'], 'action' => 'action']}>">
    <form class='form' role="form" action="#" data-role="people-quicksearch-form" method='post'>
        <div class='form-group'>
            <label class='src-only'><{translate id='Neue Personen suchen' textdomain='Core'}></label>
            <div class="field-group">
                <i class="field-icon acms-icon acms-icon-search bg-theme-text"></i>
                <input type='search'
                       role="search"
                       class="field"
                       data-role="people-search"
                       placeholder="<{translate id='Name, Vorname, Benutzername,...'}>" />
            </div>
        </div>
    </form>
    <div class="row full xxsmall-push-updown-30">
        <div class="column xxsmall-12">
            <div id="people-search-results" class="cards">
                
            </div>
        </div>
    </div>
</div>
