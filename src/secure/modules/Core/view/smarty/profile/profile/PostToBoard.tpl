<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{getProfilePostToBoard assign=boardForms}>

<div id="profile-post-to-board" class="no-border no-shadow" data-module="apps/profile/board/post-to-board">
    <div class="tab-container no-border no-shadow content-block v5" data-module="ui/navs/tabs"  data-history="false" data-effect="fadeIn" data-outeffect="fadeOut" data-tab=".item" data-panes-container=".content-block">
        <!-- Nav tabs -->
        <div class="top plugged primary labeled icon nav v1" role="tablist">
            <div class="header heading pull-updown-30">
                <h3 class="text-muted text-uppercase font-serif pull-leftright-20 title text-small push-0">
                    <{translate id="Am Board teilen" textdomain="Core"}>
                </h3>
            </div>
            <{assign var=first value=1}>
            <{foreach $boardForms as $form}>
                <{if $form['enabled']}>
                    <a class="<{if $form@iteration == 1}>active <{/if}>item"
                       href="#profile-post-<{$form['name']}>"
                       role="tab"
                       data-target="#profile-post-<{$form['name']}>">
                        <i class="<{$form['icon']}> acms-icon-2x"></i>
                        <{$form['title']}>
                    </a>
                    <{assign var=first value=0}>
                <{/if}>
            <{/foreach}>
        </div>
        <!-- Tab panes -->
        <div class="bottom plugged content-block v5">
            <{assign var=first value=1}>
            <{foreach $boardForms as $form}>
                <{if $form['enabled']}>
                    <div id="profile-post-<{$form['name']}>" 
                        class="no-border top plugged content-block v5 tab-panel<{if $first == 1}> active<{/if}>">
                        <div class="post-to-board-content">
                        <form role="form" 
                              data-remote="<{$form['form']->get('remote')}>"
                              data-board="<{$form['form']->get('board')}>"
                              >
                            <div class="post-to-board-fields">
                            <{foreach $form['form']->fields as $field}>
                                <{include file=$field['template'] field=$field nocache}>
                            <{/foreach}>
                            </div>
                            <div class="footer">
                                <button type="submit" role="button" name="submit-1" data-caption="Teilen" data-trigger="share" class="primary fading animation icon-button button">
                                    <i aria-hidden="true" class="acms-icon acms-icon-share"></i>
                                    <span class="first content">teilen</span>
                                    <span class="second content">Go!</span>
                                </button>
                            </div>
                        </form>
                        </div>
                    </div>
                    <{assign var=first value=0}>
                <{/if}>
            <{/foreach}>
        </div>
    </div>
</div>
