<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{foreach $values as $type => $value}>
    <div class="push-10">
        <h5>
            <{$value['label']}>
        </h5>
        <div class="tag-bar" data-role="profile-weblinks" data-type="<{$value['type']}>">
            <{foreach $value['links'] as $link}>
                <div class="profile-weblinks-link tag"
                     data-id="<{$link['id']}>">
                    <{if $link['favicon']}>
                        <img class="tag-image" src="<{$link['favicon']}>" alt="" title="<{if $link['title']}><{$link['title']}><{else}><{$link['url']}><{/if}>" />
                    <{/if}>
                    <a href="<{$link['url']}>" title="<{if $link['title']}><{$link['title']}><{else}><{$link['url']}><{/if}>">
                        <span><{if $link['title']}><{$link['title']}><{else}><{$link['url']}><{/if}></span>
                    </a>
                </div>
            <{/foreach}>
        </div>
    </div>
<{/foreach}>
