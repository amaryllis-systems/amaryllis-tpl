<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="profile-info-modal" class="modal large" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="profile-info-modal-title">
    <div class="modal-dialog">
        <div class="content">
            <div class="heading">
                <button class="close" type="button" data-dismiss="modal">
                    <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                </button>
                <h4 class="title" id="profile-info-modal-title">
                    <{$acmsContents['displayname']}> 
                    <small><{translate id="Profil-Informationen" textdomain="Core"}></small>
                </h4>
            </div>
            <div class="main text-left">
                <div class="profile-info-box feed-collection v1 one-small-up-only two-medium-up-only" data-cssfile="media/css/components/cards.min.css">
                    <{foreach $acmsContents->profileuser['profile']->getSorted() as $category}>
                        <{if !$acmsContents->isAdmin && $category['id'] == 1}>
                            <{continue}>
                        <{/if}>
                        <div class="profile-modal-category card" role="region" aria-label="<{$category['title']}>">
                            <div class="card-title">
                                <{if $category['fonticon']}>
                                    <i aria-hidden="true" class="acms-icon <{$category['fonticon']}>"></i>
                                <{/if}>
                                <span><{$category['title']}></span>
                            </div>
                            <div class="card-content">
                                <ul class="list-unstyled">
                                    <{foreach $category['fields'] as $field}>
                                        <li class="<{$field['name']}>">
                                            <b><{$field['title']}></b><br>
                                            <{if is_array($field['value']) && isset($field['value']['template'])}>
                                                
                                                <{include file=$field['value']['template'] values=$field['value']['values']}>
                                            <{elseif is_array($field['value'])}>
                                                <ul class="list-unstyled">
                                                    <{foreach $field['value'] as $value}>
                                                        <li><{$value}></li>
                                                    <{/foreach}>
                                                </ul>
                                            <{else}>
                                                <{$field['value']}>
                                            <{/if}>
                                        </li>
                                    <{/foreach}>
                                </ul>
                            </div>
                        </div>
                    <{/foreach}>
                </div>
            </div>
            <div class="footer">
                <button class="button primary small" data-dismiss="modal">
                    <{translate id="Schließen" textdomain="Core"}>
                </button>
            </div>
        </div>
    </div>
</div>
