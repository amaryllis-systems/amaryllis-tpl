<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" data-profileview="avatar" itemtype="http://schema.org/ProfilePage" data-module="apps/profile/avatar/avatar" data-remote="<{$acmsContents->remote}>" data-max="<{$acmsContents['avatar_maxfiles']}>">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-board profile-startpage">
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl" nocache}>
        
        <div class="white bottom arrow-box v1">
            <div class="row full pull-updown-35">
                <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
                    <div class="display-block push-15 pull-35">
                        <{if $acmsContents->total > 0}>
                            <div class="middle aligned boxed list" data-role="avatar-list">
                                <{foreach $acmsContents->avatars as $avatar}>
                                    <div class="item" data-role="avatar" data-id="<{$avatar['id']}>">
                                        <div class="right floated content">
                                            <div class="pull-down-5 right aligned">
                                                <span data-toggle="activate" 
                                                      data-active="<{$avatar['active']}>" 
                                                      class="text-xxlarge push-0 transparent white no-border icon button cursor-pointer <{if $avatar['active']}>text-success<{else}>text-muted text-success-hover<{/if}>">
                                                    <i aria-hidden="true" class="acms-icon acms-icon-check-circle"></i>
                                                </span>
                                                <span data-trigger="delete" class="text-xxlarge push-0 transparent white no-border icon button cursor-pointer text-muted text-danger-hover">
                                                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <img class="xsmall avatar image" src="<{$avatar['raw']}>" alt="" title="<{$avatar['pdate-simple']}>">
                                        <div class="content">
                                            <div class="header">
                                                <{translate id="hochgeladen am:"}> <{$avatar['pdate-simple']}>
                                            </div>
                                        </div>
                                    </div>
                                <{/foreach}>
                            </div>
                        <{else}>
                            <div class="large center aligned content-block v5 pull-updown-35">
                                <div class="large center aligned icon page-title v7 pull-updown-20">
                                    <i class="text-default circular acms-icon acms-icon-user-circle"></i>
                                    <div class="content">
                                        <h3 class="title"><{translate id="Kein Avatar gefunden" textdomain="Core"}></h3>
                                        <h4 class="sub-title"><{translate id="Sie haben aktuell keinen Avatar hochgeladen." textdomain="Core"}></h4>
                                    </div>
                                </div>
                                <{if $acmsContents->canChange}>
                                    <button type="button" class="primary rounded-light icon button push-updown-0" data-toggle="modal" data-target="#profile-avatar-dashboard-add-avatar" data-module="apps/ui/informations/modal">
                                        <i aria-hidden="true" class="acms-icon acms-icon-upload"></i>
                                        <span><{translate id="Neuer Avatar" textdomain="Core"}></span>
                                    </button>
                                <{/if}>
                            </div>
                        <{/if}>
                    </div>
                </div>
                <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
                    <div class="display-block push-15 pull-35">
                        <div class="middle aligned boxed list">
                            <div class="item" data-role="iavatar">
                                <div class="right floated content">
                                    <div class="pull-down-5 right aligned">
                                        <span class="text-xxlarge push-0 transparent white no-border icon button cursor-default" data-role="default">
                                            <i aria-hidden="true" class="acms-icon acms-icon-check"></i>
                                        </span>
                                    </div>
                                </div>
                                <img class="xsmall avatar image" src="<{$acmsContents['profileuser']['iavatar']}>" alt="" title="Standard">
                                <div class="content">
                                    <div class="header">
                                        <{translate id="Standard Avatar"}>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white page-section v1">
            <div class="full row pull-updown-35">
                <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3">
                    <div class="form-wrapper">
                        <form role="form" class="form" action="#" onsubmit="return false;">
                            <div class="form-group">
                                <div class="file-input v2">
<{capture name="acceptStringCapture" assign="acceptString" nocache}>
<{foreach $acmsContents->mimetypeRestrictions['mimetypes'] as $mime}>
.<{$mime@key}><{if ! $mime@last}>,<{/if}>
<{/foreach}>
<{/capture}>
                                    <input type="file" name="avatar" id="dashboard-avatar-file-upload" data-role="avatar-upload" accept="<{$acceptString}>">
                                    <label for="dashboard-avatar-file-upload">
                                        <i aria-hidden="true" class="acms-icon acms-icon-upload"></i>
                                        <span>upload</span>
                                    </label>
                                </div>
                            </div>
                        </form><!-- /form -->
                    </div>
                    <div class="content-blocks v5">
                        <div class="content-block v5">
                            <div class="small page-title v7">
                                <h4 class="title"><{translate id="Die folgenden Datei-Typen werden akzeptiert:" textdomain="Core"}></h4>
                            </div>
                            <div class="horizontal list">
                                <{foreach $acmsContents->mimetypeRestrictions['mimetypes'] as $ext => $mime}>
                                    <div class="item">
                                        <span class="small label v2"><{$ext}> (<{$mime}>)</span>
                                    </div>
                                <{/foreach}>
                            </div>
                        </div>
                        <div class="content-block v5">
                            <div class="small page-title v7">
                                <h4 class="title"><{translate id="Bild-Dimensionen" textdomain="Core"}></h4>
                            </div>
                            <div class="middle aligned list">
                                <{if $acmsContents->mimetypeRestrictions['minHeight'] > 0}>
                                    <div class="item">
                                        <{translate id="Mindest-Höhe"}>: <span class="small label v2"><{$acmsContents->mimetypeRestrictions['minHeight']}>px</span>
                                    </div>
                                <{/if}>
                                <{if $acmsContents->mimetypeRestrictions['maxHeight'] > 0}>
                                    <div class="item">
                                        <{translate id="Maximal-Höhe"}>: <span class="small label v2"><{$acmsContents->mimetypeRestrictions['maxHeight']}>px</span>
                                    </div>
                                <{/if}>
                                <{if $acmsContents->mimetypeRestrictions['minWidth'] > 0}>
                                    <div class="item">
                                        <{translate id="Mindest-Breite"}>: <span class="small label v2"><{$acmsContents->mimetypeRestrictions['minWidth']}>px</span>
                                    </div>
                                <{/if}>
                                <{if $acmsContents->mimetypeRestrictions['maxWidth'] > 0}>
                                    <div class="item">
                                        <{translate id="Maximal-Breite"}>: <span class="small label v2"><{$acmsContents->mimetypeRestrictions['maxWidth']}>px</span>
                                    </div>
                                <{/if}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
