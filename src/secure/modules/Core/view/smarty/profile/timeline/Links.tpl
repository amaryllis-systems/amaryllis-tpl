<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="timeline-badge">
    <i aria-hidden="true" class="acms-icon acms-icon-link"></i>
</div>
<div class="timeline-panel">
    <div class="timeline-heading">
        <h4 class="timeline-title">
            <{if $entry['title']}>
                <{$entry['title']}>
            <{/if}>
            <small>
                <{foreach $circles as $circle}>
                    <span class="tag">
                        <{$circle['label']}>
                    </span>
                <{/foreach}>
            </small>
        </h4>
        <p>
            <small class="text-muted">
                <i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$entry['pdate-relative']}>
            </small>
        </p>
    </div>
    <div class="timeline-body">
        <a href="<{$url}>" title="<{if $entry['title']}><{$entry['title']}><{/if}>" target="_blank">
            <{if $entry['favicon']}>
                <img src="<{$entry['favicon']}>" title="Website-Icon" alt="" />
            <{/if}>
            <{if $entry['title']}><{$entry['title']}><{else}><{$url}><{/if}>
        </a>
        <{if $entry['image']}>
            <div class="image-container">
                <img src="<{$entry['image']}>" alt="<{if $entry['title']}><{$entry['title']}><{/if}>" title="<{if $entry['title']}><{$entry['title']}><{/if}>" />
            </div>
        <{/if}>
    </div>
</div>
