<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="window v1 window-explorer in-site-window active open size-500"
     id="profile-explorer"
     data-module="apps/profile/media-manager/media-manager" 
     data-remote="<{$acmsContents->actionUrl}>"
     style="position: relative" 
     data-owner="<{if $acmsContents->profileuser && 'profile' == $acmsContents->view}><{$acmsContents->profileuser['id']}><{else}><{$acmsData->user['id']}><{/if}>"
     data-maximizable=true
     data-resizable=false
     data-dragable=false
     data-top="3.125rem"
     data-token="<{$acmsContents->token}>">
    <div class="title-bar">
        <div class="window-controls controls-left">
            <a href="#" onclick="return false;" class="window-icon">
                <i aria-hidden="true" class="acms-icon acms-icon-folder-open"></i>
            </a>
            <a href="#" onclick="return false;" class="window-menutoggle">
                <i aria-hidden="true" class="acms-icon acms-icon-bars"></i>
            </a>
        </div>
        <span class="window-title"><{translate id="Medien Manager"}></span>
        <div class="window-controls controls-right">
            <a class="window-maximize" href="#" onclick="return false;">
                <i class="acms-icon acms-icon-window-maximize"></i>
            </a>
            <a class="window-fullsize" href="#" onclick="return false;" data-module="apps/ui/helpers/full-screen" data-target="#profile-explorer">
                <i class="acms-icon acms-icon-expand"></i>
            </a>
        </div>
    </div>
    <ul class="window-menu" aria-hidden="true">
        
        <li>
            <span data-command="add-folder">
                <i class="acms-icon acms-icon-plus-circle menu-icon"></i> 
                <span><{translate id="Verzeichnis zufügen"}></span>
            </span>
        </li>
        <li>
            <span class="disabled" disabled="disabled" data-action="selection" data-command="rename">
                <i class="acms-icon acms-icon-pencil-alt menu-icon"></i> 
                <span><{translate id="umbenennen"}></span>
            </span>
        </li>
        <li>
            <span class="disabled" disabled="disabled" data-action="multi-selection" data-command="delete">
                <i class="acms-icon acms-icon-trash menu-icon"></i> 
                <span><{translate id="löschen"}></span>
            </span>
        </li>
        <li class="divider">
        <li>
            <span class="disabled" disabled="disabled" data-action="folder">
                <i class="acms-icon acms-icon-share-alt menu-icon"></i> 
                <span>Datei freigeben</span>
            </span>
        </li>
        <li class="divider">
        </li>
        <li>
            <a href="#" onclick="return false;" class="disabled" disabled="disabled">
                <i class="acms-icon acms-icon-cogs menu-icon"></i> 
                <span>Einstellungen</span>
            </a>
        </li>
    </ul>
    <div class="window-actions">
        <a class="window-back" href="#" onclick="return false;">
            <i aria-hidden="true" class="acms-icon acms-icon-chevron-left"></i>
        </a>
        <a class="window-forward disabled" href="#" onclick="return false;">
            <i aria-hidden="true" class="acms-icon acms-icon-chevron-right"></i>
        </a>
        <span class="window-action" 
              aria-label="Neues Verzeichnis"
              data-command="add-folder">
            <i class="acms-icon acms-icon-stack">
                <i aria-hidden="true" class="acms-icon acms-icon-folder acms-icon-stack-2x"></i>
                <i aria-hidden="true" class="acms-icon acms-icon-plus acms-icon-stack-1x text-dark text-bold"></i>
            </i>
        </span>
        <label class="window-action" aria-label="Neue Datei hochladen" data-command="upload" data-action="folder" disabled="disabled">
            <i aria-hidden="true" class="acms-icon acms-icon-upload"></i>
            <input class="src-only" type="file" data-action="folder" disabled="disabled">
        </label>
        <div class="window-path">
            <a href="" data-role="breadcrumb" data-index="0">
                /
            </a>
        </div>
        <div class="dropdown" data-module="ui/navs/dropdown">
            <a class="dropdown-toggle" href="#" onclick="return false;">
                <i aria-hidden="true" class="acms-icon acms-icon-ellipsis-v"></i> 
                <span class="src-only">Optionen</span>
            </a>
            <div class="dropdown-content window-sort">
                <ul class="dropdown-menu window-sort-menu">
                    <li>
                        <input type="radio" value="title-asc" name="profile-media-manager-sorter" checked="checked">
                        <label data-order="title-asc" for="profile-media-manager-sorter-title-asc">
                            <i class="acms-icon acms-icon-sort-alpha-up"></i>
                            <span>Sortiere nach Name aufsteigend</span>
                        </label>
                    </li>
                    <li>
                        <input type="radio" value="title-desc" name="profile-media-manager-sorter">
                        <label data-order="title-desc" for="profile-media-manager-sorter-title-desc">
                            <i class="acms-icon acms-icon-sort-alpha-down"></i>
                            <span>Sortiere nach Name absteigend</span>
                        </label>
                    </li>
                    <li>
                        <input type="radio" value="pdate-asc" name="profile-media-manager-sorter">
                        <label data-order="pdate-asc" for="profile-media-manager-sorter-pdate-asc">
                            <i class="acms-icon acms-icon-sort-up"></i>
                            <span>Sortiere nach Datum aufsteigend</span>
                        </label>
                    </li>
                    <li>
                        <input type="radio" value="pdate-desc" name="profile-media-manager-sorter">
                        <label data-order="pdate-desc" for="profile-media-manager-sorter-pdate-desc">
                            <i class="acms-icon acms-icon-sort-down"></i>
                            <span>Sortiere nach Datum absteigend</span>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
        <form class="search">
            <input type="search" class="search-input" placeholder="In den Dateien suchen...">
            <button class="search-button">
                <i class="acms-icon acms-icon-search"></i>
            </button>
        </form>

    </div>
    <div class="window-body">
        <div class="window-side">
            <ul class="side-list">
                <li class="profile-files open">
                    <a data-role="sidemenu-toggle">
                        <i class="list-toggle"></i>
                        Eigene Dateien
                    </a>
                    <ul class="my-files file-tree v1">
                    </ul>
                </li>
                <li class="profile-shared-files">
                    <a data-role="sidemenu-toggle">
                        <i class="list-toggle"></i>
                        Für mich freigegeben
                    </a>
                    <ul class="shared-files file-tree v1">
                    </ul>
                </li>
            </ul>
        </div>
        <div class="window-main">
            <div class="inner">
                <ul class="file-list v1 grid-view media-manager-files">

                </ul>
            </div>
            
            <div class="window-overlay">
                <div class="overlay-inner">
                    <button class="close" data-dismiss="window-overlay">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                    </button>
                    <div class="overlay-content">
                    </div>
                    <div class="overlay-footer">
                        <button class="button small primary" data-dismiss="window-overlay">
                            <span>Schließen</span>
                        </button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
