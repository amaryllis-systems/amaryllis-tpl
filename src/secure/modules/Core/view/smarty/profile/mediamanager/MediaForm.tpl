<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal large" 
     id="media-upload-action-modal" 
     tabindex="-1" 
     role="dialog" 
     aria-labelledby="media-upload-action-modal-title" 
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form name="add-media-form" method="post">
                <div class="header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only">
                            <{translate id="Schließen" textdomain="Core"}>
                        </span>
                    </button>
                    <h4 class="title" id="media-upload-action-modal-title">
                        Hochladen
                    </h4>
                </div>
                <div class="main text-left">
                    <div class="form-group">
                        <label for="file">Dateien</label>
                        <div class="field-group">
                            <div class="file-input v2">
                                <input name="file" multiple="multiple" 
                                       class="multiple field form-field" 
                                       value="" 
                                       id="add-media-form-0-file" 
                                       data-minwidth="0" 
                                       data-maxwidth="2048" 
                                       data-minheight="0" 
                                       data-maxheight="2048" 
                                       data-minfilesize="0" 
                                       data-maxfiles="20"  type="file">
                                <label for="add-media-form-0-file">
                                    Datei auswählen
                                </label>
                            </div>
                        </div>
                        <div id="files-to-upload" class="files-to-upload">
                        </div>
                    </div>
                    <input type="hidden" name="cid" value="0" />
                    <input type="hidden" name="op" value="upload" />
                </div>
                <div class="footer">
                    <button type="reset" 
                            class="button small default" 
                            data-trigger="abort" 
                            data-dismiss="modal">
                        <{translate id="Abbrechen" textdomain="Core"}>
                    </button>
                    <button type="submit" 
                            class="button small primary" 
                            data-trigger="save">
                        <{translate id="Hochladen" textdomain="Core"}>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
