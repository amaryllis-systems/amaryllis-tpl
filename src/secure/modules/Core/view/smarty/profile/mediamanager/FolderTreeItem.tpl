<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<li class="folder" id="folder-tree-item-<{$folder['id']}>"
    data-root="<{$folder['root']}>"
    data-id="<{$folder['id']}>"
    data-parent="<{$folder['parentid']}>"
    data-foldername="<{$folder['foldername']}>"
    data-role="folder">
    <label for="folder-tree-<{$folder['id']}>">
        <{$folder['title']}>
    </label>
    <input type="checkbox" id="folder-tree-<{$folder['id']}>" value="<{$folder['id']}>" /> 
    <{if $folder['subs']}>
        <ol>
            <{foreach $folder['subs'] as $f}>
                <{include file="acmsfile:Core|profile/mediamanager/FolderTreeItem.tpl" folder=$f nocache}>
            <{/foreach}>
        </ol>
    <{/if}>
</li>
