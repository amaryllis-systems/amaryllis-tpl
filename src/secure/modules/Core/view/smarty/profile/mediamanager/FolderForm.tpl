<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal" id="folder-add-action-modal" tabindex="-1" role="dialog" aria-labelledby="folder-add-action-modal-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="content">
            <form name="add-folder-form" method="post" action="#">
                <div class="header">
                    <button type="button" class="close" data-dismiss="modal">
                        <i aria-hidden="true" class="acms-icon acms-icon-times"></i>
                        <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span></button>
                    <h4 class="title" id="folder-add-action-modal-title">Ordner erstellen</h4>
                </div>
                <div class="main text-left">
                    <div class="form-group">
                        <label for="modal-add-foldername" class="field-label">
                            Ordner-Name
                        </label>
                        <input type="text" id="modal-add-foldername" name="foldername" class="field" />
                    </div>
                    <input type="hidden" name="parent" value="0" />
                    <input type="hidden" name="op" value="addfolder" />
                </div>
                <div class="footer">
                    <button type="submit" data-trigger="submit" class="button small primary"><{translate id="Erstellen" textdomain="Core"}></button>
                    <button type="reset" data-trigger="abort" class="button small default" data-dismiss="modal"><{translate id="Abbrechen" textdomain="Core"}></button>
                </div>
            </form>
        </div>
    </div>
</div>
