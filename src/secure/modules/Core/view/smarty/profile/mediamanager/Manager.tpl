<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="media-manager" class="media-manager" data-cssfile="media/css/ui/media-manager" data-module="apps/profile/media-manager/media-manager" data-action="<{$acmsContents->actionUrl}>">
    
    <div class="media-manager-inner">
        <div class="media-manager-toolbar">
            <div class="button-toolbar compact transparent border bottom">
                <button class="disabled small round white text-primary-hover icon-button button" 
                        data-command="upload" 
                        data-requirefolder="true" data-folder="0"
                        data-toggle="modal"
                        data-module="apps/ui/informations/modal"
                        data-target="#media-upload-action-modal"
                        data-animate-in="rollIn"
                        disabled="disabled"
                        data-animate-out="rollOut"
                        aria-label="<{translate id="hochladen"}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-cloud-upload-alt"></i>
                </button>
                <button class="disabled small round text-danger-hover white icon-button button"
                        data-command="delete"
                        data-toggle="modal"
                        data-module="apps/ui/informations/modal"
                        data-target="#folder-remove-action-modal"
                        data-animate-in="rollIn"
                        data-animate-out="rollOut"
                        disabled="disabled"
                        aria-label="<{translate id="löschen"}>"
                        >
                    <i aria-hidden="true" class="acms-icon acms-icon-trash"></i>
                </button>
                <button class="small text-success-hover white round icon-button button"
                        data-command="addfolder"
                        data-toggle="modal"
                        data-module="apps/ui/informations/modal"
                        data-target="#folder-add-action-modal"
                        data-animate-in="rollIn"
                        data-animate-out="rollOut"
                        aria-label="<{translate id="Verzeichnis erstellen"}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-plus"></i>
                </button>
                    
                <button class="right floated inverse icon-button button" data-trigger="action" data-command="sort">
                    <i aria-hidden="true" class="acms-icon acms-icon-sort-amount-down"></i>
                </button>
                <button class="right floated default icon-button button" data-trigger="action" data-command="changeview">
                    <i aria-hidden="true" class="acms-icon acms-icon-th-large"></i>
                </button>
                
                <div class="right floated">
                    <button class="disabled default split button" disabled="disabled">
                        <span>aktionen</span> 
                        <span class="splitted"></span>
                    </button>
                    <ul class="dropdown-box sidebar v1">
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="download">
                                <i aria-hidden="true" class="acms-icon acms-icon-download"></i> 
                                <span>herunterladen</span>
                            </a>
                        </li>
                        <li class="nav-separator"></li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="archive">
                                <i aria-hidden="true" class="acms-icon acms-icon-archive"></i> 
                                <span>archivieren</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="addarchive">
                                <i aria-hidden="true" class="acms-icon acms-icon-archive"></i> 
                                <span>zu ZIP hinzufüen</span>
                            </a>
                        </li>
                        <li class="nav-separator"></li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="copy">
                                <i aria-hidden="true" class="acms-icon acms-icon-clone"></i> 
                                <span>kopieren nach</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="move">
                                <i aria-hidden="true" class="acms-icon acms-icon-copy"></i> 
                                <span>verschieben nach</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="clone">
                                <i aria-hidden="true" class="acms-icon acms-icon-clone regular"></i> 
                                <span>hier klonen</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="rename">
                                <i aria-hidden="true" class="acms-icon acms-icon-pencil-alt "></i> 
                                <span>umbenennen</span>
                            </a>
                        </li>
                        <li class="nav-separator"></li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="property">
                                <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i> 
                                <span>Eigenschaften</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-box-link" href="" data-trigger="action" data-command="share">
                                <i aria-hidden="true" class="acms-icon acms-icon-share-alt rotate-270"></i> 
                                <span>Freigabe</span>
                            </a>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
        <div class="media-manager-main">
            <{*
            <div id="media-manager-overlay">
            <div class="multi-spinner v1">
            <div class="circle-1"></div>
            <div class="circle-2"></div>
            </div>
            </div>
            *}>
            <div class="row full">
                <div class="column xxsmall-12 medium-3">
                    <div class="filetree-wrapper">
                        <ol class="file-tree v1">
                            <li id="folder-tree-item-0" class="folder open" data-root="0" data-parent="0" data-id="0">
                                <label for="folder-tree-0">/</label> <input type="checkbox" checked="" value="0" id="folder-tree-0" /> 
                                <ol>
                                    <{if $acmsContents->folders}>
                                        <{foreach $acmsContents->folders as $folder}>
                                            <{include file="acmsfile:Core|profile/mediamanager/FolderTreeItem.tpl" folder=$folder nocache}>
                                        <{/foreach}>
                                    <{/if}>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="column xxsmall-12 medium-9">
                    <div class="folder-content">
                        <ol class="file-list v1">
                            <{if $acmsContents->folders}>
                                <{foreach $acmsContents->folders as $folder}>
                                    <li class="folder">
                                        <input type="checkbox" name="fileselect" value="1" id="file-list-1">
                                        <label for="file-list-1">
                                            Bilder
                                        </label>
                                    </li>
                                <{/foreach}>
                            <{/if}>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="media-manager-footer">
            <p class="text-small">
                <strong>Dateien insgesamt:</strong> 3451 <span class="separator">|</span> 
                <strong>Speicherplatz insgesamt:</strong> 10gb <span class="separator">|</span> 
                <strong>Speicherplatz frei:</strong> 2,95gb 
            </p>
        </div>
    </div>
</div>
