<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" itemtype="http://schema.org/ProfilePage">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-delete"
         data-role="profile"
         data-profileview="delete" 
         data-owner="<{if $acmsContents->isOwner}>true<{else}>false<{/if}>">
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>
        <div class="push-10">
            <{if $acmsContents->get('form')}>
                <{include file=$acmsContents->form['template'] form=$acmsContents->get('form') nocache}>
            <{/if}>
        </div>
    </div>
</section>
