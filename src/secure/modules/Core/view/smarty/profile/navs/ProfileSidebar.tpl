<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div id="profile-sidebar" class="profile-sidebar-nav">
    <div class="profile-header-sidebar">
        <div class="center-block text-center push-30">
            <div class="profile-avatar" data-cssfile="media/css/profile/profile-avatar.min.css">
                <img class="responsive" itemprop="image" src="<{$acmsContents->profileuser['avatar']}>" title="<{$acmsContents->profileuser['username']}>" alt="" data-role="current-avatar" />
                <{if $acmsContents->isOwner}>
                    <{getProfileForm component="Avatar" assign="avatarForm"}>
                    <{if $avatarForm}>
                        <div class="caption" data-module="apps/ui/informations/modal" data-target="#profile-sidebar-add-avatar">
                            <div class="blur"></div>
                            <div class="caption-text">
                                <span class="h1"><i aria-hidden="true" class="acms-icon acms-icon-camera"></i></span>
                                <p>Neues Bild hochladen</p>
                            </div>
                        </div>
                        <div class="modal" id="profile-sidebar-add-avatar" data-modal-index="1" tabindex="-1" role="dialog" aria-labelledby="add-avatar-title" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="content">
                                    <form role="form" <{$avatarForm['attributes']}> data-module="apps/profile/avatar/avatar-form">
                                        <div class="header">
                                            <span role="button" class="close right" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="src-only">Schließen</span></span>
                                            <h4 class="title" id="add-avatar-title">
                                                <{translate id="Neuen Avatar hochladen" textdomain="Core"}>
                                            </h4>
                                        </div>
                                        <div class="main">
                                            <{foreach $avatarForm['fields'] as $field}>
                                                <{include file=$field['template'] field=$field}>
                                            <{/foreach}>
                                        </div>
                                        <div class="footer">
                                            <{include file=$avatarForm['buttons']['template'] field=$avatarForm['buttons']}>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <{/if}>
                <{/if}>
            </div>
        </div>
    </div>
    <nav class="profile-nav">

        <h3 class="text-center text-theme show-medium-only">Profil Menu</h3>
        <ul class="sidebar v1" itemscope="" itemtype="http://schema.org/WPSiteNavigationElement" role="tablist">
            <{foreach $acmsContents['profileUserMenu'] as $name => $item}>
                <li role="presentation" class="profile-menu-link <{$name}><{if $item['isCurrent']}> active<{/if}>">
                    <a class="display-block" href="<{$item.url}>" title="<{$item.title}>" itemprop="url" role="tab">
                        <i class="<{$item.icon}>"></i>
                        <span itemprop="name"><{$item.title}></span>
                    </a>
                </li>
            <{/foreach}>
            <li role="presentation" class="profile-menu-link info">
                <a class="display-block"
                   data-module="apps/ui/informations/modal"
                   data-target="#profile-info-modal"
                   href="#"
                   role="tab"
                   title="Profil-Informationen">
                    <i aria-hidden="true" class="acms-icon acms-icon-info-circle"></i>
                    <span>Profil-Informationen</span>
                </a>
            </li>
        </ul>
        <{if !$acmsContents->isOwner && $acmsContents->view != 'inbox'}>
            <{acceptsPrivateMessage assign="acceptPM" uid=$acmsContents->profileuser['id'] current=$acmsData->user['id']}>
            <{if $acceptPM}>
                <div class="text-center push-updown-15">
                    <button type="button" data-module="apps/ui/informations/modal" data-target="#compose-message" class="button small primary">
                        <i aria-hidden="true" class="acms-icon acms-icon-envelope"></i> 
                        <span><{translate id="Nachricht schreiben" textdomain="Core"}></span>
                    </button>
                    <{include file="acmsfile:Core|profile/form/PrivatemessageForm.tpl" toUser=$acmsContents['profileuser']  nocache}>
                </div>
            <{/if}>
        <{/if}>
        <{if !$acmsContents->isOwner}>
            <div class="text-center push-updown-15">
                <button type="button" 
                        class="default small danger-hover button" 
                        data-module="apps/profile/spamreport/report-profile" 
                        data-remote="<{generateUrl name="core_profile_spamreport_action" parameters=['username' => $acmsContents->lowerUsername, 'action' => 'action']}>">
                    <i aria-hidden="true" class="acms-icon acms-icon-user-shield"></i> 
                    <span class="content"><{translate id="Benutzer melden" textdomain="Core"}></span>
                </button>
            </div>
        <{/if}>
        <{if $acmsContents->profileOwnerMenu}>
            
            <{include file="acmsfile:Core|profile/navs/ProfileOwnerMenu.tpl" profileOwnerMenu=$acmsContents['profileOwnerMenu'] showTitle=true}>
        <{/if}>
        <{include file="acmsfile:Core|profile/profile/ProfileInfo.tpl" nocache}>
    </nav>
</div>
