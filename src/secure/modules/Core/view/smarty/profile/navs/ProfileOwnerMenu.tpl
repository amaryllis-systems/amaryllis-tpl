<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>
<{if !isset($showTitle) || $showTitle == true}>
    <div class="page-title v7">
        <i aria-hidden="true" class="acms-icon acms-icon-user-circle"></i>
        <div class="content">
            <div class="text-center">Mein Menu</div>
            <div class="sub-title text-center">Meine Profil-Bereiche</div>
        </div>
    </div>
<{/if}>
<ul class="sidebar v1" itemscope="" itemtype="http://schema.org/WPSiteNavigationElement" role="tablist">
    <{foreach $profileOwnerMenu as $name => $item}>
        <{if $item['url'] == '#'}>
            <li class="nav-separator"></li>
        <{else}>
            <li role="presentation" class="profile-menu-link <{$name}><{if $item['isCurrent']}> active<{/if}>">
                <a class="display-block" href="<{$item.url}>" title="<{$item.title}>" itemprop="url" role="tab">
                    <i class="<{$item.icon}>"></i>
                    <span itemprop="name"><{$item.title}></span>
                </a>
            </li>
        <{/if}>
    <{/foreach}>
</ul>
