<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section id="profile" itemscope="" itemtype="http://schema.org/ProfilePage">
    <aside role="complementary" class="sidebar-main grey lighten-3">
        <{include file="acmsfile:Core|profile/navs/ProfileSidebar.tpl"}>
    </aside>
    <div class="profile-content profile-change-password"
         data-role="profile"
         data-profileview="pictures" 
         data-owner="<{if $acmsContents->isOwner}>true<{else}>false<{/if}>"
         >
        <{include file="acmsfile:Core|profile/ProfileHeader.tpl"}>

        <section data-module="apps/profile/admin/spamreport/spamreport-dashboard" data-remote="<{$acmsContents->remote}>">
            <div class="bottom white arrow-box v1 pull-updown-35">
                <div class="large center aligned icon page-title v7 pull-updown-35">
                    <i aria-hidden="true" class="circular acms-icon acms-icon-user-shield"></i>
                    <h1 class="content" itemprop="name">
                        <span class="title"><{$acmsContents->get('acmsTitle')}></span>
                        <small class="sub-title">Als Spam markierte Benutzer</small>
                    </h1>
                </div>
            </div>
            <div class="default bottom arrow-box v1 pull-updown-35">
                <div class="arrow-box-heading pull-updown-35">
                    <h2 class="text-dark text-center">Abwartende Meldungen <span class="primary badge v1" data-role="total-reports"></span></h2>
                </div>
                <div class="arrow-box-content">
                    <div class="cards" data-section="reports">
                    </div>
                </div>
            </div>
            <div class="white bottom arrow-box v1 pull-updown-35">
                <div class="arrow-box-heading pull-updown-35">
                    <h2 class="text-dark text-center">Gebannte Benutzer <span class="primary badge v1" data-role="total-banned"></span></h2>
                </div>
                <div class="arrow-box-content">
                    <div class="cards" data-section="banned">
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
