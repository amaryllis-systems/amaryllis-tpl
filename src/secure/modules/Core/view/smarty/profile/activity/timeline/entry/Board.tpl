<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>
<{assign var="custom" value=$entry->getCustomData()}>
<div class="timeline-badge">
    <i class="acms-icon acms-icon-comment-alt regular"></i>
</div>
<div class="timeline-panel">
    <div class="timeline-heading">
        <p>
            <small class="text-muted">
                <i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$entry->getLogDate('pdate-relative')}>
            </small>
        </p>
    </div>
    <div class="timeline-body">
        <p><{$custom['entry']['body']}></p>
    </div>
</div>
