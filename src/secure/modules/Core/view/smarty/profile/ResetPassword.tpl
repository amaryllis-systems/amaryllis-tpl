<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $acmsContents['acmsBreadCrumb']}>
    <div class="container full">
        <div class="row xxsmall-push-updown-25">
            <div class="column xxsmall-12">
                <{include file=$acmsContents['acmsBreadCrumb']['template']}>
            </div>
        </div>
    </div>
<{/if}>
<div class="container fixed-1200" data-cssfile="media/css/components/cards.min.css">
    <div class="row">
        <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
            <div class="xxlarge page-title v7">
                <i aria-hidden="true" class="acms-icon acms-icon-key"></i>
                <div class="content">
                    <h1 class="title"><{translate id="Passwort zurücksetzen" textdomain="Core"}></h1>
                    <div class="sub-title"><{translate id="Passwort vergessen? Kein Problem! Hier kannst du dein Passwort einfach zurücksetzen" textdomain="Core"}></div>
                </div>
            </div>
        </div>
    </div>
                    
    <div class="row xxsmall-push-updown-25">
        <div class="column xxsmall-12 medium-8 medium-offset-2 large-6 large-offset-3 xlarge-6 xlarge-offset-3 xxlarge-6 xxlarge-offset-3">
            <div class="card v1card-container center-block<{if $acmsContents->has('uuid')}> green lighten-2<{/if}>">
                <div class="text-center profile-img-card v1blue-grey darken-3 white-text">
                    <i class="image circle acms-icon acms-icon-user acms-icon-5x"></i>
                </div>
                <div class="card-content">
                    <{if $acmsContents->has('uuid')}>
                        <p><{$acmsContents->successMessage}></p>
                    <{else}>
                    <form role="form" action="<{$acmsContents->actionUrl}>" method="post" data-module="view/profile/forgot-password">
                        <div class="form-group">
                            <label for="email" class="field-label">Gib bitte die E-Mail Adresse an, die du bei der Registrierung verwendet hast.</label>
                            <input type="email" id="email" class="field" name="email" placeholder="Ihre E-Mail Adresse" />                            
                        </div>
                        <div class="button-toolbar">
                            <div class="button-group">
                                <button class="button primary primary-important">
                                    <span>Senden</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <{/if}>
                </div>
            </div>
        </div>
    </div>
</div>
