<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if isset($dismissable) && $dismissable || $message['dismissable']}>
    <div class="alert v1 warning"
         data-cssfile="media/css/components/alert.min.css"
         >
        <{if $message.title}>
            <div class="alert-title">
                <{$message.title}>
            </div>
        <{/if}>
        <div class="alert-content">
            <{if is_array($message.message)}>
                <ul>
                    <{foreach $message.message as $msg}>
                        <li>$msg</li>
                        <{/foreach}>
                </ul>
            <{else}>
                <p><{$message.message}></p>
            <{/if}>
        </div>
    </div>

<{else}>

    <div class="alert v1 warning dismissable"
         data-module="ui/components/alert">
        <{if $message.title}>
            <div class="alert-title">
                <h3><{$message.title}></h3>
                <i aria-hidden="true" class="acms-icon acms-icon-times-circle regular right" aria-label="<{translate id="Schließen" textdomain="Core"}>"></i>
            </div>
        <{/if}>
        <div class="alert-content">
            <{if is_array($message.message)}>
                <ul class="list list-unstyled">
                    <{foreach $message.message as $msg}>
                        <li>
                            <i aria-hidden="true" class="acms-icon acms-icon-exclamation-triangle red-text"></i>
                            <span>$msg</span>
                        </li>
                    <{/foreach}>
                </ul>
            <{else}>
                <p><{$message.message}></p>
            <{/if}>
        </div>
        <div class="alert-footer">
            <button type="button" data-dismiss="alert"
                    class="button success">
                <i aria-hidden="true" class="acms-icon acms-icon-check"></i>
                <{translate id="Ok" textdomain="Core"}>
            </button>
        </div>
    </div>

<{/if}>
