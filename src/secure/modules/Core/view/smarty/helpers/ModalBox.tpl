<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="modal<{if $message.size == 'lg'}> large<{elseif $message.size == 'sm'}> small<{/if}>" id="<{$message.id}>" tabindex="-1" role="dialog" aria-labelledby="<{$message.id}>-title" aria-hidden="true">
    <div class="modal-dialog<{if $acmsData->deviceIsMobile || $message.size == 'full'}> full-screen<{/if}>">
        <div class="content">
            <div class="header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="src-only"><{translate id="Schließen" textdomain="Core"}></span></button>
                <h4 class="title" id="<{$message.id}>-title"><{$message.title}></h4>
            </div>
            <div class="main text-left">
                <{$message.message}>
            </div>
            <div class="footer">
                <{if $message.customButton}>
                    <{$message.customButton}>
                <{/if}>
                <button type="button" class="button primary" data-dismiss="modal"><{translate id="Schließen" textdomain="Core"}></button>
            </div>
        </div>
    </div>
</div>
