<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="content-block v6 push-30">
    <h2>
        <i aria-hidden="true" class="acms-icon acms-icon-lightbulb regular text-muted"></i> Nicht das richtige gefunden?
    </h2>
    <p>
        Sie haben nicht das richtige gefunden? Benutzen Sie 
        unsere <a href="<{generateUrl name="core_search"}>" title="Suchen Sie auf unserer Webseite">Website-Suche</a> 
        um passende Ergebnisse zu finden.
    </p>
</div>
