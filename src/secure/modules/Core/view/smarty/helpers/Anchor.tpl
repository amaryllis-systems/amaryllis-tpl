<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<a
    href="<{$anchor.href}>"
    class="<{if $anchor.class}><{$anchor.class}><{elseif $anchor.target && $anchor.target == '__blank'}>external<{else}>link<{/if}>"
    <{if $anchor.id}>id="<{$anchor.id}>"<{/if}>
    <{if $anchor.title}>title="<{$anchor.title}>"<{/if}>
    <{if $anchor.target}>target="<{$anchor.target}>"<{/if}>
    <{foreach $anchor.attributes as $attribute => $value}>
        <{$attribute}>="<{$value}>"
    <{/foreach}>
 >
    <{if $anchor.fonticon && $anchor.fonticon.position == 1}>
        <i class="<{$anchor.fonticon.fonticon}>"></i>
    <{/if}>
    <{$anchor.anchorText}>
    <{if $anchor.fonticon && $anchor.fonticon.position == 2}>
        <i class="<{$anchor.fonticon.fonticon}>"></i>
    <{/if}>
 </a>
