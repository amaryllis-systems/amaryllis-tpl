<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="category_menu"}>
<div class="content-block v1 push-0">
    <{if $block.title}>
    <div class="block-title">
        <h4 class="text-theme"><{$block.title}></h4>
    </div>
    <{/if}>
    <div class="block-content">
        <nav class="white" role="navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
            <ul class="sidebar v2">
                <{foreach $block.content as $category}>
                    <li class="category-item<{if $block.current && $category.alias == $block.current}> active<{/if}>">
                        <a href="<{$category.itemUrl}>" class="category-link" title="<{$category.title}>">
                            <i aria-hidden="true" class="acms-icon acms-icon-folder text-muted"></i>
                            <span><{$category.title}></span>
                        </a>
                    </li>
                <{/foreach}>
                <{if $acmsData->acmsIsAdmin}>
                    <li class="nav-separator"></li>
                    <li>
                        <a class="category-permissions" href="<{generateUrl name="core_category_permissions"}>" title="Kategorie-Berechtigungen angleichen">
                            <i aria-hidden="true" class="acms-icon acms-icon-lock text-muted"></i>
                            <span><{translate id="Kategorie-Berechtigungen"}></span>
                        </a>
                    </li>
                <{/if}>
            </ul>
        </nav>
    </div>
</div>
<{/block}>
