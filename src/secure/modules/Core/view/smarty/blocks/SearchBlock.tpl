<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="core_search"}>
    <div class="content-block v5">
        <{if $block.title}>
            <div class="page-title v4">
                <h4 class="title"><{$block.title}></h4>
            </div>
        <{/if}>
        <div class="block-content">
            <div class="form-wrapper">
                <form class="form" role="search" action="<{$block['content']['url']}>">
                    <div class="form-group">
                        <div class="field-group">
                            <input name="search" type="search" placeholder="<{translate id="Suchen"}>..." class="field">
                            <button class="field-icon" data-trigger="search" type="submit" aria-label="<{translate id="Suchen"}>...">
                                <i aria-hidden="true" class="acms-icon acms-icon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<{/block}>
