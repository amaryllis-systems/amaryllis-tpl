<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="user_menu"}>
    <div class="widget v1">
        <div class="widget-heading">
            <div class="widget-title">
                <h3><i class="acms-icon acms-icon-newspaper"></i> <{$block['title']}></h3>
            </div>
            <div class="widget-icons">
                <a href=""><i class="acms-icon acms-icon-plus-circle text-primary"></i> </a>
            </div>
        </div>
        <div class="full widget-content">
            <div class="sidebar v1">
                <li>
                    <a class="item" href="" title="">
                        <span class="text"></span>
                    </a>
                </li>
            </div>
        </div>
        <div class="widget-footer">
            <button class="button small primary">
                <i class="acms-icon acms-icon-tachometer-alt"></i> 
                <span>Dashboard</span>
            </button>
        </div>
    </div>
    
<div class="profile-sidebar">
    <!-- SIDEBAR USERPIC -->
    <div class="avatar text-center">
        <img src="<{$user.avatar}>"
             alt="<{if $user.first_name && $user.last_name}><{$user.first_name}> <{$user.last_name}><{else}><{$user.username}><{/if}>">
    </div>
    <div class="profile-usertitle">
        <div class="profile-usertitle-name">
            <{if $user.first_name && $user.last_name}>
                <{$user.first_name}> <{$user.last_name}>
            <{else}>
                <{$user.username}>
            <{/if}>
        </div>
        <div class="profile-usertitle-job">
            <{$user.email}>
        </div>
    </div>
    <div class="profile-usermenu">
        <ul class="vertical nav v1">
            
            <li>
                <a href="#">
                    <i class="<{}>"></i>
                    Overview </a>
            </li>
            <li>
                <a href="#">
                    <i aria-hidden="true" class="acms-icon acms-icon-user"></i>
                    Account Settings 
                </a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <i aria-hidden="true" class="acms-icon acms-icon-check-square"></i>
                    Tasks </a>
            </li>
            <li>
                <a href="#">
                    <i aria-hidden="true" class="acms-icon acms-icon-life-ring"></i>
                    Help </a>
            </li>
        </ul>
    </div>
<{/block}>
