<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="core_theme"}>
    <div class="content-block v5">
        <div class="form-wrapper">
            <form method="POST" class="form" name="theme_force" action="<{generateUrl name='core_theme_action' params=['action' => 'enforce']}>">
                <{if !$block['content']['theme']['isApp']}>
                    <div class="form-group">
                        <div class="buttons">
                            <{if !$block['content']['theme']['isMobile'] && $block['content']['theme']['config']['settings']['has_mobile']}>
                                <label class="tiny primary border-button button">
                                    <span class="text-xsmall">Mobiles Layout erzwingen</span>
                                    <input name="op" type="radio" value="enforceMobile" onchange="this.form.submit();">
                                </label>
                            <{/if}>
                            <{if !$block['content']['theme']['isTablet'] && $block['content']['theme']['config']['settings']['has_tablet']}>
                                <label class="tiny primary border-button button">
                                    <span class="text-xsmall">Tablet Layout erzwingen</span>
                                    <input name="op" type="radio" value="enforceTablet" onchange="this.form.submit();">
                                </label>
                            <{/if}>
                            <{if !$block['content']['theme']['isDesktop'] && $block['content']['theme']['config']['settings']['has_main']}>
                                <label class="tiny primary border-button button">
                                    <span class="text-xsmall">Desktop Layout erzwingen</span>
                                    <input name="op" type="radio" value="enforceDesktop" onchange="this.form.submit();">
                                </label>
                            <{/if}>
                        </div>
                    </div>
                <{/if}>
            </form>
        </div>
    </div>
<{/block}>
