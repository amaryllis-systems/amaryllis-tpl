<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="core_multilanguage"}>
    <{if $block['content']['isML']}>
    <div class="small labels">
        <{foreach $block['content']['urls'] as $url}>
            <a class="small middle aligned image-label label v2" href="<{$url['url']}>" title="<{$url['title']}>">
                <img class="" src="<{$url['icon']}>" title="<{$url['title']}>" alt="<{$url['label']}>" >
                <{$url['label']}>
            </a>
        <{/foreach}>
    </div>
    <{/if}>
<{/block}>
