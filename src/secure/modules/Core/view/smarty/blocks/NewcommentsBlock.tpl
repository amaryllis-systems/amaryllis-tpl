<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{if $block['content']['comments']->count()}>
    <div class="comments v2">
        <{foreach $block['content']['comments']->all() as $comment}>
            <div class="comment">
                <a href="<{$comment['commentSubmitter']['itemUrl']}>" class="avatar medium">
                    <{$comment['commentSubmitter']['username']}>
                </a>
                <div class="content">
                    <a href="<{$comment['commentSubmitter']['itemUrl']}>" title="<{$comment['commentSubmitter']['username']}>" class="author">
                        <{$comment['commentSubmitter']['username']}>
                    </a>
                    <div class="info">
                        <time class="date" datetime="<{$comment['pdate-webdate']}>"><i aria-hidden="true" class="acms-icon acms-icon-clock regular"></i> <{$comment['pdate-relative']}></time>
                    </div>
                    <div class="text">
                        <{$comment['comment']}>
                    </div>
                </div>
            </div>
        <{/foreach}>
    </div>
<{/if}>
