<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="block">
    <{if $block.title}>
        <div class="block-title">
            <h4><{$block.title}></h4>
        </div>
    <{/if}>
    <div class="block-content">
        <div class="collection v1 v1">
            <div class="card" data-module="ui/components/cards">
                <div class="card-image">
                    <img class="image" src="<{$acmsData->acmsUrl}><{$block['options']['image']}>" />
                </div>
                <div class="card-content text-theme">
                    <div class="card-title">
                        <h3 class="text-theme"><small class="text-theme">Powered By</small> Amaryllis-CMS</h3>
                    </div>
                    <div class="subtitle text-theme">
                        <{$block['content']['version']}> <{$block['content']['type']}>
                    </div>
                    <div>
                        <div class="display-inline" itemprop="generator" itemscope="" itemtype="http://schema.org/WebApplication">
                            Dieses Portal wird mit <a itemprop="url" title="Erfahren Sie mehr über Amaryllis-CMS" href="https://www.amaryllis-cms.de/" target="_blank">
                                <span itemprop="name">Amaryllis-CMS</span>
                            </a> betrieben.<br>
                        </div> &copy; <{$acmsData->acmsYear}> Entwickelt und bereitgestellt von
                        <div class="display-inline" itemprop="creator" itemscope="" itemtype="http://schema.org/Organization">
                            <a itemprop="url" href="https://www.amaryllis-systems.de" title="Entwicklung: Amaryllis Systems GmbH" target="_blank" class="credits">
                                <img src="<{$acmsData->acmsUrl}>/media/icons/icon.png" alt="" title="Entwickelt von: Amaryllis Systems GmbH und Amaryllis Systems GmbH" itemprop="logo" />
                                <span itemprop="name">Amaryllis Systems GmbH</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
