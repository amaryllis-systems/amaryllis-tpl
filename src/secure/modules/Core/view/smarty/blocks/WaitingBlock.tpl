<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="waiting"}>
    <{if $block['content']}>
        <div class="content-block v1">
            <{if $block['title']}>
                <div class="block-title">
                    <h3><{$block['title']}></h3>
                </div>
            <{/if}>
            <div class="block-content">
                <{foreach $block['content'] as $dirname => $waitings}>
                    <{foreach $waitings => $waiting}>
                        <div class="collection v1 push-10 with-header">
                            <div class="collection-header">
                                <a href="<{$waiting['url']}>" title="<{$waiting['title']}>">
                                    <i aria-hidden="true" class="<{$waiting['fonticon']}>"></i> 
                                    <span><{$waiting['title']}></span>
                                    <span class="right floated primary badge v1"><{$waiting['waiting']}></span>
                                </a>
                                <p class="text-small">
                                    <small><{$waiting['description']}></small>
                                </p>
                            </div>
                            <{foreach $waiting['items'] as $item}>
                                <div class="collection-item">
                                    <a href="<{$item['url']}>" title="<{$item['title']}>" class="title">
                                        <{$item['title']}>
                                    </a>
                                </div>
                            <{/foreach}>
                        </div>
                    <{/foreach}>
                <{/foreach}>
            </div>
        </div>
    <{/if}>
<{/block}>
