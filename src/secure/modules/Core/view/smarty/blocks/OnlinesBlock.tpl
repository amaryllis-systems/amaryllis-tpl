<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="core_onlines"}>
<div class="content-teaser v1 horizontal-teaser text-center">
    <div class="teaser-item">
        <div class="inner">
            <h2 class="heading"><{$block['title']}></h2>
            <div class="deco-line"></div>
            <ul class="list-unstyled list-onliners text-left">
                <li>
                    <strong>Insgesamt</strong> 
                    <span>
                        <span class="badge v1"><{$block['content']['stats']['total']}></span>
                    </span>
                </li>
                <li>
                    <strong>Benutzer</strong> 
                    <p>Mitglieder Online:</p>
                    <ul class="list-inline">
                        <{foreach $block['content']['users'] as $data}>
                        <li>
                            <a class="tag v1 round default" href="<{$data['user']['itemUrl']}>" title="<{$data['user']['username']}>">
                                <img class="tag-image" src="<{$data['user']['avatar']}>">
                                <span><{$data['user']['displayname']}></span>
                            </a>
                        </li>
                        <{/foreach}>
                    </ul>
                </li>
                <li>
                    <strong>Benutzer</strong> 
                    <span>
                        <span class="badge v1"><{$block['content']['stats']['members']}></span>
                    </span>
                </li>
                <li>
                    <strong>Gäste</strong> 
                    <span>
                        <span class="badge v1"><{$block['content']['stats']['guests']}></span>
                    </span>
                </li>
                <li>
                    <strong>Bots</strong> 
                    <span>
                        <span class="badge v1"><{$block['content']['stats']['bots']}></span>
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>
<{/block}>
