<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>
<{nocache}>
<{if $block.content}>
    <div class="block">
        <{if $block.title}>
            <div class="block-title pull-updown-10 bg-primary">
                <h4 class="white-text text-center push-updown-10"><{$block.title}></h4>
            </div>
        <{/if}>
        <div class="block-content">
            <div class="bg-grey lighten-3 center-block pull-updown-30">
                <div class="text-center center-block xxlarge avatar bg-grey darken-2">
                    <i aria-hidden="true" class="acms-icon acms-icon-user white-text"></i>
                </div>
                <div class="form-wrapper text-dark">
                    <form role="form" <{$block['content']['attributes']}> data-module="apps/profile/login/login">
                        <{if $block['content']['title']}>
                            <legend><{$block['content']['title']}></legend>
                        <{/if}>
                        <{foreach $block['content']['fields'] as $field}>
                            <{include file=$field['template'] field=$field nocache}>
                        <{/foreach}>
                        <{include file=$block['content']['buttons']['template'] field=$block['content']['buttons'] nocache}>
                    </form>
                </div>
            </div>
        </div>
    </div>
<{/if}>
<{/nocache}>
