<{* **************************************************
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<{block name="core_recent_members"}>
    <{if $block['content']['users']->count()}>
        <div class="boxed has-image collection v1">
            <{if $block['title']}>
                <div class="collection-header">
                    <{$block['title']}>
                </div>
            <{/if}>
            <{foreach $block['content']['users'] as $user}>
                <div class="collection-item">
                    <div class="avatar item-image">
                        <img src="<{$user['user']['avatar']}>" alt="avatar" title="<{$user['user']['username']}>" />
                    </div>
                    <a class="title" href="<{$user['user']['itemUrl']}>">
                        <{$user['user']['username']}>
                    </a>
                    <div class="secondary-content">
                        <span class="online-status <{if isset($user['onlineData'])}><{$user['onlineData']->getOnline()->getName()}><{elseif isset($user['isOnline']) && $user['isOnline']}>online<{else}>offline<{/if}>" 
                                data-module="apps/ui/informations/tooltip" 
                                data-trigger="hover" 
                                data-toggle="tooltip" 
                                data-title="<{if isset($user['onlineData'])}><{$user['onlineData']->getOnline()->getLabel()}><{elseif isset($user['isOnline']) && $user['isOnline']}><{translate id="%s ist gerade online" args=[$user['username']] textdomain="Bulletboard"}><{else}><{translate id="%s ist gerade offline" args=[$user['username']] textdomain="Bulletboard"}><{/if}>" 
                                data-position="top" dir="ltr">
                        </span>
                    </div>
                </div>
            <{/foreach}>
        </div>
    <{/if}>
<{/block}>
