<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="content-block v5 bg-white no-border no-shadow">
    <{if $block['title']}>
    <div class="block-title">
        <h3><{$block['title']}></h3>
    </div>
    <{/if}>
    <div class="block-content white">
        <div class="social-icons color-icons">
            <ul>
                <{foreach $block['content'] as $name => $socials}>
                    <li class="<{$name}>">
                        <a href="<{$socials['uri']}>" 
                           title="<{$socials['title']}>" 
                           rel="nofollow" 
                           role="button"
                           target="_blank">
                            <i aria-hidden="true" class="acms-icon acms-icon-<{$socials['icon']}><{if $name != mail}> brand<{/if}>"></i>
                            <span class="src-only"><{$socials['title']}></span>
                        </a>
                    </li>
                <{/foreach}>
            </ul>
        </div>
    </div>
</div>
