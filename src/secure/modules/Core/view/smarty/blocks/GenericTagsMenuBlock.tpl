<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<div class="content-block v1">
    <{if $block.title}>
    <div class="block-title">
        <h4><{$block.title}></h4>
    </div>
    <{/if}>
    <div class="block-content">
        <div class="collection v1 v1">
        <{foreach $block.content as $link}>
            <a href="<{$link.itemUrl}>" class="collection-item" title="<{$link.title}>">
                <span><{$link.title}></span>
                <{if $link['fonticon']}>
                    <i class="<{$link['fonticon']}>"></i>
                <{/if}>
            </a>
        <{/foreach}>
        </div>
    </div>
</div>
