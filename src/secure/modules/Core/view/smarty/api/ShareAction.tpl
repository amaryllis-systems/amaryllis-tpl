<{* **************************************************
 * Copyright 2019 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************** *}>

<section itemscope="" itemtype="http://shema.org/WebPage">
    <section class="page-section v1 primary">
        <div class="container fixed-1200">
            <div class="row full xxsmall-push-updown-30">
                <div class="column xxsmall-12">
                    <div class="page-title">
                        <h1 itemprop="name">Teilen Sie einen Link</h1>
                        <p>Teilen Sie einen Link mit Ihren Profil-Kontakten</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="white">
        <{if $acmsContents->form}>
            <{include file=$acmsContents->form['template'] form=$acmsContents->form}>
        <{/if}>
    </section>
</section>
